#!/bin/bash

# include files
source bashscript/array.sh

# Format
L="------------------------------------------------------"
Log=$(git log -n 1 --pretty=format:"<b>COMMITER</b>: %cN %n<b>DATE</b>: %ci %n<b>MESSAGE</b>: %s")
Server="<b>Server</b>: ProdAP-Roadcare%0A<b>Local IP</b>: 172.19.25.14%0A<b>Sub Domain</b>: pilot-admin-roadcare.mpwt.gov.kh"

emailCommitter=$(git log -n 1 --pretty=format:%cE)

CLICKLINK=$(git log -n 1 --pretty=format:"<i>LINK TO SEE CODE HERE</i>: http://gitlab.mpwt.gov.kh/road-cares/web-admin-v3/-/commit/%h")

if [ "$emailCommitter" = "${email[0]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[0]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[1]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[1]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[2]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[2]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[3]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[3]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[4]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[4]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[5]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[5]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[6]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[6]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[7]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[7]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[8]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[8]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[9]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[9]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[10]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[10]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[11]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[11]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[12]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[12]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[13]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[13]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[14]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[14]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[15]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[15]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[16]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[16]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[17]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[17]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[18]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[18]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[19]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[19]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
elif [ "$emailCommitter" = "${email[20]}" ]; then
    MSG="${L}%0A<b>PROJECT</b>: ROADCARE%0A<b>APPLICATION</b>: WEB ADMIN%0A<b>Environment</b>: PILOT%0A<b>STATUS</b>:  Success%0A<b>VERSION</b>: ${BUILD_NUMBER}%0A${L}%0A${Log}%0A<b>TELEGRAM</b>: ${username[20]}%0A${L}%0A${Server}%0A%0A${CLICKLINK}%0A${L}"
    curl -s -X POST https://api.telegram.org/bot${BOT_TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d text="${MSG}" -d parse_mode="HTML"
else
    echo "This username does not exist"
fi


