#!/bin/bash

BUILD_NUMBER="1"

if [ -z "$BUILD_NUMBER" ]
then 
    echo "\$BUILD_NUMBER is null"
else 
    echo "BUILD_NUMBER=$BUILD_NUMBER" >> .env

    if grep -Fxq "BUILD_NUMBER=${BUILD_NUMBER}" .env

    then

    echo "\$BUILD_NUMBER is found"
    else
    echo "\$BUILD_NUMBER not found"
    fi
fi



