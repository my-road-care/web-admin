#!/bin/bash

line="---------------------------------------------------------------"

# bold text
bold=$(tput bold)

# normal text
normal=$(tput sgr0)

# Git-Log
log=$(git log -n 1 --pretty=format:"<b>COMMITER</b>: %cn %n<b>DATE</b>: %ci %n<b>MESSAGE</b>: %s")

email=(
    [0]="vicheasokan6@gmail.com"
    [1]="vituyoeun1603@gmail.com"
    [2]="kaybriyel@gmail.com"
    [3]="kim.sonen.2022@gmail.com"
    [4]="yimklok.kh@gmail.com"
    [5]="vathanakchhoeun125@gmail.com"
    [6]="kimhong@gmail.com"
    [7]="yanphearak.program@gmail.com"
    [8]="private.ngoun@gmail.com"
    [9]="theanyso007@gmail.com"
    [10]="layy.isme@gmail.com"
    [11]="kungchantha49@gmail.com"
    [12]="soksamnangvong@gmail.com"
    [13]="sophalchen82@gmail.com"
    [14]="engkhuntaing@gmail.com"
    [15]="bopisey1@gmail.com"
    [16]="chhanvirakbuth1999@gmail.com"
    [17]="khouch.koeun@gmail.com"
    [18]="yoeunsathya4@gmail.com"
    [19]="sotearithkhan013@gmail.com"
    [20]="chhrathana@gmail.com"
)

username=(
    [0]="@sovichea10"
    [1]="@yoeunvitu"
    [2]="@kaybriyel"
    [3]="@kim_sonen"
    [4]="@yim_klok"
    [5]="@Chh_Vathanak"
    [6]="@kimhongvuthy"
    [7]="@yanphearak"
    [8]="@touch_heangngoun"
    [9]="@theany_so"
    [10]="@lay_ly"
    [11]="@Kung_Chantha"
    [12]="@Soksamnang_Vong"
    [13]="@sophalchen"
    [14]="@Engkhun_Taing"
    [15]="@bopisey"
    [16]="@virakbuth_chhan"
    [17]="@KOEUNKhouch"
    [18]="@SathyaYoeun"
    [19]="@KhanNarith"
    [20]="@chhan_rathana"
)

outputEmail=$(printf "\n%s" "${email[@]}")
outputEmail=${outputEmail:1} # remove the leading comma

outputUsername=$(printf "\n%s" "${username[@]}")
outputUsername=${outputUsername:1} # remove the leading comma