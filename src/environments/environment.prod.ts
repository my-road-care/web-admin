export const environment = {
    production: true,

    //   ================================================ DEV
    apiUrl: 'https://dev-api-roadcare.mpwt.gov.kh/api',
    fileUrl: 'https://dev-file.mpwt.gov.kh/',

    //   ================================================ UAT
    // apiUrl: 'https://uat-api-roadcare.mpwt.gov.kh/api',
    // fileUrl: 'https://dev-file.mpwt.gov.kh/',

    //   ================================================ Pilot
    // apiUrl: 'https://pilot-api-roadcare.mpwt.gov.kh/api',
    // fileUrl: 'https://file.mpwt.gov.kh/',

};
