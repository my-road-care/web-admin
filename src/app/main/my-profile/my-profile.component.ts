import { ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ProfileService } from './profile.service';
import { environment as env } from 'environments/environment';
import { SnackbarService } from 'app/shared/services/snackbar.service';
// import { env } from 'process';
@Component({
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyProfileComponent implements OnInit {
  @ViewChild('profileform') profileform: NgForm;
  public form: UntypedFormGroup;
  public data:any;
  public saving:Boolean= false;
  public src:any="";
  public olephone:string;
  public passwordCurrct:boolean=true;
  public message:string="";
  public phonenumber:string;
  user: any = {
    id: null,
    name: null,
    email: null,
    avatar: null,
    phone: null,
    type_id: null
};
  constructor(
    private _formBuilder:UntypedFormBuilder,
    private _service:ProfileService,
    private _snackBar:SnackbarService,
    ) {
    }

  ngOnInit(): void {

    this._service.getprofile().subscribe((res:any)=>{
            this.user.id=res.id;
            this.user.name=res.name;
            this.user.phone =res.phone;
            this.user.email=res.email;
            this.user.avatar= env.fileUrl+res.avatar;
            this.user.type_id = res?.type_id
            this._service.userProfile=this.user;
            localStorage.setItem('user', JSON.stringify(this.user));
    });
    this.data=JSON.parse(localStorage.getItem('user'));
    this.olephone=this.data.phone;
    if(this.data){
      this.src=this.data?.avatar === null ? 'https://www.citypng.com/public/uploads/small/11639594360nclmllzpmer2dvmrgsojcin90qmnuloytwrcohikyurvuyfzvhxeeaveigoiajks5w2nytyfpix678beyh4ykhgvmhkv3r3yj5hi.png':this.data?.avatar ;
    }
    this.formBuilder();
  }
  formBuilder():void{
    // // console.log(this.data);
    this.form = this._formBuilder.group({
      name: [ this.data ? this.data?.name : 'sonen',Validators.required],
      email:[this.data ? this.data?.email===null ? 'abcd1234@gmail.com':this.data?.email : 'abcd4321@gmail.com' ,Validators.required],
      phone: [this.data ? this.data?.phone :'012345678',[Validators.required,Validators.minLength(8)]],
      avatar:['']
    })
  }

submit(){
    if(this.form.valid){
        this.saving=true;
        this.form.disable();
        this._service.updateProfile(this.form.value).subscribe((res:any)=>{
            this.saving=false;
            let update=res.user;
            this.user.id=update.id;
            this.user.name=update.name;
            this.user.phone =update.phone;
            this.user.email=update.email;
            this.user.avatar= env.fileUrl+update.avatar;
            this.form.enable();
            this._service.userProfile=this.user;
            // console.log('res',res);
            // // console.log('main',this.user);
            localStorage.setItem('user', JSON.stringify(this.user));
            this._snackBar.openSnackBar(res.message, '');
        },err=>{
          this.saving=false;
          this.form.enable();
          this.passwordCurrct=false;

          if(typeof(err.error.errors.email)!=='undefined'){
            this._snackBar.openSnackBar(err.error.errors.email[0], 'error');
          }else if(typeof(err.error.errors.phone)!=='undefined'){
            this.message=err.error.errors.phone[0];
            this.form.get('phone').setValue('');
            this._snackBar.openSnackBar(err.error.errors.phone[0], 'error');
          }else{
            this.message='';
          }
        }
        )
    }else{
      this._snackBar.openSnackBar('Please check your input.', 'error');
    }
    // // console.log(this.saving);
  }

  inputNumber($event){
      // // console.log($event.keyCode)
  this.passwordCurrct=true;
   if($event.keyCode !== 32 ){
    if( $event.keyCode !== 18){
      if($event.keyCode !=37){
        if($event.keyCode !=39){
            if(($event.key/1).toString() !== 'NaN' || $event.keyCode === 8 || $event.keyCode === 13){
              if($event.target.value[0] === '0'){
                this.olephone=$event.target.value;
              }
              if(this.olephone===''){
                  this.olephone='0';
              }
              if(this.olephone == '00'){
                this._snackBar.openSnackBar('បញ្ចូលលេខ0តែមួយគត់', 'error');
                this.olephone='0';

              }
            }

        }
      }
    }

   }
    this.form.get('phone').setValue(this.olephone);

  }
srcChange($event){
      this.form.get('avatar').setValue($event);
  }
}
