import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-security',
  templateUrl: './acc-security.component.html',
  styleUrls: ['./acc-security.component.scss']
})
export class AccSecurityComponent implements OnInit {
  public color:string="";
  public checked:boolean=true;
  public disabled:boolean=false;
  public saving:boolean=false;
  constructor() { }

  ngOnInit(): void {
  }

}
