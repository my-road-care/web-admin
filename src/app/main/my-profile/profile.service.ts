import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    private _getuser:ReplaySubject<any>=new ReplaySubject<any>(1);
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'}),

    };
    constructor(

      private http: HttpClient

      ) { }
    getprofile():Observable<any>{
        return this.http.get(this.url+'/auth/profile',this.httpOptions);
    }
    updateProfile(param:{})
    {
       return this.http.post(this.url+'/auth/profile/update',param,this.httpOptions);
    }
    changepass(param:any):Observable<any>{
      return this.http.post(this.url+'/auth/profile/change-password',param,this.httpOptions);
    }
    set userProfile(value:any){
      this._getuser.next(value);
    }
    get userProfile$():Observable<any>{
      return this._getuser.asObservable();
    }
}
