import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyProfileComponent } from './my-profile.component';
import { ChangePasswordComponent } from './change-password/change-pass.component';
import { MaterialModule } from 'app/shared/material-module';
import { ImageCropperModule } from 'ngx-image-cropper';
// import { ImageCropperComponent } from 'ngx-image-cropper';
import {PorfileDialogComponent} from './profile/profile.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { AccSecurityComponent } from './acc-security/acc-security.component';

const myProfileRoutes: Routes = [
    {
        path: '',
        component : MyProfileComponent
    },
    {
      path: 'change-pass',
      component : ChangePasswordComponent
  },
  {
    path: 'security',
    component :AccSecurityComponent
},
];
@NgModule({
    imports: [
        RouterModule.forChild(myProfileRoutes),

        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        ImageCropperModule,
        SharedModule,
    ],
    declarations: [
        MyProfileComponent,
        ProfileComponent,
        PorfileDialogComponent,
        ChangePasswordComponent,
        AccSecurityComponent,

      ],
})
export class MyProfileModule {}
