import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ProfileService } from '../profile.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Router } from '@angular/router';
@Component({
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChangePasswordComponent implements OnInit {
  @ViewChild('passform') passform: NgForm;
  public form: UntypedFormGroup;
  public data:any[]=[];
  public src:any;
  public confirm:boolean=false;
  public validateComfirm:boolean  = false;
  public saving:boolean =false;
  public oldpass:string='';
  public oldpassCorrect:boolean=false;
  public newpass:string='';
  public comfirmpass:string='';
  public validatePass:boolean=false;
  constructor(
    private _formBuilder:UntypedFormBuilder,
    private _service:ProfileService,
    private _snackBar:SnackbarService,
    private _route:Router) {}

  ngOnInit(): void {
    this.formBuilder();
    this.checkform();
  }
  checkform(){
    if(this.form.valid){
      this.confirm=true;
    }
  }
  formBuilder():void{
    this.form = this._formBuilder.group({
      old_password:['',[Validators.required,Validators.minLength(6)]],
      password:['',[Validators.required,Validators.minLength(6)]],
      password_confirmation:['',[Validators.required,Validators.minLength(6)]]
    })
  }
  submit(){


    if(this.form.value){
      this.saving=true;
      this.form.disable();
        this._service.changepass(this.form.value).subscribe((res:any)=>{
          this.saving=false;
          this.confirm=true;
          this.validateComfirm=false;
          this.validatePass=false;
          this.form.enable();
          this._snackBar.openSnackBar(res.message, '');
          setTimeout(()=>{
            this._route.navigateByUrl('/my-profile');
          },900);
        },err=>{
          this.confirm=true;
          this.validateComfirm=false;
          this.validatePass=false;
          this.saving=false;
          this.form.enable();
          if(err.error.status_code === 403 ){
            this.oldpassCorrect=true;
            // this.form.controls['old_password'].setErrors({'required': true});
          }
          this._snackBar.openSnackBar(err.error.errors.message, 'error');

          // this._snackBar.openSnackBar('Something went wrong!', 'error');
        });
    }else{
      this._snackBar.openSnackBar('Please check your input.', 'error');
    }
    // // console.log(this.form.enable())
  }
  checkoldpass(val){
    this.confirm=false;
    this.validatePass=false;
    this.oldpassCorrect=false;
    if(val.target.value.length >= 6){

      this.oldpass='';
      this.oldpass=val.target.value;
      if( this.newpass.length >=6 && this.comfirmpass.length>=6){
        if(this.oldpass !== this.newpass){
          this.confirm=true;
        }else{
          if(this.oldpass === this.newpass){
            this.validatePass=true;
          }
        }
      }else{
        if(this.oldpass === this.newpass){
          this.validatePass=true;
        }
      }
    }
    // // console.log(this.confirm)

  }
  checknewpass(val){
    this.confirm=false;
    this.validateComfirm=false;
    this.validatePass=false;
    if(val.target.value.length >= 6){
      this.newpass='';
      this.newpass=val.target.value;
      if(this.comfirmpass != ''){
          if(this.newpass === this.comfirmpass && this.oldpass !== this.newpass){
            this.confirm=true;

          }else{
            this.validateComfirm=true;
            if(this.oldpass === this.newpass){
              this.validatePass=true;
            }
          }
      }else{
        if(this.oldpass != '' && this.newpass != '' && this.oldpass === this.newpass){
          this.validatePass=true;
        }
      }

    }
  }
  checkComfirmpass(val){
    this.confirm=false;
    this.validatePass=false;
    this.validateComfirm=false;
    if(val.target.value.length >= 6){
      this.comfirmpass='';
      this.comfirmpass=val.target.value;
     if(this.newpass===this.comfirmpass &&  this.oldpass !== this.newpass){
      this.confirm=true;
     }else{
      if( this.oldpass != '' && this.newpass != '' && this.newpass!==this.comfirmpass){
        this.validateComfirm=true;
      }
      if( this.oldpass != '' && this.newpass != '' && this.oldpass === this.newpass){
        this.validatePass=true;
      }
    }
    }else{
        this.validatePass=false;
    }
    // // console.log(this.validateComfirm)
  }
}
