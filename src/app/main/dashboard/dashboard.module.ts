import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ScrollbarModule } from 'helpers/directives/scrollbar';
import { dashboardRoutes } from './dashboard.routing';


@NgModule({
    imports: [
        SharedModule,
        ScrollbarModule,
        RouterModule.forChild(dashboardRoutes),
    ],
    declarations: [],
})
export class DashboardModule {}