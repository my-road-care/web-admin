import { Component, OnInit } from '@angular/core'
import { DateTime } from 'luxon';
import { DashboardData } from '../../dashboard.service';
import { LoadingDialogComponent } from 'app/shared/loading-dialog/loading-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { DashboardService } from 'app/main/supervision/dashboard/dashboard.service';
const now = DateTime.now();
@Component({
    selector: 'minister-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class MinisterDashboardComponent implements OnInit {
    
    data: DashboardData
    constructor(private service: DashboardService, private matDialog: MatDialog) { }

    ngOnInit(): void {
        this.getData()
    }
    
    getData() {
        const dialog = this.showLoading()
        this.service.getData().subscribe((res: DashboardData) => {
            dialog.close()
            this.data = res
        })
    }

    showLoading() {
        const dialog = this.matDialog.open(LoadingDialogComponent, {
            data: {
              message: 'សូមមេត្តារង់ចាំ...'
            }
          })
          return dialog
    }
}
