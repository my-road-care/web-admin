import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { DashboardData } from '../../dashboard.service';
import { DashboardService } from 'app/main/supervision/dashboard/dashboard.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingDialogComponent } from 'app/shared/loading-dialog/loading-dialog.component';
const now = DateTime.now();
@Component({
    selector: 'sv-admin-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class SVAdminDashboardComponent implements OnInit {
    
    data: DashboardData
    constructor(private service: DashboardService, private matDialog: MatDialog) { }

    ngOnInit(): void {
        this.getData()
    }
    
    getData() {
        const dialog = this.showLoading()
        this.service.getData().subscribe((res: DashboardData) => {
            dialog.close()
            this.data = res
        })
    }

    showLoading() {
        const dialog = this.matDialog.open(LoadingDialogComponent, {
            data: {
              message: 'សូមមេត្តារង់ចាំ...'
            }
          })
          return dialog
    }
}
