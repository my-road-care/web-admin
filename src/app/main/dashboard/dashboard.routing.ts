import { Route } from "@angular/router";
import { ListingDashboardComponent } from "./listing-dashboard/listing-dashboard.component";
import { HeadCommitteeDashboardComponent } from "./layout/head-committee/dashboard.component";
import { MinisterDashboardComponent } from "./layout/minister/dashboard.component";
import { RPAdminDashboardComponent } from "./layout/rp-admin/dashboard.component";
import { SuperAdminDashboardComponent } from "./layout/super-admin/dashboard.component";
import { SVAdminDashboardComponent } from "./layout/sv-admin/dashboard.component";
import { SpecialInspectorDashboardComponent } from "./layout/special-inspector/dashboard.component";
import { SuperUserDashboardComponent } from "./layout/super-user/dashboard.component";

export const dashboardRoutes: Route[] = [
    {
        path: '',
        children: [
            {
                path: '',
                component: renderComponent()
            }
        ]
    },
];

function renderComponent() {
    const access = localStorage.getItem('access')
    switch(access) {
        case 'Super Admin': return SuperAdminDashboardComponent
        case 'Minister': return MinisterDashboardComponent
        case 'Head Of Committee': return HeadCommitteeDashboardComponent
        case 'SV Admin': return SVAdminDashboardComponent
        case 'RP Admin': return RPAdminDashboardComponent
        case 'SV Special Inspector': return SpecialInspectorDashboardComponent
        case 'Supervision User':
        case 'SV Info Entry':
            return SuperUserDashboardComponent
        default: return ListingDashboardComponent
    }
}