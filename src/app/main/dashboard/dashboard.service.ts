import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';

export interface DashboardData {

    project: {
        all: number
        completed: number
    }

    pothole: {
        all: number
        completed: number
    }
}

@Injectable({
    providedIn: 'root',
})
export class DashboardService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}

    getData() {
        return this.http.get(`${this.url}/dashboard`)
    }
}
