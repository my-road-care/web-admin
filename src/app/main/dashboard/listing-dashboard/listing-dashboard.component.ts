import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
const now = DateTime.now();
@Component({
    selector: 'app-listing-dashboard',
    templateUrl: './listing-dashboard.component.html',
    styleUrls: ['./listing-dashboard.component.scss']
})
export class ListingDashboardComponent implements OnInit {
    chartVisitors: ApexOptions = {};
    chartConversions: ApexOptions = {};
    chartImpressions: ApexOptions = {};
    chartVisits: ApexOptions = {};
    chartVisitorsVsPageViews: ApexOptions = {};
    chartNewVsReturning: ApexOptions = {};
    chartGender: ApexOptions = {};
    chartAge: ApexOptions = {};
    chartLanguage: ApexOptions = {};
    chartGithubIssues: ApexOptions = {};
    chartTaskDistribution: ApexOptions = {};

    public data: any = {
        visitors: {
            series: {
                'this-year': [
                    {
                        name: 'Visitors',
                        data: [
                            {
                                x: now.minus({ months: 12 }).plus({ day: 1 }).toJSDate(),
                                y: 4884
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 4 }).toJSDate(),
                                y: 5351
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 7 }).toJSDate(),
                                y: 5293
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 10 }).toJSDate(),
                                y: 4908
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 13 }).toJSDate(),
                                y: 5027
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 16 }).toJSDate(),
                                y: 4837
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 19 }).toJSDate(),
                                y: 4484
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 22 }).toJSDate(),
                                y: 4071
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 25 }).toJSDate(),
                                y: 4124
                            },
                            {
                                x: now.minus({ months: 12 }).plus({ day: 28 }).toJSDate(),
                                y: 4563
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 1 }).toJSDate(),
                                y: 3820
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 4 }).toJSDate(),
                                y: 3968
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 7 }).toJSDate(),
                                y: 4102
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 10 }).toJSDate(),
                                y: 3941
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 13 }).toJSDate(),
                                y: 3566
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 16 }).toJSDate(),
                                y: 3853
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 19 }).toJSDate(),
                                y: 3853
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 22 }).toJSDate(),
                                y: 4069
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 25 }).toJSDate(),
                                y: 3879
                            },
                            {
                                x: now.minus({ months: 11 }).plus({ day: 28 }).toJSDate(),
                                y: 4298
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 1 }).toJSDate(),
                                y: 4355
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 4 }).toJSDate(),
                                y: 4065
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 7 }).toJSDate(),
                                y: 3650
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 10 }).toJSDate(),
                                y: 3379
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 13 }).toJSDate(),
                                y: 3191
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 16 }).toJSDate(),
                                y: 2968
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 19 }).toJSDate(),
                                y: 2957
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 22 }).toJSDate(),
                                y: 3313
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 25 }).toJSDate(),
                                y: 3708
                            },
                            {
                                x: now.minus({ months: 10 }).plus({ day: 28 }).toJSDate(),
                                y: 3586
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 1 }).toJSDate(),
                                y: 3965
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 4 }).toJSDate(),
                                y: 3901
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 7 }).toJSDate(),
                                y: 3410
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 10 }).toJSDate(),
                                y: 3748
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 13 }).toJSDate(),
                                y: 3929
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 16 }).toJSDate(),
                                y: 3846
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 19 }).toJSDate(),
                                y: 3771
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 22 }).toJSDate(),
                                y: 4015
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 25 }).toJSDate(),
                                y: 3589
                            },
                            {
                                x: now.minus({ months: 9 }).plus({ day: 28 }).toJSDate(),
                                y: 3150
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 1 }).toJSDate(),
                                y: 3050
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 4 }).toJSDate(),
                                y: 2574
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 7 }).toJSDate(),
                                y: 2823
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 10 }).toJSDate(),
                                y: 2848
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 13 }).toJSDate(),
                                y: 3000
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 16 }).toJSDate(),
                                y: 3216
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 19 }).toJSDate(),
                                y: 3299
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 22 }).toJSDate(),
                                y: 3768
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 25 }).toJSDate(),
                                y: 3524
                            },
                            {
                                x: now.minus({ months: 8 }).plus({ day: 28 }).toJSDate(),
                                y: 3918
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 1 }).toJSDate(),
                                y: 4145
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 4 }).toJSDate(),
                                y: 4378
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 7 }).toJSDate(),
                                y: 3941
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 10 }).toJSDate(),
                                y: 3932
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 13 }).toJSDate(),
                                y: 4380
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 16 }).toJSDate(),
                                y: 4243
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 19 }).toJSDate(),
                                y: 4367
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 22 }).toJSDate(),
                                y: 3879
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 25 }).toJSDate(),
                                y: 4357
                            },
                            {
                                x: now.minus({ months: 7 }).plus({ day: 28 }).toJSDate(),
                                y: 4181
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 1 }).toJSDate(),
                                y: 4619
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 4 }).toJSDate(),
                                y: 4769
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 7 }).toJSDate(),
                                y: 4901
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 10 }).toJSDate(),
                                y: 4640
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 13 }).toJSDate(),
                                y: 5128
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 16 }).toJSDate(),
                                y: 5015
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 19 }).toJSDate(),
                                y: 5360
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 22 }).toJSDate(),
                                y: 5608
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 25 }).toJSDate(),
                                y: 5272
                            },
                            {
                                x: now.minus({ months: 6 }).plus({ day: 28 }).toJSDate(),
                                y: 5660
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 1 }).toJSDate(),
                                y: 5836
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 4 }).toJSDate(),
                                y: 5659
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 7 }).toJSDate(),
                                y: 5575
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 10 }).toJSDate(),
                                y: 5474
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 13 }).toJSDate(),
                                y: 5427
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 16 }).toJSDate(),
                                y: 5865
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 19 }).toJSDate(),
                                y: 5700
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 22 }).toJSDate(),
                                y: 6052
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 25 }).toJSDate(),
                                y: 5760
                            },
                            {
                                x: now.minus({ months: 5 }).plus({ day: 28 }).toJSDate(),
                                y: 5648
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 1 }).toJSDate(),
                                y: 5435
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 4 }).toJSDate(),
                                y: 5239
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 7 }).toJSDate(),
                                y: 5452
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 10 }).toJSDate(),
                                y: 5416
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 13 }).toJSDate(),
                                y: 5195
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 16 }).toJSDate(),
                                y: 5119
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 19 }).toJSDate(),
                                y: 4635
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 22 }).toJSDate(),
                                y: 4833
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 25 }).toJSDate(),
                                y: 4584
                            },
                            {
                                x: now.minus({ months: 4 }).plus({ day: 28 }).toJSDate(),
                                y: 4822
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 1 }).toJSDate(),
                                y: 4582
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 4 }).toJSDate(),
                                y: 4348
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 7 }).toJSDate(),
                                y: 4132
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 10 }).toJSDate(),
                                y: 4099
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 13 }).toJSDate(),
                                y: 3849
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 16 }).toJSDate(),
                                y: 4010
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 19 }).toJSDate(),
                                y: 4486
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 22 }).toJSDate(),
                                y: 4403
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 25 }).toJSDate(),
                                y: 4141
                            },
                            {
                                x: now.minus({ months: 3 }).plus({ day: 28 }).toJSDate(),
                                y: 3780
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 1 }).toJSDate(),
                                y: 3524
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 4 }).toJSDate(),
                                y: 3212
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 7 }).toJSDate(),
                                y: 3568
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 10 }).toJSDate(),
                                y: 3800
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 13 }).toJSDate(),
                                y: 3796
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 16 }).toJSDate(),
                                y: 3870
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 19 }).toJSDate(),
                                y: 3745
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 22 }).toJSDate(),
                                y: 3751
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 25 }).toJSDate(),
                                y: 3310
                            },
                            {
                                x: now.minus({ months: 2 }).plus({ day: 28 }).toJSDate(),
                                y: 3509
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 1 }).toJSDate(),
                                y: 3187
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 4 }).toJSDate(),
                                y: 2918
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 7 }).toJSDate(),
                                y: 3191
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 10 }).toJSDate(),
                                y: 3437
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 13 }).toJSDate(),
                                y: 3291
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 16 }).toJSDate(),
                                y: 3317
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 19 }).toJSDate(),
                                y: 3716
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 22 }).toJSDate(),
                                y: 3260
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 25 }).toJSDate(),
                                y: 3694
                            },
                            {
                                x: now.minus({ months: 1 }).plus({ day: 28 }).toJSDate(),
                                y: 3598
                            }
                        ]
                    }
                ],
                'last-year': [
                    {
                        name: 'Visitors',
                        data: [
                            {
                                x: now.minus({ months: 24 }).plus({ day: 1 }).toJSDate(),
                                y: 2021
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 4 }).toJSDate(),
                                y: 1749
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 7 }).toJSDate(),
                                y: 1654
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 10 }).toJSDate(),
                                y: 1900
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 13 }).toJSDate(),
                                y: 1647
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 16 }).toJSDate(),
                                y: 1315
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 19 }).toJSDate(),
                                y: 1807
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 22 }).toJSDate(),
                                y: 1793
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 25 }).toJSDate(),
                                y: 1892
                            },
                            {
                                x: now.minus({ months: 24 }).plus({ day: 28 }).toJSDate(),
                                y: 1846
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 1 }).toJSDate(),
                                y: 1804
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 4 }).toJSDate(),
                                y: 1778
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 7 }).toJSDate(),
                                y: 2015
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 10 }).toJSDate(),
                                y: 1892
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 13 }).toJSDate(),
                                y: 1708
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 16 }).toJSDate(),
                                y: 1711
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 19 }).toJSDate(),
                                y: 1570
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 22 }).toJSDate(),
                                y: 1507
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 25 }).toJSDate(),
                                y: 1451
                            },
                            {
                                x: now.minus({ months: 23 }).plus({ day: 28 }).toJSDate(),
                                y: 1522
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 1 }).toJSDate(),
                                y: 1977
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 4 }).toJSDate(),
                                y: 2367
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 7 }).toJSDate(),
                                y: 2798
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 10 }).toJSDate(),
                                y: 3080
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 13 }).toJSDate(),
                                y: 2856
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 16 }).toJSDate(),
                                y: 2745
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 19 }).toJSDate(),
                                y: 2750
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 22 }).toJSDate(),
                                y: 2728
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 25 }).toJSDate(),
                                y: 2436
                            },
                            {
                                x: now.minus({ months: 22 }).plus({ day: 28 }).toJSDate(),
                                y: 2289
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 1 }).toJSDate(),
                                y: 2804
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 4 }).toJSDate(),
                                y: 2777
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 7 }).toJSDate(),
                                y: 3024
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 10 }).toJSDate(),
                                y: 2657
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 13 }).toJSDate(),
                                y: 2218
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 16 }).toJSDate(),
                                y: 1964
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 19 }).toJSDate(),
                                y: 1674
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 22 }).toJSDate(),
                                y: 1721
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 25 }).toJSDate(),
                                y: 2005
                            },
                            {
                                x: now.minus({ months: 21 }).plus({ day: 28 }).toJSDate(),
                                y: 1613
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 1 }).toJSDate(),
                                y: 1071
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 4 }).toJSDate(),
                                y: 1079
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 7 }).toJSDate(),
                                y: 1133
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 10 }).toJSDate(),
                                y: 1536
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 13 }).toJSDate(),
                                y: 2016
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 16 }).toJSDate(),
                                y: 2256
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 19 }).toJSDate(),
                                y: 1934
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 22 }).toJSDate(),
                                y: 1832
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 25 }).toJSDate(),
                                y: 2075
                            },
                            {
                                x: now.minus({ months: 20 }).plus({ day: 28 }).toJSDate(),
                                y: 1709
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 1 }).toJSDate(),
                                y: 1831
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 4 }).toJSDate(),
                                y: 1434
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 7 }).toJSDate(),
                                y: 1293
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 10 }).toJSDate(),
                                y: 1064
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 13 }).toJSDate(),
                                y: 1080
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 16 }).toJSDate(),
                                y: 1032
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 19 }).toJSDate(),
                                y: 1280
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 22 }).toJSDate(),
                                y: 1344
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 25 }).toJSDate(),
                                y: 1835
                            },
                            {
                                x: now.minus({ months: 19 }).plus({ day: 28 }).toJSDate(),
                                y: 2287
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 1 }).toJSDate(),
                                y: 2692
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 4 }).toJSDate(),
                                y: 2250
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 7 }).toJSDate(),
                                y: 1814
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 10 }).toJSDate(),
                                y: 1906
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 13 }).toJSDate(),
                                y: 1973
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 16 }).toJSDate(),
                                y: 1882
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 19 }).toJSDate(),
                                y: 2333
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 22 }).toJSDate(),
                                y: 2048
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 25 }).toJSDate(),
                                y: 2547
                            },
                            {
                                x: now.minus({ months: 18 }).plus({ day: 28 }).toJSDate(),
                                y: 2884
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 1 }).toJSDate(),
                                y: 2771
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 4 }).toJSDate(),
                                y: 2522
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 7 }).toJSDate(),
                                y: 2543
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 10 }).toJSDate(),
                                y: 2413
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 13 }).toJSDate(),
                                y: 2002
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 16 }).toJSDate(),
                                y: 1838
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 19 }).toJSDate(),
                                y: 1830
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 22 }).toJSDate(),
                                y: 1872
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 25 }).toJSDate(),
                                y: 2246
                            },
                            {
                                x: now.minus({ months: 17 }).plus({ day: 28 }).toJSDate(),
                                y: 2171
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 1 }).toJSDate(),
                                y: 2988
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 4 }).toJSDate(),
                                y: 2694
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 7 }).toJSDate(),
                                y: 2806
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 10 }).toJSDate(),
                                y: 3040
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 13 }).toJSDate(),
                                y: 2898
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 16 }).toJSDate(),
                                y: 3013
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 19 }).toJSDate(),
                                y: 2760
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 22 }).toJSDate(),
                                y: 3021
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 25 }).toJSDate(),
                                y: 2688
                            },
                            {
                                x: now.minus({ months: 16 }).plus({ day: 28 }).toJSDate(),
                                y: 2572
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 1 }).toJSDate(),
                                y: 2789
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 4 }).toJSDate(),
                                y: 3069
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 7 }).toJSDate(),
                                y: 3142
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 10 }).toJSDate(),
                                y: 3614
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 13 }).toJSDate(),
                                y: 3202
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 16 }).toJSDate(),
                                y: 2730
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 19 }).toJSDate(),
                                y: 2951
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 22 }).toJSDate(),
                                y: 3267
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 25 }).toJSDate(),
                                y: 2882
                            },
                            {
                                x: now.minus({ months: 15 }).plus({ day: 28 }).toJSDate(),
                                y: 2885
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 1 }).toJSDate(),
                                y: 2915
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 4 }).toJSDate(),
                                y: 2790
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 7 }).toJSDate(),
                                y: 3071
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 10 }).toJSDate(),
                                y: 2802
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 13 }).toJSDate(),
                                y: 2382
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 16 }).toJSDate(),
                                y: 1883
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 19 }).toJSDate(),
                                y: 1448
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 22 }).toJSDate(),
                                y: 1176
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 25 }).toJSDate(),
                                y: 1275
                            },
                            {
                                x: now.minus({ months: 14 }).plus({ day: 28 }).toJSDate(),
                                y: 1136
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 1 }).toJSDate(),
                                y: 1160
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 4 }).toJSDate(),
                                y: 1524
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 7 }).toJSDate(),
                                y: 1305
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 10 }).toJSDate(),
                                y: 1725
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 13 }).toJSDate(),
                                y: 1850
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 16 }).toJSDate(),
                                y: 2304
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 19 }).toJSDate(),
                                y: 2187
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 22 }).toJSDate(),
                                y: 2597
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 25 }).toJSDate(),
                                y: 2246
                            },
                            {
                                x: now.minus({ months: 13 }).plus({ day: 28 }).toJSDate(),
                                y: 1767
                            }
                        ]
                    }
                ]
            }
        },
        conversions: {
            amount: 4123,
            labels: [
                now.minus({ days: 47 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 40 }).toFormat('dd MMM'),
                now.minus({ days: 39 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 32 }).toFormat('dd MMM'),
                now.minus({ days: 31 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 24 }).toFormat('dd MMM'),
                now.minus({ days: 23 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 16 }).toFormat('dd MMM'),
                now.minus({ days: 15 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 8 }).toFormat('dd MMM'),
                now.minus({ days: 7 }).toFormat('dd MMM') + ' - ' + now.toFormat('dd MMM')
            ],
            series: [
                {
                    name: 'Conversions',
                    data: [4412, 4345, 4541, 4677, 4322, 4123]
                }
            ]
        },
        impressions: {
            amount: 46085,
            labels: [
                now.minus({ days: 31 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 24 }).toFormat('dd MMM'),
                now.minus({ days: 23 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 16 }).toFormat('dd MMM'),
                now.minus({ days: 15 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 8 }).toFormat('dd MMM'),
                now.minus({ days: 7 }).toFormat('dd MMM') + ' - ' + now.toFormat('dd MMM')
            ],
            series: [
                {
                    name: 'Impressions',
                    data: [11577, 11441, 11544, 11523]
                }
            ]
        },
        visits: {
            amount: 62083,
            labels: [
                now.minus({ days: 31 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 24 }).toFormat('dd MMM'),
                now.minus({ days: 23 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 16 }).toFormat('dd MMM'),
                now.minus({ days: 15 }).toFormat('dd MMM') + ' - ' + now.minus({ days: 8 }).toFormat('dd MMM'),
                now.minus({ days: 7 }).toFormat('dd MMM') + ' - ' + now.toFormat('dd MMM')
            ],
            series: [
                {
                    name: 'Visits',
                    data: [15521, 15519, 15522, 15521]
                }
            ]
        },
        visitorsVsPageViews: {
            overallScore: 472,
            averageRatio: 45,
            predictedRatio: 55,
            series: [
                {
                    name: 'Page Views',
                    data: [
                        {
                            x: now.minus({ days: 65 }).toJSDate(),
                            y: 4769
                        },
                        {
                            x: now.minus({ days: 64 }).toJSDate(),
                            y: 4901
                        },
                        {
                            x: now.minus({ days: 63 }).toJSDate(),
                            y: 4640
                        },
                        {
                            x: now.minus({ days: 62 }).toJSDate(),
                            y: 5128
                        },
                        {
                            x: now.minus({ days: 61 }).toJSDate(),
                            y: 5015
                        },
                        {
                            x: now.minus({ days: 60 }).toJSDate(),
                            y: 5360
                        },
                        {
                            x: now.minus({ days: 59 }).toJSDate(),
                            y: 5608
                        },
                        {
                            x: now.minus({ days: 58 }).toJSDate(),
                            y: 5272
                        },
                        {
                            x: now.minus({ days: 57 }).toJSDate(),
                            y: 5660
                        },
                        {
                            x: now.minus({ days: 56 }).toJSDate(),
                            y: 6026
                        },
                        {
                            x: now.minus({ days: 55 }).toJSDate(),
                            y: 5836
                        },
                        {
                            x: now.minus({ days: 54 }).toJSDate(),
                            y: 5659
                        },
                        {
                            x: now.minus({ days: 53 }).toJSDate(),
                            y: 5575
                        },
                        {
                            x: now.minus({ days: 52 }).toJSDate(),
                            y: 5474
                        },
                        {
                            x: now.minus({ days: 51 }).toJSDate(),
                            y: 5427
                        },
                        {
                            x: now.minus({ days: 50 }).toJSDate(),
                            y: 5865
                        },
                        {
                            x: now.minus({ days: 49 }).toJSDate(),
                            y: 5700
                        },
                        {
                            x: now.minus({ days: 48 }).toJSDate(),
                            y: 6052
                        },
                        {
                            x: now.minus({ days: 47 }).toJSDate(),
                            y: 5760
                        },
                        {
                            x: now.minus({ days: 46 }).toJSDate(),
                            y: 5648
                        },
                        {
                            x: now.minus({ days: 45 }).toJSDate(),
                            y: 5510
                        },
                        {
                            x: now.minus({ days: 44 }).toJSDate(),
                            y: 5435
                        },
                        {
                            x: now.minus({ days: 43 }).toJSDate(),
                            y: 5239
                        },
                        {
                            x: now.minus({ days: 42 }).toJSDate(),
                            y: 5452
                        },
                        {
                            x: now.minus({ days: 41 }).toJSDate(),
                            y: 5416
                        },
                        {
                            x: now.minus({ days: 40 }).toJSDate(),
                            y: 5195
                        },
                        {
                            x: now.minus({ days: 39 }).toJSDate(),
                            y: 5119
                        },
                        {
                            x: now.minus({ days: 38 }).toJSDate(),
                            y: 4635
                        },
                        {
                            x: now.minus({ days: 37 }).toJSDate(),
                            y: 4833
                        },
                        {
                            x: now.minus({ days: 36 }).toJSDate(),
                            y: 4584
                        },
                        {
                            x: now.minus({ days: 35 }).toJSDate(),
                            y: 4822
                        },
                        {
                            x: now.minus({ days: 34 }).toJSDate(),
                            y: 4330
                        },
                        {
                            x: now.minus({ days: 33 }).toJSDate(),
                            y: 4582
                        },
                        {
                            x: now.minus({ days: 32 }).toJSDate(),
                            y: 4348
                        },
                        {
                            x: now.minus({ days: 31 }).toJSDate(),
                            y: 4132
                        },
                        {
                            x: now.minus({ days: 30 }).toJSDate(),
                            y: 4099
                        },
                        {
                            x: now.minus({ days: 29 }).toJSDate(),
                            y: 3849
                        },
                        {
                            x: now.minus({ days: 28 }).toJSDate(),
                            y: 4010
                        },
                        {
                            x: now.minus({ days: 27 }).toJSDate(),
                            y: 4486
                        },
                        {
                            x: now.minus({ days: 26 }).toJSDate(),
                            y: 4403
                        },
                        {
                            x: now.minus({ days: 25 }).toJSDate(),
                            y: 4141
                        },
                        {
                            x: now.minus({ days: 24 }).toJSDate(),
                            y: 3780
                        },
                        {
                            x: now.minus({ days: 23 }).toJSDate(),
                            y: 3929
                        },
                        {
                            x: now.minus({ days: 22 }).toJSDate(),
                            y: 3524
                        },
                        {
                            x: now.minus({ days: 21 }).toJSDate(),
                            y: 3212
                        },
                        {
                            x: now.minus({ days: 20 }).toJSDate(),
                            y: 3568
                        },
                        {
                            x: now.minus({ days: 19 }).toJSDate(),
                            y: 3800
                        },
                        {
                            x: now.minus({ days: 18 }).toJSDate(),
                            y: 3796
                        },
                        {
                            x: now.minus({ days: 17 }).toJSDate(),
                            y: 3870
                        },
                        {
                            x: now.minus({ days: 16 }).toJSDate(),
                            y: 3745
                        },
                        {
                            x: now.minus({ days: 15 }).toJSDate(),
                            y: 3751
                        },
                        {
                            x: now.minus({ days: 14 }).toJSDate(),
                            y: 3310
                        },
                        {
                            x: now.minus({ days: 13 }).toJSDate(),
                            y: 3509
                        },
                        {
                            x: now.minus({ days: 12 }).toJSDate(),
                            y: 3311
                        },
                        {
                            x: now.minus({ days: 11 }).toJSDate(),
                            y: 3187
                        },
                        {
                            x: now.minus({ days: 10 }).toJSDate(),
                            y: 2918
                        },
                        {
                            x: now.minus({ days: 9 }).toJSDate(),
                            y: 3191
                        },
                        {
                            x: now.minus({ days: 8 }).toJSDate(),
                            y: 3437
                        },
                        {
                            x: now.minus({ days: 7 }).toJSDate(),
                            y: 3291
                        },
                        {
                            x: now.minus({ days: 6 }).toJSDate(),
                            y: 3317
                        },
                        {
                            x: now.minus({ days: 5 }).toJSDate(),
                            y: 3716
                        },
                        {
                            x: now.minus({ days: 4 }).toJSDate(),
                            y: 3260
                        },
                        {
                            x: now.minus({ days: 3 }).toJSDate(),
                            y: 3694
                        },
                        {
                            x: now.minus({ days: 2 }).toJSDate(),
                            y: 3598
                        },
                        {
                            x: now.minus({ days: 1 }).toJSDate(),
                            y: 3812
                        }
                    ]
                },
                {
                    name: 'Visitors',
                    data: [
                        {
                            x: now.minus({ days: 65 }).toJSDate(),
                            y: 1654
                        },
                        {
                            x: now.minus({ days: 64 }).toJSDate(),
                            y: 1900
                        },
                        {
                            x: now.minus({ days: 63 }).toJSDate(),
                            y: 1647
                        },
                        {
                            x: now.minus({ days: 62 }).toJSDate(),
                            y: 1315
                        },
                        {
                            x: now.minus({ days: 61 }).toJSDate(),
                            y: 1807
                        },
                        {
                            x: now.minus({ days: 60 }).toJSDate(),
                            y: 1793
                        },
                        {
                            x: now.minus({ days: 59 }).toJSDate(),
                            y: 1892
                        },
                        {
                            x: now.minus({ days: 58 }).toJSDate(),
                            y: 1846
                        },
                        {
                            x: now.minus({ days: 57 }).toJSDate(),
                            y: 1966
                        },
                        {
                            x: now.minus({ days: 56 }).toJSDate(),
                            y: 1804
                        },
                        {
                            x: now.minus({ days: 55 }).toJSDate(),
                            y: 1778
                        },
                        {
                            x: now.minus({ days: 54 }).toJSDate(),
                            y: 2015
                        },
                        {
                            x: now.minus({ days: 53 }).toJSDate(),
                            y: 1892
                        },
                        {
                            x: now.minus({ days: 52 }).toJSDate(),
                            y: 1708
                        },
                        {
                            x: now.minus({ days: 51 }).toJSDate(),
                            y: 1711
                        },
                        {
                            x: now.minus({ days: 50 }).toJSDate(),
                            y: 1570
                        },
                        {
                            x: now.minus({ days: 49 }).toJSDate(),
                            y: 1507
                        },
                        {
                            x: now.minus({ days: 48 }).toJSDate(),
                            y: 1451
                        },
                        {
                            x: now.minus({ days: 47 }).toJSDate(),
                            y: 1522
                        },
                        {
                            x: now.minus({ days: 46 }).toJSDate(),
                            y: 1801
                        },
                        {
                            x: now.minus({ days: 45 }).toJSDate(),
                            y: 1977
                        },
                        {
                            x: now.minus({ days: 44 }).toJSDate(),
                            y: 2367
                        },
                        {
                            x: now.minus({ days: 43 }).toJSDate(),
                            y: 2798
                        },
                        {
                            x: now.minus({ days: 42 }).toJSDate(),
                            y: 3080
                        },
                        {
                            x: now.minus({ days: 41 }).toJSDate(),
                            y: 2856
                        },
                        {
                            x: now.minus({ days: 40 }).toJSDate(),
                            y: 2745
                        },
                        {
                            x: now.minus({ days: 39 }).toJSDate(),
                            y: 2750
                        },
                        {
                            x: now.minus({ days: 38 }).toJSDate(),
                            y: 2728
                        },
                        {
                            x: now.minus({ days: 37 }).toJSDate(),
                            y: 2436
                        },
                        {
                            x: now.minus({ days: 36 }).toJSDate(),
                            y: 2289
                        },
                        {
                            x: now.minus({ days: 35 }).toJSDate(),
                            y: 2657
                        },
                        {
                            x: now.minus({ days: 34 }).toJSDate(),
                            y: 2804
                        },
                        {
                            x: now.minus({ days: 33 }).toJSDate(),
                            y: 2777
                        },
                        {
                            x: now.minus({ days: 32 }).toJSDate(),
                            y: 3024
                        },
                        {
                            x: now.minus({ days: 31 }).toJSDate(),
                            y: 2657
                        },
                        {
                            x: now.minus({ days: 30 }).toJSDate(),
                            y: 2218
                        },
                        {
                            x: now.minus({ days: 29 }).toJSDate(),
                            y: 1964
                        },
                        {
                            x: now.minus({ days: 28 }).toJSDate(),
                            y: 1674
                        },
                        {
                            x: now.minus({ days: 27 }).toJSDate(),
                            y: 1721
                        },
                        {
                            x: now.minus({ days: 26 }).toJSDate(),
                            y: 2005
                        },
                        {
                            x: now.minus({ days: 25 }).toJSDate(),
                            y: 1613
                        },
                        {
                            x: now.minus({ days: 24 }).toJSDate(),
                            y: 1295
                        },
                        {
                            x: now.minus({ days: 23 }).toJSDate(),
                            y: 1071
                        },
                        {
                            x: now.minus({ days: 22 }).toJSDate(),
                            y: 799
                        },
                        {
                            x: now.minus({ days: 21 }).toJSDate(),
                            y: 1133
                        },
                        {
                            x: now.minus({ days: 20 }).toJSDate(),
                            y: 1536
                        },
                        {
                            x: now.minus({ days: 19 }).toJSDate(),
                            y: 2016
                        },
                        {
                            x: now.minus({ days: 18 }).toJSDate(),
                            y: 2256
                        },
                        {
                            x: now.minus({ days: 17 }).toJSDate(),
                            y: 1934
                        },
                        {
                            x: now.minus({ days: 16 }).toJSDate(),
                            y: 1832
                        },
                        {
                            x: now.minus({ days: 15 }).toJSDate(),
                            y: 2075
                        },
                        {
                            x: now.minus({ days: 14 }).toJSDate(),
                            y: 1709
                        },
                        {
                            x: now.minus({ days: 13 }).toJSDate(),
                            y: 1932
                        },
                        {
                            x: now.minus({ days: 12 }).toJSDate(),
                            y: 1831
                        },
                        {
                            x: now.minus({ days: 11 }).toJSDate(),
                            y: 1434
                        },
                        {
                            x: now.minus({ days: 10 }).toJSDate(),
                            y: 993
                        },
                        {
                            x: now.minus({ days: 9 }).toJSDate(),
                            y: 1064
                        },
                        {
                            x: now.minus({ days: 8 }).toJSDate(),
                            y: 618
                        },
                        {
                            x: now.minus({ days: 7 }).toJSDate(),
                            y: 1032
                        },
                        {
                            x: now.minus({ days: 6 }).toJSDate(),
                            y: 1280
                        },
                        {
                            x: now.minus({ days: 5 }).toJSDate(),
                            y: 1344
                        },
                        {
                            x: now.minus({ days: 4 }).toJSDate(),
                            y: 1835
                        },
                        {
                            x: now.minus({ days: 3 }).toJSDate(),
                            y: 2287
                        },
                        {
                            x: now.minus({ days: 2 }).toJSDate(),
                            y: 2226
                        },
                        {
                            x: now.minus({ days: 1 }).toJSDate(),
                            y: 2692
                        }
                    ]
                }
            ]
        },
        newVsReturning: {
            uniqueVisitors: 46085,
            series: [80, 20],
            labels: [
                'New',
                'Returning'
            ]
        },
        gender: {
            uniqueVisitors: 46085,
            series: [55, 45],
            labels: [
                'Male',
                'Female'
            ]
        },
        age: {
            uniqueVisitors: 46085,
            series: [35, 65],
            labels: [
                'Under 30',
                'Over 30'
            ]
        },
        language: {
            uniqueVisitors: 46085,
            series: [25, 75],
            labels: [
                'English',
                'Other'
            ]
        },
        githubIssues: {
            overview: {
                'this-week': {
                    'new-issues': 214,
                    'closed-issues': 75,
                    'fixed': 3,
                    'wont-fix': 4,
                    're-opened': 8,
                    'needs-triage': 6
                },
                'last-week': {
                    'new-issues': 197,
                    'closed-issues': 72,
                    'fixed': 6,
                    'wont-fix': 11,
                    're-opened': 6,
                    'needs-triage': 5
                }
            },
            labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            series: {
                'this-week': [
                    {
                        name: 'New issues',
                        type: 'line',
                        data: [42, 28, 43, 34, 20, 25, 22]
                    },
                    {
                        name: 'Closed issues',
                        type: 'column',
                        data: [11, 10, 8, 11, 8, 10, 17]
                    }
                ],
                'last-week': [
                    {
                        name: 'New issues',
                        type: 'line',
                        data: [37, 32, 39, 27, 18, 24, 20]
                    },
                    {
                        name: 'Closed issues',
                        type: 'column',
                        data: [9, 8, 10, 12, 7, 11, 15]
                    }
                ]
            }
        },
        taskDistribution: {
            overview: {
                'this-week': {
                    'new': 594,
                    'completed': 287
                },
                'last-week': {
                    'new': 526,
                    'completed': 260
                }
            },
            labels: ['API', 'Backend', 'Frontend', 'Issues'],
            series: {
                'this-week': [15, 20, 38, 27],
                'last-week': [19, 16, 42, 23]
            }
        },
        schedule: {
            today: [
                {
                    title: 'Group Meeting',
                    time: 'in 32 minutes',
                    location: 'Conference room 1B'
                },
                {
                    title: 'Coffee Break',
                    time: '10:30 AM'
                },
                {
                    title: 'Public Beta Release',
                    time: '11:00 AM'
                },
                {
                    title: 'Lunch',
                    time: '12:10 PM'
                },
                {
                    title: 'Dinner with David',
                    time: '05:30 PM',
                    location: 'Magnolia'
                },
                {
                    title: 'Jane\'s Birthday Party',
                    time: '07:30 PM',
                    location: 'Home'
                },
                {
                    title: 'Overseer\'s Retirement Party',
                    time: '09:30 PM',
                    location: 'Overseer\'s room'
                }
            ],
            tomorrow: [
                {
                    title: 'Marketing Meeting',
                    time: '09:00 AM',
                    location: 'Conference room 1A'
                },
                {
                    title: 'Public Announcement',
                    time: '11:00 AM'
                },
                {
                    title: 'Lunch',
                    time: '12:10 PM'
                },
                {
                    title: 'Meeting with Beta Testers',
                    time: '03:00 PM',
                    location: 'Conference room 2C'
                },
                {
                    title: 'Live Stream',
                    time: '05:30 PM'
                },
                {
                    title: 'Release Party',
                    time: '07:30 PM',
                    location: 'CEO\'s house'
                },
                {
                    title: 'CEO\'s Private Party',
                    time: '09:30 PM',
                    location: 'CEO\'s Penthouse'
                }
            ]
        }
    };

    constructor(private _router: Router) { }

    ngOnInit(): void {
        // console.log(this.data);
        // Prepare the chart data
        this._prepareChartData();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    public trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Prepare the chart data from the data
     *
     * @private
     */
    private _prepareChartData(): void {
        // Visitors
        this.chartVisitors = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                width: '100%',
                height: '100%',
                type: 'area',
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                }
            },
            colors: ['#818CF8'],
            dataLabels: {
                enabled: false
            },
            fill: {
                colors: ['#312E81']
            },
            grid: {
                show: true,
                borderColor: '#334155',
                padding: {
                    top: 10,
                    bottom: -40,
                    left: 0,
                    right: 0
                },
                position: 'back',
                xaxis: {
                    lines: {
                        show: true
                    }
                }
            },
            series: this.data.visitors.series,
            stroke: {
                width: 2
            },
            tooltip: {
                followCursor: true,
                theme: 'dark',
                x: {
                    format: 'MMM dd, yyyy'
                },
                y: {
                    formatter: (value: number): string => `${value}`
                }
            },
            xaxis: {
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false
                },
                crosshairs: {
                    stroke: {
                        color: '#475569',
                        dashArray: 0,
                        width: 2
                    }
                },
                labels: {
                    offsetY: -20,
                    style: {
                        colors: '#CBD5E1'
                    }
                },
                tickAmount: 20,
                tooltip: {
                    enabled: false
                },
                type: 'datetime'
            },
            yaxis: {
                axisTicks: {
                    show: false
                },
                axisBorder: {
                    show: false
                },
                min: (min): number => min - 750,
                max: (max): number => max + 250,
                tickAmount: 5,
                show: false
            }
        };

        // Conversions
        this.chartConversions = {
            chart: {
                animations: {
                    enabled: false
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'area',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#38BDF8'],
            fill: {
                colors: ['#38BDF8'],
                opacity: 0.5
            },
            series: this.data.conversions.series,
            stroke: {
                curve: 'smooth'
            },
            tooltip: {
                followCursor: true,
                theme: 'dark'
            },
            xaxis: {
                type: 'category',
                categories: this.data.conversions.labels
            },
            yaxis: {
                labels: {
                    formatter: (val): string => val.toString()
                }
            }
        };

        // Impressions
        this.chartImpressions = {
            chart: {
                animations: {
                    enabled: false
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'area',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#34D399'],
            fill: {
                colors: ['#34D399'],
                opacity: 0.5
            },
            series: this.data.impressions.series,
            stroke: {
                curve: 'smooth'
            },
            tooltip: {
                followCursor: true,
                theme: 'dark'
            },
            xaxis: {
                type: 'category',
                categories: this.data.impressions.labels
            },
            yaxis: {
                labels: {
                    formatter: (val): string => val.toString()
                }
            }
        };

        // Visits
        this.chartVisits = {
            chart: {
                animations: {
                    enabled: false
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'area',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#FB7185'],
            fill: {
                colors: ['#FB7185'],
                opacity: 0.5
            },
            series: this.data.visits.series,
            stroke: {
                curve: 'smooth'
            },
            tooltip: {
                followCursor: true,
                theme: 'dark'
            },
            xaxis: {
                type: 'category',
                categories: this.data.visits.labels
            },
            yaxis: {
                labels: {
                    formatter: (val): string => val.toString()
                }
            }
        };
        // Visitors vs Page Views
        this.chartVisitorsVsPageViews = {
            chart: {
                animations: {
                    enabled: false
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'area',
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                }
            },
            colors: ['#64748B', '#94A3B8'],
            dataLabels: {
                enabled: false
            },
            fill: {
                colors: ['#64748B', '#94A3B8'],
                opacity: 0.5
            },
            grid: {
                show: false,
                padding: {
                    bottom: -40,
                    left: 0,
                    right: 0
                }
            },
            legend: {
                show: false
            },
            series: this.data.visitorsVsPageViews.series,
            stroke: {
                curve: 'smooth',
                width: 2
            },
            tooltip: {
                followCursor: true,
                theme: 'dark',
                x: {
                    format: 'MMM dd, yyyy'
                }
            },
            xaxis: {
                axisBorder: {
                    show: false
                },
                labels: {
                    offsetY: -20,
                    rotate: 0,
                    style: {
                        colors: 'var(--fuse-text-secondary)'
                    }
                },
                tickAmount: 3,
                tooltip: {
                    enabled: false
                },
                type: 'datetime'
            },
            yaxis: {
                labels: {
                    style: {
                        colors: 'var(--fuse-text-secondary)'
                    }
                },
                max: (max): number => max + 250,
                min: (min): number => min - 250,
                show: false,
                tickAmount: 5
            }
        };
        // New vs. returning
        this.chartNewVsReturning = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'donut',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#3182CE', '#63B3ED'],
            labels: this.data.newVsReturning.labels,
            plotOptions: {
                pie: {
                    customScale: 0.9,
                    expandOnClick: false,
                    donut: {
                        size: '70%'
                    }
                }
            },
            series: this.data.newVsReturning.series,
            states: {
                hover: {
                    filter: {
                        type: 'none'
                    }
                },
                active: {
                    filter: {
                        type: 'none'
                    }
                }
            },
            tooltip: {
                enabled: true,
                fillSeriesColor: false,
                theme: 'dark',
                custom: ({
                    seriesIndex,
                    w
                }): string => `<div class="flex items-center h-8 min-h-8 max-h-8 px-3">
                                                    <div class="w-3 h-3 rounded-full" style="background-color: ${w.config.colors[seriesIndex]};"></div>
                                                    <div class="ml-2 text-md leading-none">${w.config.labels[seriesIndex]}:</div>
                                                    <div class="ml-2 text-md font-bold leading-none">${w.config.series[seriesIndex]}%</div>
                                                </div>`
            }
        };

        // Gender
        this.chartGender = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'donut',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#319795', '#4FD1C5'],
            labels: this.data.gender.labels,
            plotOptions: {
                pie: {
                    customScale: 0.9,
                    expandOnClick: false,
                    donut: {
                        size: '70%'
                    }
                }
            },
            series: this.data.gender.series,
            states: {
                hover: {
                    filter: {
                        type: 'none'
                    }
                },
                active: {
                    filter: {
                        type: 'none'
                    }
                }
            },
            tooltip: {
                enabled: true,
                fillSeriesColor: false,
                theme: 'dark',
                custom: ({
                    seriesIndex,
                    w
                }): string => `<div class="flex items-center h-8 min-h-8 max-h-8 px-3">
                                                     <div class="w-3 h-3 rounded-full" style="background-color: ${w.config.colors[seriesIndex]};"></div>
                                                     <div class="ml-2 text-md leading-none">${w.config.labels[seriesIndex]}:</div>
                                                     <div class="ml-2 text-md font-bold leading-none">${w.config.series[seriesIndex]}%</div>
                                                 </div>`
            }
        };

        // Age
        this.chartAge = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'donut',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#DD6B20', '#F6AD55'],
            labels: this.data.age.labels,
            plotOptions: {
                pie: {
                    customScale: 0.9,
                    expandOnClick: false,
                    donut: {
                        size: '70%'
                    }
                }
            },
            series: this.data.age.series,
            states: {
                hover: {
                    filter: {
                        type: 'none'
                    }
                },
                active: {
                    filter: {
                        type: 'none'
                    }
                }
            },
            tooltip: {
                enabled: true,
                fillSeriesColor: false,
                theme: 'dark',
                custom: ({
                    seriesIndex,
                    w
                }): string => `<div class="flex items-center h-8 min-h-8 max-h-8 px-3">
                                                    <div class="w-3 h-3 rounded-full" style="background-color: ${w.config.colors[seriesIndex]};"></div>
                                                    <div class="ml-2 text-md leading-none">${w.config.labels[seriesIndex]}:</div>
                                                    <div class="ml-2 text-md font-bold leading-none">${w.config.series[seriesIndex]}%</div>
                                                </div>`
            }
        };

        // Language
        this.chartLanguage = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'donut',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#805AD5', '#B794F4'],
            labels: this.data.language.labels,
            plotOptions: {
                pie: {
                    customScale: 0.9,
                    expandOnClick: false,
                    donut: {
                        size: '70%'
                    }
                }
            },
            series: this.data.language.series,
            states: {
                hover: {
                    filter: {
                        type: 'none'
                    }
                },
                active: {
                    filter: {
                        type: 'none'
                    }
                }
            },
            tooltip: {
                enabled: true,
                fillSeriesColor: false,
                theme: 'dark',
                custom: ({
                    seriesIndex,
                    w
                }): string => `<div class="flex items-center h-8 min-h-8 max-h-8 px-3">
                                                    <div class="w-3 h-3 rounded-full" style="background-color: ${w.config.colors[seriesIndex]};"></div>
                                                    <div class="ml-2 text-md leading-none">${w.config.labels[seriesIndex]}:</div>
                                                    <div class="ml-2 text-md font-bold leading-none">${w.config.series[seriesIndex]}%</div>
                                                </div>`
            }
        };

        // Github issues
        this.chartGithubIssues = {
            chart: {
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'line',
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                }
            },
            colors: ['#64748B', '#94A3B8'],
            dataLabels: {
                enabled: true,
                enabledOnSeries: [0],
                background: {
                    borderWidth: 0
                }
            },
            grid: {
                borderColor: 'var(--helpers-border)'
            },
            labels: this.data.githubIssues.labels,
            legend: {
                show: false
            },
            plotOptions: {
                bar: {
                    columnWidth: '50%'
                }
            },
            series: this.data.githubIssues.series,
            states: {
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.75
                    }
                }
            },
            stroke: {
                width: [3, 0]
            },
            tooltip: {
                followCursor: true,
                theme: 'dark'
            },
            xaxis: {
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    color: 'var(--helpers-border)'
                },
                labels: {
                    style: {
                        colors: 'var(--helpers-text-secondary)'
                    }
                },
                tooltip: {
                    enabled: false
                }
            },
            yaxis: {
                labels: {
                    offsetX: -16,
                    style: {
                        colors: 'var(--helpers-text-secondary)'
                    }
                }
            }
        };

        // Task distribution
        this.chartTaskDistribution = {
            chart: {
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'polarArea',
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                }
            },
            labels: this.data.taskDistribution.labels,
            legend: {
                position: 'bottom'
            },
            plotOptions: {
                polarArea: {
                    spokes: {
                        connectorColors: 'var(--helpers-border)'
                    },
                    rings: {
                        strokeColor: 'var(--helpers-border)'
                    }
                }
            },
            series: this.data.taskDistribution.series,
            states: {
                hover: {
                    filter: {
                        type: 'darken',
                        value: 0.75
                    }
                }
            },
            stroke: {
                width: 2
            },
            theme: {
                monochrome: {
                    enabled: true,
                    color: '#93C5FD',
                    shadeIntensity: 0.75,
                    shadeTo: 'dark'
                }
            },
            tooltip: {
                followCursor: true,
                theme: 'dark'
            },
            yaxis: {
                labels: {
                    style: {
                        colors: 'var(--helpers-text-secondary)'
                    }
                }
            }
        };
    }
}
