import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';
import { GlobalConstants } from 'app/shared/global-constants';
import { Animations } from 'helpers/animations';
import { AlertType } from 'helpers/components/alert';
import { environment as env } from 'environments/environment';
import { Subject } from 'rxjs';
import { ProfileService } from 'app/main/my-profile/profile.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations   : Animations
})
export class LoginComponent implements OnInit ,OnDestroy{
    @ViewChild('logInNgForm') logInNgForm: NgForm;
    private _unsubscribAll:Subject<any>=new Subject<any>();
    alert: { type: AlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    logInForm: UntypedFormGroup;
    showAlert: boolean = false;
    saving: boolean = false;
    url = env.fileUrl;
    user: any = {
        id: null,
        name: null,
        email: null,
        avatar: null,
        phone: null,
        type_id: null
    };

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private _router: Router,
        private _profileService:ProfileService,
    )
    {
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.logInForm = this._formBuilder.group({
            phone       : ['', [Validators.required, Validators.pattern(GlobalConstants.contactNumberRegex)]],
            password    : ['', Validators.required],
            rememberMe  : ['']
        });
    }
    ngOnDestroy(): void {
      this._unsubscribAll.next(null);
      this._unsubscribAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Login
     */
    Login(): void
    {
        // Return if the form is invalid
        if ( this.logInForm.invalid)
        {
            return;
        }

        // Disable the form
        this.logInForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Saving
        this.saving = true;

        // Sign in
        this._authService.login(this.logInForm.value).subscribe(
                (res: any) => {
                    if(res.user){
                        this.saving = false;
                        this.user.id = res.user.id;
                        this.user.email = res.user.email;
                        this.user.name = res.user.name;
                        this.user.avatar = res.user.avatar;
                        if (res.user.avatar == '') {
                            this.user.avatar = 'assets/images/avatars/default.jpg';
                        } else {
                            this.user.avatar = this.url + res.user.avatar;
                        }
                        this.user.phone = res.user.phone;
                        this.user.type_id = res.user.type_id;
                        localStorage.setItem('user',JSON.stringify(this.user));
                        this._profileService.userProfile=this.user;
                    }
                    localStorage.setItem('access',res?.user?.type?.name);
                    window.location.reload();
                },
                () => {

                    // Re-enable the form
                    this.logInForm.enable();

                    // saved
                    this.saving = false;

                    // Reset the form
                    this.logInNgForm.resetForm();

                    // Set the alert
                    this.alert = {
                        type   : 'error',
                        message: 'Wrong phone or password'
                    };

                    // Show the alert
                    this.showAlert = true;

                }
            );
    }
}
