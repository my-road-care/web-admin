import { ChangeDetectionStrategy, Component, EventEmitter, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { NewService } from '../new.service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { ResponseCreate } from '../new.types';
import { GlobalConstants } from 'app/shared/global-constants';
import { HttpErrorResponse } from '@angular/common/http';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class CreateComponent implements OnInit {
    public CreateNew = new EventEmitter();
    public newForm: UntypedFormGroup;
    public saving: boolean = false;
    public isLoading: boolean = false;
    public maxDate = new Date();
    constructor(
        private dialogRef: MatDialogRef<CreateComponent>,
        private _formBuilder: UntypedFormBuilder,
        private newService: NewService,
        private snackbarService: SnackbarService,
    ) { }

    ngOnInit(): void {
        this.formBuilder();
    }
    private formBuilder(): void {
        this.newForm = this._formBuilder.group({
            title: [null, Validators.required],
            posted_at: [null, Validators.required,],
            content: [null, Validators.required],
        });
    }
    submit() {
        this.newForm.get('posted_at').setValue(moment(this.newForm.get('posted_at').value).format('YYYY-MM-DD'));
        this.saving = true;
        this.newForm.disable();
        this.newService.create(this.newForm.value).subscribe({
            next: (response: ResponseCreate) => {
                this.saving = false;
                this.CreateNew.emit(response.data)
                this.dialogRef.close();
                this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
            },
            error: (err: HttpErrorResponse) => {
                this.saving = false;
                this.newForm.enable();
                const errors: { field: string, message: string }[] = err.error.errors;
                var message: string = err.error.message;
                if (errors && errors.length > 0) {
                    message = errors.map((obj) => obj.message).join(', ')
                }
                this.snackbarService.openSnackBar(message, GlobalConstants.error);
            }
        });
    }
}
