import { Component, OnInit } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { CreateComponent } from '../create/create.component';
import { NewService } from '../new.service';
import { UpdateComponent } from '../update/update.component';
import * as _moment from 'moment';
import { Data, ResponseRead } from '../new.types';
import { HttpErrorResponse } from '@angular/common/http';
import { GlobalConstants } from 'app/shared/global-constants';
import { PageEvent } from '@angular/material/paginator';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD',
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};

@Component({
    selector: 'app-listing',
    templateUrl: './listing.component.html',
    styleUrls: ['./listing.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class ListingComponent implements OnInit {
    public displayedColumns: string[] = ['no', 'tille', 'date', 'action'];
    public dataSource: MatTableDataSource<Data>;
    public loading: boolean = false;
    public selectedAcademic: number;
    public data: any[] = [];
    public total: number = 10;
    public limit: number = 10;
    public page: number = 1;
    public halsServicce: any;
    public key: string = '';
    public maxDate = new Date();
    public from: any;
    public to: any;
    constructor(
        private newService: NewService,
        private matDialog: MatDialog,
        private snackbarService: SnackbarService
    ) { }

    ngOnInit(): void {
        this.listing(this.limit, this.page);
    }

    public listing(_limit: number = 10, _page: number = 1): void {
        const param: { limit: number, page: number, key?: string | null, from?: string | null, to?: string | null } = {
            limit: _limit,
            page: _page,
        };
        if (this.key !== '') {
            param.key = this.key.trim();
        }
        if (this.from != null) {
            param.from = moment(this.from).format('YYYY-MM-DD');
        }
        if (this.to != null) {
            param.to = moment(this.to).format('YYYY-MM-DD');
        }
        this.loading = true;
        this.newService.listing(param).subscribe({
            next: (response: ResponseRead) => {
                this.loading = false;
                this.data = response.data;
                this.page = response.current_page;
                this.limit = response.per_page;
                this.total = response.total;
                this.dataSource = new MatTableDataSource(this.data);
            },
            error: (err: HttpErrorResponse) => {
                this.loading = false;
                const error: { httpStatus: 400, message: string } = err.error;
                this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
            }
        });
    }

    public onPageChanged(event: PageEvent): void {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }
    }

    public delete(row: Data): void {
        const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((res: boolean) => {
            if (res) {
                this.newService.delete(row.id).subscribe({
                    next: (response: { status: number, message: string }) => {
                        this.data = this.data.filter((v: Data) => v.id != row.id);
                        this.total -= 1;
                        this.dataSource = new MatTableDataSource(this.data);
                        this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
                    },
                    error: (err: HttpErrorResponse) => {
                        const error: { httpStatus: 400, message: string } = err.error;
                        this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
                    }
                });
            }
        });
    }

    public create(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = false;
        dialogConfig.disableClose = true;
        dialogConfig.width = "550px";
        const dialogRef = this.matDialog.open(CreateComponent, dialogConfig);
        dialogRef.componentInstance.CreateNew.subscribe((response: Data) => {
            this.data.unshift(response);
            this.total += 1;
            this.dataSource = new MatTableDataSource(this.data);
        });
    }

    public update(row: Data): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = row;
        dialogConfig.width = "550px";
        const dialogRef = this.matDialog.open(UpdateComponent, dialogConfig);
        dialogRef.componentInstance.UpdateNew.subscribe((response: Data) => {
            this.data = this.data.map((obj: Data) => {
                if (obj.id === response.id) return response;
                return obj;
            })
            this.dataSource = new MatTableDataSource(this.data);
        });
    }
}
