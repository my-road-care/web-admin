export interface ResponseRead {
    current_page: number,
    data: Data[],
    from: number,
    per_page: number,
    total: number
}

export interface RequestCreate {
    title: string,
    posted_at: Data,
    content?: string | null
}

export interface ResponseCreate {
    status: number | 200,
    data: Data,
    message: string
}

export interface RequestUpdate {
    id: number
    title: string,
    posted_at: Data,
    content?: string | null
}

export interface ResponseUpdate {
    status: number | 200,
    data: Data,
    message: string
}

export interface Data {
    id: number,
    title: string,
    posted_at: Data,
    content?: string | null,
    create_at: Data
}