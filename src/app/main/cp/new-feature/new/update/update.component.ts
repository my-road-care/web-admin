import { ChangeDetectionStrategy, Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { NewService } from '../new.service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { Data, ResponseCreate, ResponseUpdate } from '../new.types';
import { GlobalConstants } from 'app/shared/global-constants';
import { HttpErrorResponse } from '@angular/common/http';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};

@Component({
    selector: 'app-update',
    templateUrl: './update.component.html',
    styleUrls: ['./update.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class UpdateComponent implements OnInit {
    public UpdateNew = new EventEmitter();
    public newForm: UntypedFormGroup;
    public saving: boolean = false;
    public isLoading: boolean = false;
    public maxDate = new Date();
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: Data,
        private dialogRef: MatDialogRef<UpdateComponent>,
        private _formBuilder: UntypedFormBuilder,
        private newService: NewService,
        private snackbarService: SnackbarService,
    ) { }

    ngOnInit(): void {
        this.formBuilder();
    }
    private formBuilder(): void {
        this.newForm = this._formBuilder.group({
            title: [this.data.title, Validators.required],
            posted_at: [this.data.posted_at, Validators.required,],
            content: [this.data.content, Validators.required],
        });
    }
    submit() {
        this.newForm.get('posted_at').setValue(moment(this.newForm.get('posted_at').value).format('YYYY-MM-DD'));
        this.saving = true;
        this.newForm.disable();
        this.newService.update(this.data.id, this.newForm.value).subscribe({
            next: (response: ResponseUpdate) => {
                this.saving = false;
                this.UpdateNew.emit(response.data)
                this.dialogRef.close();
                this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
            },
            error: (err: HttpErrorResponse) => {
                this.saving = false;
                this.newForm.enable();
                const errors: { field: string, message: string }[] = err.error.errors;
                var message: string = err.error.message;
                if (errors && errors.length > 0) {
                    message = errors.map((obj) => obj.message).join(', ')
                }
                this.snackbarService.openSnackBar(message, GlobalConstants.error);
            }
        });
    }
}
