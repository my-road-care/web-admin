import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable } from 'rxjs';
import { Data, RequestCreate, RequestUpdate, ResponseCreate, ResponseRead, ResponseUpdate } from './new.types';

@Injectable({
    providedIn: 'root'
})
export class NewService {
    private readonly url = env.apiUrl;
    private readonly httpOptions = {
        headers: new HttpHeaders({ 'Content-type': 'application/json' })
    };
    constructor(private http: HttpClient) { }
    public listing(params = {}): Observable<ResponseRead> {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get<ResponseRead>(this.url + '/cp/what-news', httpOptions);
    }
    public create(data: RequestCreate): Observable<ResponseCreate> {
        return this.http.post<ResponseCreate>(this.url + '/cp/what-news', data, this.httpOptions);
    }
    public update(id: number = 0, data: RequestUpdate): Observable<ResponseUpdate> {
        return this.http.post<ResponseUpdate>(this.url + '/cp/what-news/' + id + '?_method=PUT', data, this.httpOptions);
    }
    public delete(id: number = 0): Observable<{ status: number, message: string }> {
        return this.http.delete<{ status: number, message: string }>(this.url + '/cp/what-news/' + id, this.httpOptions);
    }
}