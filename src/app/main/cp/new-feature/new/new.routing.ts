import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const routes: Route[] = [{
    path: 'news', component: ListingComponent
}];