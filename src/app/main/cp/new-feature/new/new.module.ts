import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { routes } from './new.routing';
import { UpdateComponent } from './update/update.component';
import { CreateComponent } from './create/create.component';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
    ],
    declarations: [
        ListingComponent,
        UpdateComponent,
        CreateComponent
    ],
})
export class NewModule { }
