import { NgModule } from '@angular/core';
import { NewModule } from './new/new.module';
import { ReferFriendModule } from './refer-friend/refer-friend.module';

@NgModule({
    exports: [
        NewModule,
        ReferFriendModule
    ]
})
export class NewFeatureModule { }
