import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { referFriendRoutes } from './refer-friend.routing';
import { ListviewComponent } from './listview.component';

@NgModule({
    imports: [
        RouterModule.forChild(referFriendRoutes),
        SharedModule,
    ],
    declarations: [
        ListviewComponent
    ],
})
export class ReferFriendModule {}
