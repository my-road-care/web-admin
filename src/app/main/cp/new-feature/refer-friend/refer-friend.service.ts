import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ReferFriendService {

    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }

    //==============================================>> Listing
    listing(): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/setting/invite-friend', httpOptions);
    }
    viewUpdate (id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/setting/invite-friend/'+ id+ '?_method=PUT', data, this.httpOptions);
    }
}