import { Route } from "@angular/router";
import { ListviewComponent } from "./listview.component";

export const referFriendRoutes: Route[] = [
    { path: 'refer_friends',component: ListviewComponent }
];