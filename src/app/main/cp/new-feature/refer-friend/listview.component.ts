import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ReferFriendService } from './refer-friend.service';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { MatTableDataSource } from '@angular/material/table';
import { environment as env } from 'environments/environment';

@Component({
  selector: 'app-listview',
  templateUrl: './listview.component.html',
  styleUrls: ['./listview.component.scss']
})
export class ListviewComponent implements OnInit{

  public fileUrl: any = env.fileUrl;
  public saving: boolean = false;
  public data :any;
  public form: UntypedFormGroup;
  public src: string = null;
  public show : any;
  public key:string = "";
  public dataSource: MatTableDataSource<unknown>;
  public isSearching : boolean = false;
  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _service : ReferFriendService,
    private snacBar: SnackbarService,
    private _dialog: MatDialog,
    private _snackBar: SnackbarService,
  ){}

  ngOnInit(): void {

    this.formBuilder();
    this.listview();
  }
  getBase64FromUrl = async (url) => {
    const data = await fetch(url,{ mode: 'no-cors'});
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64data = reader.result;
        resolve(base64data);
      }
    });
  }

  listview():any {

    const param: any = {
     
    };
    this.isSearching = true;
     this._service.listing().subscribe(
      (res:any) =>{
      this.isSearching = false;
      this.data = res.data[0];
      this.src = this.fileUrl + this.data?.image
      console.log(this.data);
      this.formBuilder();
      },
      (err :any) => {
        this._snackBar.openSnackBar('Something went wrong.','error');
      }
     )
  }
  public size: number = 0;
  public type: string = '';
  public error_size: string = '';
  public error_type: string = '';

  fileChangeEvent(e: any): void {
    if (e?.target?.files) {
      this.error_size = '';
      this.error_type = '';
      this.show = false;
      this.saving = false;
      this.type = e?.target?.files[0]?.type;
      this.size = e?.target?.files[0]?.size;
      var reader = new FileReader();
      reader.readAsDataURL(e?.target?.files[0]);
      reader.onload = (event: any) => {
        this.src = event?.target?.result;
        this.form.get('image').setValue(this.src);
      }
      if (this.size > 3145728) { //3mb
        this.saving = true;
        this.form.get('image').setValue('');
        this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
      }
      if (this.type.substring(0, 5) !== 'image') {
        this.saving = true;
        this.src = '';
        this.show = true;
        this.error_size = '';
        this.form.get('image').setValue(this.src);
        this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      }
    } else {
      this.saving = true;
      this.src = '';
      this.show = true;
      this.error_size = '';
      this.form.get('image').setValue(this.src);
      this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      //it work when user cancel select file
    }
  }

  selectFile(): any {
    document.getElementById('portrait-file').click();
  }

  preview(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      src: this.src,
      title: 'រូបភាព'
    };
    dialogConfig.width = "850px";
    this._dialog.open(PreviewImageComponent, dialogConfig);
  }

  formBuilder(): void {

    this.form = this._formBuilder.group({

      image : [this.data?.image],
      title: [this.data?.title, Validators.required],
      url :[ this.data?.url , Validators.required],
      content: [this.data?.content ],
      ios_url: [this.data?.ios_url, Validators.required],
      android_url: [this.data?.android_url, Validators.required],
      youtube_url: [this.data?.youtube_url, Validators.required],

    });
  }

  submit(): void {
    this.saving = true;
    this.form.disable();
    let data: object;
    if (this.form.value.image.substring(0, 10) === 'data:image') {
      data = {
        image: this.form.value.image,
        title: this.form.value.title,
        url: this.form.value.url,
        content: this.form.value.content,
        ios_url: this.form.value.ios_url,
        android_url: this.form.value.android_url,
        youtube_url: this.form.value.youtube_url,
      }
    } else {
      data = {
        image: this.form.value.image,
        title: this.form.value.title,
        url: this.form.value.url,
        content: this.form.value.content,
        ios_url: this.form.value.ios_url,
        android_url: this.form.value.android_url,
        youtube_url: this.form.value.youtube_url,
      }
    }

    this._service.viewUpdate(this.data.id, this.form.value).subscribe(
      (res: any) => {
        this.saving = false;
        this.snacBar.openSnackBar(res.message, '');
        this.form.enable();
      },
      () => {
        this.form.enable(); 
        this.saving = false;
        this.snacBar.openSnackBar('Something went wrong!', 'error');
      }
    );
  }

}

