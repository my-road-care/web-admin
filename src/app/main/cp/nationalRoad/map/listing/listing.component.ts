import { MouseEvent } from '@agm/core';
import { GoogleMap, LatLngBounds } from '@agm/core/services/google-maps-types';
import { Polyline } from '@agm/core/services/google-maps-types';
import { Component, OnInit } from '@angular/core';
import svg from '../../../../supervision/dashboard/components/map/marker.json';

import styles from './map-styles/grey.json'
import { MatDialog } from '@angular/material/dialog';
import { RoadAllService } from '../../all/all.service';
import { LoadingDialogComponent } from 'app/shared/loading-dialog/loading-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { CacheService } from 'app/shared/services/cache.service';
import { ICacheResponse } from 'app/shared/interfaces/cache-service.interfaces';

@Component({
  selector: 'app-dashboard',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  // Custom polylines
  public road_polylines: any[] = [];
  loading: boolean = false;
  routeMarkers: marker[] = []
  styles = styles
  mainRoads: any = []
  map: GoogleMap
  roadName: string = null;
  roadLength: number | string = null;
  roadStart: string = null;
  roadEnd: string = null;
  station: string = '';
  constructor(
    private _service: RoadAllService,
    private matDialog: MatDialog,
    private _snackBar: SnackbarService,
    private cacheService: CacheService
  ) { }
  openDialog: any;
  ngOnInit(): void {
    window['abc'] = this
  }

  viewRoads(): any {
    this.openDialog = this.showLoading();
    this.loading = true;
    this.cacheService.get('main_roads_map', this._service.listingMap()).toPromise()
      .then((res: ICacheResponse) => {
        this.mainRoads = res.data?.data || []
        if (this.mainRoads[0]) {
          this.viewPKs(this.mainRoads[0]);
        }
      })
      .catch(() => this._snackBar.openSnackBar('Something went wrong.', 'error'))
  }

  /*
  selectMulti(id: number | string) {

    this.mainRoad.forEach(m => {
      m.subRoad.forEach(s => {
        if (s.id == id) {

          if (s.check == 0) {
            s.check = 1;

            this.viewPKs(id);
          } else {
            s.check = 0;
            let roaddelete = this.allRoad.filter(r => r.road_id == id)
            this.allRoad = this.allRoad.filter(r => r.road_id != id)
            const dialog = this.matDialog.open(LoadingDialogComponent, {
              data: {
                message: 'សូមមេត្តារង់ចាំ...'
              }
            })
            setTimeout(() => {
              this.setPoliline(roaddelete)
              dialog.close()
            }, 500)

          }
        }
      })
    })
  }
  */
  viewPKs(road: any) {
    if (road.name === this.roadName) return
    setTimeout(() => {
      let dialog: any = '';
      if (this.openDialog._state !== 0) {
        dialog = this.matDialog.open(LoadingDialogComponent, {
          data: {
            message: 'សូមមេត្តារង់ចាំ...'
          }
        })
      }

      setTimeout(() => {
        this.clearMap()

        this.loading = true;
        this.station = road.stations.join(', ');
        this.roadName = road.name;
        this.roadEnd = road.end_point;
        this.roadStart = road.start_point;
        this.roadLength = (road.length / 1000).toFixed(1);

        const cache_key = `road_pks_${road.id}_map`

        this.loading = false;
        this.cacheService.get(cache_key, this._service.viewPkOnMap(road.id, { limit: 449, with_potholes: 0 }))
          .toPromise().then((res: ICacheResponse) => {
            this.setPoliline(res.data);
            setTimeout(() => {
              this.road_polylines.forEach(rp => {
                if (rp.showing) {
                  this.routeMarkers = rp.renderMarkerFns.map(fn => fn()).flat()
                }
              })
            }, 800)

            if (this.openDialog._state !== 0) {
              dialog.close();
            } else {
              this.openDialog.close();
            }

            this.loading = false;

          }).catch(() => {
            if (this.openDialog._state !== 0) {
              dialog.close();
            } else {
              this.openDialog.close();
            }

            this.loading = false;
            this._snackBar.openSnackBar('Something went wrong.', 'error');
          })
      }, 20)
    }, 500)
  }

  clearMap() {
    delete this.routeMarkers
    this.routeMarkers = []
    this.road_polylines.forEach(road_polyline => {
      if (road_polyline.showing) {
        road_polyline.showing = false
        road_polyline.lines.forEach(l => l.setMap(null))
      }
    })
  }

  calculator(markers, numStyles, count) {

    return {
      text: '',
      index: 2
      // title: count
    };
  }

  setPoliline(res) {
    const pkPaths = res.road_pks
    const origin = res.origin
    const destination = res.destination
    const isMainRoad = this.roadName.length === 1
    const road_color = isMainRoad ? 'green' : 'blue'

    const bounds = new google.maps.LatLngBounds() as LatLngBounds
    bounds.extend(origin)
    bounds.extend(destination)
    this.map.fitBounds(bounds)
    this.map.fitBounds(bounds)

    const renderMarkerFns = []

    const road_polyline = this.road_polylines.find(rp => rp.road_name === this.roadName)
    if (road_polyline) {
      road_polyline.showing = true
      road_polyline.lines.forEach(l => l.setMap(this.map))
    } else {
      const pk_polylines = pkPaths.map(({ code, points }) => {
        /*
        setTimeout(() => {
          points.forEach(({ latlng, meter }) => {
            const info = ''
            const color = info ? 'red' : 'black'
            const icon: google.maps.Symbol = {
              path: 0,
              scale: 4,
              fillColor: color,
              strokeColor: color,
              labelOrigin: new google.maps.Point(11, 1),
              anchor: new google.maps.Point(0, 0)
            }
            const iconSmall: google.maps.Symbol = {
              path: 0,
              scale: 1,
              fillColor: color,
              strokeColor: color,
              labelOrigin: new google.maps.Point(11, 1),
              anchor: new google.maps.Point(0, 0)
            }
            let label = '';
            if (code == 0 && meter == points[0].meter) {
  
              const infos = { code, meter }
              label = `${code}+${meter}`;
              this.routeMarkers.push({
                ...latlng, label: label, icon, info
              })
            } else if (code == pkPaths[pkPaths.length - 1].code && meter == points[points.length - 1].meter) {
              label = `${code}+${meter}`;
  
              this.routeMarkers.push({
                ...latlng, label: label, icon, info: info
              })
            } else {
              // label = `${code}+${meter}`;
              this.routeMarkers.push({
                ...latlng, label: label, icon: iconSmall, info: info
              })
            }
          })
        }, s += 8)
        */
        renderMarkerFns.push(() => {
          return points.map(({ latlng, meter }) => {
            const info = ''
            const color = info ? 'red' : 'black'
            const icon: google.maps.Symbol = {
              path: 0,
              scale: 4,
              fillColor: color,
              strokeColor: color,
              labelOrigin: new google.maps.Point(11, 1),
              anchor: new google.maps.Point(0, 0)
            }
            const iconSmall: google.maps.Symbol = {
              path: 0,
              scale: 1,
              fillColor: color,
              strokeColor: color,
              labelOrigin: new google.maps.Point(11, 1),
              anchor: new google.maps.Point(0, 0)
            }
            let label = '';
            if (code == 0 && meter == points[0].meter) {

              const infos = { code, meter }
              label = `${code}+${meter}`;
              return { ...latlng, label: label, icon, info }
            } else if (code == pkPaths[pkPaths.length - 1].code && meter == points[points.length - 1].meter) {
              label = `${code}+${meter}`;

              return {
                ...latlng, label: label, icon, info: info
              }
            } else {
              // label = `${code}+${meter}`;
              return {
                ...latlng, label: label, icon: iconSmall, info: info
              }
            }
          })
        })


        const line = new google.maps.Polyline({ strokeColor: road_color, strokeOpacity: .4, strokeWeight: isMainRoad ? 6 : 4 })
        line.setPath(points.map(({ latlng }) => latlng))
        line.setMap(this.map as any)
        return line
      })
      this.road_polylines.push({
        road_name: this.roadName,
        lines: pk_polylines,
        showing: true,
        renderMarkerFns
      })
    }
  }

  /*
    onResponse(direction, event) {
  
      if (typeof (this.openDialog) != 'undefined') {
        console.log(typeof (this.openDialog));
        this.openDialog.close();
        this.openDialog = undefined;
      }
      setTimeout(() => {
        this.restrictMap()
        this.directions = []
      }, 1000)
    }
    */
  mapClicked($event: MouseEvent) {
    // this._snackBar.openSnackBar(l, '');
    // alert('lat:'+ $event.coords.lat+
    //   'lng:'+ $event.coords.lng)
    // console.log('clicked', $event.coords)
    // if (this.markers.length) this.markers.pop()
    // else this.markers.push({
    //   lat: $event.coords.lat,
    //   lng: $event.coords.lng,
    //   draggable: true
    // });
  }
  clickedMarker(label, i) {
    // this._snackBar.openSnackBar(label, '');
  }
  mapReady(map: GoogleMap) {
    this.map = map
    this.restrictMap()
    this.viewRoads()


    window['map'] = map
  }

  restrictMap() {
    this.map.setOptions({
      // center: { lat: 12.71642962743061, lng: 104.92398335602526 },
      zoom: 0,
      restriction: {
        latLngBounds: {
          south: 9.069775255302284,
          west: 98.801119586494,
          north: 14.946719947756558,
          east: 110.0468471255565
        },
        strictBounds: true,

      },
    })
  }
  ///
  showLoading() {
    const dialog = this.matDialog.open(LoadingDialogComponent, {
      data: {
        message: 'សូមមេត្តារង់ចាំ...'
      }
    })
    return dialog
  }
}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  info?: any;
  icon?: any;
  draggable: boolean;
}

interface Direction {
  origin: string | { lat: number, lng: number }
  destination: string | { lat: number, lng: number }
  renderOptions: {
    draggable: Boolean
  }
}
