import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListingComponent } from './listing/listing.component';
import { mapsRoutes } from './map.routing';
import { AgmCoreModule } from '@agm/core'
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'app/shared/material-module';
import { AgmDirectionModule } from 'agm-direction';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

@NgModule({
    imports: [
        RouterModule.forChild(mapsRoutes),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAeOPNbz8UGspqu4TqT2vAA7XFYBClQpnU'
        }),
        AgmDirectionModule,
        AgmJsMarkerClustererModule,
        CommonModule,
        MaterialModule
    ],
    declarations: [
        ListingComponent
    ],
})
export class MapModule { }
