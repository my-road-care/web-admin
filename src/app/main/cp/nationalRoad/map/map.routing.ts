import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const mapsRoutes: Route[] = [{
    path: 'maps',
    children: [
        { path  : '',  component: ListingComponent },
        
    ]
}];