import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class RoadAllService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({
        'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }


    //==================================================================================>> Listing
    listing(params = {}): any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/roads', httpOptions);
    }

    /**
     |--------------------------------------------------------------------------------------- 
     | View Detail 
     |---------------------------------------------------------------------------------------  
     */ 

    // =============================================>> Get User
    view(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/roads/'+ id , httpOptions);
    }
    // ===============================================>> View Detail PKs
    viewPk(view_id: any = '',params = {} ): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/roads/' + view_id + '/pks', httpOptions);
    }
    // ===============================================>> View Detail pks_on_map
    viewPkOnMap(view_id: any = '',params = {} ): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/roads/' + view_id + '/pks_on_map', httpOptions);
    }
    // ===============================================>> View Detail Parts
    viewPart(view_id: any = '', params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/roads/' + view_id + '/parts', httpOptions);
    }
    // =============================================>> Migrate
    migrate(id:any,startDate:any,endDate:any):any{
        return this.http.get(this.url + `/CreateProjectInfo?road=${id}&startDate=${startDate}&endDate=${endDate}`);
    }


    /**
     |--------------------------------------------------------------------------------------- 
     | View Ministry 
     |---------------------------------------------------------------------------------------  
     */

    // ============================================= >> View Authority Minsitry
    viewMinstry (view_id : any = '', params = {}):any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/roads/' + view_id +'/ministries', httpOptions);
    }
    // ============================================= >> Get Ministry Selecte
    getMinistry(){
        return this.http.get(this.url + '/cp/quick-get/ministry');
    }
     // ============================================== >> Create Ministry
    createmini(id:any,data: any = {}): any {
        return this.http.post(this.url + '/cp/roads/' +id+'/ministries', data, this.httpOptions);
    }
    // =============================================>> Service Delete Ministry
    deletemini( id :number = 0 ,min_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/roads/' + id +'/ministries/' + min_id , this.httpOptions);
    }


    /**
     |--------------------------------------------------------------------------------------- 
     | View MO
     |---------------------------------------------------------------------------------------  
     */

    // ============================================ >> View Authurity MO
    viewMO ( view_id : any = '', params = {}): any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/roads/' +view_id+ '/mos' , httpOptions);
    }
    // ========================================= >> Get Mo
    GetMos(id){
        return this.http.get(this.url + '/cp/roads/mos?ministry=' + id);
    }
    // ============================================== >> Create Mo
    createmo(id:any,data: any = {}): any {
        return this.http.post(this.url + '/cp/roads/' +id+'/mos', data, this.httpOptions);
    }
    // =============================================>> Service Delete Mo
    deletemo( id :number = 0 ,mo_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/roads/' + id +'/mos/' + mo_id , this.httpOptions);
    }


    /**
     |--------------------------------------------------------------------------------------- 
     | View MT
     |---------------------------------------------------------------------------------------  
     */

    // ========================================= >> View Authority Mt
    viewMT ( view_id : any = '', params = {}): any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/roads/' +view_id+ '/mts' , httpOptions);
    }
    // ========================================= >> Get Mt
    GetMts(id){
        return this.http.get(this.url + '/cp/roads/mts?mo=' +id);
    }
    // ============================================== >> Create Mt
    createmt(id:any,data: any = {}): any {
        return this.http.post(this.url + '/cp/roads/' +id+ '/mts', data, this.httpOptions);
    }
    // =============================================>> Service Delete Mt
    deletemt( id :number = 0 ,mt_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/roads/' + id + '/mts/' + mt_id , this.httpOptions);
    }


    updateStatus(id :number = 0, data: any): any {
        return this.http.post(this.url + '/cp/roads/update/' +id, data, this.httpOptions);
    }
    listingMap(): any{
        return this.http.get(this.url + '/cp/roads/map', this.httpOptions);
    }

}
