import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'helpers/services/loading';
import { RoadAllService } from '../all.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  public index = 0;
  public isLoading:boolean=false;
  public data:any;
  public view_id: number = 0;
  public Part:any;
  public Pk:any;
  public isMapLoaded: boolean = false

  constructor(
    private _Service: RoadAllService,
    private _route: ActivatedRoute,
  ) {
    this._route.paramMap.subscribe((params: any) => {
      this.view_id = params.get('id');
    });
  }

  ngOnInit(): void {
    
    let id  = this._route.snapshot.params['id'];
    this.isLoading=true;
    this._Service.view(id).subscribe(res =>{
      this.isLoading=false;
      this.data = res;
    })
  }

  tabChanged(event) {
    if(event.tab.textLabel === 'ផែនទី') this.isMapLoaded = true
  }

}
