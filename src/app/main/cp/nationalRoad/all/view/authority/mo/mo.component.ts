import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { RoadAllService } from '../../../all.service';

@Component({
  selector: 'app-mo',
  templateUrl: './mo.component.html',
  styleUrls: ['./mo.component.scss']
})
export class MoComponent implements OnInit {

  @Input () public id :any;
  @Input() public data: any;
  @Output() output = new EventEmitter<any>();
  public displayedColumns: string[] = ['no','mo', 'manager', 'create_at','action' ];
  public dataSource: any;
  public isClosing :boolean = false;
  public isSearching: boolean = false;
  public total : number =10;
  public limit : number =10;
  public page  : number =1;
  public key   : string = '';
  public mo :any;
  public ministry:any;
  public Authority_type_id: string = "MO";

  constructor(
    private _service: RoadAllService,
    private _snackBar: SnackbarService,
    private _dialog: MatDialog,
    private _loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this._service.getMinistry().subscribe(res => {
      this.ministry = res;
    });
    this._service.GetMos(this.id).subscribe(res => {
      this.mo = res;
    });

    this.isSearching = true;
    this.listing(this.limit, this.page);
  }

  ngOnChanges(changes: any) {
    if (changes.data) {
      this.dataSource = new MatTableDataSource(this.data);
      this.total+=1;
    }
  }

  listing(_limit: number = 10, _page: number = 1):any {

    const param: any = {
      limit: _limit,
      page: _page,
    };

    if (this.page != 0) {
      param.page = this.page;
    }
    this.isSearching = true;
    this._service.viewMO(this.id,param).subscribe(
     (res:any) =>{
      this.isSearching = false;
      this.total = res.total;
      this.page = res.current_page;
      this.limit = res.per_page;
      this.data.splice(0, this.data.length);
      res.data.forEach(d => this.data.push(d));
      this.dataSource = new MatTableDataSource(this.data);
     },(err :any) => {
       this.isSearching = false;
       this._snackBar.openSnackBar('Something went wrong.', 'error');
       // console.log(err);
     }
    )
  }

  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

  onDelete(data:any) :void{

    const dialogRef = this._dialog.open(ConfirmDialogComponent,{
    data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if(res){
        this.isClosing = true;
        this._loadingService.show();
        // // console.log(this.data.indexOf(data));
        this._service.deletemo(this.id,data.id).subscribe(
          (res) => {
            this.isClosing = false;
            this._loadingService.hide();
            // // console.log(this.data.indexOf(data));
            this.data.splice(this.data.indexOf(data),1);
            this.total-=1;
            this._snackBar.openSnackBar(res.message,'');
            this.dataSource = new MatTableDataSource(this.data);
            // this.listing();
          },(err: any)=>{
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.','error');
            // console.log(err);
          }
        );
      }
    });
  }

}
