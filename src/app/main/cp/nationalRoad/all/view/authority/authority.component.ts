import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { RoadAllService } from '../../all.service';
import { MinistryDialogComponent } from './ministry-dialog/ministry-dialog.component';
import { MoDialogComponent } from './mo-dialog/mo-dialog.component';
import { MtDialogComponent } from './mt-dialog/mt-dialog.component';

@Component({
  selector: 'app-authority',
  templateUrl: './authority.component.html',
  styleUrls: ['./authority.component.scss']
})
export class authorityComponent implements OnInit {

  @Input () public id: any;
  @Output () outputt = new EventEmitter<any>();
  public Authority_type_id: string = "Ministry";
  public isSearching :boolean = false;
  public key  : string = '';
  public view_id: number = 0;
  public dataSource: any;
  public mo :any;
  public onCreatemi:boolean=false;
  public ministry:any;
  public mt :any;
  public data:any;
  public ministryData: Array<any> = [];
  public moData: Array<any> = [];
  public mtData: Array<any> = []

  constructor(
    private _service: RoadAllService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
  )
  {
    this._route.paramMap.subscribe((params: any) => {
      this.view_id = params.get('id');
    });
   }

  ngOnInit(): void {
    // // console.log(this.view_id);

    this._service.getMinistry().subscribe(res => {
      this.ministry = res;
    });


  }
  checkSelect(event:any){
    this.Authority_type_id=event.value;
  }

  // Create Ministry Dialog
  CreateMini() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id         : this.view_id,
      Ministry   : this.ministry,
      list       : this.ministryData
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(MinistryDialogComponent, dialogConfig);

    dialogRef.componentInstance.CreateMinistry.subscribe((response: any) => {
      if(response.status !== 'error') {
        this.ministryData = [ ...this.ministryData, response.data ];
        this.onCreatemi=true;
      }

    });

  }

  // Create MO Dialog
  CreateMo() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id         : this.view_id,
      Ministry   : this.ministry,
      Mo         : this.mo,
      list       : this.moData
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(MoDialogComponent, dialogConfig);

    dialogRef.componentInstance.CreateMo.subscribe((response: any) => {
      if(response.status !== 'error') {
        this.moData = [ ...this.moData, response.data ]

      }
    });
  }

  // Create MT Dialog
  CreateMt() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id         : this.view_id,
      Ministry   : this.ministry,
      Mo         : this.mo,
      Mt         : this.mt,
      list       : this.mtData
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(MtDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.CreateMt.subscribe((response: any) => {
      if(response.status !== 'error') this.mtData = [ ...this.mtData, response.data ]
    });
  }

}
