
import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, NgForm, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoadAllService } from '../../../all.service';

@Component({
  selector: 'app-mt-dialog',
  templateUrl: './mt-dialog.component.html',
  styleUrls: ['./mt-dialog.component.scss']
})
export class MtDialogComponent implements OnInit {

  @ViewChild ('CreateMtNgForm') CreateMtNgForm: NgForm;
  CreateMt = new EventEmitter();
  public mtForm : UntypedFormGroup;
  public isLoading : boolean = false;
  public saving    : boolean = false;
  public fromPK: string = '';
  public toPK  : string = '';
  public canSubmit: boolean = true;
  public ministry:any;
  public mo:any;
  public mt:any;

  constructor(
    @Inject (MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<MtDialogComponent>,
    private _service : RoadAllService,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
  ) 
  {
    this.ministry = this.data.Ministry
    this.mo       = this.data.Mo
    this.mt       = this.data.mt
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {

    this.formBuilder();

  }

  formBuilder(): void {
    this.mtForm = this._formBuilder.group({
      ministry: ['', Validators.required],
      mo      : ['', Validators.required],
      mt      : ['', Validators.required],
      fromPK  : ['', { validators: [Validators.required, this.validateFromPK(), this.validateExistPK()] } ],
      toPK    : ['', { validators: [ Validators.required, this.validateToPK(), this.validateExistPK() ] } ],
    });
    this.mtForm.valueChanges.subscribe(mtForm => {
      if (mtForm.fromPK) {
        this.mtForm.patchValue({
          fromPK: mtForm.fromPK
        }, {
          emitEvent: false
        })
      }
      if (mtForm.toPK) {
        this.mtForm.patchValue({
          toPK: mtForm.toPK
        }, {
          emitEvent: false
        })
      }
    })
  }
  validateFromPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const form = control.parent
      if(!form)
        return null
      const toPK = form.get('toPK').value
      if(toPK === '' || toPK === null) return null
      const fromPK = Number(control.value)
      return fromPK < Number(toPK) ? null : { greaterThanToPK: true }
    }
  }
  validateToPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const form = control.parent
      if(!form)
        return null
      const fromPK = form.get('fromPK').value
      if(fromPK === '' || fromPK === null) return null
      const toPK = Number(control.value)
      return fromPK >= Number(toPK) ? { lessThanFromPK: true } : null
    }
  }

  validateExistPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const PK = Number(control.value)
      const { list } = this.data
      return list.find(e => e.end_pk === PK || e.start_pk === PK) ? { exist: true } : null
    }
  }

  submit(){
    if(this.mtForm.value){
        this.mtForm.disable();
        this.saving = true;
        this._service.createmt(this.data.id,this.mtForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.CreateMt.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },err =>{
            this.mtForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.mtForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
  ministrySelected(id:any) {
    this._service.GetMos(id).subscribe(res => {
      this.mo = res;
    });
  }

  mosSelected(id:any){
    this._service.GetMts(id).subscribe(res => {
        this.mt = res; 
    });
  }
}
