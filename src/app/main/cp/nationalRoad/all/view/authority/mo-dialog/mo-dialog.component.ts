import { CurrencyPipe } from '@angular/common';
import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, NgForm, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoadAllService } from '../../../all.service';

@Component({
  selector: 'app-mo-dialog',
  templateUrl: './mo-dialog.component.html',
  styleUrls: ['./mo-dialog.component.scss']
})
export class MoDialogComponent implements OnInit {

  @ViewChild ('CreateMoNgForm') CreateMoNgForm: NgForm;
  CreateMo = new EventEmitter();
  public moForm : UntypedFormGroup;
  public isLoading : boolean = false;
  public saving    : boolean = false;
  public fromPK: string = '';
  public toPK  : string = '';
  public canSubmit: boolean = true;
  public ministry:any;
  public mo:any;
  constructor(

    @Inject (MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<MoDialogComponent>,
    private _service : RoadAllService,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
  ) 
  { 
    this.ministry = this.data.Ministry
    this.mo       = this.data.Mo
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.formBuilder();
  }

  formBuilder(): void {
    this.moForm = this._formBuilder.group({
      ministry: ['', Validators.required],
      mo      : ['', Validators.required],
      fromPK  : ['', { validators: [Validators.required, this.validateFromPK(), this.validateExistPK()] } ],
      toPK    : ['', { validators: [ Validators.required, this.validateToPK(), this.validateExistPK() ] } ],
    });
    this.moForm.valueChanges.subscribe(moForm => {
      if (moForm.fromPK) {
        this.moForm.patchValue({
          fromPK: moForm.fromPK
        }, {
          emitEvent: false
        })
      }
      if (moForm.toPK) {
        this.moForm.patchValue({
          toPK: moForm.toPK
        }, {
          emitEvent: false
        })
      }
    })
  }
  validateFromPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const form = control.parent
      if(!form)
        return null
      const toPK = form.get('toPK').value
      if(toPK === '' || toPK === null) return null
      const fromPK = Number(control.value)
      return fromPK < Number(toPK) ? null : { greaterThanToPK: true }
    }
  }
  validateToPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const form = control.parent
      if(!form)
        return null
      const fromPK = form.get('fromPK').value
      if(fromPK === '' || fromPK === null) return null
      const toPK = Number(control.value)
      return fromPK >= Number(toPK) ? { lessThanFromPK: true } : null
    }
  }

  validateExistPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const PK = Number(control.value)
      const { list } = this.data
      return list.find(e => e.end_pk === PK || e.start_pk === PK) ? { exist: true } : null
    }
  }

  submit(){
    if(this.moForm.value){
        this.moForm.disable();
        this.saving = true;
        this._service.createmo(this.data.id,this.moForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.CreateMo.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },(err:any) =>{
            this.moForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.moForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
  
  ministrySelected(id:any) {
    this._service.GetMos(id).subscribe(res => {
      this.mo = res;
      // // console.log(this.mo);
    });
  }

}
