import { CurrencyPipe } from '@angular/common';
import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, NgForm, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoadAllService } from '../../../all.service';

@Component({
  selector: 'app-ministry-dialog',
  templateUrl: './ministry-dialog.component.html',
  styleUrls: ['./ministry-dialog.component.scss']
})
export class MinistryDialogComponent implements OnInit {

  @ViewChild ('CreateMinistryNgForm') CreateMinistryNgForm: NgForm;
  CreateMinistry = new EventEmitter();
  public miniForm : UntypedFormGroup;
  public saving  : boolean = false;
  public isLoading: boolean = false;
  public fromPK: string = '';
  public toPK  : string = '';
  public canSubmit: boolean = true;
  public ministry:any;

  constructor(
    @Inject (MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef <MinistryDialogComponent>,
    private _service : RoadAllService,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
  ) 
  {
    this.ministry = this.data.Ministry
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.formBuilder();
  }

  formBuilder(): void {
    this.miniForm = this._formBuilder.group({
      ministry: ['', Validators.required ],
      fromPK  : ['', { validators: [Validators.required, this.validateFromPK(), this.validateExistPK()] } ],
      toPK    : ['', { validators: [ Validators.required, this.validateToPK(), this.validateExistPK() ] } ],
    });
    this.miniForm.valueChanges.subscribe(miniForm => {
      if (miniForm.fromPK) {
        this.miniForm.patchValue({
          fromPK: miniForm.fromPK
        }, {
          emitEvent: false
        })
      }
      if (miniForm.toPK) {
        this.miniForm.patchValue({
          toPK: miniForm.toPK
        }, {
          emitEvent: false
        })
      }
    })
  }

  validateFromPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const form = control.parent
      if(!form)
        return null
      const toPK = form.get('toPK').value
      if(toPK === '' || toPK === null) return null
      const fromPK = Number(control.value)
      return fromPK < Number(toPK) ? null : { greaterThanToPK: true }
    }
  }

  validateToPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const form = control.parent
      if(!form)
        return null
      const fromPK = form.get('fromPK').value
      if(fromPK === '' || fromPK === null) return null
      const toPK = Number(control.value)
      return fromPK >= Number(toPK) ? { lessThanFromPK: true } : null
    }
  }

  validateExistPK(): ValidatorFn {
    return (control:AbstractControl): ValidationErrors | null => {
      const PK = Number(control.value)
      const { list } = this.data
      return list.find(e => e.end_pk === PK || e.start_pk === PK) ? { exist: true } : null
    }
  }

  submit(){
    if(this.miniForm.value){
        this.miniForm.disable();
        this.saving = true;
        this._service.createmini(this.data.id,this.miniForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.CreateMinistry.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },(err:any) =>{
            this.miniForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.miniForm.get(key);
            control.setErrors({'servererror':true});
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }

}

