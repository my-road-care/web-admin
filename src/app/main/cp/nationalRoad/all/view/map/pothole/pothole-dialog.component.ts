import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'map-pothole-dialog',
    templateUrl: './pothole-dialog.component.html',
    styleUrls: ['./pothole-dialog.component.scss'],
})
export class PotholeDialogComponent implements OnInit {
    constructor(
        @Inject(MAT_DIALOG_DATA) public data
    ) { }

    content: string = ''

    ngOnInit(): void {
        // console.log(this.data)
    }

    get title() {
        return `${this.data.code}+${this.data.meter}`
    }

    get countPothole() {
        return this.data.n_of_potholes
    }
}
