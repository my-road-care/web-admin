import { MouseEvent } from '@agm/core';
import { GoogleMap, Polyline, PolylineOptions } from '@agm/core/services/google-maps-types';
import { Component, Input, OnInit } from '@angular/core';
import { RoadAllService } from '../../all.service';
import styles from './map-styles/grey.json'
import { MatDialog, MatDialogRef } from '@angular/material/dialog'
import { PotholeDialogComponent } from './pothole/pothole-dialog.component';
import { LoadingDialogComponent } from 'app/shared/loading-dialog/loading-dialog.component';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() public data: any;

  // Custom polylines
  public polylines: Array<Polyline> = [];
  routeMarkers: marker[] = []
  markers: marker[] = []
  styles = styles
  map: GoogleMap
  directions: Direction[] = []
  pks: any[] = []


  constructor(
    private _service: RoadAllService,
    private matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    window['abc'] = this
    this.getPKs(this.data.id)
  }

  getPKs(id: number | string) {
    const dialog = this.matDialog.open(LoadingDialogComponent, {
      data: {
        message: 'សូមមេត្តារង់ចាំ...'
      }
    })
    this._service.viewPk(id, { limit: 173, with_potholes: 1 }).subscribe(res => {
      this.pks = res.data
      const pkPaths = this.pks
      const origin = pkPaths[0].points[0].latlng
      const destination = pkPaths[pkPaths.length - 1].points[0].latlng

      this.directions.push({
        origin, destination, renderOptions: { draggable: false }
      })

      const lines = pkPaths.map(({ code, points }) => {
        const line = new google.maps.Polyline({ strokeColor: 'orange', strokeOpacity: .5, strokeWeight: 6 })

        points.forEach(({ latlng, meter, n_of_potholes, potholes }) => {
          const info = potholes.length ? { ...potholes[0], code, meter, n_of_potholes } : null
          const color = info ? 'red' : 'black'
          const icon: google.maps.Symbol = {
            path: 0,
            scale: 3,
            fillColor: color,
            strokeColor: color,
            labelOrigin: new google.maps.Point(11, 1)
          }

          this.routeMarkers.push({
            ...latlng, label: `${code}+${meter}`, icon, info
          })
        })
        line.setPath(points.map(({ latlng }) => latlng))
        line.setMap(this.map as any)
        return line
      })
      dialog.close()
    })
  }

  clickedMarker(label: string, index: number) {
    // console.log(`clicked the marker: ${label || index}`)
    // console.log(this.routeMarkers[index])
  }
  filterParentheses(s: string) {
    return s.includes('(') ? s.substring(0, s.indexOf('(')) : s
  }

  onResponse(event, direction) {
    // console.log(event)
    this.map.fitBounds(event.routes[0].bounds)

    this.directions.splice(this.directions.indexOf(direction), 1)
    // this.renderDirection(event)
  }

  renderDirection(event) {
    // Default style
    const polylineOptions: PolylineOptions = {
      strokeWeight: 6,
      strokeOpacity: .5,
      icons: [
        // {
        //   icon: {
        //     path: 0,
        //     fillColor: '#000'
        //   },
        //   offset: '0%',
        //   repeat: '10%'
        // }
      ]
    };

    // Polylines strokeColor
    const colors = ['#FF0000', '#00FF00', '#0000FF'];

    // Clear exist polylines
    this.polylines.forEach(polyline => polyline.setMap(null));
    const { legs } = event.routes[0];

    const markers = this.routeMarkers
    class PLine extends google.maps.Polyline {
      GetPointAtDistance(metres) {
        // some awkward special cases
        if (metres == 0) return this.getPath().getAt(0);
        if (metres < 0) return null;
        if (this.getPath().getLength() < 2) return null;
        var dist = 0;
        var olddist = 0;
        var i = 0
        for (i = 1; (i < this.getPath().getLength() && dist < metres); i++) {
          olddist = dist;
          dist += google.maps.geometry.spherical.computeDistanceBetween(
            this.getPath().getAt(i),
            this.getPath().getAt(i - 1)
          );
        }

        if (dist < metres) {
          return null;
        }
        var p1 = this.getPath().getAt(i - 2);
        var p2 = this.getPath().getAt(i - 1);
        var m = (metres - olddist) / (dist - olddist);

        return new google.maps.LatLng(p1.lat() + (p2.lat() - p1.lat()) * m, p1.lng() + (p2.lng() - p1.lng()) * m);
      }

      renderPoints(m) {
        let od = 0
        let p, op
        let d = 0
        while (p = this.GetPointAtDistance(od += m)) {
          const { lat, lng } = p.toJSON()
          markers.push({
            lat: lat,
            lng: lng,
            icon: {
              path: 0,
              scale: 4
            },
            info: d += m,
            draggable: false
          })
          op = p
        }
        const { lat, lng } = [...this.getPath().getArray()].pop().toJSON()
        markers.push({
          lat,
          lng,
          icon: {
            path: 0,
            scale: 4
          },
          info: 'x',
          draggable: false
        })
      }
    }

    let points = []

    legs.forEach((leg) => {
      leg.steps.forEach((step, index) => {
        points = [...points, ...step.path]
        const stepPolyline: PLine = new PLine(polylineOptions as any);

        // Custom color
        stepPolyline.setOptions({ strokeColor: colors[index % 3] });
        stepPolyline.setPath(step.path)
        stepPolyline.setMap(this.map as any);
        // stepPolyline.renderPoints(100)
        this.polylines.push(stepPolyline as any);
      });
    });

    // render markers 1/100m
    const pline: PLine = new PLine(polylineOptions as any)
    pline.setPath(points)
    pline.renderPoints(1000)
  }

  mapReady(map: GoogleMap) {
    this.map = map
    // this.restrictMap()

    // this.directions = [
    //   {
    //     origin: this.filterParentheses(this.data.start_point),
    //     destination: this.filterParentheses(this.data.end_point),
    //     renderOptions: {
    //       draggable: false,
    //     }
    //   }
    // ]
    window['map'] = map
  }

  restrictMap() {
    this.map.setOptions({
      // center: { lat: 12.71642962743061, lng: 104.92398335602526 },
      // zoom: 7.8
      restriction: {
        latLngBounds: {
          south: 10.069775255302284,
          west: 101.801119586494,
          north: 14.746719947756558,
          east: 108.0468471255565
        },
        strictBounds: true,
      },
    })
  }

  mapClicked($event: MouseEvent) {
    // console.log('clicked', $event.coords)
    // if (this.markers.length) this.markers.pop()
    // else this.markers.push({
    //   lat: $event.coords.lat,
    //   lng: $event.coords.lng,
    //   draggable: true
    // });
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    // console.log('dragEnd', m, $event);
  }

  calculator(markers, numStyles) {
    return {
      text: '',
      index: 2
      // title: count
    };
  }

  potholeImgClick(data) {
    const dialog = this.matDialog.open(PotholeDialogComponent, {
      panelClass: 'none-margin-padding',
      data
    })
  }

}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  info?: any;
  icon?: any;
  draggable: boolean;
}

interface Direction {
  origin: string | { lat: number, lng: number }
  destination: string | { lat: number, lng: number }
  renderOptions: {
    draggable: Boolean
  }
}