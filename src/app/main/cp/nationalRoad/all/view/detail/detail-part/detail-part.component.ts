import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoadAllService } from '../../../all.service';

@Component({
  selector: 'app-detail-part',
  templateUrl: './detail-part.component.html',
  styleUrls: ['./detail-part.component.scss']
})
export class DetailPartComponent implements OnInit {

  @Input () public id : any;
  public displayedColumns: string[] = ['no', 'object_id', 'type_road', 'Length', 'PK','action'];
  public dataSource : any;
  public Detail_type_id: number = 2;
  public isSearching : boolean =false;
  public total  : number  = 20;
  public limit  : number  = 20;
  public page   : number  = 1;
  public key    : string = '';
  public data   : any;
  public pks: any[] = [];
  constructor(
    private _service: RoadAllService,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {

    this.isSearching=true;
    this.listing(this.limit, this.page);
  }
  listing(_limit: number = 20, _page: number = 1):any {

    const params: any = {
      limit: _limit,
      page: _page,
    };

    if (this.key != '') {
      params.key = this.key.trim();
    }
    
    this.isSearching = true;
    this._service.viewPart(this.id,params).subscribe(
     (res:any) =>{
     this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data   = res.data;
      // console.log(this.data);
      // Add new column for no
      let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        // console.log(this.data);
      // cut , and add ,
      this.data.forEach((v: any) => {
        this.pks.push(v.pks)
      });

      let pk: any[] = [];
      this.pks.forEach((v: any) => {
        if (v.length > 0) {
          let copy: any[] = [];
          v.forEach((array: any) => {
            copy.push(array.pk.code);
            
          });
          pk.push(copy.join(', '));
        }
      });
      let newData: any[] = [];
      this.data.forEach((obj: any, index: number) => {
        obj = {
          ...obj,
          pkss: pk[index]
        }
        newData.push(obj);
      });
      // // console.log(this.data);
      
      this.data = newData;
      // // console.log(this.data);
      
      
     this.dataSource = new MatTableDataSource (this.data);
     },
     (err :any) => {
       // console.log(err);
       this._snackBar.openSnackBar('Something went wrong.', 'error');
     }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
}
