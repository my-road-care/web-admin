import { AbstractType, Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoadAllService } from '../../../all.service';

@Component({
  selector: 'app-detail-pk',
  templateUrl: './detail-pk.component.html',
  styleUrls: ['./detail-pk.component.scss']
})
export class DetailPkComponent implements OnInit {


  @Input () public id : any;
  public displayedColumns: string[] = ['no', 'code', 'point', 'totalPotholes','action'];
  public dataSource : any;
  public Detail_type_id: string = 'PK';
  public isSearching : boolean =false;
  public total  : number  = 20;
  public limit  : number  = 20;
  public page   : number  = 1;
  public key    : string = '';
  public data   : any;

  constructor(
    private _service: RoadAllService,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    this.isSearching=true;
    this.listing(this.limit, this.page);
    
  }
  listing(_limit: number = 20, _page: number = 1):any {

    const params: any = {
      limit: _limit,
      page: _page,
    };

    if (this.key != '') {
      params.key = this.key.trim();
    }
  
    // if (this.page != 0) {
    //   params.page = this.page;
    // }
    console.log(params);
    
    this.isSearching = true;
    this._service.viewPk(this.id,params).subscribe(
     (res:any) =>{
     this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data   = res.data;
      // console.log(this.data);
      // add new column for no
      let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        // console.log(this.data);
     this.dataSource = new MatTableDataSource (this.data);
     },
     (err :any) => {
       // console.log(err);
       this._snackBar.openSnackBar('Something went wrong.', 'error');
     }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

}
