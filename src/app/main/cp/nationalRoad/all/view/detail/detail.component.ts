import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoadAllService } from '../../all.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class detailComponent implements OnInit {

  @Input() public isLoading: boolean = false;
  @Input() public id: number;

  public Detail_type_id: string = "PK";
  public isSearching :boolean = false;
  public key  : string = '';
  public view_id: number = 0;
 
  constructor(
    private _route: ActivatedRoute,
    private _service: RoadAllService

  ) {
    this._route.paramMap.subscribe((params: any) => {
      this.view_id = params.get('id');
    });
  }
  ngOnInit(): void {

  }

  checkSelect(event:any){
  this.Detail_type_id=event.value;
  }

}

