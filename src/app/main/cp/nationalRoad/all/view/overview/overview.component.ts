import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class overviewComponent implements OnInit {

  public form: FormGroup;
  @Input()public  data:any;

  constructor() { }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(){
    this.form        = new FormGroup({
      name           : new FormControl(this.data? this.data?.data?.name            : ' ', []),
      start_point    : new FormControl(this.data? this.data?.data?.start_point     : ' ', []),
      end_point      : new FormControl(this.data? this.data?.data?.end_point       : ' ', []),
      length         : new FormControl(this.data? this.data?.data?.length          : ' ', []),
      provinces      : new FormControl(this.data? this.data?.data?.provinces[0]?.province?.abbre   : ' ', []),
      n_of_pks       : new FormControl(this.data? this.data?.data?.n_of_pks        : ' ', []),
      ministries     : new FormControl(this.data? this.data?.data?.ministries[0]?.ministry?.abbre  : ' ', []),
      n_of_mos       : new FormControl(this.data? this.data?.data?.n_of_mos        : ' ', []),
      n_of_mts       : new FormControl(this.data? this.data?.data?.n_of_mts        : ' ', []),
     
    });
  }
}
