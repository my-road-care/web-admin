import { ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoadAllService } from '../all.service';
@Component({
  selector: 'nationalRoad',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss'],
})

export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id', 'road', 'ministry', 'cross', 'fixer','project', 'PK', 'status', 'action',];

  public tagsEditMode: boolean = false;
  public disableClose: boolean = false;
  public isSearching: boolean = false;
  public data: any[] = [];
  public total: number = 20;
  public limit: number = 20;
  public page: number = 1;
  public provinces: any[] = [];
  public key    : string = '';
  public dataSource: MatTableDataSource<unknown>;

  constructor(
    private _service: RoadAllService,
    private _dialog: MatDialog,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing(this.limit, this.page);

  }

  // ===============================================Function Listing
  listing(_limit: number = 20, _page: number = 1): any {

    const params: any = {
      limit: _limit,
      page: _page,
    };

    if (this.key != '') {
      params.key = this.key.trim();
    }

    this.isSearching = true;
    this._service.listing(params).subscribe(
      (res: any) => {
        this.isSearching = false;
        this.total = res.total;
        this.page = res.current_page;
        this.limit = res.per_page;
        this.data = res.data;
        // add page when next page 
        let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v: any) => {
          v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        
        // console.log(this.data);
        // cut , and add ,
        this.data.forEach((v: any) => {
          this.provinces.push(v.provinces)
        });

        let province: any[] = [];
        this.provinces.forEach ((v: any)=>
        {
          if(v.length > 0) {
            let copy: any [] = [];
              v.forEach((array: any) => {
              copy.push(array.province.abbre);
            });
            province.push(copy.join(', '));
          }
        });
        let newData: any[] = [];
        this.data.forEach((obj:any, index:number) => {
          obj = {
            ...obj,
            provincess : province[index]
          }
          newData.push(obj);
        });
        
        this.data = newData
        // END ,
        this.dataSource = new MatTableDataSource(this.data);
      },
      (err: any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
  //=============================================>> Status
  onChange(status: any, id: any): any {
    const data = {
      status: status == true ? 1 : 0,
    };
    this._service.updateStatus(id, data).subscribe((res: any)=>{
      this._snackBar.openSnackBar(res?.message, '');
    }, (err: any)=> {
      this._snackBar.openSnackBar('Something went wrong.', 'error');
      console.log(err);
    })
  }
}
