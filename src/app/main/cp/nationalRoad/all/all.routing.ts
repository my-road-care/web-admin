import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";
import { DetailPartComponent } from "./view/detail/detail-part/detail-part.component";
import { DetailPkComponent } from "./view/detail/detail-pk/detail-pk.component";
import { ViewComponent } from "./view/view.component";

export const allRoutes: Route[] = [{
    path: 'alls',
    children: [
        { path  : '',  component: ListingComponent },
        { path  : ':id', component: ViewComponent },
    ]
}];