import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { Dialog } from './dialog/Dialong.compnent';
import { ViewComponent } from './view/view.component';
import { allRoutes } from './all.routing';
import { overviewComponent } from './view/overview/overview.component';
import { detailComponent } from './view/detail/detail.component';
import { authorityComponent } from './view/authority/authority.component';
import { DetailPartComponent } from './view/detail/detail-part/detail-part.component';
import { DetailPkComponent } from './view/detail/detail-pk/detail-pk.component';
import { MinistryComponent } from './view/authority/ministry/ministry.component';
import { MoComponent } from './view/authority/mo/mo.component';
import { MtComponent } from './view/authority/mt/mt.component';
import { MinistryDialogComponent } from './view/authority/ministry-dialog/ministry-dialog.component';
import { MoDialogComponent } from './view/authority/mo-dialog/mo-dialog.component';
import { MtDialogComponent } from './view/authority/mt-dialog/mt-dialog.component';

import {MapComponent  } from './view/map/map.component'
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ScrollbarModule } from 'helpers/directives/scrollbar';


@NgModule({
    imports: [
        RouterModule.forChild(allRoutes),
        SharedModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAeOPNbz8UGspqu4TqT2vAA7XFYBClQpnU'
        }),
        AgmDirectionModule,
        AgmJsMarkerClustererModule,
        UiSwitchModule,
        ScrollbarModule
    ],
    declarations: [
        ListingComponent,
        ViewComponent,
        Dialog,
        overviewComponent,
        detailComponent,
        authorityComponent,
        DetailPkComponent,
        DetailPartComponent,
        MinistryComponent,
        MoComponent,
        MtComponent,
        MinistryDialogComponent,
        MoDialogComponent,
        MtDialogComponent,
        MapComponent,
        
    ],
})
export class AllModule {}
