import {NgModule } from '@angular/core';
import { AllModule } from 'app/main/cp/nationalRoad/all/all.module';
import { MapModule } from 'app/main/cp/nationalRoad/map/map.module';

@NgModule({
    imports: [
        AllModule,
        MapModule,
    ],
    exports: [
        AllModule,
        MapModule]
})
export class RoadModule{}
