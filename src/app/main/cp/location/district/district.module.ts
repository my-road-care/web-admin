import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { districtsRoutes } from './district.routing';
import { ListingComponent } from './listing/listing.component';
import { ViewComponent } from './view/view.component';
import { OverviewComponent } from './view/overview/overview.component';
import { ViewCommuneComponent } from './view/view-commune/view-commune.component';
import { ViewVillageComponent } from './view/view-village/view-village.component';
@NgModule({
    imports: [
        RouterModule.forChild(districtsRoutes),
        SharedModule
    ],
    declarations: [
        ListingComponent,
        ViewComponent,
        OverviewComponent,
        ViewCommuneComponent,
        ViewVillageComponent
    ],
})
export class DistrictModule {}
