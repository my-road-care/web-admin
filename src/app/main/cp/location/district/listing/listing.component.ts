import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { DistrictService } from '../district.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  
  displayedColumns: string[] = ['id','province','code','name','no_commune','no_village','update_at','action'];
    public dataSource: any;
    public tagsEditMode: boolean = false;
    public disableClose: boolean =false;
    public isSearching: boolean =false;
    public data : any[]   = [];
    public total  : number  = 20;
    public limit  : number  = 20;
    public page   : number  = 1;
    public key    : string = '';
        
  constructor(
    private _service: DistrictService,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing( this.limit,this.page);
  }

  // ===============================================Function Listing
  listing(_limit: number = 20, _page: number = 1): any {
    const param: any = {
      limit: _limit,
      page: _page,
    };

    if (this.key != '') {
      param.key = this.key.trim();
    }
    
    this.isSearching = true;
    this._service.listing(param).subscribe((res: any) => {
          this.isSearching = false;
          this.total  = res.data.total;
          this.page   = res.data.current_page;
          this.limit  = res.data.per_page;
          this.data = res.data.data;
          let copy: any[] = [];
        let j = res?.data?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        // console.log(this.data);
          this.dataSource = new MatTableDataSource(this.data);
          
        },
        (err: any) => {  
          this.isSearching = false;
          this._snackBar.openSnackBar('Something went wrong.', 'error');
          // console.log(err);
        }
    );
  }
  //=======================================>> On Page Changed
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
}