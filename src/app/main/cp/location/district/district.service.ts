import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DistrictService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({
        'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }


    //==================================================================================>> Listing
    listing(params = {}): any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/locations/districts', httpOptions);
    }
    // =============================================================================== >> View District
    viewdistrict(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/locations/districts/'+ id , httpOptions);
    }
    // ============================================= >> Update District
    updateDistrict(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/locations/districts/' +id+ '?_method=PUT', data, this.httpOptions);
    } 
     // ============================================== >> Get View commune
     viewcommune(id: any = '',params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/locations/districts/' + id + '/communes', httpOptions);
    }
     // ============================================== >> Get View commune
     viewvillage(id: any = '',params = {}): any {
        const httpOptions = {};
        httpOptions ['params'] = params;
        return this.http.get(this.url + '/cp/locations/districts/' + id + '/villages', httpOptions);
    }
   

}
