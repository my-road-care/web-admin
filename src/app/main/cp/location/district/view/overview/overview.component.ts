import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { DistrictService } from '../../district.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  @Input () public data :any;
  @Input () public isLoading: any;
  ministryForm: UntypedFormGroup;
  public form: FormGroup;
  constructor(
    private _districtService: DistrictService ,
    private snacBar: SnackbarService,
    private _route: Router,
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(){
    this.form           = new FormGroup({
     
      province          : new FormControl(this.data? this.data?.province?.name : ' ', [ Validators.required]),
      code              : new FormControl(this.data? this.data?.code : ' ', [ Validators.required]),
      name              : new FormControl(this.data? this.data?.name : ' ', [ Validators.required]),
      latlng            : new FormControl(this.data? this.data?.latlng : ' ' ),
      lat               : new FormControl(this.data? this.data?.lat : ' ', [ Validators.required]),
      lng               : new FormControl(this.data? this.data?.lng : ' ', [ Validators.required]),
      
    });
  }
  submit(): void{
  
    if (this.form.valid){
      this.form.disable();
      this.isLoading = true;
      this._districtService.updateDistrict (this.data.id, this.form.value).subscribe (res =>{
        this.isLoading =false;
        this.snacBar.openSnackBar(res.message,'');
        this.form.enable();
        // this._route.navigate(['/cp/location/districts']);

      },( err:any) => {
        this.form.enable();
        this.isLoading=false;
        for(let key in err.error.errors){
        let control = this.form.get(key);
        control.setErrors({'servererror':true});
        control.errors.servererror = err.error.errors[key][0];
        this.snacBar.openSnackBar('Something went wrong!','error');
        }
      }); 
    }
  }

}
