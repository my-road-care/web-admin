import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'helpers/services/loading';
import { DistrictService } from '../district.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
 
  public data :any;
  public Commune : any;
  public Village :any;
  public id :any;
  public isLoading :boolean = false;
  public district_id : boolean = false;


  constructor(
    private _loadingService: LoadingService,
    private _service: DistrictService,
    private _route: ActivatedRoute,
  ) { 
    this._route.paramMap.subscribe((params: any) => {
      this.district_id = params.get('id');
    });
  }

  ngOnInit(): void {
  
    let id  = this._route.snapshot.params['id'];
    this.isLoading = true;
    this._service.viewdistrict(id).subscribe((res:any) =>{
      this.isLoading = false;
      this.data = res.data;
    })
  }
  // getdistrict(): void {
  //   this._loadingService.show();
  //   this.isLoading = true;
  //   this._service.viewdistrict(this.id).subscribe((res: any)=>{
  //     this._loadingService.hide();
  //     this.isLoading    = false;
      
  //   },
  //   (err: any)=>// console.log(err));
  // }
  
}
