import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { DistrictService } from '../../district.service';

@Component({
  selector: 'app-view-commune',
  templateUrl: './view-commune.component.html',
  styleUrls: ['./view-commune.component.scss']
})
export class ViewCommuneComponent implements OnInit {

  @Input() public id :any;
  public displayedColumns: string[] = ['no', 'province', 'district','code','name','total_village','action'];
  public dataSource: any;
  public total  : number  = 20;
  public limit  : number  = 20;
  public page   : number  = 1;
  public key    : string = '';
  public isSearching :boolean = false;
  public data:any;
  constructor(
    private _service : DistrictService,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    this.listing();
  }
  listing(limit: number = 10, page: number = 1):any {

    const params: any = {

      limit: limit,
      page: page,

    };

    if (this.key != '') {
      params.key = this.key.trim();
    }
    if (this.page != 0) {
      params.page = this.page;
    }

    this.isSearching = true;
    this._service.viewcommune(this.id,params).subscribe(
     (res:any) =>{
     this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data = res.data;
      // console.log(this.data);
      
     this.dataSource = new MatTableDataSource (this.data);
     },
     (err :any) => {
       // console.log(err);
       this._snackBar.openSnackBar('Something went wrong.', 'error');
     }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
}
