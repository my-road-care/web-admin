import {NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommuneModule } from 'app/main/cp/location/commune/commune.module';
import { DistrictModule } from 'app/main/cp/location/district/district.module';
import { ProvinceModule } from 'app/main/cp/location/province/province.module';
import { VillageModule } from 'app/main/cp/location/village/village.module';

@NgModule({
    imports: [
        ProvinceModule,
        DistrictModule,
        CommuneModule,
        VillageModule
    ],
    exports: [
        ProvinceModule,
        DistrictModule,
        CommuneModule,
        VillageModule
    ]
})
export class LocationModule{}
