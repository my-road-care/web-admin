import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ProvinceService {

    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }

    //==============================================>> Listing
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/locations/provinces', httpOptions);
    }
    // =============================================================================== >> View District
    viewprovince(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/locations/provinces/'+ id , httpOptions);
    }
     // ============================================= >> Update Province
     updateprovince(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/locations/provinces/' +id+ '?_method=PUT', data, this.httpOptions);
    } 
    // ============================================== >> Get View District
    viewdistrict(id: any = '', params={}): any {
        const httpOptions = {};
        httpOptions['params']=params;
        return this.http.get(this.url + '/cp/locations/provinces/' + id + '/districts', httpOptions);
    }
    // ============================================== >> Get View Commune
    viewcommune(id: any = '', params ={}): any {
        const httpOptions = {};
        httpOptions['params']=params;
        return this.http.get(this.url + '/cp/locations/provinces/' + id + '/communes', httpOptions);
    }
    // ============================================== >> Get View Village
    viewvillage(id: any = '', params ={}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/locations/provinces/' + id + '/villages', httpOptions);
    }
}