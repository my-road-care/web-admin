import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ProvinceService } from '../../province.service';

@Component({
  selector: 'app-view-village',
  templateUrl: './view-village.component.html',
  styleUrls: ['./view-village.component.scss']
})
export class ViewVillageComponent implements OnInit {

  @Input() public id:any;
  public displayedColumns: string[] = ['no', 'province', 'district','commune','code','name','action'];
  public dataSource: any;
  public total  : number  = 20;
  public limit  : number  = 20;
  public page   : number  = 1;
  public key    : string = '';
  public isSearching : boolean = false;
  public data : any;
  constructor(
    private _snackBar: SnackbarService,
    private _service: ProvinceService
  ) { }

  ngOnInit(): void {

    this.isSearching = true;
    this.listing(this.limit, this.page);
    
  }
  listing(_limit: number = 20, _page: number = 1):any {

    const params: any = {
      limit: _limit,
      page: _page,

    };

    if (this.key != '') {
      params.key = this.key.trim();
    }

    if (this.page != 0) {
      params.page = this.page;
    }

    this.isSearching = true;
    this._service.viewvillage(this.id, params).subscribe(
     (res:any) =>{
     this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data = res.data;
     this.dataSource = new MatTableDataSource (this.data);
     },
     (err :any) => {
       // console.log(err);
       this._snackBar.openSnackBar('Something went wrong.', 'error');
     }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
}
