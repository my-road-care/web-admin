import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'helpers/services/loading';
import { ProvinceService } from '../province.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  public isLoading : boolean = false;
  public province_id :any;
  public data : any;
  public district : any;
  public commune : any;
  public village : any;
  public id :any;
  constructor(
    private _loadingService: LoadingService,
    private _service: ProvinceService,
    private _route: ActivatedRoute,
  ) { 
    this._route.paramMap.subscribe((params: any) => {
      this.province_id = params.get('id');
    });
  }

  ngOnInit(): void {

    this.isLoading = true;
    let id  = this._route.snapshot.params['id'];
    this._service.viewprovince(id).subscribe((res:any) =>{
      this.isLoading = false;
      this.data = res;
    })
    
  }
}
