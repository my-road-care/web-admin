import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ProvinceService } from '../../province.service';

@Component({
  selector: 'app-view-commune',
  templateUrl: './view-commune.component.html',
  styleUrls: ['./view-commune.component.scss']
})
export class ViewCommuneComponent implements OnInit {

  @Input() public id:any;
  public displayedColumns: string[] = ['no', 'province','district', 'code', 'name', 'n_of_villages','action'];
  public dataSource: any;
  public isLoading :boolean = false;
  public isSearching: boolean = false;
  public total  : number  = 20;
  public limit  : number  = 20;
  public page   : number  = 1;
  public key    : string = '';
  public data: any;
  constructor(
    private _snackBar: SnackbarService,
    private _service: ProvinceService
  ) { }

  ngOnInit(): void {

    this.isSearching = true;
    this.listing(this.limit, this.page);
    
  }
  listing(_limit: number = 20, _page: number = 1):any {

    const params: any = {
      limit: _limit,
      page: _page,

    };

    if (this.key != '') {
      params.key = this.key.trim();
    }

    if (this.page != 0) {
      params.page = this.page;
    }
    
    this.isSearching = true;
    this._service.viewcommune(this.id,params).subscribe(
     (res:any) =>{
     this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data = res.data;
     this.dataSource = new MatTableDataSource (this.data);
     },
     (err :any) => {
       // console.log(err);
       this._snackBar.openSnackBar('Something went wrong.', 'error');
     }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

}
