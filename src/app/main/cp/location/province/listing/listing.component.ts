import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ProvinceService } from '../province.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  
  displayedColumns: string[] = ['no','code','name','report','action'];

  public isSearching :boolean =false;
  public data : any[]   = [];
  public dataSource: MatTableDataSource<unknown>;
  public key    : string = '';
  
  constructor(
    private _service: ProvinceService,
    private _dialog: MatDialog,
    private _snackBar: SnackbarService, 
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing( );
  }
  listing(_limit: number = 20, _page: number = 1):any {

    const param: any = {
      limit: _limit,
      page: _page,
    };

    if (this.key != '') {
      param.key = this.key.trim();
    }

    this.isSearching = true;
     this._service.listing(param).subscribe(
      (res:any) =>{
      this.isSearching = false;
      this.data = res;
      this.dataSource = new MatTableDataSource (this.data);
      // // console.log(this.dataSource);
      },
      (err :any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
     )
  }
}
