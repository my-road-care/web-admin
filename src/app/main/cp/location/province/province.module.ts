import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { provinceRoutes } from './province.routing';
import { ViewComponent } from './view/view.component';
import { OverviewComponent } from './view/overview/overview.component';
import { ViewDistrictComponent } from './view/view-district/view-district.component';
import { ViewCommuneComponent } from './view/view-commune/view-commune.component';
import { ViewVillageComponent } from './view/view-village/view-village.component';

@NgModule({
    imports: [
        RouterModule.forChild(provinceRoutes),
        SharedModule
    ],
    declarations: [
        ListingComponent,
        ViewComponent,
        OverviewComponent,
        ViewDistrictComponent,
        ViewCommuneComponent,
        ViewVillageComponent
    ],
})
export class ProvinceModule {}
