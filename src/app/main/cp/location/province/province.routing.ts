import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";
import { ViewComponent } from "./view/view.component";

export const provinceRoutes: Route[] = [{
    path: 'provinces',
    children: [
        { path  : '',  component: ListingComponent },
        { path  : ':id',  component: ViewComponent }
    ]
}];