import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CommuneService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({
        'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }


    //=============================================== >> Listing
    listing(params = {}): any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/locations/communes', httpOptions);
    }
    // ============================================== >> Get View Commune
    viewcommune(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/locations/communes/' + id , httpOptions);
    }
    // ============================================= >> Update Commune
    updateCommune(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/locations/communes/' +id+ '?_method=PUT', data, this.httpOptions);
    } 
     // ============================================== >> Get View Village
     viewvillage(id: any = '', params = {}): any {
        const httpOptions = {};
        httpOptions [ 'params']=params;
        return this.http.get(this.url + '/cp/locations/communes/' + id + '/villages', httpOptions);
    }
     // ============================================= >> Update Village
     update(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/locations/villages/'+id+ '?_method=PUT', data, this.httpOptions);
    }
}
