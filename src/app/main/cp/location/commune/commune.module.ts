import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { communesRoutes } from './commune.routing';
import { ListingComponent } from './listing/listing.component';
import { ViewComponent } from './view/view.component';
import { OverviewComponent } from './view/overview/overview.component';
import { VillageComponent } from './view/village/village.component';
import { CurrencyPipe } from '@angular/common';

@NgModule({
    imports: [
        RouterModule.forChild(communesRoutes),
        SharedModule
    ],
    declarations: [
        ListingComponent,
        ViewComponent,
        OverviewComponent,
        VillageComponent,
    ],
    providers: [
        CurrencyPipe
      ],
      entryComponents: [
    ]
})
export class CommuneModule {}
