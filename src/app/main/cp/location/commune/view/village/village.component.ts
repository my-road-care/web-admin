import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { CommuneService } from '../../commune.service';

@Component({
  selector: 'app-village',
  templateUrl: './village.component.html',
  styleUrls: ['./village.component.scss']
})
export class VillageComponent implements OnInit {

  @Input () public id:any;

  public displayedColumns: string[] = ['no', 'province', 'district', 'commune','code','name','update_at','action'];
  public dataSource: any;
  public total  : number  = 10;
  public limit  : number  = 10;
  public page   : number  = 1;
  public key    : string = '';
  public isSearching :boolean = false;
  public data:any;
  constructor(
    private _service : CommuneService,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.search(this.limit, this.page);
  }
  search(_limit: number = 10, _page: number = 1):any {

    const params: any = {
      limit: _limit,
      page: _page,
    };

    if (this.key != '') {
      params.key = this.key.trim();
    }
    
    if (this.page != 0) {
      params.page = this.page;
    }

    this.isSearching = true;
    this._service.viewvillage(this.id, params).subscribe(
     (res:any) =>{
     this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data = res.data;
     this.dataSource = new MatTableDataSource (this.data);
     },
     (err :any) => {
       // console.log(err);
       this._snackBar.openSnackBar('Something went wrong.', 'error');
     }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.search(this.limit, this.page);
    }
  }
  
}
