import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { CommuneService } from '../../commune.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  @Input () public data:any;
  ministryForm: UntypedFormGroup;
  public form: FormGroup;
  public isLoading : boolean =false;
  constructor(
    private _communeService: CommuneService ,
    private snacBar: SnackbarService,
    private _route: Router,
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(){
    this.form           = new FormGroup({
     
      province          : new FormControl(this.data? this.data?.data?.district?.province?.name : ' ', [ Validators.required]),
      district          : new FormControl(this.data? this.data.data?.district?.name : ' ', [ Validators.required]),
      code              : new FormControl(this.data? this.data?.data?.code : ' ', [ Validators.required]),
      name              : new FormControl(this.data? this.data?.data?.name : ' ', [ Validators.required]),
      latlng            : new FormControl(this.data? this.data?.data?.latlng : ' ' ),
      lat               : new FormControl(this.data? this.data?.data?.lat : ' ', [ Validators.required]),
      lng               : new FormControl(this.data? this.data?.data?.lng : ' ', [ Validators.required]),
      
    });
  }
  submit(): void{
  
    if (this.form.valid){
      this.form.disable();
      this.isLoading = true;
      this._communeService.updateCommune (this.data.data.id, this.form.value).subscribe (res =>{
        this.isLoading =false;
        this.snacBar.openSnackBar(res.message,'');
        // this._route.navigate(['/cp/location/communes']);
        this.form.enable();

      }, (err:any) => {
        this.form.enable();
        this.isLoading=false;
        for(let key in err.error.errors){
        let control = this.form.get(key);
        control.setErrors({'servererror':true});
        control.errors.servererror = err.error.errors[key][0];
        this.snacBar.openSnackBar('Something went wrong!','error');
        }
      }); 
    }
  }
}
