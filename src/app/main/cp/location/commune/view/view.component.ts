
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { LoadingService } from 'helpers/services/loading';
import { CommuneService } from '../commune.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
 
})
export class ViewComponent implements OnInit {

  public id:number = 0; 
  public isLoading:boolean=false;
  public data :any;
  public commune_id :any;

  constructor(
    private _route: ActivatedRoute,
    private _service: CommuneService,
  )
  {
    this._route.paramMap.subscribe((params: any) => {
      this.commune_id = params.get('id');
    });
  }

  ngOnInit(): void {
    let id  = this._route.snapshot.params['id'];
    this.isLoading=true;
    this._service.viewcommune(id).subscribe(res =>{
      this.isLoading=false;
      this.data = res;
    })

  }
  
}
