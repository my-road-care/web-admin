import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";
import { ViewComponent } from "./view/view.component";

export const villageRoutes: Route[] = [{
    path: 'villages',
    children: [
        { path  : '',  component: ListingComponent },
        { path  : ':id', component: ViewComponent }
    ]
}];