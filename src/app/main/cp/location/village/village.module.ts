import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { villageRoutes } from './village.routing';
import { ViewComponent } from './view/view.component';
import { UpdateComponent } from './view/update/update.component';

@NgModule({
    imports: [
        RouterModule.forChild(villageRoutes),
        SharedModule
    ],
    declarations: [
        ListingComponent,
        ViewComponent,
        UpdateComponent
        
    ],
})
export class VillageModule {}
