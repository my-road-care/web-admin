import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { VillageService } from '../village.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id', 'code','village','commune','district','province','action'];

  public isSearching :boolean =false;
  public isLoading : boolean = false;
  public id :any;
  public data : any[]   = [];
  public total  : number  = 20;
  public limit  : number  = 20;
  public page   : number  = 1;
  public dataSource: MatTableDataSource<unknown>;
  public key    : string = '';
  
  constructor(
    private _service: VillageService,
    private _loadingService: LoadingService,
    private _snackBar: SnackbarService,
    private _dialog: MatDialog,
    private _route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing( this.limit, this.page);
  }
  listing( _limit: number = 20, _page: number = 1):any {
    const param: any = {
      limit: _limit,
      page: _page,
    };
    if (this.key != '') {
      param.key = this.key;
    }
    this.isSearching = true;
     this._service.listing(param).subscribe(
      (res:any) =>{
      this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data = res.data;
      // // console.log(this.data);
      let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        // console.log(this.data);
      this.dataSource = new MatTableDataSource (this.data);
      },
      (err :any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
     )
  }
 
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

}
