import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'helpers/services/loading';
import { VillageService } from '../village.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  public data:any;
  public isLoading:any;
  public id :any;
  constructor(
    private _loadingService: LoadingService,
    private _service: VillageService,
    private _route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    let id  = this._route.snapshot.params['id'];
    this.isLoading = true;
    this._service.viewvillage(id).subscribe((res:any) =>{
    this.isLoading = false;
      this.data = res;
      
    })
  }
}
