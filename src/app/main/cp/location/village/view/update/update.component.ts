import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { VillageService } from '../../village.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  @Input() public data:any;
  villageForm: UntypedFormGroup;
  public form: FormGroup;
  public isLoading : boolean = false;
  constructor(
    private _villageService: VillageService ,
    private snacBar: SnackbarService,
    private _route: Router,
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(){
    this.form           = new FormGroup({
     
      province          : new FormControl(this.data? this.data?.data?.commune?.district?.province?.en_name  : ' ', [ Validators.required]),
      district          : new FormControl(this.data? this.data?.data?.commune?.district?.name : ' ', [ Validators.required]),
      commune          : new FormControl(this.data? this.data?.data?.commune?.name : ' ', [ Validators.required]),
      code              : new FormControl(this.data? this.data?.data?.code : ' ', [ Validators.required]),
      name              : new FormControl(this.data? this.data?.data?.name : ' ', [ Validators.required]),
      lat               : new FormControl(this.data? this.data?.data?.lat : ' ', [ Validators.required]),
      lng               : new FormControl(this.data? this.data?.data?.lng : ' ', [ Validators.required]),
      
    });
  }
  submit(): void{
  
    if (this.form.valid){
      this.form.disable();
      this.isLoading = true;
      this._villageService.update (this.data.data.id, this.form.value).subscribe (res =>{
        this.isLoading =false;
        this.snacBar.openSnackBar(res.message,'');
        this._route.navigate(['/cp/location/villages']);

      }, (err:any) => {
        this.form.enable();
        this.isLoading=false;
        for(let key in err.error.errors){
        let control = this.form.get(key);
        control.setErrors({'servererror':true});
        control.errors.servererror = err.error.errors[key][0];
        this.snacBar.openSnackBar('Something went wrong!','error');
        }
      }); 
    }
  }

}
