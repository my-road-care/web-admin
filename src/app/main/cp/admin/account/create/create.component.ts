import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { Validators, UntypedFormGroup, UntypedFormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { AccountService } from "../account.service";
import { MatDialog } from '@angular/material/dialog';
import { GlobalConstants } from "app/shared/global-constants";
import { ResponseCreate, Setup, UserTypeId } from "../account.types";
import { HttpErrorResponse } from "@angular/common/http";
@Component({
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

    public createForm: UntypedFormGroup;
    public setup: Setup;
    public src: string = "";
    public show: boolean = false;
    public saving: boolean = false;
    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';
    public user_type_id: UserTypeId;
    public CreateAccount = new EventEmitter();

    constructor(
        public dialogRef: MatDialogRef<CreateComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { user_type_id: UserTypeId, setup: Setup },
        private accountService: AccountService,
        private _dialog: MatDialog,
        private snackbarService: SnackbarService,
        private _formBuilder: UntypedFormBuilder,
    ) {
        this.dialogRef.disableClose = true;
    }

    public ngOnInit(): void {
        this.user_type_id = this.data.user_type_id as UserTypeId;
        this.setup = this.data.setup;
        this.formBuilder();
    }

    private formBuilder(): void {
        this.createForm = this._formBuilder.group({
            name: [null, [Validators.required, Validators.maxLength(50)]],
            phone: [null, [Validators.required, Validators.pattern(/^(\+855|0)[1-9]\d{7,8}$/)]],
            email: [null, [Validators.required, Validators.pattern(GlobalConstants.emailRegex)]],
            organization_id: [null, Validators.required],
            role_id: [null, Validators.required],
            password: [null, Validators.required],
            image_uri: [null, Validators.required],
            type_id: [this.user_type_id === 8 ? 8 : null, this.user_type_id === 8 ? [] : Validators.required],
        });
    }

    public create(): void {
        this.saving = true;
        this.createForm.disable();
        this.accountService.create(this.createForm.value).subscribe({
            next: (response: ResponseCreate) => {
                this.saving = false;
                this.CreateAccount.emit(true);
                this.dialogRef.close();
                this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
            },
            error: (err: HttpErrorResponse) => {
                this.saving = false;
                this.createForm.enable();
                const errors: { field: string, message: string }[] = err.error.errors;
                var message: string = err.error.message;
                if (errors && errors.length > 0) {
                    message = errors.map((obj) => obj.message).join(', ')
                }
                this.snackbarService.openSnackBar(message, GlobalConstants.error);
            }
        });
    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }

    fileChangeEvent(files: FileList): void {
        if (files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = files[0]?.type;
            this.size = files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.createForm.get('image_uri').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.createForm.get('image_uri').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.createForm.get('image_uri').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.createForm.get('image_uri').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        }
    }

    preview() {
        if (this.src == "") {
            document.getElementById('portrait-file').click();
        }
        else {
            this._dialog.open(PreviewImageComponent, {
                width: '850px',
                data: {
                    title: 'រូបភាពប្រភេទការងារ',
                    src: this.src
                }
            })
        }
    }
}