import { Component, Inject } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { AccountService } from '../account.service';
import { GlobalConstants } from 'app/shared/global-constants';
import { ResponseUpdate } from '../account.types';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-update-password',
    templateUrl: './update-password.component.html'
})
export class UpdatePasswordComponent {
    public updateForm: UntypedFormGroup;
    public saving: boolean = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: { id: number },
        private dialogRef: MatDialogRef<UpdatePasswordComponent>,
        private _formBuilder: UntypedFormBuilder,
        private accountService: AccountService,
        private snackbarService: SnackbarService,
    ) { }

    ngOnInit(): void {
        this.formBuilder();
    }
    formBuilder(): void {
        this.updateForm = this._formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15)]]
        });
    }
    submit() {
        this.saving = true;
        const data: { id: number, password: string } = {
            id: this.data.id,
            ...this.updateForm.value
        }
        this.updateForm.disable();
        this.accountService.updatePassword(data).subscribe({
            next: (response: ResponseUpdate) => {
                this.saving = false;
                this.dialogRef.close();
                this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
            },
            error: (err: HttpErrorResponse) => {
                this.saving = false;
                this.updateForm.enable();
                const errors: { field: string, message: string }[] | undefined = err.error.errors;
                var message: string = err.error.message;
                if (errors && errors.length > 0) {
                    message = errors.map((obj) => obj.message).join(', ')
                }
                this.snackbarService.openSnackBar(message, GlobalConstants.error);
            }
        });
    }
}
