import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable } from 'rxjs';
import { ResponseCreate, RequestCreate, RequestUpdate, ResponseAccount, Setup, ResponseUpdate } from './account.types';

@Injectable({
    providedIn: 'root',
})
export class AccountService {
    private readonly url = env.apiUrl;
    private readonly httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(private readonly http: HttpClient) { }
    public listing(params = {}): Observable<ResponseAccount> {
        const httpOptions = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this.http.get<ResponseAccount>(this.url + '/authority/user', httpOptions);
    }
    public setup(): Observable<Setup> {
        return this.http.get<Setup>(this.url + '/authority/user/setup?slug=authority', this.httpOptions);
    }
    public create(data: RequestCreate): Observable<ResponseCreate> {
        return this.http.post<ResponseCreate>(this.url + '/authority/user', data, this.httpOptions);
    }
    public update(data: RequestUpdate, userId: number): Observable<ResponseUpdate> {
        return this.http.post<ResponseUpdate>(this.url + `/authority/user/update?id=${userId}`, data, this.httpOptions);
    }
    public updatePassword(data: { id: number, password: string }): Observable<ResponseUpdate> {
        return this.http.post<ResponseUpdate>(this.url + `/authority/user/update-password`, data, this.httpOptions);
    }
    public updateStatus(data: { user_id: number, is_active: 1 | 0 }): Observable<{ httpStatus: number, message: string }> {
        return this.http.post<{ httpStatus: number, message: string }>(this.url + `/authority/user/update-status`, data, this.httpOptions);
    }
    public delete(id: number): Observable<{message: string}> {
        return this.http.delete<{message: string}>(this.url + `/authority/user?id=${id}`, this.httpOptions);
    }
}
