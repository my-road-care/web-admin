import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { accountRoutes } from './account.routing';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { ScrollbarModule } from 'helpers/directives/scrollbar';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
    imports: [
        SharedModule,
        ScrollbarModule,
        UiSwitchModule,
        RouterModule.forChild(accountRoutes)
    ],
    declarations: [
        ListingComponent,
        CreateComponent,
        UpdateComponent,
        UpdatePasswordComponent
    ]
})

export class AccountModule{}