import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const accountRoutes: Route[] = [
    { path: 'accounts', component: ListingComponent }
];