import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { AccountService } from '../account.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { environment as env } from 'environments/environment';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { UpdatePasswordComponent } from '../update-password/update-password.component';
import { CreateComponent } from '../create/create.component';
import { UpdateComponent } from '../update/update.component';
import { Data, ResponseAccount, Setup } from '../account.types';
import { PageEvent } from '@angular/material/paginator';
import { GlobalConstants } from 'app/shared/global-constants';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'account-listing',
    templateUrl: './listing.component.html',
    styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
    public isSearching: boolean = false;
    public displayedColumns: string[] = ['id', 'name', 'organization', 'type', 'status', 'action'];
    public setup: Setup;
    public data: Data[] = [];
    public total: number = 10;
    public limit: number = 10;
    public page: number = 1;
    public dataSource: MatTableDataSource<Data>;
    public searchValue: string = "";
    public fileUrl = env.fileUrl;
    public user: any;
    constructor(
        private accountService: AccountService,
        private snackbarService: SnackbarService,
        private matDialog: MatDialog
    ) { }

    public ngOnInit(): void {
        this.user = localStorage.getItem('user');
        if (this.user) {
            this.user = JSON.parse(this.user);
        }
        this.listing(this.limit, this.page);
        this.getSetup();
    }

    private getSetup(): void {
        this.accountService.setup().subscribe((res: Setup) => {
            this.setup = res;
        });
    }

    public create(): void {
        const dialog = this.matDialog.open(CreateComponent, {
            width: '900px',
            data: {
                user_type_id: this.user.type_id,
                setup: this.setup
            },
            autoFocus: false
        });
        dialog.componentInstance.CreateAccount.subscribe((res: boolean) => {
            if (res) this.listing();
        })
    }

    public listing(_limit: number = 10, _page: number = 1): void {
        const params: { limit: number, page: number, key?: string | null } = {
            limit: this.limit,
            page: this.page,
            key: this.searchValue
        };
        this.isSearching = true;
        this.accountService.listing(params).subscribe({
            next: (response: ResponseAccount) => {
                this.isSearching = false;
                this.data = response.data;
                this.page = response.current_page;
                this.limit = response.per_page;
                this.total = response.total;
                let j = response.from;
                this.data = this.data.map((v: Data) => {
                    v.rn = j; j++; return v;
                });
                this.dataSource = new MatTableDataSource(this.data);
            },
            error: (err: HttpErrorResponse) => {
                this.isSearching = false;
                const error: { httpStatus: 400, message: string } = err.error;
                this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
            }
        });
    }

    public onPageChanged(event: PageEvent): void {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }
    }

    public delete(id: number): void {
        const dia = this.matDialog.open(ConfirmDialogComponent, {
            width: '350px',
            data: {
                message: "តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?"
            }
        })
        dia.afterClosed().subscribe((res: boolean) => {
            if (res) {
                this.accountService.delete(id).subscribe({
                    next: (response: { message: string }) => {
                        this.listing(this.limit, this.page);
                        this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
                    },
                    error: (err: HttpErrorResponse) => {
                        const error: { httpStatus: 400, message: string } = err.error;
                        this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
                    }
                });
            }
        })
    }

    public update(row: Data): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            user_type_id: this.user.type_id,
                row: row,
                setup: this.setup
        }
        dialogConfig.autoFocus = false;
        dialogConfig.width = "900px";
        dialogConfig.disableClose = true;
        const dialogRef = this.matDialog.open(UpdateComponent, dialogConfig);
        dialogRef.componentInstance.UpdateAccount.subscribe((res: boolean) => {
            if (res) {
                this.listing(this.limit, this.page)
            }
        });
    }
    public updatePassword(id: number = 0): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            id: id,
        }
        dialogConfig.autoFocus = false;
        dialogConfig.width = "700px";
        dialogConfig.disableClose = true;
        this.matDialog.open(UpdatePasswordComponent, dialogConfig);
    }

    public preview(src: string): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: src,
            title: 'រូបគណនី'
        };
        dialogConfig.width = "850px";
        this.matDialog.open(PreviewImageComponent, dialogConfig);
    }

    //=============================================>> Status
    public onChange(is_active: boolean, id: number): void {
        const data: { user_id: number, is_active: 1 | 0 } = {
            user_id: id,
            is_active: is_active == true ? 1 : 0,
        };
        this.accountService.updateStatus(data).subscribe({
            next: (response: { httpStatus: number, message: string }) => {
                this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
            },
            error: (err: HttpErrorResponse) => {
                const error: { httpStatus: 400, message: string } = err.error;
                this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
            }
        });
    }
}
