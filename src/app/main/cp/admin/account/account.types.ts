export enum UserTypeId {
    SuperAdmin = 1,
    SVAdmin = 6,
    SupervisionUser = 7,
    RPAdmin = 8,
    HeadOfCommittee = 9,
    Minister = 10,
    SVInfoEntry = 11,
    SVSpecialInspector = 12
}

export interface Setup {
    roles: { id: number, name: string }[],
    organizations: { id: number, name: string }[],
    types?: { id: number, name: string }[] | null,
}

export interface ResponseAccount {
    current_page: number,
    data: Data[],
    from: number,
    per_page: number,
    total: number
}

export interface ResponseCreate {
    status: number | 200,
    data: Data,
    message: string
}

export interface RequestCreate {
    name: string,
    phone: string,
    email: string,
    organization_id: number,
    role_id: number,
    password: string,
    image_uri: string,
    type_id: number
}

export interface ResponseUpdate {
    status: number | 200,
    message: string
}
export interface RequestUpdate {
    name: string,
    phone: string,
    email: string,
    organization_id: number,
    role_id: number,
    image_uri?: string | null,
    type_id: number
}

export interface ReqPutPassword {
    id: number,
    passwork: string
}

export interface Data {
    rn: number,
    id: number,
    name: number,
    phone: string,
    email: string,
    avatar: string,
    is_active: 1 | 0,
    type: {id: number, name: string}
    organization?: {id: number, name: string} | null,
    oranization_role?: {id: number, name: string} | null
}