import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { Validators, UntypedFormGroup, UntypedFormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { AccountService } from "../account.service";
import { environment } from "environments/environment";
import { MatDialog } from '@angular/material/dialog';
import { GlobalConstants } from "app/shared/global-constants";
import { Data, RequestUpdate, ResponseUpdate, Setup, UserTypeId } from "../account.types";
import { HttpErrorResponse } from "@angular/common/http";
@Component({
    templateUrl: './update.component.html',
    styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

    public updateForm: UntypedFormGroup;
    public setup: Setup;
    public src: string = "";
    public show: boolean = false;
    public saving: boolean = false;
    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';
    public user_type_id: UserTypeId;
    public UpdateAccount = new EventEmitter();

    constructor(
        public dialogRef: MatDialogRef<UpdateComponent>,
        @Inject(MAT_DIALOG_DATA) public data: {user_type_id: UserTypeId, row: Data, setup: Setup},
        private accountService: AccountService,
        private _dialog: MatDialog,
        private snackbarService: SnackbarService,
        private _formBuilder: UntypedFormBuilder,
    ) {
        this.dialogRef.disableClose = true;
    }

    ngOnInit(): void {
        this.user_type_id = this.data.user_type_id;
        this.setup = this.data.setup;
        this.src = environment.fileUrl + this.data.row?.avatar;
        this.formBuilder();
    }

    private formBuilder(): void {
        this.updateForm = this._formBuilder.group({
            name: [this.data.row.name, [Validators.required, Validators.maxLength(50)]],
            phone: [this.data.row.phone, [Validators.required, Validators.pattern(/^(\+855|0)[1-9]\d{7,8}$/)]],
            email: [this.data.row.email, [Validators.required, Validators.pattern(GlobalConstants.emailRegex)]],
            organization_id: [this.data.row?.organization?.id, Validators.required],
            role_id: [this.data.row?.oranization_role?.id, Validators.required],
            image_uri: [null, ],
            type_id: [this.data.row?.type?.id, Validators.required],
        });
    }

    public update(): void {
        this.saving = true;
        this.updateForm.disable();
        this.accountService.update(this.updateForm.value,this.data.row.id).subscribe({
            next: (response: ResponseUpdate) => {
                this.saving = false;
                this.UpdateAccount.emit(true);
                this.dialogRef.close();
                this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
            },
            error: (err: HttpErrorResponse) => {
                this.saving = false;
                this.updateForm.enable();
                const errors: { field: string, message: string }[] = err.error.errors;
                var message: string = err.error.message;
                if (errors && errors.length > 0) {
                    message = errors.map((obj) => obj.message).join(', ')
                }
                this.snackbarService.openSnackBar(message, GlobalConstants.error);
            }
        });
    }

    public selectFile(): void {
        document.getElementById('portrait-file').click();
    }

    public fileChangeEvent(files: FileList): void {
        if (files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = files[0]?.type;
            this.size = files[0]?.size;
            console.log(this.size);
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                console.log(this.src)
                this.updateForm.get('image_uri').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.updateForm.get('image_uri').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.updateForm.get('image_uri').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.updateForm.get('image_uri').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        }
    }

    preview() {
        if (this.src == "") {
            document.getElementById('portrait-file').click();
        }
        else {
            this._dialog.open(PreviewImageComponent, {
                width: '850px',
                data: {
                    title: 'រូបភាពប្រភេទការងារ',
                    src: this.src
                }
            })
        }
    }
}