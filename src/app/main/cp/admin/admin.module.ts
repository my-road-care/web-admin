import {NgModule } from '@angular/core';
import { AccountModule } from './account/account.module';
import { RolesModule } from './role/role.module';
import { OrganizationModule } from './organization/organization.module';
@NgModule({
    exports: [
        AccountModule,
        RolesModule,
        OrganizationModule
    ]
})
export class AdminModule{}
