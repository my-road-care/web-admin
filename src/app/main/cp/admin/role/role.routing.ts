import { Route } from "@angular/router";
import { RoleComponent } from "./role.component";

export const routes: Route[] = [
    { path  : 'roles',  component: RoleComponent }
];