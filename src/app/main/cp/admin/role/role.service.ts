import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable } from 'rxjs';
import { ResponseRole } from './role.types';

@Injectable({
    providedIn: 'root',
})
export class RoleService {
    private url = env.apiUrl;
    private httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(private http: HttpClient) { }

    listing(): Observable<ResponseRole> {
        return this.http.get<ResponseRole>(this.url + '/sup/authority/role', this.httpOptions);
    }
}
