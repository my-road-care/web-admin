export interface ResponseRole {
    data: Data[],
    total: number
}

export interface Data {
    id: number,
    name: string,
    created_at: Data,
    qty: number
}