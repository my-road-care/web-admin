import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoleService } from './role.service';
import { MatTableDataSource } from '@angular/material/table';
import { Data, ResponseRole } from './role.types';
import { HttpErrorResponse } from '@angular/common/http';
import { GlobalConstants } from 'app/shared/global-constants';
@Component({
    selector: 'app-role',
    templateUrl: './role.component.html',
    styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

    public displayedColumns: string[] = ['id', 'name', 'date', 'qty'];
    public isSearching: boolean = false;
    public data: Data[];
    public dataSource: MatTableDataSource<Data>;
    constructor(
        private roleService: RoleService,
        private snackbarService: SnackbarService,

    ) { }

    ngOnInit(): void {
        this.isSearching = true;
        this.roleService.listing().subscribe({
            next: (response: ResponseRole) => {
                this.isSearching = false;
                this.data = response.data;
                this.dataSource = new MatTableDataSource(this.data);
            },
            error: (err: HttpErrorResponse) => {
                this.isSearching = false;
                const error: { httpStatus: 400, message: string } = err.error;
                this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
            }
        });
    }
    getTotal(): number {
        return this.data.map(t => t.qty).reduce((acc, value) => acc + value, 0);
    }
}
