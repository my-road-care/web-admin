import { NgModule } from '@angular/core';
import { RoleComponent } from './role.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { routes } from './role.routing';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        RoleComponent
    ]
})
export class RolesModule{}