import { Component, OnInit } from '@angular/core';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { OrganizationService } from './organization.service';
import { Data, ResponseOrgization } from './organization.types';
import { HttpErrorResponse } from '@angular/common/http';
import { GlobalConstants } from 'app/shared/global-constants';
import { MatTableDataSource } from '@angular/material/table';
@Component({
    selector: 'app-organization',
    templateUrl: './organization.component.html',
    styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {

    public displayedColumns: string[] = ['id', 'name', 'date', 'qty'];
    public isSearching: boolean = false;
    public data: Data[];
    public dataSource: MatTableDataSource<Data>;
    constructor(
        private organizationService: OrganizationService,
        private snackbarService: SnackbarService,

    ) { }

    ngOnInit(): void {
        this.isSearching = true;
        this.organizationService.listing().subscribe({
            next: (response: ResponseOrgization) => {
                this.isSearching = false;
                this.data = response.data;
                this.dataSource = new MatTableDataSource(this.data);
            },
            error: (err: HttpErrorResponse) => {
                this.isSearching = false;
                const error: { httpStatus: 400, message: string } = err.error;
                this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
            }
        });
    }
    getTotal(): number {
        return this.data.map(t => t.qty).reduce((acc, value) => acc + value, 0);
    }
}
