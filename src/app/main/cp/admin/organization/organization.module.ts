import { NgModule } from '@angular/core';
import { OrganizationComponent } from './organization.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { routes } from './organization.routing';
@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        OrganizationComponent
    ]
})
export class OrganizationModule { }