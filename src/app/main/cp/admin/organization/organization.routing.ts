import { Route } from "@angular/router";
import { OrganizationComponent } from "./organization.component";

export const routes: Route[] = [
    { path: 'organizations', component: OrganizationComponent }
];