import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable } from 'rxjs';
import { ResponseOrgization } from './organization.types';

@Injectable({
    providedIn: 'root',
})
export class OrganizationService {
    private url = env.apiUrl;
    private httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(private http: HttpClient) { }

    listing(): Observable<ResponseOrgization> {
        return this.http.get<ResponseOrgization>(this.url + '/sup/authority/user/organization', this.httpOptions);
    }
}
