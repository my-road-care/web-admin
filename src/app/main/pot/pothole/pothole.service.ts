import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class potholeService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}
    // =============================================>> Get Status
    getstatus(){
        return this.http.get(this.url + '/cp/quick-get/status');
    }
    // ==============================================>> Listing
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/potholes', httpOptions);
    }
    // =============================================>> Delete
    delete(pol_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/potholes/' + pol_id , this.httpOptions);
    }
    // =============================================>> View
    view(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/potholes/'+ id , httpOptions);
    }
}
