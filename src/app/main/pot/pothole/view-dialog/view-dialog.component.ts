import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-dialog',
  templateUrl: './view-dialog.component.html',
  styleUrls: ['./view-dialog.component.scss']
})
export class ViewDialogComponent implements OnInit {

  @ViewChild('ViewPotholeNgForm') ViewPotholeNgForm: NgForm;
  ViewGroup    = new EventEmitter();
  public viewForm :UntypedFormGroup;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(MAT_DIALOG_DATA) public id: any,
    private dialogRef: MatDialogRef<ViewDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
  ) { }

  ngOnInit(): void {
    this.formBuilder();
  }

  formBuilder(): void {
    this.viewForm = this._formBuilder.group({
      code :         [this.data?.data?.pothole?.code],
      name :         [this.data?.data?.ru?.user?.name ],
      description:   [this.data?.data?.description],
      statuss :      [this.data?.data?.statuses[0]?.status?.name],
      village :      [this.data?.data?.pothole?.location?.village?.name],
      commune :      [this.data?.data?.pothole?.location?.commune?.name],
      district:      [this.data?.data?.pothole?.location?.district?.name],
      province:      [this.data?.data?.pothole?.location?.province?.name],
      lat:           [this.data?.data?.lat],      
      lng:           [this.data?.data?.lng]


    });
  }
}
