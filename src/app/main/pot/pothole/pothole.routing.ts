import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";
import { ViewDialogComponent } from "./view-dialog/view-dialog.component";

export const portholeRoutes: Route[] = [
    { path: '', component: ListingComponent },
    { path: ':id', component: ViewDialogComponent }
];