import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { SharedModule } from 'app/shared/shared.module';
import { portholeRoutes } from './pothole.routing';
import { RouterModule } from '@angular/router';
import { ViewDialogComponent } from './view-dialog/view-dialog.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(portholeRoutes)
    ],
    declarations: [
      ListingComponent,
      ViewDialogComponent,
    ]
})
export class PotholeModule{}