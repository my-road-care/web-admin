import { Component, OnInit } from '@angular/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Animations } from 'helpers/animations';
import { LoadingService } from 'helpers/services/loading';
import { potholeService } from '../pothole.service';
import * as _moment from 'moment';
import { Router } from '@angular/router';
import { ViewDialogComponent } from '../view-dialog/view-dialog.component';
const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY MMMM ',
  },
};
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss'],
  animations: Animations,
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id', 'user', 'code', 'road_lo_auth', 'status', 'date_update', 'action'];
  public tagsEditMode: boolean = false;
  public disableClose: boolean = false;
  public isSearching: boolean = false;
  public data: any[] = [];
  public total: number = 20;
  public limit: number = 20;
  public page: number = 1;
  public dataSource: MatTableDataSource<unknown>;
  public dialog: any;
  public snackBar: any;
  public isClosing: any;
  public isLoading: boolean = false;
  public status: any;
  public startdate: any;
  public status_id: number = 0;
  maxDate = new Date();
  public from: any ;
  public to: any ;

  constructor(
    private _potholeService: potholeService,
    private _snackBar: SnackbarService,
    private _dialog: MatDialog,
    private _loadingService: LoadingService,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing(this.limit, this.page);
    this._potholeService.getstatus().subscribe(res => {
      this.status = res;
    });
  }

  // ===============================================Function Listing
  listing(_limit: number = 20, _page: number = 1): any {
    // // console.log(this.from,moment(this.from).format('YYYY-MM-DD'),moment(this.to).format('YYYY-MM-DD'))
    const param: any = {
      limit: _limit,
      page: _page,
    };
    //  Search selector name 
    if (this.status_id != 0) {
      param.status_id = this.status_id;
    }
    // search Date
    if(this.from != null){
      param.from = moment(this.from).format('YYYY-MM-DD');
    }
    if(this.to != null){
      param.to = moment(this.to).format('YYYY-MM-DD');
    }

    this.isSearching = true;
    this._potholeService.listing(param).subscribe(
      (res: any) => {
        this.isSearching = false;
        this.total = res.total;
        this.page = res.last_page;
        this.limit = res.per_page;
        this.data = res.data;

        // // console.log(this.data);
        // add new culonm for លេខរៀង
        let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        // console.log(this.data);

        this.dataSource = new MatTableDataSource(this.data);
      },
      (err: any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
    );
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

  onDelete(data: any = null): void {
    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        // console.log(data.id);

        this.isClosing = true;
        this._loadingService.show();
        this._potholeService.delete(data.id).subscribe(
          (res) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            this.listing();

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
          }
        );
      }
    });
  }
  View(row: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      data: row,
    }
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(ViewDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.ViewGroup.subscribe((response: any) => {
      this.listing();
      this.dataSource = new MatTableDataSource(this.data);
    })
  }
}
