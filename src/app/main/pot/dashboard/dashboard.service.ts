import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable, ReplaySubject } from 'rxjs';
import { ResponseDashboard } from './dashboard.types';

@Injectable({
    providedIn: 'root',
})
export class DashboardService {
    private _filters: ReplaySubject<{ year: number, listing: boolean }> = new ReplaySubject<{ year: number, listing: boolean }>();
    private _show: ReplaySubject<{ search: boolean }> = new ReplaySubject<{ search: boolean }>();
    private url = env.apiUrl;
    constructor(private http: HttpClient) { }

    listing(params = {}): Observable<ResponseDashboard> {
        const httpOptions = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this.http.get<ResponseDashboard>(this.url + '/cp/dashboard', httpOptions);
    }


    set filters(value: { year: number, listing: boolean }) {
        this._filters.next(value);
    }
    get filters$(): Observable<any> {
        return this._filters.asObservable();
    }
    set show(value: { search: boolean }) {
        this._show.next(value);
    }
    get show$(): Observable<any> {
        return this._show.asObservable();
    }
}