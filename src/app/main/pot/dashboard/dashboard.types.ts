export interface ResponseDashboard {
    statusCode: number
    data: Data,
    message: string
}

export interface Data {
    pothole: number
    member: number
    mo: number
    mt: number
    all_done: Pothole
    declined: Pothole
    fixed: Pothole
    pending: Pothole
    planning: Pothole
    repairing: Pothole
    specialist: Pothole
}

interface Pothole {
    totlal: number,
    percent: number
}