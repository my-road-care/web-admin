import { Component, OnDestroy, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Data, ResponseDashboard } from './dashboard.types';
import { HttpErrorResponse } from '@angular/common/http';
import { GlobalConstants } from 'app/shared/global-constants';
import { Subject, takeUntil } from 'rxjs';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
    private _unsubscribeAll: Subject<{ year: number, listing: boolean }> = new Subject<{ year: number, listing: boolean }>();
    spinner: boolean;
    data: Data;
    year: number = 0;
    constructor(
        private readonly dashboardService: DashboardService,
        private readonly snackBarService: SnackbarService
    ) {
        this.dashboardService.show = { search: true };
    };
    ngOnInit(): void {
        this.dashboardService.filters$.pipe(takeUntil(this._unsubscribeAll)).subscribe((filter: { year: number, listing: boolean }) => {
            if (filter.listing) {
                this.year = filter.year;
                this.listing();
            }
        });
    }
    listing(): void {
        const params: { year: number } = {
            year: this.year
        }
        this.spinner = true;
        this.dashboardService.listing(params).subscribe({
            next: (response: ResponseDashboard) => {
                this.spinner = false;
                this.data = response.data;
            },
            error: (err: HttpErrorResponse) => {
                this.spinner = false;
                this.snackBarService.openSnackBar(err.error ? err.error.message : GlobalConstants.genericError, GlobalConstants.error);
            }
        });
    }
    ngOnDestroy(): void {
        this.dashboardService.show = { search: false };
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
