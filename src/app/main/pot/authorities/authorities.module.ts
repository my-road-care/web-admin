import { NgModule } from '@angular/core';
import { MinistriesModule } from './ministries/ministries.module';
import { MosModule } from './mos/mos.module';
import { MtsModule } from './mts/mts.module';

@NgModule({
    imports: [
        MinistriesModule,
        MosModule,
        MtsModule
    ],
    exports: [
        MinistriesModule,
        MosModule,
        MtsModule,
    ]
})
export class AuthoritiesModule{}