import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { MinistryService } from '../../ministries.service';

@Component({
  selector: 'app-mt',
  templateUrl: './mt.component.html',
  styleUrls: ['./mt.component.scss']
})
export class mtComponent implements OnInit {

  @Input () public id : any;
  displayedColumns: string[] = ['id','name','contact','n_of_mos','n_of_m_roads','action'];
  public dataSource: any;
  public isSearching: boolean = false;
  public data : any[]   = [];
  public total: number = 20;
  public limit: number = 20;
  public page: number = 1;
  public key    : string = '';

  constructor(
    private _service : MinistryService,
    private _snackBar: SnackbarService,
    private _loadingService: LoadingService,
    private _dialog : MatDialog
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing(this.limit, this.page);
    
  }
  listing( _limit: number = 10, _page: number = 1): any {

    const params: any = {
      limit: _limit,
      page: _page,
    };

    if(this.key != ''){
      params.key = this.key.trim();
    }

    this.isSearching = true;
    this._service.listingMT(this.id, params).subscribe(
        (res: any) => {
          this.isSearching = false;
          this.total  = res.total;
          this.page   = res.current_page;
          this.limit  = res.per_page;
          this.data = res.data;
          // // console.log(this.data);
          
          this.dataSource = new MatTableDataSource(this.data);
        },
        (err: any) => {
            // console.log(err);
            this._snackBar.openSnackBar('Something went wrong.', 'error');
        }
    );
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
}
