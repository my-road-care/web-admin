
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { LoadingService } from 'helpers/services/loading';
import { MinistryService } from '../ministries.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
 
})
export class ViewComponent implements OnInit {

  public id:number = 0; 
  public isLoading:boolean=false;
  public data :any;
  public ministry_id :any;

  constructor(
    private _route: ActivatedRoute,
    private _service: MinistryService,
    private _loadingService: LoadingService,
  )
  {
    this._route.paramMap.subscribe((params: any) => {
      this.ministry_id = params.get('id');
    });
  }

  ngOnInit(): void {
    let id  = this._route.snapshot.params['id'];
    this.isLoading=true;
    this._service.view(id).subscribe( (res :any)  =>{
      this.isLoading=false;
      this.data = res;
      // // console.log(this.data);
      
    })
  }

}
