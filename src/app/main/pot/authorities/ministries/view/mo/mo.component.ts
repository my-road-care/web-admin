import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { MinistryService } from '../../ministries.service';
import { MoCreateDialogComponent } from '../mo-create-dialog/mo-create-dialog.component';

@Component({
  selector: 'app-mo',
  templateUrl: './mo.component.html',
  styleUrls: ['./mo.component.scss']
})
export class MoComponent implements OnInit {
  @Input() public id: any;
  displayedColumns: string[] = ['id', 'name', 'create_at', 'action',];
  public dataSource: any;
  public isSearching: boolean = false;
  public isClosing: boolean = false;
  public data: any[] = [];
  public total: number = 10;
  public limit: number = 10;
  public page: number = 1;
  public key: string = '';
  public mos: any;
  constructor(
    private _service: MinistryService,
    private _dialog: MatDialog,
    private _router: Router,
    private _snackBar: SnackbarService,
    private _loadingService: LoadingService,
  ) { }

  ngOnInit(): void {
    this._service.getmo().subscribe(res => {
      this.mos = res;
    });
    this.listing(this.limit, this.page);

  }

  listing(_limit: number = 10, _page: number = 1): any {

    const params: any = {
      limit: _limit,
      page: _page,
    };
    this.isSearching = true;
    this._service.listingMo(this.id).subscribe(
      (res: any) => {
        this.isSearching = false;
        this.total = res.total;
        this.page = res.current_page;
        this.limit = res.per_page;
        this.data = res;
        this.dataSource = new MatTableDataSource(this.data);
      },
      (err: any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
    );
  }

  createMo() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id: this.id,
      mos: this.mos,
      existed: this.data
    }
    dialogConfig.width = "750px";
    dialogConfig.autoFocus = false;
    const dialogRef = this._dialog.open(MoCreateDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.CreateMo.subscribe((response: any) => {
      this.listing();
      this.dataSource = new MatTableDataSource(this.data);
    });
  }

  OnDelete(data: any = null): void {

    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      // console.log(data.id);

      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._service.deletemo(this.id, data.id).subscribe(
          (res: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');

            let copy: any[] = [];
            this.data.forEach((obj: any) => {
              if (obj.id !== data.id) {
                copy.push(obj);
              }
            });
            this.data = copy;
            this.dataSource = new MatTableDataSource(this.data);

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
          }
        );
      }
    });
  }
}
