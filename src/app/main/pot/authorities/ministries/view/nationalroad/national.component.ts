import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MinistryService } from '../../ministries.service';

@Component({
  selector: 'app-national',
  templateUrl: './national.component.html',
  styleUrls: ['./national.component.scss']
})
export class NrComponent implements OnInit {
    @Input () public id :any;
   displayedColumns: string[] = ['id','road','ministry','cross','fixer','PK','action',];
   public dataSource:any;
    public isSearching: boolean =false;
    public data : any[]   = [];
    public total  : number  = 10;
    public limit  : number  = 10;
    public page   : number  = 1;
    public key    : string ='';
    public provinces: any[] = [];
    public ministries: any[] = [];
        
  constructor(
    private _service: MinistryService,
    private _snackBar: SnackbarService, 
  ) { }

  ngOnInit(): void {
    this.isSearching= true;
    this.listing( this.limit, this.page);
  }

  // ===============================================Function Listing
  listing( _limit: number = 10, _page: number = 1): any {

    const params: any = {
      limit: _limit,
      page: _page,
    };
    if (this.key != '' ) {
      params.key = this.key.trim();
    }
    this.isSearching = true;
    this._service.listingRoad(this.id,params).subscribe(
        (res: any) => {
          this.isSearching = false;
          this.total  = res.total;
          this.page   = res.current_page;
          this.limit  = res.per_page;
          this.data = res.data;
        // cut , in provice 
          this.data.forEach((v: any) => {
            this.provinces.push(v.provinces)
          });
    
          let province: any[] = [];
          this.provinces.forEach((v: any) => {
            if (v.length > 0) {
              let copy: any[] = [];
              v.forEach((array: any) => {
                copy.push(array.province.abbre);
                
              });
              province.push(copy.join(', '));
            }
          });
          let newData: any[] = [];
          this.data.forEach((obj: any, index: number) => {
            obj = {
              ...obj,
              provincess: province[index]
            }
            newData.push(obj);
          });
          // // console.log(this.data);
          
          this.data = newData;
        // End , in Province 

        // fumction cut , ministry 
          this.data.forEach((v: any) => {
            this.ministries.push(v.ministries)
          });
    
          let ministry: any[] = [];
          this.ministries.forEach((v: any) => {
            if (v.length > 0) {
              let copy: any[] = [];
              v.forEach((array: any) => {
                copy.push(array.ministry.abbre);
                
              });
              ministry.push(copy.join(', '));
            }
          });
          let newmini: any[] = [];
          this.data.forEach((obj: any, index: number) => {
            obj = {
              ...obj,
              ministriess: ministry[index]
            }
            newmini.push(obj);
          });
          // // console.log(this.data);
          
          this.data = newmini;

          // // console.log(this.data);

          // End , in minisry

          this.dataSource = new MatTableDataSource(this.data);
        },
        (err: any) => {
            // console.log(err);
            this._snackBar.openSnackBar('Something went wrong.', 'error');
        }
    );
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
}
