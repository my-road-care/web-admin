import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MinistryService } from '../../ministries.service';
import { environment as env } from 'environments/environment';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})

export class OwerviewComponent implements OnInit {
  
  @Input() public  data: any;
  ministryForm: UntypedFormGroup;

  public form: FormGroup;
  public fileUrl :any = env.fileUrl;
  public fs: any;
  public mode: any;
  public show: boolean = false;
  public src: string = null;
  public isLoading : boolean = false;
  public saving : boolean = false;

  constructor(
    private _route: Router,
    private _MinistryService: MinistryService,
    private snacBar: SnackbarService,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getBase64FromUrl('https://lh3.googleusercontent.com/i7cTyGnCwLIJhT1t2YpLW-zHt8ZKalgQiqfrYnZQl975-ygD_0mOXaYZMzekfKW_ydHRutDbNzeqpWoLkFR4Yx2Z2bgNj2XskKJrfw8').then(console.log)
    if (this.data?.data?.logo) {
      this.src = this.fileUrl + this.data?.data?.logo;
    } else {
      this.src = null;
    }
    this.buildForm();
  }
  getBase64FromUrl = async (url) => {
    const data = await fetch(url,{ mode: 'no-cors'});
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64data = reader.result;
        resolve(base64data);
      }
    });
  }
  public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';

    fileChangeEvent(e: any): void {
      if (e?.target?.files) {
        this.error_size = '';
        this.error_type = '';
        this.show = false;
        this.saving = false;
        this.type = e?.target?.files[0]?.type;
        this.size = e?.target?.files[0]?.size;
        var reader = new FileReader();
        reader.readAsDataURL(e?.target?.files[0]);
        reader.onload = (event: any) => {
            this.src = event?.target?.result;
            this.form.get('logo').setValue(this.src);
        }
        if (this.size > 3145728) { //3mb
            this.saving = true;
            this.form.get('logo').setValue('');
            this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
        }
        if (this.type.substring(0, 5) !== 'image') {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.form.get('logo').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        }
      } else {
        this.saving = true;
        this.src = '';
        this.show = true;
        this.error_size = '';
        this.form.get('logo').setValue(this.src);
        this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      }
    }

    selectFile(): any {
      document.getElementById('portrait-file').click();
    }

  preview(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
        src: this.src,
        title: 'រូបភាពក្រសួង'
    };
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
  }
  private buildForm(){
    this.form               = new FormGroup({
      logo                  : new FormControl(this.data? this.data?.data?.logo:' ', []),
      name                  : new FormControl(this.data? this.data?.data?.name: ' ', [Validators.required]),
      abbre                 : new FormControl(this.data? this.data?.data?.abbre:' ', [Validators.required]),
      description           : new FormControl(this.data? this.data?.data?.description:' ', []),
      n_of_roads            : new FormControl(this.data? this.data?.data?.n_of_roads: '', []),
      n_of_mos              : new FormControl(this.data? this.data?.data?.n_of_mos:' ', []),
     
    });
  }
  submit(): void{
  
    if (this.form.valid){
      this.form.disable();
      this.isLoading = true;
      this._MinistryService.updateMinistry (this.data.data.id, this.form.value).subscribe (res =>{
        this.isLoading =false;
        this.snacBar.openSnackBar(res.message,'');
        this.form.enable();
        // this._route.navigate(['/pot/authorities/ministries']);

      }, err => {
        this.form.enable();
        this.isLoading=false;
        for(let key in err.error.errors){
        let control = this.form.get(key);
        control.setErrors({'servererror':true});
        control.errors.servererror = err.error.errors[key][0];
        this.snacBar.openSnackBar('Something went wrong!','error');
        }
      }); 
    }
  }
}

