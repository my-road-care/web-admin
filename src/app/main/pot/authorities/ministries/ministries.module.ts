import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { CreateComponent} from './create/create.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ministriesRoutes } from './ministries.routing';
import { CurrencyPipe } from '@angular/common';
import { MoComponent } from './view/mo/mo.component';
import { mtComponent } from './view/mt/mt.component';
import { NrComponent } from './view/nationalroad/national.component';
import { OwerviewComponent } from './view/overview/overview.component';
import { ViewComponent } from './view/view.component';
import { MoCreateDialogComponent } from './view/mo-create-dialog/mo-create-dialog.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(ministriesRoutes)
    ],
    declarations: [
      ListingComponent,
      CreateComponent,
      ViewComponent,
      MoComponent,
      mtComponent,
      NrComponent,
      OwerviewComponent,
      MoCreateDialogComponent,
      
    ],
    providers: [
      CurrencyPipe
    ],
    entryComponents: [
  ]
})
export class MinistriesModule{}