import { Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MinistryService } from '../ministries.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  
})
export class CreateComponent implements OnInit {
  @Input() action:string = "CREATE";
  
  public isLoading: Boolean = false;
  public form: FormGroup;
  public roles: any;
  public mode:any;
  public data: any;
  public fs: any;
  public src: string = null;
  public show: boolean = false;
  public saving : boolean = false;

  constructor(
      private service: MinistryService,
      private _snackBar: SnackbarService,
      private _route: Router,
      private _dialog: MatDialog,
  ) { 
  }

    ngOnInit(): void {
        this._buildForm();
    }
    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';

    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.form.get('logo').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.form.get('logo').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.form.get('logo').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.form.get('logo').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }

    preview(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: this.src,
            title: 'រូបភាពក្រសួង'
        };
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
    }
    private _buildForm() {
        this.form = new FormGroup({
        logo          : new FormControl('', [Validators.required]),
        name          : new FormControl('', [ Validators.required,Validators.maxLength(150)]),
        abbre         : new FormControl('', [ Validators.required, Validators.maxLength(50)]),
        description   : new FormControl('', []),
        });
    }   
    
    submit(){
        if(this.form.valid){
            this.form.disable();
            this.saving = true;
            let data = this.form.value;
            this.service.create (this.form.value).subscribe (res=> {
                this.isLoading=false;
                this._snackBar.openSnackBar(res.message, '');
                this._route.navigate(['/pot/authorities/ministries']);
            }, err =>{
                this.form.enable();
                this.saving = false;
                this.isLoading=false;
                for(let key in err.error.errors){
                let control = this.form.get(key);
                control.setErrors({'servererror':true});
                control.errors.servererror = err.error.errors[key][0];
                }
            });
        }else{
            this._snackBar.openSnackBar('Please check your input agian.', 'error');
        }
    } 

    srcChange($event, $index = -1){
         
        this.form.get('logo').setValue($event); 
      }

}