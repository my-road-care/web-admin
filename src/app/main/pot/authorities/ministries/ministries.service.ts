import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MinistryService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }

    //==============================================>> Service Listing
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/authorities/ministries', httpOptions);
    }

    // ==============================================>> Service Create
    create(data: any = {}): any {
        return this.http.post(this.url + '/cp/authorities/ministries', data ,this.httpOptions);
    }

    // =============================================>> Service Delete
    delete(ministry_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/authorities/ministries/' + ministry_id , this.httpOptions);
    }
    // =============================================>> Service update Ministry
    updateMinistry(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/authorities/ministries/' +id+ '?_method=PUT', data, this.httpOptions);
    } 

    // =============================================>> Get User
    view(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/ministries/'+ id , httpOptions);
    }
  
    //==============================================>>> Listing NationalRoad 
    listingRoad(id: any = '',params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/authorities/ministries/'+ id + '/roads', httpOptions);
    }



     //==============================================>>> Listing MO 
    listingMo(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/ministries/'+ id + '/existing-mos', httpOptions);
    }
    getmo(){
        return this.http.get(this.url + '/cp/authorities/mts/~/mos ');
    }
    // ============================================== >> Create
    createmo(data: any = {} , Mos_id :any=''): any {
        return this.http.post(this.url + '/cp/authorities/ministries/' +Mos_id+ '/add-mos' + '?_method=PUT' , data, this.httpOptions);
    }
    // ==============================================>> Service Delete
    deletemo(min_id: number = 0, mo_id:number = 0 ): Observable<any> {
        return this.http.delete(this.url+ '/cp/authorities/ministries/' + min_id+'/remove-mos/' + mo_id , this.httpOptions);
    }




    listingMT(id: any = '',params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/authorities/ministries/'+ id + '/mts', httpOptions);
    }

    search(data:any,params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + `/cp/authorities/ministries?key=${data}`, httpOptions);
    }
}