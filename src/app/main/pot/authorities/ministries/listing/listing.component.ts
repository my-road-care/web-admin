import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { MinistryService } from '../ministries.service';
import { environment as env } from 'environments/environment';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  @Output() output = new EventEmitter<string>();
  public fileUrl: any = env.fileUrl;
  displayedColumns: string[] = ['id', 'name', 'logo', 'n_of_road', 'no_of_mos', 'action'];
  public isSearching: boolean = false;
  public isClosing: boolean = false;
  public selectedAcademic: number;
  public data: any = [];
  public total: number = 20;
  public limit: number = 20;
  public page: number = 1;
  public dataSource: any;
  public halsServicce: any;
  public key: String;

  ngOnInit(): void {
    this.isSearching = true;
    this.listing();
  }
  constructor(
    private _service: MinistryService,
    private _dialog: MatDialog,
    private _snackBar: SnackbarService,
    private _loadingService: LoadingService

  ) { }
  // =======================================================>> Function List
  listing(): any {

    const param: any = {
      
    };
    this.isSearching = true;
    this._service.listing(param).subscribe(
      (res: any) => {
        this.isSearching = false;
        this._loadingService.hide();
      this.total = res.total;
      this.page = res.current_page;
      this.limit = res.per_page;
        this.data = res.data;
        this.dataSource = new MatTableDataSource(this.data);
      }, (err: any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
    )
  }

  // ==========================================>> Function delete
  onDelete(data: any): void {

    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._service.delete(data.id).subscribe(
          (res) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            this.listing();
          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
          }
        );
      }
    });
  }
  preview(src: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      src: src,
      title: 'រូបភាពក្រសួង'
    };
    dialogConfig.width = "850px";
    this._dialog.open(PreviewImageComponent, dialogConfig);
  }

}
