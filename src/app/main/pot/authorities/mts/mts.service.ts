import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MtsService {

    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }

    //==============================================>> Listing MTS
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/authorities/mts', httpOptions);
    }
    // ============================================>> Create MTS

    createmts(data: any = {}): any {
        return this.http.post(this.url + '/cp/authorities/mts', data ,this.httpOptions);
    }
    // ============================================>> Get Data select
    getprovince_id(){
        return this.http.get(this.url + '/cp/authorities/mts/get/province');
    }
    // ==============================================>>  Delete MTS
    deletemts(mts_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/authorities/mts/' + mts_id , this.httpOptions);
    }
     //==================================================================================>>  View Project Info
     viewMts(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mts/' +id+ '/ministries', httpOptions);
    }
     // ==============================================>> Service View
     view(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mts/'+ id , httpOptions);
    }
    // ===============================================>> Update MTS
    update(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/authorities/mts/' +id+ '?_method=PUT', data, this.httpOptions);
    }



    listingMinistry(id: any = ''){
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mts/'+ id + '/ministries', httpOptions);
    }


     // ============================================== >> Get Date MO
    getmo(){
        return this.http.get(this.url + '/cp/authorities/mts/~/mos');
    }
    // =====================================>> Listing Mo
    listingMO(id: any = ''){
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mts/'+ id +'/existing-mos', httpOptions);
    }
    // ===================================== >> Create Mo
    createmos(data: any = {} , Mos_id :any=''): any {
        return this.http.post(this.url + '/cp/authorities/mts/' +Mos_id+ '/add-mos' + '?_method=PUT' , data, this.httpOptions);
    }
    // ==============================================>> Service Delete
    deletemos(mos_id: number = 0,min_id:number =0): Observable<any> {
        return this.http.delete(this.url+ '/cp/authorities/mts/' + mos_id+'/remove-mos/' + min_id , this.httpOptions);
    }
    

    listingRoad(id:any = ''){
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mts/'+ id + '/roads', httpOptions);
    }
    

    //============================================>> Change Password
    change_pass (data:any = {}): any {
        return this.http.post(this.url + '/cp/authorities/mts/change-password', data, this.httpOptions);
    }
}