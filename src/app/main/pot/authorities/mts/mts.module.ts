import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { mtsRoutes } from './mts.routing';
import { MinistryComponent } from './view/ministry/ministry.component';
import { MOComponent } from './view/MO/mo.component';
import { NRComponent } from './view/NR/nr.component';
import { OverViewComponent } from './view/overview/overview.component';
import { MODialogComponent } from './view/mo-dialog/mo-dialog.component';
import { UpdatePasswordDialogComponent } from './update-password-dialog/update-password-dialog.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(mtsRoutes)
    ],
    declarations: [
      ListingComponent,
      CreateComponent,
      ViewComponent,
      MinistryComponent,
      MOComponent,
      NRComponent,
      OverViewComponent,
      MODialogComponent,
      UpdatePasswordDialogComponent
    ]
})
export class MtsModule{}