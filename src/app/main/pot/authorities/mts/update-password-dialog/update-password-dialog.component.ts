import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MtsService } from '../mts.service';

@Component({
  selector: 'app-update-password-dialog',
  templateUrl: './update-password-dialog.component.html',
  styleUrls: ['./update-password-dialog.component.scss']
})
export class UpdatePasswordDialogComponent implements OnInit {

  @ViewChild('ChangePasswordNgForm') ChangePasswordNgForm: NgForm;
  ChangePassword    = new EventEmitter();
  public changeForm :UntypedFormGroup;
  public confirm:boolean=false;
  public saving: boolean = false;
  public validateComfirm:boolean  = false;
  public validatePass: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<UpdatePasswordDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _service: MtsService,
    private _snackBar: SnackbarService,
  ) { 
    _dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    // console.log(this.data);
    
    this.formBuilder();
    
  }
  formBuilder():void{
    this.changeForm = this._formBuilder.group({
      password:['',[Validators.required,Validators.minLength(6),Validators.maxLength(15)]],
      user_id: [this.data?.data?.user_id, []],
      
    })
  }
  submit(){
    if(this.changeForm.value){
      this.saving=true;
      this.changeForm.disable();
        this._service.change_pass(this.changeForm.value).subscribe((res:any)=>{
          this.saving=false;
          this.confirm=true;
          this.validateComfirm=false;
          this.validatePass=false;
          this._dialogRef.close();
          this.changeForm.enable();
          this._snackBar.openSnackBar(res.message, '');
          
        },(err:any)=>{
          this.changeForm.enable();
            this.saving = false;
            this._dialogRef.close();
            for(let key in err.error.errors){
            let control = this.changeForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }else{
      this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
}
