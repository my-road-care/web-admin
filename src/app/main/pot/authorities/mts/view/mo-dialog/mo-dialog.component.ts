import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MtsService } from '../../mts.service';

@Component({
  selector: 'app-mo-dialog',
  templateUrl: './mo-dialog.component.html',
  styleUrls: ['./mo-dialog.component.scss']
})
export class MODialogComponent implements OnInit {

  @ViewChild('CreateMoNgForm') CreateMoNgForm: NgForm;
  CreateMo = new EventEmitter();
  public moForm: UntypedFormGroup;
  public saving: boolean = false;
  public isLoading : boolean = false;
  public mos = [];
  public existed = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private _formBuilder: UntypedFormBuilder,
    private dialogRef: MatDialogRef<MODialogComponent>,
    private _service : MtsService,
    private _snackBar: SnackbarService,

  ) 
  { 
    dialogRef.disableClose = true;
  }


  ngOnInit(): void {

    this.mos   = this.data.mos;
    this.existed = this.data.existed;

    // console.log('mts', this.mos)
    // console.log('existed', this.existed)
    
    this.mos = this.mos.map(o => ({ ...o }));
    this.existed = this.existed

    this.existed.forEach(({ mo_id }) => {
      const mo = this.mos.find(({ id }) => mo_id === id)
      if (mo) mo.disabled = true
    })

    this.formBuilder();
  }
  
  formBuilder():void {
    this.moForm = this._formBuilder.group({
      object : ['',Validators.required],

    });
  }
  submit(){
    if(this.moForm.value){
      this.moForm.disable();
      this.saving = true;
      let data: object = {
        object: "["+this.moForm.value.object.join(',')+"]"
      }
      this._service.createmos(data,this.data.id).subscribe((res:any) =>{
        this.isLoading = false;
        this.dialogRef.close();
        this.CreateMo.emit(res);
        this._snackBar.openSnackBar(res.message, '');
      },err =>{
          this.moForm.enable();
          this.saving = false;
          this.dialogRef.close();
          for(let key in err.error.errors){
          let control = this.moForm.get(key);
          control.setErrors({'servererror':true});4
          control.errors.servererror = err.error.errors[key][0];
          }
      });
  }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
}
