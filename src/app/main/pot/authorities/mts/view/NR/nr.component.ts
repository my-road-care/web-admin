import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MtsService } from '../../mts.service';

@Component({
  selector: 'app-nr',
  templateUrl: './nr.component.html',
  styleUrls: ['./nr.component.scss']
})
export class NRComponent implements OnInit {

  @Input () public id :any;
   displayedColumns: string[] = ['id','road','PKStart','PKEnd'];
   public dataSource:any;
    public isSearching: boolean =false;
    public data : any[]   = [];
    public total  : number  = 10;
    public limit  : number  = 10;
    public page   : number  = 1;
    public key    : string ='';
   
        
  constructor(
    private _service: MtsService,
    private _snackBar: SnackbarService, 
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing( this.limit, this.page);
  }

  // ===============================================Function Listing
  listing( _limit: number = 10, _page: number = 1): any {

    const params: any = {
      limit: _limit,
      page: _page,
    };
    if (this.key != '' ) {
      params.key = this.key;
    }
    this.isSearching = true;
    this._service.listingRoad(this.id).subscribe(
        (res: any) => {
          this.isSearching = false;
          this.total  = res.total;
          this.page   = res.current_page;
          this.limit  = res.per_page;
          this.data = res.data;
          // console.log(this.data);
          
          this.dataSource = new MatTableDataSource(this.data);
        },
        (err: any) => {
            // console.log(err);
            this._snackBar.openSnackBar('Something went wrong.', 'error');
        }
    );
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

}
