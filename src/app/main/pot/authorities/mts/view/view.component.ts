import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'helpers/services/loading';
import { MtsService } from '../mts.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  public id:number = 0; 
  public isLoading: boolean = false;
  public data:any;
  public province_type:any;
  public min_id : any;
  constructor(
    private _route: ActivatedRoute,
    private _ServiceMts : MtsService,
    private _loadingService: LoadingService,
  ) { 
    this._route.paramMap.subscribe((params:any)=>{
      this.min_id = params.get('id');
    });
  }
 

  ngOnInit(): void {
    let id  = this._route.snapshot.params['id'];
    this.isLoading=true;
    this._ServiceMts.view(id).subscribe( (res :any)  =>{
      this.isLoading=false;
      this.data = res;
      
    })
    this._ServiceMts.getprovince_id().subscribe(res => {
      this.province_type = res; 
      // // console.log(this.province_type);
      
    });
  }

  getprovince_id(){
    this.isLoading = true;
    this._ServiceMts.getprovince_id().subscribe((res:any)=>{  
      this.data = res;
      this.isLoading = false;
    })
  }
  
}
