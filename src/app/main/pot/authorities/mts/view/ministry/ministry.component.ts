import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MtsService } from '../../mts.service';
import { environment as env } from 'environments/environment';

@Component({
  selector: 'app-ministry',
  templateUrl: './ministry.component.html',
  styleUrls: ['./ministry.component.scss']
})
export class MinistryComponent implements OnInit {

  @Input () public id:any;
  public fileUrl :any = env.fileUrl;
  displayedColumns: string[] = ['id','name','logo','n_of_road','no_of_mos','action'];
  public dataSource: any;
  public limit :number =10;
  public page  :number =1;
  public total :number =10;
  public key   :string = '';
  public data  :any;
  public isSearching: boolean = false;
  
  constructor(
    private _service: MtsService,
    private _snackBar: SnackbarService, 
    private _dialog: MatDialog,

  ) { }

  ngOnInit(): void {

    this.isSearching = true;
    this.listing(this.limit, this.page); 
  }

  listing( _limit: number = 10, _page : number = 1 ):any{

    const param: any = {
      limit: _limit,
      page: _page,
    };
    if (this.key != '' ) {
      param.key = this.key;
    }

    this.isSearching = true;
    this._service.listingMinistry(this.id).subscribe((res:any)=>{
      this.isSearching = false;
      this.total = res.total;
      this.page  = res.current_page;
      this.limit = res.per_page;
      this.data  = res.data;
      this.dataSource = new MatTableDataSource(this.data);

    },
    (err : any)=>{
      // console.log(err);
      this._snackBar.openSnackBar('Something went wrong.', 'error');
    });

  }
  onPageChanged(event:any):any{
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }
  preview(src: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
        src: src,
        title: 'រូបភាពក្រសួង'
    };
    dialogConfig.width = "850px";
    this._dialog.open(PreviewImageComponent, dialogConfig);
  }
  
}
