import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { environment as env } from 'environments/environment';
import { MtsService } from '../../mts.service';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { GlobalConstants } from "app/shared/global-constants";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverViewComponent implements OnInit {

  public form: FormGroup;
  @Input() public data:any;
  @Input() public province_type:any;

  public fileUrl :any = env.fileUrl;

  public isLoading: Boolean = false;
  public isSearching  :boolean = false
  public mode:any;
  public olephone:string;
  public passwordCurrct:boolean=true;
  public show: boolean = false;
  public src: string = null;
  public saving : boolean = false;
  public isloading : boolean = false;
  public fs:any;
  constructor(
    private mtservice: MtsService,
    private _snackBar: SnackbarService,
    private _route: Router,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {

    this.getBase64FromUrl('https://lh3.googleusercontent.com/i7cTyGnCwLIJhT1t2YpLW-zHt8ZKalgQiqfrYnZQl975-ygD_0mOXaYZMzekfKW_ydHRutDbNzeqpWoLkFR4Yx2Z2bgNj2XskKJrfw8').then(console.log)
    if (this.data?.data?.user?.avatar) {
      this.src = this.fileUrl + this.data?.data?.user?.avatar;
    } else {
      this.src = null;
    }
    this._building();
    this.mtservice.getprovince_id().subscribe(res => {
      this.province_type = res;
      
    });
  }
  getBase64FromUrl = async (url) => {
    const data = await fetch(url,{ mode: 'no-cors'});
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64data = reader.result;
        resolve(base64data);
      }
    });
  }

  public size: number = 0;
  public type: string = '';
  public error_size: string = '';
  public error_type: string = '';

  fileChangeEvent(e: any): void {
    if (e?.target?.files) {
      this.error_size = '';
      this.error_type = '';
      this.show = false;
      this.saving = false;
      this.type = e?.target?.files[0]?.type;
      this.size = e?.target?.files[0]?.size;
      var reader = new FileReader();
      reader.readAsDataURL(e?.target?.files[0]);
      reader.onload = (event: any) => {
        this.src = event?.target?.result;
        this.form.get('avatar').setValue(this.src);
      }
      if (this.size > 3145728) { //3mb
        this.saving = true;
        this.form.get('avatar').setValue('');
        this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
      }
      if (this.type.substring(0, 5) !== 'image') {
        this.saving = true;
        this.src = '';
        this.show = true;
        this.error_size = '';
        this.form.get('avatar').setValue(this.src);
        this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      }
    } else {
        this.saving = true;
        this.src = '';
        this.show = true;
        this.error_size = '';
        this.form.get('avatar').setValue(this.src);
        this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        //it work when user cancel select file
    }
  }

  selectFile(): any {
      document.getElementById('portrait-file').click();
  }

  preview(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
        src: this.src,
        title: 'រូបភាពក្រុមជួសជុល'
    };
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
  }
    
  private _building(){
    this.form             = new FormGroup({
    user_id             : new FormControl(this.data ? this.data?.data?.user_id : '', []),
    avatar              : new FormControl( this.data ? this.data?.data?.user?.avatar   : '', [] ),
    name                : new FormControl(this.data ? this.data?.data?.user?.name      :   '', []),
    phone               : new FormControl( this.data ? this.data?.data?.user?.phone    : '', [ Validators.required, Validators.pattern('(^[0][0-9].{7}$)|(^[0][0-9].{8}$)|(^[855][0-9].{9}$)|(^[855][0-9].{10}$)|(.+@.+\..+)')]),
    email               : new FormControl( this.data ? this.data?.data?.user?.email    : '', [ Validators.required, Validators.pattern( '(^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$)' )]),
    mt_name             : new FormControl( this.data ? this.data?.data?.name           : '', [ ]),
    province            : new FormControl( this.data ? this.data?.data?.province_id    : '', [ ]),
    description         : new FormControl( this.data ? this.data?.data?.description    : '', []),
    n_of_m_roads        : new FormControl(this.data ? this.data?.data?.n_of_m_roads    :   '', []),
    n_of_mts            : new FormControl(this.data ? this.data?.data?.n_of_mos        :   '', []),
  })
  }
  
  submit(): void{
    if (this.form.valid){
      this.form.disable();
      this.isLoading = true;
      this.mtservice.update (this.data.data.id, this.form.value).subscribe ((res:any) =>{
        this.isLoading =false;
        this._snackBar.openSnackBar(res.message,'');
        this.form.enable();
        // this._route.navigate(['/pot/authorities/mts']);

      }, (err:any) => {
        this.isloading = false;
            this.saving = false;
            this.form.enable();
            let errors: any[] = [];
            errors = err.error.errors;
            let messages: any[] = [];
            let text: string = '';
            if (errors.length > 0) {
                errors.forEach((v: any) => {
                    messages.push(v.message)
                });
                if (messages.length > 1) {
                    text = messages.join('-');
                } else {
                    text = messages[0];
                }
            } else {
                text = err.error.message;
            }
            this._snackBar.openSnackBar(text, GlobalConstants.error);
      }); 
    }
  }
}
