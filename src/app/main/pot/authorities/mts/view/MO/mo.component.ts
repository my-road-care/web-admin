import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { MtsService } from '../../mts.service';
import { MODialogComponent } from '../mo-dialog/mo-dialog.component';

@Component({
  selector: 'app-mo',
  templateUrl: './mo.component.html',
  styleUrls: ['./mo.component.scss']
})
export class MOComponent implements OnInit {

  @Input() public id: any;
  displayedColumns: string[] = ['id', 'name', 'create_at', 'action'];
  public dataSource: any;
  public isSearching: boolean = false;
  public isClosing: boolean = false;
  public limit: number = 10;
  public page: number = 1;
  public total: number = 10;
  public data: any;
  public mos: any;
  constructor(
    private _service: MtsService,
    private _dialog: MatDialog,
    private _router: Router,
    private _snackBar: SnackbarService,
    private _loadingService: LoadingService,
  ) { }

  ngOnInit(): void {
    this._service.getmo().subscribe(res => {
      this.mos = res;
    });
    this.listing(this.limit, this.page);
  }

  listing(_limit: number = 10, _page: number = 10): any {
    const params: any = {
      limit: _limit,
      page: _page
    }
    this.isSearching = true;
    this._service.listingMO(this.id).subscribe((res: any) => {
      this.isSearching = false;
      this.total = res.total;
      this.page = res.current_page;
      this.limit = res.per_page;
      this.data = res;
      this.dataSource = new MatTableDataSource(this.data);
    })
  }

  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

  createMo() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id: this.id,
      mos: this.mos,
      existed: this.data
    }

    dialogConfig.width = "700px";
    const dialogRef = this._dialog.open(MODialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.CreateMo.subscribe((response: any) => {
      this.listing();
      this.dataSource = new MatTableDataSource(this.data);

    });
  }

  OnDelete(data: any = null): void {

    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      // console.log(data.id);

      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._service.deletemos(this.id, data.id).subscribe(
          (res: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            let copy: any[] = [];
            this.data.forEach((obj: any) => {
              if (obj.id !== data.id) {
                copy.push(obj);
              }
            });
            this.data = copy;
            this.dataSource = new MatTableDataSource(this.data);

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
          }
        );
      }
    });
  }
}
