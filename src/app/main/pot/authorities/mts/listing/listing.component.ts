import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { MtsService } from '../mts.service';
import { environment as env } from 'environments/environment';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { UpdatePasswordDialogComponent } from '../update-password-dialog/update-password-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  public fileUrl: any = env.fileUrl;
  displayedColumns: string[] = ['id', 'name', 'contact', 'image', 'MO', 'action'];

  public isSearching: boolean = false;
  public data: any[] = [];
  public total: number = 20;
  public limit: number = 20;
  public page: number = 1;
  public name: string;
  public phone: number = 1;
  public key: string = '';
  public dataSource: MatTableDataSource<unknown>;
  public isClosing: boolean = false;


  constructor(
    private _service: MtsService,
    private _snackBar: SnackbarService,
    private _dialog: MatDialog,
    private _loadingService: LoadingService,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing(this.limit, this.page);
  }

  listing(_limit: number = 20, _page: number = 1): any {

    const param: any = {
      limit: _limit,
      page: _page,
    };
    if (this.key != '') {
      param.key = this.key.trim();
    }
    this.isSearching = true;
    this._service.listing(param).subscribe(
      (res: any) => {
        this.isSearching = false;
        this.total = res.total;
        this.page = res.current_page;
        this.limit = res.per_page;
        this.data = res.data;
        this.dataSource = new MatTableDataSource(this.data);
      },
      (err: any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

  onRemove(data: any = null): void {
    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      // console.log(data.id);

      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._service.deletemts(data.id).subscribe(
          (res: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            this.listing();

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
          }
        );
      }
    });
  }

  preview(src: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      src: src,
      title: 'រូបភាពក្រុមជួសជុល'
    };
    dialogConfig.width = "850px";
    this._dialog.open(PreviewImageComponent, dialogConfig);
  }
  // Update Password
  ChangePassword(row:any):void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      data: row,
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = "700px";
    dialogConfig.autoFocus = false;
    const dialogRef = this._dialog.open(UpdatePasswordDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.ChangePassword.subscribe((response: any) => {
      let copy: any[] = [];
      copy.push(response.data);
      this.data.forEach((row: any) => {
        copy.push(row);
      })
      this.data = copy;
      // console.log(this.data);
      this.dataSource = new MatTableDataSource(this.data);
    });
  }
}
