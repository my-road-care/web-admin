import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MtsService } from '../mts.service';
import { GlobalConstants } from "app/shared/global-constants";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  @Input() action:string = "CREATE";
  public isLoading: Boolean = false;
  public saving   : boolean =false;
  public mtsForm: FormGroup;
  public data:any;
  public mode:any;
  public src: string = null;
  public show: boolean = false;
  public fs:any;
  public olephone:string;
  public passwordCurrct:boolean=true;
  public manage :any;
  public isloading:boolean = false;


  constructor(
    private mtservice: MtsService,
    private _snackBar: SnackbarService,
    private _route: Router,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.mtservice.getprovince_id().subscribe(res => {
      this.manage = res; 
    });
    this._building();
  }
  province_id(){
    this.isLoading = true;
    this.mtservice.getprovince_id().subscribe((res:any)=>{
      this.manage = res;
      this.isLoading = false;
    })
  }
  public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';

    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.mtsForm.get('avatar').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.mtsForm.get('avatar').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.mtsForm.get('avatar').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.mtsForm.get('avatar').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }

    preview(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: this.src,
            title: 'រូបភាពក្រុមជួសជុល'
        };
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
    }

  submit(){
    // // console.log(this.mtsForm.value);
    
    if(this.mtsForm.valid){
      this.mtsForm.disable();
      this.saving = true;
      let data = this.mtsForm.value;
      this.mtservice.createmts (this.mtsForm.value).subscribe (res => {
        this.isLoading =false;
        this._snackBar.openSnackBar(res.message, '');
        this._route.navigate(['/pot/authorities/mts']);
      },err =>{
        this.isloading = false;
            this.saving = false;
            this.mtsForm.enable();
            let errors: any[] = [];
            errors = err.error.errors;
            let messages: any[] = [];
            let text: string = '';
            if (errors.length > 0) {
                errors.forEach((v: any) => {
                    messages.push(v.message)
                });
                if (messages.length > 1) {
                    text = messages.join('-');
                } else {
                    text = messages[0];
                }
            } else {
                text = err.error.message;
            }
            this._snackBar.openSnackBar(text, GlobalConstants.error);
      });
    }
    else{
      this._snackBar.openSnackBar('Please check your input.', '');
    }
  }
  srcChange($event, $index = -1){
         
    this.mtsForm.get('avatar').setValue($event); 
  }
  private _building(){
    this.mtsForm = new FormGroup({
      avatar              : new FormControl( '', [Validators.required]),
      name                : new FormControl( '', [ Validators.required,Validators.maxLength(60)]),
      phone               : new FormControl( '', [ Validators.required, Validators.pattern('(^[0][0-9].{7}$)|(^[0][0-9].{8}$)|(^[855][0-9].{9}$)|(^[855][0-9].{10}$)|(.+@.+\..+)')]),
      email               : new FormControl( '', [ Validators.required, Validators.pattern( '(^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$)' )]),
      password            : new FormControl( '', [ Validators.required,Validators.minLength(6), Validators.maxLength(19)]),
      mt_name             : new FormControl( '', [ Validators.required,Validators.maxLength(60)]),
      province            : new FormControl( '', [ Validators.required,Validators.maxLength(60)]),
      description         : new FormControl( '', []),
    })
  }
}
