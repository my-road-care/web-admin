import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { mosRoutes } from './mos.routing';
import { MinistryComponent } from './view/ministry/ministry.component';
import { MtComponent } from './view/mt/mt.component';
import { NationalComponent } from './view/nationalroad/national.component';
import { OverViewComponent } from './view/overview/view.component';
import { MinistryDialogComponent } from './view/ministry-dialog/ministry-dialog.component';
import { MtDialogComponent } from './view/mt-dialog/mt-dialog.component';
import { UpdatePasswordDialogComponent } from './update-password-dialog/update-password-dialog.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(mosRoutes)
    ],
    declarations: [
      ListingComponent,
      CreateComponent,
      ViewComponent,
      MinistryComponent,
      MtComponent,
      NationalComponent,
      OverViewComponent,
      MinistryDialogComponent,
      MtDialogComponent,
      UpdatePasswordDialogComponent
    ]
})
export class MosModule{}