import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MosService } from '../mos.service';
import { GlobalConstants } from "app/shared/global-constants";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  @Input() action:string = "CREATE";
  public isLoading: Boolean = false;
  public saving   : boolean = false;
  public form: FormGroup;
  public data:any;
  public mode:any;
  public olephone:string;
  public passwordCurrct:boolean=true;
  public fs:any;
  public manage :any;
  public src: string = null;
  public show: boolean = false;
  public phone_error:string='';
  public isloading = false;
  constructor(
    private moservice: MosService,
    private _snackBar: SnackbarService,
    private _route: Router,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this._building();
    this.moservice.getManage_Type().subscribe(res => {
      this.manage = res; 
    });
  }
  public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';

    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.form.get('avatar').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.form.get('avatar').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.form.get('avatar').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.form.get('avatar').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }

    preview(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: this.src,
            title: 'រូបភាពការិយាល័យ'
        };
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
    }

  // getManage_Type(){
  //   this.isLoading = true;
  //   this.moservice.getManage_Type().subscribe((res:any)=>{
  //     this.manage = res;
  //     this.isLoading = false;
  //   })
  // }
  submit(){
    if(this.form.valid){
      this.form.disable();
      this.saving = true;
      let data = this.form.value;
      this.moservice.create (this.form.value).subscribe (res => {
        this.isLoading =false;
        this._snackBar.openSnackBar(res.message,'');
        this._route.navigate(['/pot/authorities/mos']);
      },err =>{
        this.isloading = false;
            this.saving = false;
            this.form.enable();
            let errors: any[] = [];
            errors = err.error.errors;
            let messages: any[] = [];
            let text: string = '';
            if (errors.length > 0) {
                errors.forEach((v: any) => {
                    messages.push(v.message)
                });
                if (messages.length > 1) {
                    text = messages.join('-');
                } else {
                    text = messages[0];
                }
            } else {
                text = err.error.message;
            }
            this._snackBar.openSnackBar(text, GlobalConstants.error);
      }
      );
    }
    else{
      this._snackBar.openSnackBar('Please check your input.', '');
    }
  }

  srcChange($event, $index = -1){
         
    this.form.get('avatar').setValue($event); 
  }

  private _building(){
    this.form = new FormGroup({
      avatar              : new FormControl( '' ),
      mo_name             : new FormControl( '', [ Validators.required, ]),
      phone               : new FormControl( '', [ Validators.required, Validators.pattern('(^[0][0-9].{7}$)|(^[0][0-9].{8}$)|(^[855][0-9].{9}$)|(^[855][0-9].{10}$)|(.+@.+\..+)')]),
      email               : new FormControl( '', [ Validators.required, Validators.pattern( '(^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$)' ) ]),
      password            : new FormControl( '', [ Validators.required, Validators.minLength(6), Validators.maxLength(19) ]),
      name                : new FormControl( '', [ Validators.required, ]),
      management_type_id  : new FormControl( '', [ Validators.required, Validators.maxLength(150)]),
      description         : new FormControl( '', []),
    })
  }
  
}
