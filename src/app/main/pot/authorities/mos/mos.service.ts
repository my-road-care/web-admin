import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MosService {
    getSetup() {
      throw new Error('Method not implemented.');
    }
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }

    // ============================================>> Function Listing
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/authorities/mos', httpOptions);
    }
    // =============================================>> Function Search
    search(data:any,params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/authorities/mos?key=data}', httpOptions);
    }
    // ==============================================>> Service Create
    create(data: any = {}): any {
        return this.http.post(this.url + '/cp/authorities/mos', data ,this.httpOptions);
    }
    // ==============================================>> Service Delete
    delete(mos_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/authorities/mos/' + mos_id , this.httpOptions);
    }
    // ==============================================>> Service View
    view(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mos/'+ id , httpOptions);
    }


    // =============================================>> Service update Ministry
    updateMos(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/authorities/mos/' +id+ '?_method=PUT', data, this.httpOptions);
    }
    // =============================================>>  Get data Management
    getManage_Type(){
        return this.http.get(this.url + '/cp/quick-get/management_type');
    }



    //==============================================>>> Listing MO 
    listingMini(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mos/'+ id + '/existing-ministries', httpOptions);
    }
    // ============================================== >> Get Date Ministry
    getmini(){
        return this.http.get(this.url + '/cp/authorities/mos/~/ministries ');
    }

    // ============================================== >> Create
    createmini(data: any = {} , Mos_id :any=''): any {
        return this.http.post(this.url + '/cp/authorities/mos/' +Mos_id+ '/add-ministries' + '?_method=PUT' , data, this.httpOptions);
    }
    // ==============================================>> Service Delete
    deletemini(mos_id: number = 0,min_id:number =0): Observable<any> {
        return this.http.delete(this.url+ '/cp/authorities/mos/' + mos_id+'/remove-ministries/' + min_id , this.httpOptions);
    }

    
    // ============================================= >> Get Mts 
    getmts(){
        return this.http.get(this.url + '/cp/authorities/mos/~/mts');
    }
    //==============================================>>> Listing MT
    listingMt(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mos/'+ id + '/existing-mts', httpOptions);
    }
    createmts(data: any = {} , Mos_id :any=''): any {
        return this.http.post(this.url + '/cp/authorities/mos/' +Mos_id+ '/add-mts' + '?_method=PUT' , data, this.httpOptions);
    }
    // ==============================================>> Service Delete
    deletemts(mos_id: number = 0,min_id:number =0): Observable<any> {
        return this.http.delete(this.url+ '/cp/authorities/mos/' + mos_id+'/remove-mts/' + min_id , this.httpOptions);
    }



    //==============================================>>> Listing Road
     listingNR(id: any = ''): any {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/authorities/mos/'+ id + '/roads', httpOptions);
    }


    //============================================>> Change Password
    change_pass (data:any = {}): any {
        return this.http.post(this.url + '/cp/authorities/mos/change-password', data, this.httpOptions);
    }
}
