import { Route } from "@angular/router";
import { CreateComponent } from "./create/create.component";
import { ListingComponent } from "./listing/listing.component";
import { ViewComponent } from "./view/view.component";

export const mosRoutes: Route[] = [{
    path: 'mos',
    children: [
        { path  : '',  component: ListingComponent },
        { path  : 'create', component: CreateComponent },
        { path  : ':id', component: ViewComponent }
    ]
}];