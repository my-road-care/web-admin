import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'helpers/services/loading';
import { MosService } from '../mos.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  public id:number = 0; 
  public isLoading:boolean=false;
  public data :any;
  public manage:any;
  public key    : string = '';
  public isSearching:boolean=false;
  public mos_id :any;

  constructor(
    private _route: ActivatedRoute,
    private _Service: MosService,
  )
  {
    this._route.paramMap.subscribe((params: any) => {
      this.mos_id = params.get('id');
    });
  }

  ngOnInit(): void {
    let id  = this._route.snapshot.params['id'];
    this.isLoading=true;
    this._Service.view(id).subscribe((res:any) =>{
      this.isLoading=false;
      this.data = res;
    })
    this._Service.getManage_Type().subscribe(res => {
      this.manage = res; 
    });
  }
  getManage_Type(){
    this.isLoading = true;
    this._Service.getManage_Type().subscribe((res:any)=>{  
      this.manage = res;
      this.isLoading = false;

    })
  }
}
