import { Component, EventEmitter, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MosService } from '../../mos.service';

@Component({
  selector: 'app-ministry-dialog',
  templateUrl: './ministry-dialog.component.html',
  styleUrls: ['./ministry-dialog.component.scss']
})
export class MinistryDialogComponent implements OnInit {
  @ViewChild('CreateMiniNgForm') CreateMiniNgForm: NgForm;
  CreateMin = new EventEmitter();
  public miniForm: UntypedFormGroup;
  public saving: boolean = false;
  public isLoading : boolean = false;
  public mos = [];
  public existed = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<MinistryDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
    private _service: MosService,
  ) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    // // console.log(this.data);
    
    this.mos = this.data.mos
    this.existed = this.data.existed
    
    // // console.log('mos', this.mos)
    // // console.log('existed', this.existed)
    
    this.mos = this.mos.map(o => ({ ...o }));
    this.existed = this.existed

    this.existed.forEach(({ ministry_id }) => {
      const mo = this.mos.find(({ id }) => ministry_id === id)
      if (mo) mo.disabled = true
    })
    this.formBuilder();
    
  }
  formBuilder(): void {
    this.miniForm = this._formBuilder.group({
      object: ['', Validators.required],
      
    });
  }

  submit(){
    if(this.miniForm.value){
        this.miniForm.disable();
        this.saving = true;
        let data: object = {
          object: "["+this.miniForm.value.object.join(',')+"]"
        }
        this._service.createmini(data,this.data.id).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.CreateMin.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },err =>{
            this.miniForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.miniForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }

}
