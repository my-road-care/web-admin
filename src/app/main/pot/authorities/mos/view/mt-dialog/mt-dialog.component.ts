import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MosService } from '../../mos.service';

@Component({
  selector: 'app-mt-dialog',
  templateUrl: './mt-dialog.component.html',
  styleUrls: ['./mt-dialog.component.scss']
})
export class MtDialogComponent implements OnInit {

  @ViewChild('CreateMtNgForm') CreateMtNgForm: NgForm;
  CreateMt = new EventEmitter();
  public mtForm: UntypedFormGroup;
  public saving: boolean = false;
  public isLoading : boolean = false;
  public mts = [];
  public existed = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<MtDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
    private _service: MosService,
    private _route: ActivatedRoute,
  ) 
  { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.mts   = this.data.mts;
    this.existed = this.data.existed;
    
    // // console.log('mts', this.mts)
    // // console.log('existed', this.existed)
    
    this.mts = this.mts.map(o => ({ ...o }));
    this.existed = this.existed

    this.existed.forEach(({ mt_id }) => {
      const mt = this.mts.find(({ id }) => mt_id === id)
      if (mt) mt.disabled = true
    })

    this.formBuilder();
  }

  formBuilder(): void {
    this.mtForm = this._formBuilder.group({
      object: ['', Validators.required],
      
    });
  }

  submit(){
    if(this.mtForm.value){
        this.mtForm.disable();
        this.saving = true;
        let data: object = {
          object: "["+this.mtForm.value.object.join(',')+"]"
        }
        this._service.createmts(data,this.data.id).subscribe((res:any) =>{
          this.isLoading = false;
          this.dialogRef.close();
          this.CreateMt.emit(res);
          this._snackBar.openSnackBar(res.message, '');
        },err =>{
            this.mtForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.mtForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
}
