import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { MosService } from '../../mos.service';
import { MtDialogComponent } from '../mt-dialog/mt-dialog.component';

@Component({
  selector: 'app-mt',
  templateUrl: './mt.component.html',
  styleUrls: ['./mt.component.scss']
})
export class MtComponent implements OnInit {
  @Input() public id: any;
  displayedColumns: string[] = ['id', 'name', 'create_at', 'action'];
  public dataSource: any;
  public isSearching: boolean = false;
  public isClosing: boolean = false;
  public total: number = 10;
  public limit: number = 10;
  public page: number = 1;
  public key: string = '';
  public data: any;
  public mts: any;

  constructor(
    private _service: MosService,
    private _dialog: MatDialog,
    private _router: Router,
    private _snackBar: SnackbarService,
    private _loadingService: LoadingService,
  ) { }

  ngOnInit(): void {

    this._service.getmts().subscribe(res => {
      this.mts = res;
    });
    this.listing();
  }

  listing(): any {
    this.isSearching = true;
    this._service.listingMt(this.id).subscribe(
      (res: any) => {
        this.isSearching = false;
        this.data = res;
        this.dataSource = new MatTableDataSource(this.data);
      },
      (err: any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.', 'error');
      }
    );
  }

  createMts() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      id: this.id,
      mts: this.mts,
      existed: this.data
    }
    dialogConfig.width = "700px";
    const dialogRef = this._dialog.open(MtDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.CreateMt.subscribe((response: any) => {
      this.listing();
      this.dataSource = new MatTableDataSource(this.data);

    });
  }

  OnDelete(data: any = null): void {

    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._service.deletemts(this.id, data.id).subscribe(
          (res: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            let copy: any[] = [];
            this.data.forEach((obj: any) => {
              if (obj.id !== data.id) {
                copy.push(obj);
              }
            });
            this.data = copy;
            this.dataSource = new MatTableDataSource(this.data);

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
          }
        );
      }
    });
  }
}
