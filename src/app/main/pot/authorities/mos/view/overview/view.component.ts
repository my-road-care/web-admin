import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MosService } from '../../mos.service';
import { environment as env } from 'environments/environment';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { GlobalConstants } from "app/shared/global-constants";

@Component({
  selector: 'app-Overview',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class OverViewComponent implements OnInit {

  @Input() public data:any;
  @Input() public manage:any;

  public MosForm: FormGroup;
  public fileUrl :any = env.fileUrl;
  public isLoading: Boolean = false;
  public isSearching  :boolean = false
  public mode:any;
  public olephone:string;
  public passwordCurrct:boolean=true;
  public fs:any;
  public show: boolean = false;
  public src: string = null;
  public isloading : boolean = false;
  public saving : boolean = false;

  constructor(
    private _moservice: MosService,
    private _snackBar: SnackbarService,
    private _route: Router,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getBase64FromUrl('https://lh3.googleusercontent.com/i7cTyGnCwLIJhT1t2YpLW-zHt8ZKalgQiqfrYnZQl975-ygD_0mOXaYZMzekfKW_ydHRutDbNzeqpWoLkFR4Yx2Z2bgNj2XskKJrfw8').then(console.log)
    if (this.data?.data?.user?.avatar) {
      this.src = this.fileUrl + this.data?.data?.user?.avatar;
    } else {
      this.src = null;
    }
    console.log(this.data);
    
    this._building();
    this._moservice.getManage_Type().subscribe(res => {
      this.manage = res; 
      console.log(this.manage);
      
    });
  }
  getBase64FromUrl = async (url) => {
    const data = await fetch(url,{ mode: 'no-cors'});
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64data = reader.result;
        resolve(base64data);
      }
    });
  }
  public size: number = 0;
  public type: string = '';
  public error_size: string = '';
  public error_type: string = '';

  fileChangeEvent(e: any): void {
    if (e?.target?.files) {
      this.error_size = '';
      this.error_type = '';
      this.show = false;
      this.saving = false;
      this.type = e?.target?.files[0]?.type;
      this.size = e?.target?.files[0]?.size;
      var reader = new FileReader();
      reader.readAsDataURL(e?.target?.files[0]);
      reader.onload = (event: any) => {
          this.src = event?.target?.result;
          this.MosForm.get('avatar').setValue(this.src);
      }
      if (this.size > 3145728) { //3mb
          this.saving = true;
          this.MosForm.get('avatar').setValue('');
          this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
      }
      if (this.type.substring(0, 5) !== 'image') {
          this.saving = true;
          this.src = '';
          this.show = true;
          this.error_size = '';
          this.MosForm.get('avatar').setValue(this.src);
          this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      }
    } else {
      this.saving = true;
      this.src = '';
      this.show = true;
      this.error_size = '';
      this.MosForm.get('avatar').setValue(this.src);
      this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      //it work when user cancel select file
    }
  }

  selectFile(): any {
      document.getElementById('portrait-file').click();
  }

  preview(): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: this.src,
          title: 'រូបភាពការិយាល័យ'
      };
      dialogConfig.width = "850px";
      const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
  }

  private _building(){
    this.MosForm        = new FormGroup({
    avatar              : new FormControl (this.data ? this.data?.data?.user?.avatar         : '' ,[Validators.required]),
    user_id             : new FormControl (this.data ? this.data?.data?.user_id              : '', []),
    mo_name             : new FormControl (this.data ? this.data?.data?.name                 : '', [ Validators.required,Validators.maxLength(60)]),
    phone               : new FormControl (this.data ? this.data?.data?.user?.phone          : '', [ Validators.required, Validators.pattern('(^[0][0-9].{7}$)|(^[0][0-9].{8}$)|(^[855][0-9].{9}$)|(^[855][0-9].{10}$)|(.+@.+\..+)')]),
    email               : new FormControl (this.data ? this.data?.data?.user?.email          : '', [ Validators.required, Validators.pattern( '(^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$)' )]),
    management          : new FormControl (this.data ? this.data?.data?.management_type_id   : '', [ Validators.required, Validators.maxLength(100)]),
    name                : new FormControl (this.data ? this.data?.data?.user?.name           : '', [ Validators.required,Validators.maxLength(60)]),
    n_of_m_ministries   : new FormControl (this.data ? this.data?.data?.n_of_m_ministries    : '', []),
    n_of_m_roads        : new FormControl (this.data ? this.data?.data?.n_of_m_roads         : '', []),
    n_of_mts            : new FormControl (this.data ? this.data?.data?.n_of_mts             : '', []),
    description         : new FormControl (this.data ? this.data?.data?.description          : '', []),
    })
  }
  submit(): void{
  
    if (this.MosForm.valid){
      this.MosForm.disable();
      this.isLoading = true;
      this._moservice.updateMos (this.data.data.id, this.MosForm.value).subscribe ((res:any) =>{
        this.isLoading =false;
        this._snackBar.openSnackBar(res.message,'');
        this.MosForm.enable();
      },
      (err) => {
        this.isloading = false;
            this.saving = false;
            this.MosForm.enable();
            let errors: any[] = [];
            errors = err.error.errors;
            let messages: any[] = [];
            let text: string = '';
            if (errors.length > 0) {
                errors.forEach((v: any) => {
                    messages.push(v.message)
                });
                if (messages.length > 1) {
                    text = messages.join('-');
                } else {
                    text = messages[0];
                }
            } else {
                text = err.error.message;
            }
            this._snackBar.openSnackBar(text, GlobalConstants.error);
      }); 
    }
  }

}
