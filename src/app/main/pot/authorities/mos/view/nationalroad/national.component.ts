import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MosService } from '../../mos.service';

@Component({
  selector: 'app-nationalroad',
  templateUrl: './national.component.html',
  styleUrls: ['./national.component.scss']
})
export class NationalComponent implements OnInit {

  @Input () public id: any; 
  displayedColumns: string[] = ['id','nationaroad','manage','total_pothole','action'];
  public dataSource: any;
  public isSearching: boolean = false;
  public total  : number  = 20;
  public limit  : number  = 20;
  public page   : number  = 1;
  public key    : string ='';
  public data : any;
  constructor(
    private _service: MosService,
    private _dialog: MatDialog,
    private _router: Router,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    
    this.listing(this.limit, this.page);
  }
  listing( _limit: number = 20, _page: number = 1): any {

    const params: any = {
      limit: _limit,
      page: _page,
    };
    this.isSearching = true;
    this._service.listingNR(this.id).subscribe(
        (res: any) => {
          this.isSearching = false;
          this.total  = res.total;
          this.page   = res.current_page;
          this.limit  = res.per_page;
          this.data = res.data;
          this.dataSource = new MatTableDataSource(this.data);
        },
        (err: any) => {
            // console.log(err);
            this._snackBar.openSnackBar('Something went wrong.', 'error');
        }
    );
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

}
