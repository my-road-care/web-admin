import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const mrcRoutes : Route[] = [{

    path: 'mrc',
    children : [
        {path : '' , component : ListingComponent},
    ]
}]