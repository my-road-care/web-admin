import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { mrcRoutes } from './mrc.routing';
import { NgApexchartsModule } from "ng-apexcharts";
@NgModule({
    imports: [
      SharedModule,
      NgApexchartsModule,
      RouterModule.forChild(mrcRoutes)
    ],
    declarations: [
      ListingComponent
    ]
})
export class MrcModule{}