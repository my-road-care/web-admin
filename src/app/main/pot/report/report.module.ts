import { NgModule } from '@angular/core';
import { MonthlyModule } from './monthly/monthly.module';
import { MrcModule } from './mrc/mrc.module';
import { MucModule } from './muc/muc.module';
import { ProvincailChartModule } from './provincail_chart/provincail.module';
import { ProvinceModule } from './province/province.module';
import { TrcModule } from './trc/trc.module';

@NgModule({
    imports: [
        MonthlyModule,
        MrcModule,
        MucModule,
        ProvincailChartModule,
        ProvinceModule,
        TrcModule
    ],
    exports: [
        MonthlyModule,
        MrcModule,
        MucModule,
        ProvincailChartModule,
        ProvinceModule,
        TrcModule
    ]
})
export class ReportModule{}