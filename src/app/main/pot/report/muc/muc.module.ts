import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { mucRoutes } from './muc.routing';
@NgModule({
    imports: [
      SharedModule,
      NgApexchartsModule,
      RouterModule.forChild(mucRoutes)
    ],
    declarations: [
      ListingComponent,
    ],
    providers: [
      
    ]
})
export class MucModule{}