import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MonthlyUserChartService {
 
    
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };
    constructor(private http: HttpClient) { }
    //===========================================================================================>> get Statistic
    getuserreport() {
        const httpOptions = {};
        return this.http.get(this.url + '/cp/dashboard', httpOptions);
    }
}