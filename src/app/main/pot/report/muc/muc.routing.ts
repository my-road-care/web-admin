import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const mucRoutes : Route[] = [{
    path: 'muc',
    children : [
        {path: '' , component: ListingComponent},
    ]
}]