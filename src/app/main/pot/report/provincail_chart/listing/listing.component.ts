  
import { Component, OnInit, ViewChild } from "@angular/core";
import {ApexAxisChartSeries,ApexChart,ChartComponent,ApexTooltip,ApexYAxis,ApexTitleSubtitle,ApexXAxis,ApexFill} from "ng-apexcharts";
import { ProvincailChartService } from "../provincail.service";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  title: ApexTitleSubtitle;
  labels: string[];
  stroke: any; 
  dataLabels: any; 
  fill: ApexFill;
  tooltip: ApexTooltip;
};


@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit{
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public isSearching : boolean = false;

  ngOnInit(): void {
    this._infoService.getprovincailreport().subscribe((res: any) => {
      // this.data = res;
      // this.data.sum.series = this.data.sum.keys.map((k) => res[k]);
      // console.log(this.data);
      console.log(res);
     
    });
  }
  
  constructor(
    private _infoService:ProvincailChartService,
  ) {
    this.chartOptions = {
      series: [
        {
          name: "របាយការណ៍ការរាយការណ៍",
          type: "column",
          data: [1426, 3047, 433, 284, 170, 128, 113, 55, 178, 485, 104, 181,82, 41, 70, 83, 42, 173, 62, 34, 20, 71, 18, 23,2]
        },
        {
          name: "ជួសជុលេរួច",
          type: "line",
          data: [505,459, 138,95, 63,39, 30,30, 30,29, 26,23, 23,18, 17,16, 14,9, 7,6, 5,5, 3,3,2]
        }
      ],
      chart: {
        height: 350,
        type: "line"
      },
      stroke: {
        width: [0, 4]
      },
      title: {
        text: ""
      },
      dataLabels: {
        enabled: true,
        enabledOnSeries: [1]
      },
      labels: [
        
      ],
      xaxis: {
        categories: [
          "បាត់ដំបង", "ភ្នំពេញ","កណ្ដាល", "កំពង់ស្ពឺ", "កំពង់ចាម", "កំពត", "កំពង់ឆ្នាំង","ពោធិ៍សាត់","ព្រៃវែង","សៀមរាប","កំពង់ធំ","តាកែវ","ស្វាយរៀង","ក្រចះ","ឧត្តមានជ័យ","បន្ទាយមានជ័យ","មណ្ឌលគីរី","ព្រះសីហនុ","ត្បូងឃ្មុំ","ព្រះវិហារ","កែប","រតនះគីរី","កោះកុង","ស្ទឹងត្រែង","ប៉ៃលិន"
        ],
      },
      yaxis: [
        {
          title: {
            text: "របាយការណ៍ការរាយការណ៍"
          }
        },
        {
          opposite: true,
          title: {
            text: "ជួសជុលេរួច"
          }
        }
      ]
    };
  }
}

