import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { provincailRoutes } from './provincail.routing';
import { ScrollbarModule } from 'helpers/directives/scrollbar';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgApexchartsModule } from 'ng-apexcharts';

@NgModule({
    imports: [
      SharedModule,
      ScrollbarModule,
      NgxDropzoneModule,
      NgApexchartsModule,
      RouterModule.forChild(provincailRoutes)
    ],
    declarations: [
      ListingComponent
    ]
})
export class ProvincailChartModule{}