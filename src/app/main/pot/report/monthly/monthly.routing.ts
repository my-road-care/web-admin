import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const monthlyRoutes : Route[] = [{
    path : 'monthly',
    children : [
        
        {path : '' , component : ListingComponent},
    ]
}]