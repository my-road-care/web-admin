
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root',
})
export class MonthlyService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}
    listingMonthly(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/reports/monthly', httpOptions);
    }
}
