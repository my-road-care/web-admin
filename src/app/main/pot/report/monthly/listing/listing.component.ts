import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MonthlyService } from '../monthly.service';
import * as _moment from 'moment';
import { Animations } from 'helpers/animations';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM-YYYY',
  },
  display: {
    dateInput: 'MM-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY MMMM ',
  },
};
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss'],
  animations: Animations,
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['month','user','all_report','pending','declined','repairing','done','fixed','in_planning','specialist'];
    // public isLoading: boolean = false;
    public tagsEditMode: boolean = false;
    public disableClose: boolean =false;
    public isSearching: boolean =false;
    public monthNumber :any;
    public data : any  = [];
    public total  : number  = 10;
    public limit  : number  = 10;
    public page   : number  = 1;
    public dataSource: any;

    maxDate = new Date();
    public from: any ;
    public to: any;
    public monthName: any;
    public datePipe: any;
        
  constructor(
    private _service: MonthlyService,
    private _snackBar: SnackbarService,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing();
  }

  // ===============================================Function Listing
  listing(): any {

    const params: any = {
      
    };

    // search Date
      params.month_from = moment(this.from).format('MM-YYYY');
    
      params.month_to = moment(this.to).format('MM-YYYY');
    
    // if(this.monthNumber = moment(this.from).format('MM-YYYY')){
    //   this.monthName = this.datePipe.transform(this.monthNumber.toString(), 'MMMM');
      
    // }

    this.isSearching = true;
    this._service.listingMonthly(params).subscribe(
        (res: any) => {
          this.isSearching = false;
          this.data = res;
          // console.log(this.data);
          
          this.dataSource = new MatTableDataSource(this.data);
        },
        (err: any) => {
            this.isSearching = false;
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
        }
    );
  }

  getTotal(key:string) {
    return this.data.map(data =>data[key]).reduce((total, value) => {
      return total + value
    }, 0);
  }
  
}