import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { monthlyRoutes } from './monthly.routing';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(monthlyRoutes)
    ],
    declarations: [
      ListingComponent,
    ]
})
export class MonthlyModule{}