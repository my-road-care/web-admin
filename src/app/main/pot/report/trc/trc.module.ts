import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { trcRoutes } from './trc.routing';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ScrollbarModule } from 'helpers/directives/scrollbar';
import { NgxDropzoneModule } from 'ngx-dropzone';
@NgModule({
    imports: [
      SharedModule,
      ScrollbarModule,
      NgxDropzoneModule,
      NgApexchartsModule,
      RouterModule.forChild(trcRoutes),
    ],
    declarations: [
      ListingComponent,
    ],
    providers: [
      
    ]
})
export class TrcModule{}