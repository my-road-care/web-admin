import { Component, OnInit } from "@angular/core";
import { ApexOptions } from "ng-apexcharts";
import { TotalReportChartService } from "../trc.service";

@Component({
  selector: "app-listing",
  templateUrl: "./listing.component.html",
  styleUrls: ["./listing.component.scss"],
})
export class ListingComponent implements OnInit {
  public workchart: ApexOptions;
  public isSearching: boolean = false;
  public data: any = {
    sum: {
      series: [],
      keys: ["pending", "repairing", "planning", "specialist"],
      labels: [
        "រង់ចាំដោះស្រាយ",
        "ជួសជុលរួច",
        "ផែនការឆ្នាំ",
        "បញ្ជូនទៅសមត្ថកិច្ចផ្សេង",
      ],
    },
  };
  constructor(private _infoService: TotalReportChartService) {}

  ngOnInit(): void {
    this._infoService.gettotalreport().subscribe((res: any) => {
      // this.data = res;
      this.data.sum.series = this.data.sum.keys.map((k) => res[k]);
      // console.log(this.data);
      console.log(res);
      this.getchartreport();
    });
  }

  getchartreport(): void {
    this.workchart = {
      chart: {
        width: 520,
        type: 'pie',
      },
       
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
        },
      }],
      dataLabels: { enabled: false },
      colors: ["#D81C17", "#008E51", "#2fadad", "#0f4ddb"],
      labels: this.data.sum.labels,
      series: this.data.sum.series,
      tooltip: {
        enabled: true,
        fillSeriesColor: false,
        theme: "dark",
        custom: ({ seriesIndex, w }): string =>
          `<div class="flex items-center h-8 min-h-8 max-h-8 px-3">
              <div class="w-3 h-3 rounded-full" style="background-color: ${w.config.colors[seriesIndex]};"></div>
              <div class="ml-2 text-md leading-none">${w.config.labels[seriesIndex]}:</div>
              <div class="ml-2 text-md font-bold leading-none">${w.config.series[seriesIndex]}</div>
          </div>`,
      },
    };
  }
}
