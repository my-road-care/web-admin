import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const trcRoutes: Route[] = [{
    path: 'trc',
    children: [
        { path  : '',  component: ListingComponent },
    ]
}];