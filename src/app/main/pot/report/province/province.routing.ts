import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const provinceRoutes: Route[] = [{
    path: 'provinces',
    children: [
        { path  : '',  component: ListingComponent },
    ]
}];