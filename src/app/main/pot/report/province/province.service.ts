import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ProvinceService {

    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }

    //==============================================>> Listing
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/reports/province', httpOptions);
    }
    // ==============================================>> Get MTS
    getmts_name(){
        return this.http.get(this.url + '/cp/reports/mts');
    }
    getsms(params:any){
        var formData: any = new FormData();
        formData.append('way',params.way);
        formData.append('objs',JSON.stringify(params.objs));
        return this.http.post(this.url + '/cp/reports/send',formData);
    }
}