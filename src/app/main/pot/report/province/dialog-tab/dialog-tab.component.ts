import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ProvinceService } from '../province.service';

@Component({
  selector: 'app-dialog-tab',
  templateUrl: './dialog-tab.component.html',
  styleUrls: ['./dialog-tab.component.scss']
})
export class DialogTabComponent implements OnInit {

  @ViewChild('ChangePasswordNgForm') ChangePasswordNgForm: NgForm;
  ChangePassword    = new EventEmitter();
  public changeForm :UntypedFormGroup;
  public mts : any;
  public isLoading : boolean =false;
  public data:any;

  constructor(
  ) 
  { }

  ngOnInit(): void {
    
  }
}
