import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ProvinceService } from '../../province.service';

@Component({
  selector: 'app-option1',
  templateUrl: './option1.component.html',
  styleUrls: ['./option1.component.scss']
})
export class Option1Component implements OnInit {

  @Input () public data:any;
  public form: FormGroup;
  public ways:any='';
  public isSearching : boolean = false;
  public object :any = [];
  public isLoading :boolean = false;
  constructor(
    private dialogRef: MatDialogRef<Option1Component>,
    private _service: ProvinceService,
    private _snackBar: SnackbarService,
  ) 
  { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.isSearching = true;
    this._service.getmts_name().subscribe(res => {
      this.isSearching = false;
      this.data = res;
      // // console.log(this.data);
    });
    this.buildForm();
  }
  private buildForm(){
    this.form               = new FormGroup({
      way                   : new FormControl(),
      objs                  : new FormControl('', [Validators.required]),
    });
  }
  ChangeOption(type){
    if(type==='sms'){
      this.form.get('way').setValue("sms");
    }
  }
  ChangeObject(event,row){
   if(event.checked ){
    this.object.push(row);
   }else{
    this.object.splice(this.object.indexOf(row));
   }
   this.form.get('objs').setValue(this.object);
  }
  submit(){
  if (this.form.valid){
      const params: any = {
      way: this.form.get('way').value,
      objs : this.form.get('objs').value,
  };
  // // console.log(params)
    this.form.disable();
    this.isLoading = true;
    this._service.getsms(params).subscribe ((res:any) =>{
      this.isLoading =false;
      this.dialogRef.close();
      this._snackBar.openSnackBar('SMSបញ្ជូនបានជោគជ័យ!','');
    }, (err:any) => {
      this.form.enable();
      this.isLoading=false;
      this.dialogRef.close();
      for(let key in err.error.errors){
      let control = this.form.get(key);
      control.setErrors({'servererror':true});
      control.errors.servererror = err.error.errors[key][0];
      }
    }); 
  }
}
}
