import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { provinceRoutes } from './province.routing';
import { DialogTabComponent } from './dialog-tab/dialog-tab.component';
import { Option1Component } from './dialog-tab/option1/option1.component';
import { Option2Component } from './dialog-tab/option2/option2.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(provinceRoutes)
  ],
  declarations: [
    ListingComponent,
    DialogTabComponent,
    Option1Component,
    Option2Component,
  ]
})
export class ProvinceModule{}