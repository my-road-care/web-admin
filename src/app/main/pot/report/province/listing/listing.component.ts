import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { DialogTabComponent } from '../dialog-tab/dialog-tab.component';
import { ProvinceService } from '../province.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id','province','t_report','request','reject','wait_solution','solutioned','fixed','plan_year','refer_authority'];
    // public isLoading: boolean = false;
    public tagsEditMode: boolean = false;
    public disableClose: boolean =false;
    public isSearching: boolean =false;
    public province : any[]   = [];
    public total  : number  = 10;
    public limit  : number  = 10;
    public page   : number  = 1;
    public dataSource: any;
        
  constructor(
    private _service: ProvinceService,
    private _snackBar: SnackbarService,
    private _dialog: MatDialog,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.isSearching = true;
    this.listing();
    // window['listing'] = this
  }

  // ===============================================Function Listing
  listing(): any {
    let  params :any;
    this.isSearching = true;
    this._service.listing(params).subscribe(
        (res: any) => {
          this.isSearching = false;
          this.province = res.province;
          // console.log(this.province);
          this.dataSource = new MatTableDataSource(this.province);
        },
        (err: any) => {
            this.isSearching = false;
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
        }
    );
  }

  getTotal(key: string) {
    return this.province.map(p => p.pothole[key]).reduce((total, value) => {    
      return total + value
  }, 0);
  }

  // Send SMS
  GetMts():void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = "px";
    dialogConfig.autoFocus = false;
    const dialogRef = this._dialog.open(DialogTabComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.ChangePassword.subscribe((response: any) => {
      
      // this.dataSource = new MatTableDataSource(this.data);
    });
  }
}
