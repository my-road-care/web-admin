import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { main_groupRoutes } from './main_group.routing';
import { UpdateGrouopComponent } from './update/update.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(main_groupRoutes)
    ],
    declarations: [
      ListingComponent,
      UpdateGrouopComponent
    ]
})
export class MainGroupModule{}