import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const main_groupRoutes: Route[] = [
    { path: 'groups', component: ListingComponent }
]