import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const main_subRoutes : Route[] = [
    {path : 'subs', component : ListingComponent}
]