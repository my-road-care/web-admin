import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { main_subRoutes } from './main_sub.routing';
import { UpdateSubComponent } from './update/update.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(main_subRoutes)
    ],
    declarations: [
      ListingComponent,
      UpdateSubComponent
    ]
})
export class MainSubModule{}