import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Main_subService } from '../main_sub.service';
import { UpdateSubComponent } from '../update/update.component';
import * as _moment from 'moment';
import { Animations } from 'helpers/animations';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY MMMM ',
  },
};

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss'],
  animations: Animations,
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id','kh_name','en_name','code','date','action'];
  public isSearching: boolean = false;
  public selectedAcademic: number;
  public data : any[]   = [];
  public total: number = 10;
  public limit: number = 10;
  public page: number = 1;
  public dataSource: any;
  public key: string = '';
  maxDate = new Date();
  public from: any=null;
  public to: any=null;

  constructor(
    private _service: Main_subService,
    private _dialog: MatDialog,
    private _snackBar: SnackbarService, 
    private _router: Router,

  ){}

  ngOnInit(): void {
    this.isSearching = true;
    this.listing(this.limit,this.page);
      
  }
  // =======================================================>> Function List
  listing(_limit: number = 10, _page: number = 1):any {

    const param: any = {
      limit: _limit,
      page: _page,
    };
    // Search name
    if (this.key != '') {
      param.key = this.key.trim();
    }
    // Search Date
    // param.from = moment(this.from).format('YYYY-MM-DD');
    // param.to = moment(this.to).format('YYYY-MM-DD');

    this.isSearching = true;
     this._service.listing(param).subscribe(
      (res:any) =>{
        this.isSearching = false;
        this.total  = res.total;
        this.page   = res.current_page;
        this.limit  = res.per_page;
        this.data = res.data;
      this.dataSource = new MatTableDataSource (this.data);
      },
      (err :any) => {
        // console.log(err);
        this._snackBar.openSnackBar('Something went wrong.','error');
      }
     )
  }
  
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

  UpdateSub(row:any) : void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      data    : row,
    }
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(UpdateSubComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.UpdateGroup.subscribe((response: any) => {
    this.listing();
    this.dataSource = new MatTableDataSource(this.data);
    })
  }
}
