import { NgModule } from '@angular/core';
import { MainCodeModule } from './main_code/main_code.module';
import { MainGroupModule } from './main_group/main_group.module';
import { MainSubModule } from './main_sub/main_sub.module';
import { MainTypeModule } from './main_type/main_type.module';
import { MainUnitModule } from './main_unit/main_unit.module';

@NgModule({
    imports: [
        MainCodeModule,
        MainGroupModule,
        MainSubModule,
        MainTypeModule,
        MainUnitModule
    ],
    exports: [
        MainCodeModule,
        MainGroupModule,
        MainSubModule,
        MainTypeModule,
        MainUnitModule
    ]
})
export class MaintenanceModule{}