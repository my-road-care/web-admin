import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { main_unitRoutes } from './main_unit.routing';
import { UpdateUnitComponent } from './update/update.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(main_unitRoutes)
    ],
    declarations: [
      ListingComponent,
      UpdateUnitComponent
    ]
})
export class MainUnitModule{}