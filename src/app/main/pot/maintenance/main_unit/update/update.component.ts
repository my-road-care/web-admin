import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Main_unitService } from '../main_unit.service';

@Component({
  selector: 'app-view',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateUnitComponent implements OnInit {

  @ViewChild('UpdateMainSubNgForm') UpdateMainSubNgForm: NgForm;
  UpdateGroup    = new EventEmitter();
  public SubForm :UntypedFormGroup;
  public saving   : boolean =false;
  public isLoading : boolean = false;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<UpdateUnitComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _main_unitService : Main_unitService,
    private _snackBar: SnackbarService,
  ) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    // console.log(this.data.data.id);
    this.formBuilder();
  }

  formBuilder(): void {
    this.SubForm = this._formBuilder.group({
      kh_name :         [this.data?.data?.kh_name , Validators.required],
      en_name :         [this.data?.data?.en_name , Validators.required ],
    });
  }
  submit(){
    // console.log(this.SubForm.value);
    if(this.SubForm.value){
        this.SubForm.disable();
        this.saving = true;
        this._main_unitService.update(this.data.data.id,this.SubForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.UpdateGroup.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },(err:any) =>{
            this.SubForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.SubForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }

}
