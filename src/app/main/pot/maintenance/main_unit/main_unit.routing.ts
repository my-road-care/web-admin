import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";
import { UpdateUnitComponent } from "./update/update.component";

export const main_unitRoutes : Route[] = [
    {path : 'units',component : ListingComponent}
]