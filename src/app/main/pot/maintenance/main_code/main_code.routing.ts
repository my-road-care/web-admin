import { Route } from "@angular/router";
import { CreateDialogComponent } from "./create-dialog/create-dialog.component";
import { ListingComponent } from "./listing/listing.component";

export const main_codeRoutes: Route[] = [{
    path: 'codes',
    children: [
        { path: '', component: ListingComponent },
        { path: 'create', component: CreateDialogComponent }
    ]
}]