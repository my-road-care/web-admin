import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment  as env} from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class Main_codeService {

    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    constructor(private http: HttpClient) { }

    //============================================== >> Listing
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/cp/setting/maintence-codes', httpOptions);
    }
    // ============================================== >> Create
    create(data: any = {}): any {
        return this.http.post(this.url + '/cp/setting/maintence-codes', data, this.httpOptions);
    }
    // ============================================== >> Get Date Group
    getgroup(){
        return this.http.get(this.url + '/cp/quick-get/maintences_group');
    }
    // ============================================== >> Get Date Unit
    getunit(){
        return this.http.get(this.url + '/cp/quick-get/maintences_unit');
    }
    // ============================================== >> Get Date Type
    gettype(){
        return this.http.get(this.url + '/cp/quick-get/maintences_type');
    }
    // ============================================== >> Get Date SubType
    getsubtype(){
        return this.http.get(this.url + '/cp/quick-get/maintences_subtype');
    }
    // =============================================>> Service Delete
    delete(maincode_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/cp/setting/maintence-codes/' + maincode_id , this.httpOptions);
    }
    // =============================================>> Service update Main_code
    update(id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/cp/setting/maintence-codes/'+id+ '?_method=PUT', data, this.httpOptions);
    }
}