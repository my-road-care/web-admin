import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Main_codeService } from '../main_code.service';

@Component({
  selector: 'app-create',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.scss']
})
export class CreateDialogComponent implements OnInit {

  @ViewChild('CreateMainCodeNgForm') CreateMainCodeNgForm: NgForm;
  CreateMainCode    = new EventEmitter();
  public mainForm   : UntypedFormGroup;
  public saving     : boolean = false;
  public isLoading  : boolean = false;
  public group:any;
  public unit:any;
  public type:any;
  public subtype:any;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CreateDialogComponent>,
    private _main_codeService : Main_codeService,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
  ) 
  { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.group   = this.data.group;
    this.unit    = this.data.unit;
    this.type    = this.data.type;
    this.subtype = this.data.subtype;
    this.formBuilder();
   
  }
  formBuilder(): void {
    this.mainForm = this._formBuilder.group({
      code: ['', [Validators.required]],
      rate: ['', [Validators.required]],
      kh_name: ['',Validators.required],
      en_name: ['' ,Validators.required],
      group: ['', Validators.required], 
      unit: ['',Validators.required],
      type: ['' ,Validators.required],
      subtype: ['', Validators.required ],
      description: ['' ],
    });
  }

  submit(){
    // // console.log(this.mainForm.value);
    if(this.mainForm.value){
        this.mainForm.disable();
        this.saving = true;
        this._main_codeService.create(this.mainForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.CreateMainCode.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },err =>{
            this.mainForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.mainForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
  
}
