import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
import { CreateDialogComponent } from '../create-dialog/create-dialog.component';
import { Main_codeService } from '../main_code.service';
import { ViewComponent } from '../view/view.component';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id', 'code', 'name', 'group', 'type', 'sub_type', 'unit', 'pothole', 'action'];
  public isSearching: boolean = false;
  public isLoading: boolean = false;
  public selectedAcademic: number;
  public data: any[] = [];
  public total: number = 20;
  public limit: number = 20;
  public page: number = 1;
  public dataSource: any;
  public id: any;
  public name: string;
  public key: string = "";
  public group: any;
  public unit: any;
  public type: any;
  public subtype: any;
  public isClosing: boolean = false;

  constructor(
    private _service: Main_codeService,
    private _dialog: MatDialog,
    private _snackBar: SnackbarService,
    private _router: Router,
    private _loadingService: LoadingService

  ) { }

  ngOnInit(): void {
    this._service.getgroup().subscribe(res => {
      this.group = res;
    });
    this._service.getunit().subscribe(res => {
      this.unit = res;
    });
    this._service.gettype().subscribe(res => {
      this.type = res;
    });
    this._service.getsubtype().subscribe(res => {
      this.subtype = res;
    });
    this.isSearching = true;
    this.listing(this.limit, this.page);


  }
  // =======================================================>> Function List
  listing(_limit: number = 20, _page: number = 1): any {

    const param: any = {
      limit: _limit,
      page: _page,
    };

    if (this.key != '') {
      param.key = this.key.trim();
    }
    this.isSearching = true;
    this._service.listing(param).subscribe(
      (res: any) => {
        this.isSearching = false;
        this.total = res.total;
        this.page = res.current_page;
        this.limit = res.per_page;
        this.data = res.data;
        // console.log(this.data);
        // Add new Column
        let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        // console.log(this.data);
        this.dataSource = new MatTableDataSource(this.data);
      },
      (err: any) => {
        this.isSearching = false;
        this._snackBar.openSnackBar('Something went wrong.', 'error');
        // console.log(err);
      }
    )
  }
  onPageChanged(event: any): any {
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
    }
  }

  CreateMain() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      group: this.group,
      unit: this.unit,
      type: this.type,
      subtype: this.subtype
    }
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(CreateDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.CreateMainCode.subscribe((response: any) => {
      this.listing();
      this.data.push(response.data);
      this.dataSource = new MatTableDataSource(this.data);
    });
  }

  getGroup_id() {
    this.isLoading = true;
    this._service.getgroup().subscribe((res: any) => {
      this.group = res;
      this.isLoading = false;
    })
  }
  getUnit_id() {
    this.isLoading = true;
    this._service.getunit().subscribe((res: any) => {
      this.unit = res;
      this.isLoading = false;
    })
  }
  getType_id() {
    this.isLoading = true;
    this._service.gettype().subscribe((res: any) => {
      this.type = res;
      this.isLoading = false;
    })
  }
  getSubtype_id() {
    this.isLoading = true;
    this._service.getsubtype().subscribe((res: any) => {
      this.subtype = res;
      this.isLoading = false;
    })
  }

  onDelete(data: any): void {

    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._service.delete(data.id).subscribe(
          (res) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            this.listing();

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
            // console.log(err);
          }
        );
      }
    });
  }
  UpdateMain(row: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      data: row,
      group: this.group,
      unit: this.unit,
      type: this.type,
      subtype: this.subtype
    }
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(ViewComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.UpdateMain.subscribe((response: any) => {
      this.listing();
      this.dataSource = new MatTableDataSource(this.data);
    })
  }
}