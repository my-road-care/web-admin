import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Main_codeService } from '../main_code.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  @ViewChild('UpdateMainCodeNgForm') UpdateMainCodeNgForm: NgForm;
  UpdateMain    = new EventEmitter();
  public mainForm :UntypedFormGroup;
  public saving   : boolean =false;
  public isLoading : boolean = false;
  public group:any;
  public unit:any;
  public type:any;
  public subtype:any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ViewComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _main_codeService : Main_codeService,
    private _snackBar: SnackbarService,
  )
  { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    
    this.group   = this.data.group;
    this.unit    = this.data.unit;
    this.type    = this.data.type;
    this.subtype = this.data.subtype;
    this.formBuilder();
  }

  formBuilder(): void {
    this.mainForm = this._formBuilder.group({
      code :         [this.data?.data?.code , Validators.required],
      rate :         [this.data?.data?.rate , Validators.required ],
      kh_name :      [this.data?.data?.kh_name ,Validators.required],
      en_name :      [this.data?.data?.en_name ,Validators.required],
      group :        [this.data?.data?.group?.id , Validators.required], 
      unit :         [this.data?.data?.unit?.id ,Validators.required],
      type :         [this.data?.data?.type?.id ,Validators.required],
      subtype :      [this.data?.data?.subtype?.id , Validators.required ],
      description:   [this.data?.data?.description ],
    });
  }
  submit(){
    // console.log(this.mainForm.value);
    if(this.mainForm.value){
        this.mainForm.disable();
        this.saving = true;
        this._main_codeService.update(this.data.data.id,this.mainForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.UpdateMain.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },err =>{
            this.mainForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.mainForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
}
