import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { CreateDialogComponent } from './create-dialog/create-dialog.component';
import { ListingComponent } from './listing/listing.component';
import { main_codeRoutes } from './main_code.routing';
import { ViewComponent } from './view/view.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(main_codeRoutes)
    ],
    declarations: [
      ListingComponent,
      CreateDialogComponent,
      ViewComponent
    ]
})
export class MainCodeModule{}