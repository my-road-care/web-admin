import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Main_typeService } from '../main_type.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateTypeComponent implements OnInit {

  @ViewChild('UpdateMainTypeNgForm') UpdateMainTypeNgForm: NgForm;
  UpdateType    = new EventEmitter();
  public groupForm :UntypedFormGroup;
  public saving   : boolean =false;
  public isLoading : boolean = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<UpdateTypeComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _main_typeService : Main_typeService,
    private _snackBar: SnackbarService,
  ) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.formBuilder();
  }

  formBuilder(): void {
    this.groupForm = this._formBuilder.group({
      kh_name :         [this.data?.data?.kh_name , Validators.required],
      en_name :         [this.data?.data?.en_name , Validators.required ],
    });
  }
  submit(){
    // console.log(this.groupForm.value);
    if(this.groupForm.value){
        this.groupForm.disable();
        this.saving = true;
        this._main_typeService.update(this.data.data.id,this.groupForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.UpdateType.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        },err =>{
            this.groupForm.enable();
            this.saving = false;
            this.dialogRef.close();
            for(let key in err.error.errors){
            let control = this.groupForm.get(key);
            control.setErrors({'servererror':true});4
            control.errors.servererror = err.error.errors[key][0];
            }
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
}
