import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListingComponent } from './listing/listing.component';
import { main_typeRoutes } from './main_type.routing';
import { UpdateTypeComponent } from './update/update.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(main_typeRoutes)
    ],
    declarations: [
      ListingComponent,
      UpdateTypeComponent
    ]
})
export class MainTypeModule{}