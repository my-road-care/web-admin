import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const main_typeRoutes : Route[] = [
    {path : 'types',component : ListingComponent}
]