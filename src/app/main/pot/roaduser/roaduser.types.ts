export interface ListUsers {
    data: Data[],
    pagination: Pagination
}

export interface ListReports {
    data: Report[],
    pagination: Pagination
}


export interface Data {
    rn: number,
    id: number,
    created_at: Data,
    user_id: number,
    n_of_reports: number,
    n_of_paddings: number,
    n_of_repairings: number,
    n_of_declineds: number,
    n_of_fixeds: number,
    n_of_plannings: number,
    n_of_specialists: number,
    n_of_dones: number,
    user: User
}

export interface Report {
    rn: number,
    id: number,
    description: string,
    created_at: Date,
    file?: { id: number, uri: string } | null,
    location: { village: Location, commune: Location, district: Location, province: Location },
    point?: {pk: string, road: {id: null, name: string}} | null
}

interface Location {
    id: number,
    name: string
}

export interface User {
    id: number,
    name: string,
    phone: string,
    email?: string | null,
    avatar?: string | null,
    is_phone_verified: number,
    is_email_verified: number,
    social_type_id: number,
    social?: { id: number, name: string } | null
}

interface Pagination {
    total: number,
    from: number,
    per_page: number,
    current_page: number,
    total_pages: number
}

export interface ResponseUpdate {
    status: number | 200,
    message: string
}

export interface ReqPutPassword {
    id: number,
    passwork: string
}