import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { ViewComponent } from './view/view.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { roaduserRoutes } from './roaduser.routing';
import { OverviewComponent } from './view/overview/overview.component';
import { ReportComponent } from './view/report/report.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(roaduserRoutes)
    ],
    declarations: [
        ListingComponent,
        ViewComponent,
        OverviewComponent,
        ReportComponent,
        UpdatePasswordComponent
    ]
})
export class RoadUserModule { }