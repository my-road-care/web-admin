import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { environment as env } from 'environments/environment';
import { User } from '../../roaduser.types';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

    @Input() public user: User;
    public fileUrl: any = env.fileUrl;
    public src: string = null;

    constructor(
        private _dialog: MatDialog,
    ) { }

    ngOnInit(): void {
        if (this.user.avatar) {
            this.src = this.fileUrl + this.user.avatar;
        } else {
            this.src = null;
        }
    }

    preview(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: this.src,
            title: 'អ្នកប្រើប្រាស់'
        };
        dialogConfig.width = "850px";
        this._dialog.open(PreviewImageComponent, dialogConfig);
    }
}
