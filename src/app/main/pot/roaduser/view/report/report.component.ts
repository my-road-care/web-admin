import { Component, Input, OnInit } from '@angular/core';
import { RoadUserService } from '../../roaduser.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { environment as env } from 'environments/environment';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ListReports, Report } from '../../roaduser.types';
import { HttpErrorResponse } from '@angular/common/http';
import { GlobalConstants } from 'app/shared/global-constants';
import { PageEvent } from '@angular/material/paginator';
@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
    @Input() public id: number;
    public url: any = env.fileUrl;
    public loading: boolean = false;
    public data: Report[];
    public total: number = 20;
    public limit: number = 20;
    public page: number = 1;
    constructor(
        private roadUserService: RoadUserService,
        private matDialog: MatDialog,
        private snackbarService: SnackbarService
    ) { }
    ngOnInit(): void {
        this.listing(this.limit, this.page);
    }
    public listing(_limit: number = 20, _page: number = 1): void {
        const params: { limit: number, page: number, } = {
            limit: _limit,
            page: _page,
        };
        this.loading = true;
        this.roadUserService.reports(this.id, params).subscribe({
            next: (response: ListReports) => {
                this.loading = false;
                this.data = response.data;
                this.page = response.pagination.current_page;
                this.limit = response.pagination.per_page;
                this.total = response.pagination.total;
            },
            error: (err: HttpErrorResponse) => {
                this.loading = false;
                const error: { httpStatus: 400, message: string } = err.error;
                this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
            }
        });
    }
    public onPageChanged(event: PageEvent): void {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }
    }
    preview(src: string): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: src,
            title: 'រូបភាពអ្នកប្រើប្រាស់'
        };
        dialogConfig.width = "850px";
        this.matDialog.open(PreviewImageComponent, dialogConfig);
    }
}
