import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoadUserService } from '../roaduser.service';
import { User } from '../roaduser.types';
import { HttpErrorResponse } from '@angular/common/http';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { GlobalConstants } from 'app/shared/global-constants';

@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

    public id: number = 0;
    public loading: boolean = false;
    public data: User;
    constructor(
        private _route: ActivatedRoute,
        private roadUserService: RoadUserService,
        private snackbarService: SnackbarService
    ) {
        this._route.paramMap.subscribe((params: any) => {
            this.id = params.get('id');
        });
    }

    ngOnInit(): void {
        this.loading = true;
        this.roadUserService.view(this.id).subscribe({
            next: (response: User) => {
                this.loading = false;
                this.data = response;
            },
            error: (err: HttpErrorResponse) => {
                this.loading = false;
                var message: string = err.error.message;
                this.snackbarService.openSnackBar(message, GlobalConstants.error);
            }
        });
    }
}
