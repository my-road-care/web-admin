import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable } from 'rxjs';
import { ListReports, ListUsers, ResponseUpdate, User } from './roaduser.types';


@Injectable({
    providedIn: 'root',
})
export class RoadUserService {
    private readonly url = env.apiUrl;
    private readonly httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(private readonly http: HttpClient) { }
    listing(params = {}): Observable<ListUsers> {
        const httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
        httpOptions['params'] = params;
        return this.http.get<ListUsers>(this.url + '/cp/reporters/list', httpOptions);
    }
    // ==================================================================================>> View UserRoad
    public view(id: number = 0): Observable<User> {
        return this.http.get<User>(this.url + '/cp/reporters/view/' + id, this.httpOptions);
    }

    public delete(user_id: number = 0): Observable<{ message: string }> {
        return this.http.delete<{ message: string }>(this.url + '/cp/reporters/' + user_id, this.httpOptions);
    }

    public updatePassword(data: { id: number, password: string }): Observable<ResponseUpdate> {
        return this.http.post<ResponseUpdate>(`${this.url}/cp/reporters/change-password`, data, this.httpOptions);
    }

    //=============================================>> list Reports
    reports(id: number = 0, params: { limit: number, page: number }): Observable<ListReports> {
        const httpOptions = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
        httpOptions['params'] = params;
        return this.http.get<ListReports>(`${this.url}/cp/reporters/${id}/reports`, httpOptions);
    }
}
