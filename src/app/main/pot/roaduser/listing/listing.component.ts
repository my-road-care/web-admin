import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { RoadUserService } from '../roaduser.service';
import * as _moment from 'moment';
import { Animations } from 'helpers/animations';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { UpdatePasswordComponent } from '../update-password/update-password.component';
import { Data, ListUsers } from '../roaduser.types';
import { HttpErrorResponse } from '@angular/common/http';
import { GlobalConstants } from 'app/shared/global-constants';
import { PageEvent } from '@angular/material/paginator';

const moment = _moment;
export const MY_FORMATS = {
    parse: {
        dateInput: 'YYYY-MM-DD',
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};
@Component({
    selector: 'app-listing-roaduser',
    templateUrl: './listing.component.html',
    styleUrls: ['./listing.component.scss'],
    animations: Animations,
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    ],
})
export class ListingComponent implements OnInit {

    public displayedColumns: string[] = ['no', 'name', 'phone', 'create_at', 't_report', 'request', 'reject', 'MT_solution', 'valble', 'solution', 'fixed', 'year_plant', 'authority', 'action'];
    public dataSource: MatTableDataSource<Data>;
    public loading: boolean = false;
    public data: Data[];
    public total: number = 20;
    public limit: number = 20;
    public page: number = 1;
    public key: string = '';
    public maxDate: Date = new Date();
    public from: Date;
    public to: Date;

    constructor(
        private roadUserService: RoadUserService,
        private matDialog: MatDialog,
        private snackbarService: SnackbarService
    ) { }

    ngOnInit(): void {
        this.listing(this.limit, this.page);
    }

    public listing(_limit: number = 20, _page: number = 1): void {
        const param: { limit: number, page: number, key?: string | null, from?: string | null, to?: string | null } = {
            limit: _limit,
            page: _page,
        };
        if (this.key != '') {
            param.key = this.key.trim();
        }
        if (this.from != null) {
            param.from = moment(this.from).format('YYYY-MM-DD');
        }
        if (this.to != null) {
            param.to = moment(this.to).format('YYYY-MM-DD');
        }
        this.loading = true;
        this.roadUserService.listing(param).subscribe({
            next: (response: ListUsers) => {
                this.loading = false;
                this.data = response.data;
                this.page = response.pagination.current_page;
                this.limit = response.pagination.per_page;
                this.total = response.pagination.total;
                this.dataSource = new MatTableDataSource(this.data);
            },
            error: (err: HttpErrorResponse) => {
                this.loading = false;
                const error: { httpStatus: 400, message: string } = err.error;
                this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
            }
        });
    }
    public onPageChanged(event: PageEvent): void {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }
    }
    public delete(row: Data): void {
        const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((res: boolean) => {
            if (res) {
                this.roadUserService.delete(row.id).subscribe({
                    next: (response: { status: number, message: string }) => {
                        this.data = this.data.filter((v: Data) => v.id != row.id);
                        this.total -= 1;
                        this.dataSource = new MatTableDataSource(this.data);
                        this.snackbarService.openSnackBar(response.message, GlobalConstants.success);
                    },
                    error: (err: HttpErrorResponse) => {
                        const error: { httpStatus: 400, message: string } = err.error;
                        this.snackbarService.openSnackBar(error.message, GlobalConstants.error);
                    }
                });
            }
        });
    }
    public updatePassword(id: number = 0): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            id: id,
        }
        dialogConfig.autoFocus = false;
        dialogConfig.width = "700px";
        dialogConfig.disableClose = true;
        this.matDialog.open(UpdatePasswordComponent, dialogConfig);
    }
}
