// ===================================================================>> Core Library
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ScrollbarModule } from 'helpers/directives/scrollbar';

// ===================================================================>> Third Library

import { SharedModule } from '../../../shared/shared.module';
import { DialogComponent } from './dialog/dialog.component';
// ===================================================================>> Custom Library
import { ListingComponent } from './listing/listing.component';
import { organizationRoutes } from './organization.routing';
import { UpdateDialogComponent } from './update/dialog.component';
import { ViewProjectDialogComponent } from './view-project-dialog/view-project-dialog.component';

@NgModule({
  declarations: [
    ListingComponent,
    DialogComponent,
    UpdateDialogComponent,
    ViewProjectDialogComponent
  ],
  imports: [
    SharedModule,
    ScrollbarModule,
    RouterModule.forChild(organizationRoutes)
  ],
  providers: []
})
export class OrganizationModule { }
