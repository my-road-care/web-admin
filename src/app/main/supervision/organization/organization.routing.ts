import { Route } from "@angular/router";
import { ListingComponent} from './listing/listing.component';

export const organizationRoutes: Route[] = [{
    path: '',
    children: [
        { path  : '',  component: ListingComponent },
    ]
}];