import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { Validators, UntypedFormGroup, UntypedFormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackbarService } from "app/shared/services/snackbar.service";
import { OrganizationService } from "../organization.service";
@Component({
    templateUrl: './dialog.component.html',
})
export class DialogComponent implements OnInit {
    public types: any[] = [];
    public organization: string = null;
    public create: UntypedFormGroup;
    public saving: boolean = false;
    public createEmit = new EventEmitter();
    constructor(
        @Inject(MAT_DIALOG_DATA) public dataDialog: any,
        private _service: OrganizationService,
        public dialogRef: MatDialogRef<DialogComponent>,
        private _formBuilder: UntypedFormBuilder,
        private snackBar: SnackbarService
    ) {
        this.dialogRef.disableClose = true;
    }
    ngOnInit(): void {
        // this.types = this.dataDialog?.setup;
        // this.types.forEach((v: any)=>{
        //     if(v.id == this.dataDialog?.type_id){
        //         this.organization = v?.name;
        //     }
        // })
        // console.log(this.types);
        this.formBuilder();
    }
    formBuilder(): void {
        this.create = this._formBuilder.group({
            kh_name: [this.dataDialog?.kh_name, Validators.required],
            en_name: ['', Validators.required],
            abbre: ['', Validators.required],
        });
    }
    submit() {
        // Return if the form is invalid
        if (this.create.invalid) {
            return;
        }
        let data: object = {
            ...this.create.value,
            type_id: this.dataDialog?.type_id
        }

        // Disable the form
        this.create.disable();

        // Saving
        this.saving = true;
        this._service.create(data).subscribe((res: any) => {
            this.dialogRef.close();
            this.createEmit.emit(res.data);
            this.snackBar.openSnackBar(res.message, '');
        }, (err: any) => {
            // Re-enable the form
            this.create.enable();
            // saved
            this.saving = false;
            let errors: any[] = [];
            errors = err.error.errors;
            let messages: any[] = [];
            let text: string = '';
            if (errors.length > 0) {
                errors.forEach((v: any) => {
                    messages.push(v.message)
                });
                if (messages.length > 1) {
                    text = messages.join('-');
                } else {
                    text = messages[0];
                }
            } else {
                text = err.error.message;
            }
            this.snackBar.openSnackBar(text, 'error');
        })
    }
}