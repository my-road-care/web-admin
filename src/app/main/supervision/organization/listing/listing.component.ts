import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { OrganizationService } from '../organization.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { UpdateDialogComponent } from '../update/dialog.component';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { DialogRef } from '@angular/cdk/dialog';
import { ViewProjectDialogComponent } from '../view-project-dialog/view-project-dialog.component';
import { Router } from '@angular/router';
import { lowerFirst } from 'lodash';

@Component({
    selector: 'app-listing',
    templateUrl: './listing.component.html',
    styleUrls: ['./listing.component.scss'],
})
export class ListingComponent implements OnInit {

    displayedColumns: string[] = ['no', 'kh_name', 'en_name', 'abbre', 'project' , 'update_at', 'action'];
    public data: any = [];
    public total: number = 20;
    public limit: number = 20;
    public page: number = 1;
    public dataSource: any;
    public searchValue: String;
    public setup: any = null;
    public type_id: number = 1;
    public isSearching: boolean = false;
    constructor(
        private _snackbar: SnackbarService,
        private _service: OrganizationService,
        private _dialog: MatDialog,
        private _router: Router,
        private _snackBar: SnackbarService,
    ) { }
    ngOnInit(): void {
        this._service.setup().subscribe((res: any) => this.setup = res?.data);
        this.listing();
    }

    onPageChanged(event: any) {
        this.isSearching = true;
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing();
        }
    }

    listing(): any {
        const params: any = {
           
        };
        if (this.type_id != 0) {
            params.type_id = this.type_id;
        }
        this.isSearching = true;
        this._service.listing(params).subscribe((res: any) => {
            this.isSearching = false;
            this.data = res;
            console.log(this.data);
            
            this.dataSource = new MatTableDataSource(this.data);
        },
        (err :any) => {
        // console.log(err);
            this.isSearching = false;
            this._snackBar.openSnackBar('Something went wrong.','error');
        }
        )
    }

    openDialog() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            setup: this.setup,
            type_id: this.type_id
        }
        dialogConfig.autoFocus = false;
        dialogConfig.width = "550px";
        const dialogRef = this._dialog.open(DialogComponent, dialogConfig);
        dialogRef.componentInstance.createEmit.subscribe((res: any) => {
          if (res) {
            this.type_id = res?.type_id;
              let copy: any[] = [];
              copy.push(res);
              this.data.forEach((v: any) => {
                copy.push(v);
              });
              this.data = copy;
              this.dataSource = new MatTableDataSource(this.data);
              this.total += 1;
              this.limit += 1;
          }
        });
    }


    delete(id: any) {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((result) => {
            // console.log(result);
            if (result) {
                this._service.delete(id).subscribe((res: any) => {
                    this.isSearching = false;
                    this._snackbar.openSnackBar(res.message, '');
                    let copy: any[] = [];
                    this.data.forEach((obj: any) => {
                        if (obj.id !== id) {
                            copy.push(obj);
                        }
                    });
                    this.data = copy;
                    this.total -= 1;
                    this.limit -= 1;
                    this.dataSource = new MatTableDataSource(this.data);
                }, (err: any) => {
                    this.isSearching = false;
                    this._snackbar.openSnackBar('Something went wrong.', 'error');
                });
            }
        });
    }

    updateDialog(data: any) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            setup: this.setup,
            data: data,
            type_id: this.type_id
        }
        
        dialogConfig.autoFocus = false;
        dialogConfig.width = "550px";
        const dialogRef = this._dialog.open(UpdateDialogComponent, dialogConfig);
        dialogRef.componentInstance.createEmit.subscribe((res: any) => {
            if (res) {
                let copy: any[] = [];
                this.data.forEach((v: any) => {
                    if (v.id == data.id) {
                        v = res
                    }
                    copy.push(v);
                })
                this.data = copy;
                this.dataSource = new MatTableDataSource(this.data);
            }
        });
    }
    ViewProject(row: any): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
          data: row,
        }
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(ViewProjectDialogComponent, dialogConfig);
        this._router.events.subscribe(() => {
          dialogRef.close();
        })
      }
    
}
