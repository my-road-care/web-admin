import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class OrganizationService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}

    setup():any{
        return this.http.get(this.url+`/sup/organization/data-setup`,this.httpOptions);
    }

    listing(params= {}):any{
        const httpOptions = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this.http.get(this.url+`/sup/organization`,httpOptions);
    }

    create(value:any){
        return this.http.post(this.url+'/sup/organization',value,this.httpOptions);
    }

    delete(id:any){
        return this.http.delete(this.url+`/sup/organization/${id}`,this.httpOptions);
    }
    update(id:any,data:any){
        return this.http.post(this.url+`/sup/organization/update/${id}`,data,this.httpOptions);
    }
}
