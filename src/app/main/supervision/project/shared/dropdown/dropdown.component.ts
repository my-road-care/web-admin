import { Component, Input,  OnInit  } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable, map, startWith } from 'rxjs';

@Component({
    selector: 'app-dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

    public form: UntypedFormGroup;
    
    @Input() data:any [] = [];
    @Input() label:string = '';

    public filterData: Observable<any[]>;

    public selected:any         = null;
    public hasNoData:boolean    = false;
    public hasError: boolean    = false;

    constructor(
        private _formBuilder: UntypedFormBuilder
    ) {
       
        
    }

    ngOnInit(): void {
        this.buildForm();
    }
    

    public select($event: MatAutocompleteSelectedEvent) {
        this.selected = $event.option.value;
        this.form.get('select').setValue(this.selected.name);
    }

    buildForm(){

        this.form = this._formBuilder.group({
            select: [null, Validators.required]
        });

        this.filterData = this.form.get('select').valueChanges.pipe(startWith(''), map(value => {
            const name = typeof value === 'string' ? value : value?.name;
            return name ? this._filter(name as string) : this.data.slice();
        }));

    }

    private _filter(name: string = ''): any[] {
        const filterValue =  name.toLowerCase();
        return this.data.filter((option) => option.name.toLowerCase().includes(filterValue));
    }



    markAsError(){
        console.log('x'); 
        this.form.markAsTouched();
    }


}
