import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable, map, startWith } from 'rxjs';

@Component({
    selector: 'app-organization',
    templateUrl: './organization.component.html',
    styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
    @Input() label: string = '';
    @Input() organizations: any[] = [];
    public organizationForm: UntypedFormGroup;
    public filteredOrganizations: Observable<string[]>;
    public noOrganization: boolean = false;
    public organization: any;
    constructor(
        private _formBuilder: UntypedFormBuilder,
    ) { }
    ngOnInit(): void {
        this.formBuilder();
    }
    private formBuilder(): void {
        this.organizationForm = this._formBuilder.group({
            organization_id: [null, Validators.required]
        });
        console.log(this.organizations)
        this.filteredOrganizations = this.organizationForm.get('organization_id').valueChanges.pipe(startWith(''), map(value => this._filter(value || '')));
        console.log(this.organizations)
        this.organizationForm.get('organization_id').valueChanges.subscribe((res: string) => {
            let j = 0;
            if (res != '') {
                this.organizations.forEach((v: any) => {
                    if (v.name.includes(res)) {
                        j++;
                    }
                });
            }
            if (j == 0) {
                this.noOrganization = true;
            } else {
                this.noOrganization = false;
            }
        })
    }
    public onSelected(event: MatAutocompleteSelectedEvent) {
        this.organizationForm.get('organization_id').setValue(event.option.value);
        this.organizations.forEach((v: any) => {
            if (v.name == event.option.value) {
                this.organization = v;
            }
        });
    }
    public createOrganization(value: string, type_id: number): void {
        alert(value+', '+type_id)
    }
    private _filter(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.organizations.filter((option: any) => option.name.toLowerCase().includes(filterValue));
    }
}
