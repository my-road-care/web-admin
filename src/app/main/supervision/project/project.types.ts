export interface ListProject {
    data: Data[],
    pagination: Pagination
}

export interface Params {
    pageSize: number
    page: number
    key?: string | number | null
    category_id?: number
    status_id?: number
    province_id?: number
    budget_year_id?: number
}

export interface Data {
    id: number,
    name: string,
    code: string,
    category: {
        id: number,
        name: string
    },
    road_id: number,
    reviewer?: {
        id: number,
        name: string,
        abbre: string
    } | null,
    entity?: {
        id: number,
        name: string,
        abbre:  string
    } | null,
    status: {
        id: number,
        kh_name: string,
        en_name: string,
        color: string
    },
    budget_plans: {
        id: number,
        name: string
    },
    is_approved: 0 | 1
}

interface Pagination {
    count: number
    current_page: number
    per_page: number
    total: number
    total_pages: number
}
