import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../project.service';
import { SetupService } from '../setup.service';
import { Subject, takeUntil } from 'rxjs';
import { ReportService } from './report.service';
import { TestingStandardService } from '../technical/testing-standard/testing-standard.service';
@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit,OnDestroy {
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    // private testingReport: string = 'របាយការណ៍ធ្វើតេស្ត';
    // private letter: string = 'លិខិតណែនាំ';
    // private picture: string = 'រូបភាព';
    public AllowTap:Alltap;
    public check_testingReport: boolean = true;
    public check_letter: boolean = true;
    public check_picture: boolean = true;
    public project_type_id: number = 3;
    public project_id: number = 0;
    public checkQurey:boolean = true;
    public access_permission:any;
    public code_project: string = undefined;
    public setupLayer:any={};
    public loading:boolean=false;
    public setStandard:any;
    constructor(
        private _projectService: ProjectService,
        private _route: ActivatedRoute,
        private _setupService: SetupService,
        private _standardService:TestingStandardService,
        private _reportService: ReportService
    ) {
        this._route.paramMap.subscribe((params: any) => {
            this.project_id = params.get('id');
        });
    }


    ngOnInit(): void {
        
        this._setupService.code$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) =>{
        this.code_project = res;
        });
        this.getCode();
        this.loading=true;
        this._reportService.getLayersetup(this.project_id).subscribe((res:any)=>{
           
            this.setupLayer=res;
            this.loading=false;
        })
        const tabdisplay:Alltap={
                    picture: 1,
                    testing: 0,
                    letter: 0,
                    system: 0,
                    references:0
                }
        this._reportService.allowTap=tabdisplay;
    }
    checkTab(event){
        // console.log(event)
       if(event.tab.textLabel === "របាយការណ៍វឌ្ឍនភាព"){
        const tabdisplay:Alltap={
            picture: 1,
            testing: 0,
            letter: 0,
            system: 0,
            references:0
        }
        this._reportService.allowTap=tabdisplay;
       }
       if(event.tab.textLabel === "របាយការណ៍ធ្វើតេស្ត"){
        const tabdisplay:Alltap={
            picture: 0,
            testing: 1,
            letter: 0,
            system: 0,
            references:0
        }
        this._reportService.allowTap=tabdisplay;
       }
       if(event.tab.textLabel === "លិខិត"){
        const tabdisplay:Alltap={
            picture: 0,
            testing: 0,
            letter: 1,
            system: 0,
            references:0
        }
        this._reportService.allowTap=tabdisplay;
       }
       if(event.tab.textLabel === "របាយការណ៍ពីប្រព័ន្ធ"){
        const tabdisplay:Alltap={
            picture: 0,
            testing: 0,
            letter: 0,
            system: 1,
            references:0
        }
        this._reportService.allowTap=tabdisplay;
       }
       if(event.tab.textLabel === "របាយការណ៍យោង"){
        const tabdisplay:Alltap={
            picture: 0,
            testing: 0,
            letter: 0,
            system: 0,
            references:1
        }
        this._reportService.allowTap=tabdisplay;
       }
       
    }

    getCode(): void {
      this.checkQurey = true;
      this._projectService.getCode(this.project_id).subscribe((res: any) => {
          if (res) {
              this.code_project = res.code;
              this._setupService.code = res.code;
              this.access_permission = res?.permission;
             
              this.checkQurey = false;
          }
      }, (err: any) => {
          console.log(err);
      });
  }
    ngOnDestroy(): void {
      this._unsubscribeAll.next(null);
      this._unsubscribeAll.complete();
  }
}
interface Alltap{
    picture:number | String,
    testing:number | String,
    letter:number | String,
    system:number | String,
    references:number | String
  }
