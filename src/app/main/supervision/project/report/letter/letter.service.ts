import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env } from 'environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LetterService {
  public apiUrl=env.apiUrl;
  public httpOption: Object ={
    header:new HttpHeaders().set("Content-Type","application/json")
  }
  constructor(
    private _http:HttpClient
  ) { }
  setup(id:any):Observable<any>{
    return this._http.get(this.apiUrl+'/sup/projects/'+id+'/report/letter/setup',this.httpOption);
  }
  listing(id:any,params:any){
    const httpOPtion:object={
      header:new HttpHeaders().set("Content-Type","application/json")
    }
    httpOPtion['params']=params;

    return this._http.get(this.apiUrl+'/sup/projects/'+id+'/report/letter',httpOPtion);
  }
  create(id:any,data:any):Observable<any>{
    const httpOptions:Object={
      headers: new HttpHeaders({
          'Accept': 'application/json',
          'withCredentials': 'true',
      })

      }
      let formdata=new FormData();
      formdata.append('file', data.file);
      formdata.append('letter_type',data.letter_type);
      formdata.append('title', data.title);
    return this._http.post(this.apiUrl + '/sup/projects/' + id + '/report/letter',formdata,httpOptions);

  }
  update(proid:any,data:any,id:any):Observable<any>{
    const httpOptions:Object={
      headers: new HttpHeaders({
          'Accept': 'application/json',
          'withCredentials': 'true',
      })

      }
      let formdata=new FormData();
      formdata.append('file', data.image);
      formdata.append('letter_type',data.letter_type);
      formdata.append('title', data.title);
    return this._http.post(this.apiUrl + '/sup/projects/' + proid + '/report/letter/'+id,formdata,httpOptions);

  }
  delete(proid:any,id:any):Observable<any>{

    return this._http.delete(this.apiUrl + '/sup/projects/' + proid + '/report/letter/'+id,this.httpOption);

  }
  preview(proid:any,id:any):Observable<any>{

    return this._http.get( this.apiUrl + '/sup/projects/' + proid + '/report/letter'+id , this.httpOption);
  }
}
