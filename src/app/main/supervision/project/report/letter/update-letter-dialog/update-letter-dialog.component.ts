import { Component, EventEmitter,Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA,MatDialogConfig, MatDialog, } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LetterService } from '../letter.service';
import { environment as env } from 'environments/environment';
@Component({
  selector: 'app-update-letter-dialog',
  templateUrl: './update-letter-dialog.component.html',
  styleUrls: ['./update-letter-dialog.component.scss']
})
export class UpdateLetterDialogComponent implements OnInit {

  @ViewChild('Updateletterform') Updateletterform: NgForm;
  updateLetter = new EventEmitter();
  public letterform: UntypedFormGroup;
  public mode:any;
  public src:any='../../../../../../../assets/images/logo/PDF_file_icon.svg.png';
  public saving:boolean=false;
  public types:any={};
  constructor(
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
    private dialogRef: MatDialogRef<UpdateLetterDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _service:LetterService,
    private _dialog:MatDialog,
    private _snackBar: SnackbarService,
  ) {
    this.types=this.dataDialog?.types;
    // if(this.dataDialog?.image_uri !='' || this.dataDialog?.image_uri !=null){
    //   this.src=env.fileUrl+this.dataDialog?.data?.image_uri;
    // }
   }

  ngOnInit(): void {
    this.formBuilder();
  }
  formBuilder(){
    this.letterform = this._formBuilder.group({
      title:       [this.dataDialog ? this.dataDialog?.data?.title :'', Validators.required],
      letter_type: [this.dataDialog ? this.dataDialog?.data?.letter_type?.id :'', Validators.required],
      image:       ['']
  });
  }
  submit(){
    if(this.letterform.value){
      this.saving=true;
      this.letterform.disable();
      this._service.update(this.dataDialog?.id,this.letterform.value,this.dataDialog?.data?.id).subscribe(
        (res:any)=>{
          if(res.statusCode===200){
            this.saving = false;
            this.dialogRef.close();
            this.updateLetter.emit(res);
            this._snackBar.openSnackBar(res.message, '');
            this.letterform.enable();
          }else{
            this.letterform.enable();
            this.saving = false;
            this._snackBar.openSnackBar(res.message, 'error');
          }

      },(err: any) =>{

          this.dialogRef.close();
          for(let key in err.error.errors){
          let control = this.letterform.get(key);
          control.setErrors({'servererror':true});
          control.errors.servererror = err.error.errors[key][0];
          this._snackBar.openSnackBar(err.error.message.message, '');
          }
      });
  }
  else{
      this._snackBar.openSnackBar('Please check your input.', 'error');
  }
  }
  public show:Boolean=false;
  public size: number = 0;
  public type: string = '';
  public error_size: string = '';
  public error_type: string = '';

  fileChangeEvent(e: any): void {
      if (e?.target?.files) {
          this.error_size = '';
          this.error_type = '';
          this.show = false;
          this.saving = false;
          this.type = e?.target?.files[0]?.type;
          this.size = e?.target?.files[0]?.size;
          var reader = new FileReader();
          reader.readAsDataURL(e?.target?.files[0]);
          reader.onload = (event: any) => {
              this.letterform.get('image').setValue(e?.target?.files[0]);
          }
      //     if (this.size > 3145728) { //3mb
      //         this.saving = true;
      //         this.letterform.get('image').setValue('');
      //         this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';

      //     }
      //     if (this.type.substring(0, 5) !== 'image') {
      //         this.saving = true;
      //         this.src = '';
      //         this.show = true;
      //         this.error_size = '';
      //         this.letterform.get('image').setValue(this.src);
      //         this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      //     }
      // } else {
      //     this.saving = true;
      //     this.src = '';
      //     this.show = true;
      //     this.error_size = '';
      //     this.letterform.get('image').setValue(this.src);
      //     this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
          //it work when user cancel select file
      }
  }

  selectFile(): any {
      document.getElementById('portrait-file').click();
  }

  preview(): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: this.src,
          title: 'រូបភាពនៃលិខិត'
      };
      dialogConfig.width = "850px";
      const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
  }


}


