import { Component, EventEmitter,Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA,MatDialogConfig, MatDialog, } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';

import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LetterService } from '../letter.service';
@Component({
  selector: 'app-create-letter-dialog',
  templateUrl: './create-letter-dialog.component.html',
  styleUrls: ['./create-letter-dialog.component.scss']
})
export class CreateLetterDialogComponent implements OnInit {

  @ViewChild('Createletterform') Createletterform: NgForm;
  CreateLetter = new EventEmitter();
  public letterform: UntypedFormGroup;
  public mode:any;
  public src:String = null;
  public saving:boolean=false;
  public types:any={};
  public show:Boolean=false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
    private dialogRef: MatDialogRef<CreateLetterDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _dialog:MatDialog,
    private _service:LetterService,
    private _snackBar: SnackbarService,
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.formBuilder();
    this.types=this.dataDialog?.types;
  }
  formBuilder(){
    this.letterform = this._formBuilder.group({
      title:       ['', Validators.required],
      letter_type: ['', Validators.required],
      file:       ['',Validators.required]
  });
  }
  submit(){
    if(this.letterform.value){
      this.saving=true;
      this.letterform.disable();
      this._service.create(this.dataDialog?.id,this.letterform.value).subscribe(
        (res:any)=>{
          if(res.statusCode===200){
            this.saving = false;
            this.dialogRef.close();
            this. CreateLetter.emit(res);
            this._snackBar.openSnackBar(res.message, '');
            this.letterform.enable();
          }else{
            this.letterform.enable();
            this.saving = false;
            this._snackBar.openSnackBar(res.message, 'error');
          }

      },(err: any) =>{

          this.dialogRef.close();
          for(let key in err.error.errors){
          let control = this.letterform.get(key);
          control.setErrors({'servererror':true});
          control.errors.servererror = err.error.errors[key][0];
          this._snackBar.openSnackBar(err.error.message.message, '');
          }
      });
  }
  else{
      this._snackBar.openSnackBar('Please check your input.', 'error');
  }
  }
  public size: number = 0;
  public type: string = '';
  public error_size: string = '';
  public error_type: string = '';

  fileChangeEvent(e: any): void {
      if (e?.target?.files) {
          this.error_size = '';
          this.error_type = '';
          this.show = false;
          this.saving = false;
          this.type = e?.target?.files[0]?.type;
          this.size = e?.target?.files[0]?.size;
          var reader = new FileReader();
          reader.readAsDataURL(e?.target?.files[0]);
          reader.onload = (event: any) => {
              this.src = '../../../../../../../assets/images/logo/PDF_file_icon.svg.png';
              this.show=false;
              this.letterform.get('file').setValue(e?.target?.files[0]);
          }

      }
  }

  selectFile(): any {
      document.getElementById('portrait-file').click();
  }

  preview(): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: this.src,
          title: 'រូបភាពនៃលិខិត'
      };
      dialogConfig.width = "850px";
      const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
  }

}

