import { Component, Input, OnDestroy, OnInit } from '@angular/core';
// import { ReportService } from '../report.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LetterService } from './letter.service';
import * as _moment from 'moment';
const moment = _moment;
import { environment as env } from 'environments/environment';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { CreateLetterDialogComponent } from './create-letter-dialog/create-letter-dialog.component';
import { UpdateLetterDialogComponent } from './update-letter-dialog/update-letter-dialog.component';
import { ReportService } from '../report.service';
import { Subject, takeUntil } from 'rxjs';
@Component({
    selector: 'app-letter',
    templateUrl: './letter.component.html',
    styleUrls: ['./letter.component.scss']
})
export class LetterComponent implements OnInit,OnDestroy {

    @Input() public id: any='';
    @Input() public access_permission:any;
    public isLoading :boolean = false;
    public limit:any = 10 ;
    public dataSource:any={};
    public displayedColumns: string[] = ['nos','type','image_uri','title','creator','create_at','action'];
    private page:any = 1 ;
    public total:any = 0 ;
    public data:any  = {} ;
    public Letter_Types:any=[];
    public Letter_Type:any='';
    public listView=true;
    public gridView=false;
    public url:any=env.fileUrl;
    private _unsubscribeAll:Subject<any> = new Subject<any>();
    constructor(
      private _service:ReportService,
      private _letterService:LetterService,
      private _dialog: MatDialog,
      private _snackBar: SnackbarService,) { }
  

    ngOnInit(): void {
     
      this._service.allowTap$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res:any)=>{
        if(res.letter == 1){
           this.listing();
           this.getSetup();
        }
       })
      
     
    }
   
    getSetup(){
      this._letterService.setup(this.id).subscribe((res:any)=>{
        this.Letter_Types=res;
        

      });
    }
    listing(_limit: number = 10, _page: number = 1): any {
      const param: any = {
        per_page: _limit, 
          page: _page,
      };
      if (this.page != 0) {
          param.page = this.page;
      }
      if(this.Letter_Type !=''){
        param.letter_type = this.Letter_Type;
      }
      this.isLoading = true;
     
      this._letterService.listing(this.id,param).subscribe((res:any)=>{
       
        this.data=res?.data?.letters;
        this.total = res?.data?.pagination?.total;
        this.page = res?.data?.pagination?.current_page;
        this.limit = res?.data?.pagination?.per_page;
        this.dataSource = new MatTableDataSource(this.data);
        this.isLoading=false;
      })
    }
    onChangeLetter(event){
      this.listing();
    }
    onPageChanged(event){
      if (event && event.pageSize) {
        this.limit = event.pageSize;
        this.page = event.pageIndex + 1;
        this.listing(this.limit ,this.page);
    }
    }
    preview(src: any,type:any): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: src,
          title: 'លិខិត'+type
      };
      dialogConfig.width = "850px";
      this._dialog.open(PreviewImageComponent, dialogConfig);
  }
    changeView(event:any){
      if(event=='grid'){
        this.gridView=false;
        this.listView=true;
      }else{
        this.gridView=true;
        this.listView=false;
      }
    }
    create(){
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          types: this.Letter_Types,
          id:this.id
      }
      dialogConfig.autoFocus = false;
      dialogConfig.width = "850px";
      const dialogRef = this._dialog.open(CreateLetterDialogComponent, dialogConfig);
      dialogRef.componentInstance.CreateLetter.subscribe((response: any) => {
         this.data.push(response?.data[0]);
         this.total+=1;
         this.dataSource= new MatTableDataSource(this.data);
      });

    }

    delete(data:any){
      const dialogRef = this._dialog.open(ConfirmDialogComponent, {
        data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((result) => {
       
        if (result) {
            this._letterService.delete(this.id, data?.id).subscribe((res: any) => {
                this._snackBar.openSnackBar(res.message, '');
                this.data.splice(this.data.indexOf(data),1);
                this.total-=1;
                this.dataSource = new MatTableDataSource(this.data);
            }, (err: any) => {
                let message = err?.error?.message ? err.error.message : "Something went wrong.";
                this._snackBar.openSnackBar(message, 'error');
            });
        }
    });
    }
    update(data:any){
      const copy=[];
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          id:this.id,
          types: this.Letter_Types,
          data:data
      }
      dialogConfig.autoFocus = false;
      dialogConfig.width = "850px";
      const dialogRef = this._dialog.open(UpdateLetterDialogComponent, dialogConfig);
      dialogRef.componentInstance.updateLetter.subscribe((response: any) => {
       
        this.data.forEach((e:any)=>{
          if(e.id===response?.data[0]?.id){
            copy.push(response?.data[0]);
          }else{
            copy.push(e);
          }
        });
        this.data=[];
        this.data=copy;
         this.dataSource= new MatTableDataSource(this.data);
      });

    }
    openPDF(data){
      window.open(this.url+'v3/get-file/'+data.image_uri);
  }
    ngOnDestroy(): void {
      this._unsubscribeAll.next(null);
      this._unsubscribeAll.complete();
    }
}
