import { Component, Inject, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ReportService } from '../../report.service';
import { MatTableDataSource } from '@angular/material/table';
import { environment as env } from 'environments/environment';
import { PictureReportService } from '../picture.service';
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-overview-dialog',
  templateUrl: './overview-dialog.component.html',
  styleUrls: ['./overview-dialog.component.scss']
})
export class OverviewDialogComponent implements OnInit{
  @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
  @Input() setup: any;
  construction:any[]=[];
  isLoading:boolean=true;
  data:any[]=[];
  public url= env.fileUrl;
  dataSource:any=[];
  displayedColumns: string[] = ['nos','pk','status'];
  limit:any = 10 ;
  page:any = 1 ;
  total:any = 0 ;
  isSaving:boolean=false;
  public id :any[]=[];
  public suffect:any='';
  constructor(
     @Inject(MAT_DIALOG_DATA) public dataDialog: any,
      private dialogRef: MatDialogRef<OverviewDialogComponent>,
      private _service:ReportService,
      private _dialog: MatDialog
      ){}
      
  ngOnInit(): void {
    // console.log(this.dataDialog);
    this.dataDialog.setup.project.surface.forEach(element => {
        if(element.name ==  this.dataDialog.setup.project.surface[0].name) this.suffect+= this.dataDialog.setup.project.surface[0].name;
        else{
          this.suffect+=', '+this.dataDialog.setup.project.surface[0].name;
        }
         
    });
    this.construction=[
      {id:0,name:"ទាំងអស់",color:"white"},
      {id:1,name:"មិនទាន់អនុវត្ត",color: "#EEEEEE"},
      {id:2,name:"កំពុងអនុវត្ត",color: "#90CAF9"},
      {id:3,name:"បញ្ចប់ការងារ",color: "#00FF00"},
      {id:4,name:"បានធ្វើតេស្តរួច",color: "#FFB10D"},
    ]
   this.listing();
   
   
   
  }
  listing(){
    this.isLoading=true;
    this._service.progress_picture_listing(this.dataDialog.id,this.dataDialog.data.id).subscribe((res:any)=>{
      this.data=res.data.pictures;
      
      this.isLoading=false;
      this.dataSource=new MatTableDataSource(this.data);
    })
  }
  onPageChanged(event){
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing();
  }
  }
  downloadReport(report: number,progress_trx_id:number){
    let obj = {
      report: report,
      progress_trx_id: this.dataDialog.data.id,
    }
    if(report === 5){
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
      const day = ("0" + currentDate.getDate()).slice(-2);
      const hours = ("0" + currentDate.getHours()).slice(-2);
      const minutes = ("0" + currentDate.getMinutes()).slice(-2);
      const seconds = ("0" + currentDate.getSeconds()).slice(-2);
      const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
      let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
      this._service.getjsReport(this.dataDialog.id, obj).subscribe((res: any) => {
        //console.log(res);
          this.isSaving = false;
          let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
          FileSaver.saveAs(blob,this.dataDialog.setup.project.code + '-របាយការណ៍វឌ្ឍនភាពការងារ-' + formattedDate +'.pdf');
          dialog.close();
      }, (err: any) => {
          this.isSaving = false;
          //console.log(err);
          dialog.close();
      });
  }
  }
  // =================================>> Convert base64 to blob 
  b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}
}
