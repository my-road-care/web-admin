import { Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import * as FileSaver from 'file-saver';
import { ReportService } from '../report.service';
import { environment as env } from 'environments/environment';
import { PreviewImageMultiComponent } from 'app/shared/preview-image-multi/preview-image-multi.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PictureReportService } from './picture.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { OverviewDialogComponent } from './overview-dialog/overview-dialog.component';
import { TechnicalService } from '../../technical/technical.service';
import { Subject, takeUntil } from 'rxjs';
// import { PictureReportService } from './picture.service';


@Component({
    selector: 'app-picture',
    templateUrl: './picture.component.html',
    styleUrls: ['./picture.component.scss']
})
export class PictureComponent implements OnInit,OnDestroy {
   @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
    public isLoading: boolean = false;
    @Input() public id: any ;
    @Input() public setup: any ;
    @Input() public setupMethods:any;
    public constructionType:any='';
    public general: any;
    public construction: any =[];
    public displayedColumns: string[] = ['nos','pk','layer','image_uri','title','create_at','created_by','action'];
    public dataSource:any=[];
    public dataBackup:any=[];
    public Setupstatus:any=[];
    public body:any[]=[];
    public code :string = '';
    public url= env.fileUrl;
    public data:any=[];
    public limit:any = 10 ;
    private page:any = 1 ;
    public total:any = 0 ;
    public listView=true;
    public gridView=false;
    public timer:any;
    public isSaving:boolean = false;
    public isSearching:boolean=false;
    public pkstart:any='';
    public pkend:any='';
    public listPkStart:any[]=[];
    public listPkEnd:any[]=[];
    public listPkAll:any[]=[];
    public layers:any[]=[];
    public test_method:any='';
    private _unsubscribeAll:Subject<any> = new Subject<any>();
    constructor(
      private _snackBar: SnackbarService,
      private _dialog: MatDialog,
      private _service:ReportService,
      private service:PictureReportService,
    ) {
     }
 

    ngOnInit(): void {
      this._service.allowTap$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res:any)=>{
        if(res.picture == 1){
           this.listing();
        }
       })
      this.isLoading=true;
      this.isSearching = false;
      this.code = this.setup.code;
      this.listPkAll=this.setup.pks;
      this.listPkStart=this.listPkAll.filter(r=>parseInt(r.pk) < parseInt(this.listPkAll[this.listPkAll.length -1].pk));
      this.listPkEnd=this.listPkAll.filter(r=>parseInt(r.pk) > parseInt(this.listPkAll[0].pk));
      this.pkstart=this.listPkAll[0].id;
      this.pkend=this.listPkAll[this.listPkAll.length -1].id;
      this.code = this.setup.project.code;
      
        
    }
    listing(_limit: number = 10, _page: number = 1){
      const param: any = {
        per_page: _limit,

    };  
    if (this.page != 0) {
        param.page = this.page;
    }
    if(this.constructionType !=''){
     param.picture_type=this.constructionType;
    }
    if(this.pkstart != ''){
      param.pk_start=this.pkstart;
    }
    if(this.pkend != ''){
      param.pk_end =this.pkend;
    }
    if(this.test_method != ''){
      param.work_id=this.test_method;
    }
      this.isLoading=true;
      this._service.picture_listing(this.id,param).subscribe(
        (res:any)=>{
          this.data=res?.data?.progress;
          this.dataSource = new MatTableDataSource(this.data);
          this.total = res?.data?.pagination?.total;
          this.page = res?.data?.pagination?.current_page;
          this.limit = res?.data?.pagination?.per_page;
          this.isLoading=false;
      });
    }
    preview(src: any,date:any,comment:any): void {
      let srcs=[src];
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: srcs,
          date:date,
          comment:comment,
          title: 'របាយការណ៍វឌ្ឍនភាព'
      };
      dialogConfig.width = "850px";
      this._dialog.open(PreviewImageMultiComponent, dialogConfig);
  }
  onChangeMethod(value){
    this.page=1;
    this.listing();
  }
  onPageChanged(event){
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit ,this.page);
  }
  }
  changeView(event:any){
    if(event=='grid'){
      this.gridView=false;
      this.listView=true;
    }else{
      this.gridView=true;
      this.listView=false;
    }
  }
  downloadReport(report: number,data:any){
    let obj = {
      report: report,
      progress_trx_id: data.id,
    }
    if(report === 5){
      const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            const day = ("0" + currentDate.getDate()).slice(-2);
            const hours = ("0" + currentDate.getHours()).slice(-2);
            const minutes = ("0" + currentDate.getMinutes()).slice(-2);
            const seconds = ("0" + currentDate.getSeconds()).slice(-2);
            const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
      let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
      this.service.getjsReport(this.id, obj).subscribe((res: any) => {
          this.isSaving = false;
          let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
          FileSaver.saveAs(blob,this.code + '-របាយការណ៏វឌ្ឍនភាពការងារ-' + formattedDate+ '.pdf');
          dialog.close();
      }, (err: any) => {
          this.isSaving = false;
          
          dialog.close();
      });
    }

  }
  b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  changePkStart(value){
    let pk:any=this.listPkAll.filter(r=>parseInt(r.id) ==parseInt(value.value))
    this.listPkEnd=this.listPkAll.filter(r=>parseInt(r.pk) > parseInt(pk[0].pk))
    this.listing();
  }
  changePkEnd(value){
    let pk:any=this.listPkAll.filter(r=>parseInt(r.id) ==parseInt(value.value))
   
    this.listPkStart=this.listPkAll.filter(r=>parseInt(r.pk) < parseInt(pk[0].pk))
    this.listing();
  }
  view(data){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
        id:this.id,
        setup:this.setup,
        data:data
    }
    
    dialogConfig.disableClose=true;
    dialogConfig.autoFocus = false;
    dialogConfig.width = "850px";
    // const dialogRef = this._dialog.open(CreateLetterDialogComponent, dialogConfig);
    // dialogRef.componentInstance.CreateLetter.subscribe((response: any) => {
    this._dialog.open(OverviewDialogComponent ,dialogConfig);
  }
  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}