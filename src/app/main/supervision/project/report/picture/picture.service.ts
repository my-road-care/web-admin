import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PictureReportService {
  private url: string = env.apiUrl;
  private httpOptions: object = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
  };
  constructor(private _http:HttpClient) { }
  
  getjsReport(id: any, body: any): any {
    return this._http.post(this.url + '/sup/projects/' + id + '/report/system-report', body, this.httpOptions);
  }
  
}
