import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class ReportService {
    constructor(private _http: HttpClient) { }
    private url: string = env.apiUrl;
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    test_listing(params: any, id: any): Observable<any> {
        const httpOptions: object = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this._http.get(this.url + '/sup/projects/' + id + '/report/testing-report', httpOptions);
    }
    setUPPicture(id: any){
        return this._http.get(this.url+'/sup/projects/'+id+'/report/testing-report/setup',this.httpOptions);
    }
    picture_listing(id: any, params: any): Observable<any> {
        const httpOptions: object = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this._http.get(this.url + '/sup/projects/' + id + '/report/picture', httpOptions);
    }
    progress_picture_listing(id: any, progressId:any): Observable<any> {
        return this._http.get(this.url + '/sup/projects/' + id + '/report/picture/'+progressId,this.httpOptions);
    }

    letter_listing(params: any, id: any): Observable<any> {
        const httpOptions: object = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this._http.get(this.url + '/sup/projects/' + id + '/report/letter', httpOptions);
    }
    getLayersetup(id: any): any {
        const httpOptions = {};
        
        return this._http.get(this.url + '/sup/projects/' + id + '/report/system-report/setup', httpOptions);
    }
    viewTesting(id: any, testId:any){
        return this._http.get(this.url + '/sup/projects/' + id + '/report/testing-report/'+testId,this.httpOptions);
    }
    private _report: BehaviorSubject<any> = new BehaviorSubject<any>(1);
    set report(value: object) {
        this._report.next(value);
    }
    get report$(): Observable<any> {
        return this._report.asObservable();
    }
    private _allowtap: BehaviorSubject<any> = new BehaviorSubject<any>(1);
    set allowTap(value: object){
        this._allowtap.next(value);
    }
    get allowTap$():Observable<any>{
        return this._allowtap;
    }
    getjsReport(id: any, body: any): any {
        return this._http.post(this.url + '/sup/projects/' + id + '/report/system-report', body, this.httpOptions);
      }
    getChart(id: any): any {
        return this._http.get(this.url+'/sup/projects/'+id+'/report/testing-report-chart',this.httpOptions);
    }
}
