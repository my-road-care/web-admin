import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
@Injectable({
    providedIn: 'root'
})
export class SystemReportService {
    private url: string = env.apiUrl;
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(private _http: HttpClient) { }
    // get plan 
    getjsReport(id: any, body: any): any {
        return this._http.post(this.url + '/sup/projects/' + id + '/report/system-report', body, this.httpOptions);
    }

    getjsReportoverview(id: any, body: any): any {
        return this._http.post(this.url + '/sup/projects/' + id + '/report/picture/getGeneralInfo', body, this.httpOptions);
    }
    // listing(id: any): any {
    //     const httpOptions = {};
    //     return this._http.get(this.url + '/sup/projects/' + id + '/report/system-report/setup', httpOptions);
    // }
}
