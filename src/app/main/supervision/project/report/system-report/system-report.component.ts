import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import * as _moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { SystemReportService } from './system-report.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as FileSaver from 'file-saver';
import { ReportWorkingTableComponent } from 'app/shared/report-working-table/report-working-table.component';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};
@Component({
    selector: 'app-system-report',
    templateUrl: './system-report.component.html',
    styleUrls: ['./system-report.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class SystemReportComponent implements OnInit {
    @Input() id: any;
    @Input() setup: any;
    public filename:any;
    public isSearching:boolean=false;
    public data :any;
    public isSaving: boolean = false;

    public overviewStart: any = null;
    public overviewEnd: any = null;

    public progressReportStart: any = null;
    public progressReportEnd: any = null;

    public tableStart: any = null;
    public tableEnd: any = null;

    public testingStandardStart: any = null;
    public testingStandardEnd: any = null;

    public planStart: any = null;
    public planEnd: any = null;

    public testingReportStart: any = null;
    public testingReportEnd: any = null;

    @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
    constructor(
        private _service: SystemReportService,
        private _dialog: MatDialog,
    ) { }

    ngOnInit(): void {
        this.data = this.setup.code;
        
    }
    submit(report: number, formDate: any = null, toDate: any = null) {
        if (formDate != null && toDate != null) {
            formDate = moment(formDate).format('YYYY-MM-DD');
            toDate = moment(toDate).format('YYYY-MM-DD');
        } else {
            formDate = null;
            toDate = null;
        }
        let obj = {
            report: report,
            formDate: formDate,
            toDate: toDate
        }
        this.isSaving = true;
        if( report ==1  ){
            const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            const day = ("0" + currentDate.getDate()).slice(-2);
            const hours = ("0" + currentDate.getHours()).slice(-2);
            const minutes = ("0" + currentDate.getMinutes()).slice(-2);
            const seconds = ("0" + currentDate.getSeconds()).slice(-2);
            const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
            let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            this._service.getjsReportoverview(this.id, obj).subscribe((res: any) => {
                this.isSaving = false;
                let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
                FileSaver.saveAs(blob, this.data + '-របាយការណព័ត៌មានគម្រោង និងស្ថិតិ-' + formattedDate+ '.pdf');
                dialog.close();
            }, (err: any) => {
                this.isSaving = false;
                console.log(err);
                dialog.close();
            })
        }
        if (report === 2) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.data = {
                project_id: this.id
            };
            dialogConfig.width = "750px"
            dialogConfig.autoFocus = false
            dialogConfig.disableClose = true
            this._dialog.open(ReportWorkingTableComponent, dialogConfig);
        } 
        if(report === 3){
            let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            this._service.getjsReport(this.id, obj).subscribe((res: any) => {
                this.isSaving = false;
                let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
                FileSaver.saveAs(blob, 'របាយការណ៏ស្តង់ដាតេស្ត' + '.pdf');
                dialog.close();
            }, (err: any) => {
                this.isSaving = false;
                console.log(err);
                dialog.close();
            });
        }
        if(report === 4){
            let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            this._service.getjsReport(this.id, obj).subscribe((res: any) => {
                this.isSaving = false;
                let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
                FileSaver.saveAs(blob, 'របាយការណ៏ប្លង់' + '.pdf');
                dialog.close();
            }, (err: any) => {
                this.isSaving = false;
                console.log(err);
                dialog.close();
            });
        }
        if(report === 5){
            let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            this._service.getjsReport(this.id, obj).subscribe((res: any) => {
                this.isSaving = false;
                let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
                FileSaver.saveAs(blob, 'របាយការណ៏វឌ្ឍនភាពការងារ' + '.pdf');
                dialog.close();
            }, (err: any) => {
                this.isSaving = false;
                console.log(err);
                dialog.close();
            });
        }
        if(report === 6){
            const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            const day = ("0" + currentDate.getDate()).slice(-2);
            const hours = ("0" + currentDate.getHours()).slice(-2);
            const minutes = ("0" + currentDate.getMinutes()).slice(-2);
            const seconds = ("0" + currentDate.getSeconds()).slice(-2);
            const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
            let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            this._service.getjsReport(this.id, obj).subscribe((res: any) => {
                this.isSaving = false;
                let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
                FileSaver.saveAs(blob, this.data + 'របាយការណ៏ធ្វើតេស្ត' + formattedDate +'.pdf');
                dialog.close();
            }, (err: any) => {
                this.isSaving = false;
                console.log(err);
                dialog.close();
            });
        }
        
        
    }
    // =================================>> Convert base64 to blob 
    b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
}
