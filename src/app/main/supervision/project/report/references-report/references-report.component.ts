import { Component, Input, OnInit } from '@angular/core';
import { ReferencesReportService } from './references-report.service';
import { MatTableDataSource } from '@angular/material/table';
import { environment as env } from 'environments/environment';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import * as _moment from 'moment';
const moment = _moment;
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { CreateReportDialogComponent } from './create-dialog/create-dialog.component';
import { UpdateRefernceDialogComponent } from './update-dialog/update-dialog.component';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { ReportService } from '../report.service';
import { Subject, takeUntil } from 'rxjs';
@Component({
  selector: 'app-references-report',
  templateUrl: './references-report.component.html',
  styleUrls: ['./references-report.component.scss']
})
export class ReferencesReportComponent implements OnInit {
  @Input() public id:any;
  public displayedColumns: string[] = ['nos','title','image_uri','creator','create_at','action'];
  public dataSource: any;
  public isLoading:boolean= false;
  public limit:any = 10 ;
  private page:any = 1 ;
  public total:any = 0 ;
  public key:any ='';
  public data:any  = {} ;
  public listView=true;
  public gridView=false;
  public url= env.fileUrl;
  private _unsubscribeAll:Subject<any> = new Subject<any>();
  constructor(
    private _reportService:ReportService,
    private _service:ReferencesReportService,
    private _dialog:MatDialog,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    this._reportService.allowTap$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res:any)=>{
      if(res.references == 1){
         this.listing();
      }
     })
    // this.listing();
  }
  public listing( _size:any=10,_page:any=1){
    const param:any={
      per_page:_size,
      page:_page
    }
    if (this.page != 0) {
      param.page = this.page;
    }
    if(this.key != ''){
      param.key =this.key
    }
    this.isLoading=true;
    this._service.listing(this.id,param).subscribe(
      (res:any)=>{
        this.data=res?.data?.references;
        this.total = res?.data?.pagination?.total;
        this.page = res?.data?.pagination?.current_page;
        this.limit = res?.data?.pagination?.per_page;
        this.isLoading=false;
        this.dataSource = new MatTableDataSource(this.data);
      },
      err=>{

      }
    )

  }
  onPageChanged(event){
    if (event && event.pageSize) {
      this.limit = event.pageSize;
      this.page = event.pageIndex + 1;
      this.listing(this.limit, this.page);
  }
}
changeView(event:any){
  if(event=='grid'){
    this.gridView=false;
    this.listView=true;
  }else{
    this.gridView=true;
    this.listView=false;
  }
}
preview(src: any): void {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.data = {
      src: src,
      title: 'រូបភាពរបាយការណ៍យោង'
  };
  dialogConfig.width = "850px";
  this._dialog.open(PreviewImageComponent, dialogConfig);
}
openPDF(data){
  window.open(this.url+'v3/get-file/'+data.image_uri);
}
create(){
  const dialogConfig = new MatDialogConfig();
  dialogConfig.data = {
      id: this.id
  }
  dialogConfig.autoFocus = false;
  dialogConfig.width = "850px";
  const dialogRef = this._dialog.open(CreateReportDialogComponent, dialogConfig);
  dialogRef.componentInstance.CreateReference.subscribe((response: any) => {
     this.data.push(response?.data);
     this.total+=1;
     this.dataSource= new MatTableDataSource(this.data);
  });

}
update(data:any){
  const copy=[];
  const dialogConfig = new MatDialogConfig();
  dialogConfig.data = {
      id:this.id,
      data:data
  }
  dialogConfig.autoFocus = false;
  dialogConfig.width = "850px";
  const dialogRef = this._dialog.open(UpdateRefernceDialogComponent, dialogConfig);
  dialogRef.componentInstance.updateReference.subscribe((response: any) => {
    this.data.forEach((e:any)=>{
      if(e.id===response?.data?.id){
        response.data.created_at = (moment(response?.data?.created_at)).format('YYYY-MM-DD');
        copy.push(response?.data);
      }else{
        copy.push(e);
      }
    });
    this.data=[];
    this.data=copy;
     this.dataSource= new MatTableDataSource(this.data);
  });

}
delete(data:any){
  const dialogRef = this._dialog.open(ConfirmDialogComponent, {
    data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
});
dialogRef.afterClosed().subscribe((result) => {
    // // console.log(result);
    if (result) {
        this._service.delete(this.id, data?.id).subscribe((res: any) => {

            this._snackBar.openSnackBar(res.message, '');
            this.data.splice(this.data.indexOf(data),1);
            this.total-=1;
            this.dataSource = new MatTableDataSource(this.data);
        }, (err: any) => {

            let message = err?.error?.message ? err.error.message : "Something went wrong.";
            this._snackBar.openSnackBar(message, 'error');
        });
    }
});
}
}

