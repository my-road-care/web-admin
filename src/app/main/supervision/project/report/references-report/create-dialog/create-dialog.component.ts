
import { Component, EventEmitter,Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA,MatDialogConfig,MatDialog } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
// import { Subject } from 'rxjs';

import { ReferencesReportService } from '../references-report.service';
@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.scss']
})
export class CreateReportDialogComponent implements OnInit {

  @ViewChild('CreateReferenceForm') CreateReferenceForm: NgForm;
  CreateReference = new EventEmitter();
  public Referenceform: UntypedFormGroup;
  public mode:any;
  // public src:any='https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-vector-picture-icon-png-image_695350.jpg';
  public src:String =null;
  public saving:boolean=false;
  public types:any={};
  constructor(
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
    private dialogRef: MatDialogRef<CreateReportDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _dialog:MatDialog,
    private _service:ReferencesReportService,
    private _snackBar: SnackbarService,
  ) { }

  ngOnInit(): void {
    this.formBuilder();
  }
  formBuilder(){
    this.Referenceform = this._formBuilder.group({
      title:       ['', Validators.required],
      image:       ['',Validators.required]
  });
  }
  submit(){
    if(this.Referenceform.value){
      this.saving=true;
      this.Referenceform.disable();
      this._service.create(this.dataDialog?.id,this.Referenceform.value).subscribe(
        (res:any)=>{
          if(res.statusCode===200){
            this.saving = false;
            this.dialogRef.close();
            this. CreateReference.emit(res);
            this._snackBar.openSnackBar(res.message, '');
            this.Referenceform.enable();
          }else{
            this.Referenceform.enable();
            this.saving = false;
            this._snackBar.openSnackBar(res.message, 'error');
          }

      },(err: any) =>{

          this.dialogRef.close();
          for(let key in err.error.errors){
          let control = this.Referenceform.get(key);
          control.setErrors({'servererror':true});
          control.errors.servererror = err.error.errors[key][0];
          this._snackBar.openSnackBar(err.error.message.message, '');
          }
      });
  }
  else{
      this._snackBar.openSnackBar('Please check your input.', 'error');
  }
  }
  public show:Boolean=false;
  public size: number = 0;
  public type: string = '';
  public error_size: string = '';
  public error_type: string = '';

  fileChangeEvent(e: any): void {
      if (e?.target?.files) {
          this.error_size = '';
          this.error_type = '';
          this.show = false;
          this.saving = false;
          this.type = e?.target?.files[0]?.type;
          this.size = e?.target?.files[0]?.size;
          var reader = new FileReader();
          reader.readAsDataURL(e?.target?.files[0]);
          reader.onload = (event: any) => {
              this.src = '../../../../../../../assets/images/logo/PDF_file_icon.svg.png';
              this.show=false;
              this.Referenceform.get('image').setValue(e?.target?.files[0]);
          }
      //     if (this.size > 3145728) { //3mb
      //         this.saving = true;
      //         this.Referenceform.get('image').setValue('');
      //         this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';

      //     }
      //     if (this.type.substring(0, 5) !== 'image') {
      //         this.saving = true;
      //         this.src = '';
      //         this.show = true;
      //         this.error_size = '';
      //         this.Referenceform.get('image').setValue(this.src);
      //         this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      //     }
      // } else {
      //     this.saving = true;
      //     this.src = '';
      //     this.show = true;
      //     this.error_size = '';
      //     this.Referenceform.get('image').setValue(this.src);
      //     this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
      //     //it work when user cancel select file
      }
  }

  selectFile(): any {
      document.getElementById('portrait-file').click();
  }

  preview(): void {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: this.src,
          title: 'រូបភាពនៃលិខិត'
      };
      dialogConfig.width = "850px";
      const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
  }



}
