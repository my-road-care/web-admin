import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env} from 'environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ReferencesReportService {
  public Url=env.apiUrl;
  public httpOption: Object ={
    header:new HttpHeaders().set("Content-Type","application/json")
  }
  constructor(
    private _http:HttpClient ) {

  }

  listing(id:any,params:any){
    const httpOPtion:object={
      header:new HttpHeaders().set("Content-Type","application/json")
    }
    httpOPtion['params']=params;
    return this._http.get(this.Url+'/sup/projects/'+id+'/report/references-report',httpOPtion);
  }
  create(id:any,data:any):Observable<any>{
    const httpOptions:Object={
      headers: new HttpHeaders({
          'Accept': 'application/json',
          'withCredentials': 'true',
      })

      }
      let formdata=new FormData();
      formdata.append('file', data.image);
      formdata.append('title', data.title);
    return this._http.post(this.Url+'/sup/projects/'+id+'/report/references-report/create',formdata,httpOptions);
  }
  update(proId:any,data:any,id:any):Observable<any>{
    const httpOptions:Object={
      headers: new HttpHeaders({
          'Accept': 'application/json',
          'withCredentials': 'true',
      })

      }
      let formdata=new FormData();
      formdata.append('file', data.image);
      formdata.append('title', data.title);
    return this._http.post(this.Url+'/sup/projects/'+proId+'/report/references-report/'+id,formdata,httpOptions);
  }
  delete(proId:any,id:any):Observable<any>{
    return this._http.delete(this.Url+'/sup/projects/'+proId+'/report/references-report/'+id,this.httpOption);
  }
}
