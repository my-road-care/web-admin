import { Component, Input, OnDestroy, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ReportService } from '../report.service';
import { MatTableDataSource } from '@angular/material/table';
import { Subject, takeUntil } from 'rxjs';
import { TechnicalService } from '../../technical/technical.service';
import { TesingReportDialogComponent } from './tesing-report-dialog/tesing-report-dialog.component';
import { environment as env } from 'environments/environment';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PreviewImageMultiComponent } from 'app/shared/preview-image-multi/preview-image-multi.component';
import * as FileSaver from 'file-saver';
import { ApexOptions } from 'ng-apexcharts';
@Component({
    selector: 'app-testing-report',
    templateUrl: './testing-report.component.html',
    styleUrls: ['./testing-report.component.scss']
})
export class TestingReportComponent implements OnInit,OnDestroy {
  @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
    public isLoading: boolean = false;
    @Input() public id:any;
    @Input() public setup:any;
    @Input() public setupPk:any;
    @Input() public setupMethods:any;
    public displayedColumns: string[] = ['nos','location','layer','methods','image_uri','title','create_at','creator','action'];
    public dataSource: any;
    public isSaving:boolean = false;
    public isLoadings:boolean= true;
    public gridView:boolean=false;
    public listView:boolean=true;
    public limit:any = 10 ;
    private page:any = 1 ;
    public total:any = 0 ;
    public data:any  = {} ;
    public datas:any  = {} ;
    public key='';
    public work_type: any = '';
    public method: any = '';
    public methos:any=[];
    public code :string = '';
    public url= env.fileUrl;
    public listPkStart:any[]=[];
    public listPkEnd:any[]=[];
    public listPkAll:any[]=[];
    public layers:any[]=[];
    public test_method:any='';
    public pkstart:any='';
    public pkend:any='';
    public serial:serialData[]=[];
    public chartOptions:ApexOptions;
    public dataChart:any[]=[]; 
    public testPass:testPass[]=[];
    // rxjs
    private _unsubscribeAll:Subject<any> = new Subject<any>();
    constructor(
      private _reportService:ReportService,
      private _tecnicalService:TechnicalService,
      private _dialog:MatDialog){ }
    ngOnInit(): void {
       // rxjs
      this._reportService.allowTap$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res:any)=>{
       if(res.testing == 1){
          this.gridView=false;
          this.listView=true;
          this.listing();
          this.dataChart=[];
          this._reportService.getChart(this.id).subscribe((res:any)=>{
            this.dataChart=res.data;
          })
       }
      })
      this.listPkAll=this.setupPk.pks;
      this.listPkStart=this.listPkAll.filter(r=>parseInt(r.pk) < parseInt(this.listPkAll[this.listPkAll.length -1].pk));
      this.listPkEnd=this.listPkAll.filter(r=>parseInt(r.pk) > parseInt(this.listPkAll[0].pk));
      this.pkstart=this.listPkAll[0].id;
      this.pkend=this.listPkAll[this.listPkAll.length -1].id;
      this.code = this.setup.project.code;
      
    }
    listing(_limit: number = 10, _page: number = 1): any {
      let copy=[];
      const param: any = {
          per_page: _limit,
          page: _page,
      };
      if (this.page != 0) {
          param.page = this.page;
      }
      if(this.pkstart != ''){
        param.pk_start=this.pkstart;
      }
      if(this.pkend != ''){
        param.pk_end =this.pkend;
      }
    if (this.test_method != '') {
        param.work_id = this.test_method;
    }
      this.isLoading = true;
      this._reportService.test_listing(param,this.id).subscribe((res:any)=>{
       
         res?.data.testing.forEach((e:any)=>{
          copy.push(e);
        });
      
        this.data=copy;
        this.total = res?.data.pagination?.total;
        this.page = res?.data?.pagination?.current_page;
        this.limit = res?.data?.pagination?.per_page;
        this.dataSource = new MatTableDataSource(this.data);
        this.isLoading=false;
      })

    }
    onPageChanged(event){
      if (event && event.pageSize) {
        this.limit = event.pageSize;
        this.page = event.pageIndex + 1;
        this.listing(this.limit, this.page);
    }
    }
    onChangeMethod(value){
      this.page=1;
      this.listing();
    }
    onChangeTest(){
      this.listing();
    }
    ngOnDestroy(): void {
       // rxjs
      this._unsubscribeAll.next(null);
      this._unsubscribeAll.complete();
    }
    view(data){
    const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: data?.images,
          id:this.id,
          setup:this.setup,
          code: this.setup.code,
          data:data,
          date:data?.created_at,
          comment:data?.comment,
          title: 'របាយការណ៍ធ្វើតេស្ត'
      };
      dialogConfig.width = "850px";
      this._dialog.open(TesingReportDialogComponent, dialogConfig);
    }
    changePkStart(value){
      let pk:any=this.listPkAll.filter(r=>parseInt(r.id) ==parseInt(value.value))
      this.listPkEnd=this.listPkAll.filter(r=>parseInt(r.pk) > parseInt(pk[0].pk))
      
      this.listing();
    }
    changePkEnd(value){
      let pk:any=this.listPkAll.filter(r=>parseInt(r.id) == parseInt(value.value))
      this.listPkStart=this.listPkAll.filter(r=>parseInt(r.pk) < parseInt(pk[0].pk))
      this.listing();
    }
    preview(src: any,date:any,comment:any): void {
      let srcs=[src];
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {
          src: srcs,
          date:date,
          comment:comment,
          title: 'របាយការណ៍ធ្វើតេស្ត'
      };
      
      dialogConfig.width = "850px";
      this._dialog.open(PreviewImageMultiComponent, dialogConfig);
    }

    downloadReport(report: number,data:any ){
      let obj = {
        report: report,
        testing_trx_id: data.id,
      }
      
      if(report === 6){
        const currentDate = new Date();
        const year = currentDate.getFullYear();
        const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
        const day = ("0" + currentDate.getDate()).slice(-2);
        const hours = ("0" + currentDate.getHours()).slice(-2);
        const minutes = ("0" + currentDate.getMinutes()).slice(-2);
        const seconds = ("0" + currentDate.getSeconds()).slice(-2);
        const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
        let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
        this._reportService.getjsReport(this.id, obj).subscribe((res: any) => {
        
            this.isSaving = false;
            let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
            FileSaver.saveAs(blob,this.code + '-របាយការណ៍ធ្វើតេស្ត-' + formattedDate +'.pdf');
            dialog.close();
        }, (err: any) => {
            this.isSaving = false;
          
            dialog.close();
        });
      }
    }
    // =================================>> Convert base64 to blob 
    b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;
      var byteCharacters = atob(b64Data);
      var byteArrays = [];
      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);
          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }
          var byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
      }
      var blob = new Blob(byteArrays, { type: contentType });
      return blob;
    }
    changeView(value){
     if(value =="grid"){
      this.listView=true;
      this.gridView=false;
     }else{
      this.listView=false;
      this.gridView=true;
      this.setDataChart(this.dataChart);
     }
    }
    setDataChart(data){
      // console.log(this.dataChart)
      this.serial=[];
      this.testPass=[];
      let categories:string[]=[];
      let color:string[]=[];
      // let da=[10, 20, 40, 33, 55, 100, 2, 66, 22, 90, 57, 59, 61, 38, 53];
      if(data.length > 0){
        data[0].data.forEach(e=> {
          let name=e.name;
          this.serial.push({
            name: name,
            data: []
          })
          this.testPass.push({
            name:name,
            data:[]
          })
          color.push(e.color)
        });
      }
      data.forEach(r=>{
          categories.push('Pk'+r.pk);
          r.data.forEach(d=>{
            this.serial.forEach((s,i)=>{
              if(s.name == d.name){
                s.data.push(d.data);
                this.testPass[i].data.push(d.is_pass)
              }
            })
          })
      })
      
      // console.log(categories,this.serial,this.testPass)
      
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
      const day = ("0" + currentDate.getDate()).slice(-2);
      const hours = ("0" + currentDate.getHours()).slice(-2);
      const minutes = ("0" + currentDate.getMinutes()).slice(-2);
      const seconds = ("0" + currentDate.getSeconds()).slice(-2);
      const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
        
      this.chartOptions = {
        series: this.serial
    //     [
    //       {
    //     name: 'DBST',
    //     data: [44, 55, 57, 56, 61, 58, 63, 60, 66, 55, 77, 56, 98, 28, 63]
    //     }, {
    //     name: 'Base Course',
    //     data: [76, 85, 101, 98, 87, 105, 91, 114, 94, 5, 11, 11, 18, 98, 43]
    //   }, {
    //     name: 'Sub Base',
    //     data: [35, 41, 36, 26, 45, 48, 52, 53, 41, 88, 7, 90, 23, 18, 13]
    //   }, {
    //     name: 'Upper Subgrade',
    //     data: [13, 11, 56, 16, 65, 78, 12, 3, 41, 15, 17, 71, 55, 8, 33]
    //   }, {
    //     name: 'Lower Subgrade',
    //     data: [10, 20, 40, 33, 55, 100, 2, 66, 22, 90, 57, 59, 61, 38, 53],
        
    //   }
    // ]
    ,
      
      chart: {
        type: 'bar',
        toolbar: {
          show: true,
          tools:{
            download:true
            // download:'<button class=" rounded-md text-white bg-yellow-300 p-1 w-9 h-9"><img src="https://as1.ftcdn.net/v2/jpg/00/82/56/50/1000_F_82565082_VlCkD9spnOhdjlB9LnO6ukaUXTHKZm0v.jpg" alt=""/></button>' // <== line to add
          },
          export: {
            csv: {
              filename: undefined,
              columnDelimiter: ',',
              headerCategory: 'category',
              headerValue: 'value',
              dateFormatter(timestamp) {
                return new Date(timestamp).toDateString()
              }
            },
            svg: {
              filename:this.code + '-របាយការណ៍ធ្វើតេស្ត-' + formattedDate ,
            },
            png: {

              filename:this.code + '-របាយការណ៍ធ្វើតេស្ត-' + formattedDate,
            }
          },
        },
        
        height: 550
      },
      
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '93%',
          borderRadius: 4,
          dataLabels: {
            hideOverflowingLabels:true,
            position: 'top'
          }
        }, 
      }, 
      dataLabels: {
        enabled: false,
        
      },
      colors:color,
      // colors: ['#3b82f6','#34d399','#fbbf24', '#ef4444','#22d3ee','#805AD5', '#B794F4'],
      // colors: ['#e5e7eb','#e5e7eb', '#e5e7eb','#e5e7eb', '#e5e7eb','#e5e7eb', '#e5e7eb'],
      stroke: {
        show: true,
        width: 1,
        colors: ['#fff']
      },
      xaxis: {
        categories:categories
        //  ['PK15', 'PK16', 'PK17', 'PK18', 'PK19', 'PK20', 'PK21', 'PK22', 'PK23', 'PK24', 'PK25', 'PK26', 'PK27', 'PK28', 'PK29']
         ,
      },
      yaxis: {
        title: {
          text: ""+this.code
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
          
            fillSeriesColor: false,
            x:{
                show:true
            },
            custom: ({
                series, seriesIndex, dataPointIndex, w
            }): string => `<div class="flex flex-col items-center h-[100px] min-h-[100px] max-h-[100px] z-999 w-[170px] ">
                                                <div class="flex items-center py-2 border-b w-full  "> 
                                                  <div class="ml-2 w-3  h-3 rounded-full" style="background-color:${w.config.colors[seriesIndex]};"></div>
                                                  <div class="ml-2 text-md leading-none">${w.globals.initialSeries[seriesIndex].name}</div>
                                                </div>
                                                <div class="flex pl-3 py-2 flex-col w-full ">
                                                  <div class=" text-md  leading-none pb-1 "> បានតេស្ត ${w.globals.initialSeries[seriesIndex].data[dataPointIndex]} ដង </div>
                                                  <div class=" text-md leading-none pb-1"> ជាប់( <span class=" text-green-600">${this.testPass[seriesIndex].data[dataPointIndex]}</span> ) </div>
                                                  <div class=" text-md leading-none pb-1"> ធ្លាក់( <span class=" text-red-600">${w.globals.initialSeries[seriesIndex].data[dataPointIndex] - this.testPass[seriesIndex].data[dataPointIndex]}</span> ) </div>
                                                 </div>
                                                
                                            </div>`
            }
      };

    }
}


interface serialData{
  name:string;
  data:number[];
}
interface testPass{
  name:string;
  data:number[];
}
