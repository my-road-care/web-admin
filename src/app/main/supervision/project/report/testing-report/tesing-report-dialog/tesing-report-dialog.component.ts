import { Component,  Inject, OnInit, TemplateRef, ViewChild,} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment as env } from 'environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { ReportService } from '../../report.service';
import * as FileSaver from 'file-saver';
@Component({
  selector: 'app-tesing-report-dialog',
  templateUrl: './tesing-report-dialog.component.html',
  styleUrls: ['./tesing-report-dialog.component.scss']
})
export class TesingReportDialogComponent implements OnInit {
  @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
  public url:any=env.fileUrl;
  public displayedColumns: string[] = ['nos','location','standard','result','ispass'];
  public dataSource:any;
  public data:any=[];
  public isLoading:boolean=false;
  public isSaving:boolean =false;
  public suffect:any='';
  constructor(
  
    @Inject(MAT_DIALOG_DATA) public datas: any,
    private dialogRef: MatDialogRef<TesingReportDialogComponent>,
    private _service:ReportService,
    private _dialog: MatDialog
  )
  {}

  ngOnInit(): void {
    this.data.push(this.datas.data);
    // console.log(this.datas.setup.project.code);
    this.datas.setup.project.surface.forEach(element => {
      if(element.name ==  this.datas.setup.project.surface[0].name) this.suffect+= this.datas.setup.project.surface[0].name;
      else{
        this.suffect+=', '+this.datas.setup.project.surface[0].name;
      }
       
  });
    this.listing();
  }
  listing(){
    this.isLoading=true;
    this._service.viewTesting(this.datas.id,this.datas.data.id).subscribe((res:any)=>{
      this.data=res.data;
      this.isLoading=false;
      this.dataSource=new MatTableDataSource(this.data);
    })

  }
  downloadReport(report: number,testing_trx_id:number ){
    let obj = {
      report: report,
      testing_trx_id: this.datas.data.id,
    }
    if(report === 6){
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
      const day = ("0" + currentDate.getDate()).slice(-2);
      const hours = ("0" + currentDate.getHours()).slice(-2);
      const minutes = ("0" + currentDate.getMinutes()).slice(-2);
      const seconds = ("0" + currentDate.getSeconds()).slice(-2);
      const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
      let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
      this._service.getjsReport(this.datas.id, obj).subscribe((res: any) => {
        //console.log(res);
          this.isSaving = false;
          let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
          FileSaver.saveAs(blob,this.datas.setup.project.code + '-របាយការណ៍ធ្វើតេស្ត-' + formattedDate +'.pdf');
          dialog.close();
      }, (err: any) => {
          this.isSaving = false;
          ////console.log(err);
          dialog.close();
      });
    }
  }
  // =================================>> Convert base64 to blob 
  b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
   }
}
