import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import * as _moment from 'moment';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class FilterComponent implements OnInit {
    Filters = new EventEmitter();
    public categories: any[] = [];
    public budget_years: any[] = [];
    public status: any[] = [];
    public provinces: any[] = [];
    public budgets: { id: number, name: string }[] = [
        {
            id: 1,
            name: 'ជំពូក២១'
        },
        {
            id: 2,
            name: 'ជំពូក៦១'
        },
    ]

    public category_id: number = 0;
    public province_id: number = 0;
    public budget_year_id: number = 0;
    public budget_id: number = 0;
    public status_id: number = 0;

    public form: any = null;
    public to: any = null;
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _dialogRef: MatDialogRef<FilterComponent>
    ) { }
    ngOnInit(): void {
        this.budget_years = this.data?.budget_years;
        this.categories = this.data?.categories;
        this.status = this.data?.status;
        this.provinces = this.data?.provinces;
        this.budget_id = this.data?.budget_id;
        this.budget_year_id = this.data?.budget_year_id;
        this.category_id = this.data?.category_id;
        this.province_id = this.data?.province_id;
        this.status_id = this.data?.status_id;
        this.form = this.data?.form;
        this.to = this.data?.to;
    }
    submit(): void {
        if (this.form != null) this.form = moment(this.form).format('YYYY-MM-DD');
        if (this.to != null) this.to = moment(this.form).format('YYYY-MM-DD');
        const filters: Filters = {
            budget_id: this.budget_id,
            budget_year_id: this.budget_year_id,
            category_id: this.category_id,
            province_id: this.province_id,
            status_id: this.status_id,
            form: this.form,
            to: this.to
        }
        this.Filters.emit(filters);
        this._dialogRef.close();
    }
    clear(formControll: string): void {
        if (formControll === 'from') this.form = null;
        else this.to = null;
    }
}

export interface Filters {
    budget_id?: null | number
    budget_year_id?: null | number
    category_id?: null | number
    province_id?: null | number
    status_id?: null | number
    form?: null | Date
    to?: null | Date
}
