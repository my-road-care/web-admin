import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ProjectService } from '../project.service';
import { SetupService } from '../setup.service';
import { ChooseCategoryComponent } from '../create/choose-category/choose-category.component';
import { Router } from '@angular/router';
import { FilterComponent, Filters } from './filter/filter.component';

@Component({
    selector: 'app-listing',
    templateUrl: './listing.component.html',
    styleUrls: ['./listing.component.scss'],
})
export class ListingComponent implements OnInit {

    public displayedColumns: string[] = ['no', 'code', 'project', 'piu', 'type', 'status', 'action'];
    public dataSource: any;
    public isSearching: boolean = true;
    public data: any = [];
    public total: number = 10;
    public limit: number = 10;
    public page: number = 1;
    public key: string = '';
    public budget_id: number = 0;
    public category_id: number = 0;
    public province_id: number = 0;
    public budget_year_id: number = 0;
    public status_id: number = 0;
    public form: any = null;
    public to: any = null;
    public piu: any[] = [];
    public categories: Category[] = [];
    public budget_years: BudgetYear[] = [];
    public status: Status[] = [];
    public provinces: Province[] = [];
    public setup: DataSetUp;
    // public access_permission: any;

    public createProject: string = localStorage.getItem('access');
    /**
     * Constructor
     */
    constructor(
        private _projectService: ProjectService,
        private _snackBar: SnackbarService,
        private _setupService: SetupService,
        private _dialog: MatDialog,
        private _router: Router
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.getSetup();
        this.listing(this.limit, this.page);
    }

    filter(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            budget_years: this.budget_years,
            categories: this.categories,
            status: this.status,
            provinces: this.provinces,
            budget_id: this?.budget_id,
            budget_year_id: this?.budget_year_id,
            category_id: this?.category_id,
            province_id: this?.province_id,
            status_id: this?.status_id,
            form: this?.form,
            to: this?.to
        }
        dialogConfig.width = "650px";
        dialogConfig.autoFocus = false;
        dialogConfig.disableClose = true;
        const dialogRef = this._dialog.open(FilterComponent, dialogConfig);
        dialogRef.componentInstance.Filters.subscribe((response: Filters) => {
            this.budget_id = response?.budget_id;
            this.budget_year_id = response?.budget_year_id;
            this.category_id = response?.category_id;
            this.province_id = response?.province_id;
            this.status_id = response?.status_id;
            this.form = response?.form;
            this.to = response?.to;
            this.listing();
        });
    }

    getSetup(): void {
        this._projectService.getSetup().subscribe((res: any) => {
            this.setup = res?.data as DataSetUp;
            this.budget_years = this.setup.budget_years;
            this.categories = this.setup.categories;
            this.status = this.setup.status;
            this.provinces = this.setup.provinces;
        });
    }

    comfirmCategory(): void {
        const dialogRef = this._dialog.open(ChooseCategoryComponent, { width: 'auto', height: 'auto', disableClose: false });
        dialogRef.afterClosed().subscribe((result) => {
            if (result === 1) {
                this._router.navigateByUrl('/sup/projects/create/1');
            }
            else if (result === 2) {
                this._router.navigateByUrl('/sup/projects/create/2');
            }
        });
    }

    setCode(code: string): void {
        this._setupService.code = code;
    }

    //===================================>> List
    listing(_limit: number = 10, _page: number = 1): any {

        const param: any = {
            pageSize: _limit,
            page: _page,
        };

        if (this.key != '') {
            param.key = this.key;
        }
        if (this.category_id != 0) {
            param.category_id = this.category_id;
        }
        if (this.status_id != 0) {
            param.status_id = this.status_id;
        }
        if (this.page != 0) {
            param.page = this.page;
        }
        if (this.province_id != 0) {
            param.province_id = this.province_id
        }
        if (this.budget_year_id != 0) {
            param.budget_year_id = this.budget_year_id
        }

        this.isSearching = true;
        this._projectService.listing(param).subscribe(
            (res: any) => {
                this.isSearching = false;
                this.data = res.data;
                localStorage.setItem('project_road_ids_mapping', JSON.stringify(this.data.map(({ id, road_id }) => ({ id, road_id }))))
                this.dataSource = new MatTableDataSource(this.data);
                this.total = res.pagination.total;
                this.page = res.pagination.current_page;
                this.limit = res.pagination.per_page;
            },
            (err: any) => {
                this.isSearching = false;
                this._snackBar.openSnackBar('Something went wrong.', 'error');
            }
        )
    }

    deleteProject(project_id: number = 0): void {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this._projectService.deleteProject(project_id).subscribe((res: any) => {
                    this.isSearching = false;
                    this._snackBar.openSnackBar(res.message, '');
                    this.data = this.data.filter((v: any) => v.id !== project_id);
                    this.dataSource = new MatTableDataSource(this.data);
                    this.total -= 1;
                }, (err: any) => {
                    this.isSearching = false;
                    this._snackBar.openSnackBar('Something went wrong.', 'error');
                });
            }
        });
    }

    //=======================================>> On Page Changed
    onPageChanged(event: any): any {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }
    }
}

interface Category {
    id: number,
    name: string
}
interface Status {
    id: number,
    kh_name: string,
    en_name: string,
    color: string
}
interface Province {
    id: number,
    name: string
}
interface BudgetYear {
    id: number,
    years: string
}
interface DataSetUp {
    categories: Category[]
    status: Status[],
    provinces: Province[],
    budget_years: BudgetYear[]
}
