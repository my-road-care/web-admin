import { ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Observable, Subject, map, startWith } from 'rxjs';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { InputNumberFormatService } from 'app/shared/input-number';
import { MatStepper } from '@angular/material/stepper';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ProjectService } from '../project.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as _moment from 'moment';
import { GlobalConstants } from 'app/shared/global-constants';
import { DialogComponent } from '../../organization/dialog/dialog.component';
import { CreateDialogComponent } from '../../setting/budget-year/create-dialog/create-dialog.component';
import { DropdownComponent } from '../shared/dropdown/dropdown.component';
import { Budget, Category, DataSetup, Entity, Executive, Framework, Organization, ProjectOwner, Reviewer, Road, Role, Surface, Type, Year } from './create.tyeps';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class CreateComponent implements OnInit, OnDestroy {

    @ViewChild('form', { static: true }) form: ElementRef;
    @ViewChild('createProjectNgForm') createProjectNgForm: NgForm;
    @ViewChild('stepper', { static: false }) stepper: MatStepper;
    @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;

    private _unsubscribeAll: Subject<any> = new Subject<any>();
    public firstcreateProjectForm: UntypedFormGroup;
    public secondcreateProjectForm: UntypedFormGroup;
    public thirdcreateProjectForm: UntypedFormGroup;

    public setup: DataSetup;
    public types: Type[];
    public budget_plan: Budget[];
    public years: any[] = [];
    public project_owners: any[] = [];
    public entities: any[];
    public surfaces: Surface[];
    public frameworks: any[];
    public executives: any[] = [];
    public reviewers: any[];
    public categories: Category;
    public roads: any[] = [];
    public roles: Role[] = [];

    public data: any[] = [];
    public code_error: string = '';
    public road: any = null;
    public wait: boolean = false;
    public provinces: any[] = [];

    public bud_request_total: number = 0;
    public bud_after_warranty_total: number = 0;
    public bud_contract_total: number = 0;

    public hideForm: boolean = false;
    public category_id: number = null;
    public checkRoad: boolean = false;

    public filteredRoads: Observable<string[]>;
    public filteredYears: Observable<string[]>;
    public filteredExecutives: Observable<string[]>;
    public filteredEntities: Observable<string[]>;
    public filteredReviewers: Observable<string[]>;
    public filteredFrameworks: Observable<string[]>;
    public filteredProjectOwners: Observable<string[]>;

    constructor(
        private inputNumberSevice: InputNumberFormatService,
        private _formBuilder: UntypedFormBuilder,
        private _projectService: ProjectService,
        private _snackBar: SnackbarService,
        private _dialog: MatDialog,
        private _router: Router,
        private _route: ActivatedRoute,
    ) {
        this._route.paramMap.subscribe((params: any) => {
            this.category_id = params.get('category_id');
        });
    }

    ngOnInit(): void {
        if (this.category_id == 1) {
            this.hideForm = true;
            this.categories = {
                id: 1,
                name: 'ថវិការជាតិ'
            };
        }
        else if (this.category_id == 2) {
            this.hideForm = false;
            this.categories = {
                id: 2,
                name: 'កម្ចីឥទានក្រៅប្រទេស'
            };
        }
        else {
            // not comfirm category
            this._router.navigateByUrl('/sup/projects');
        }
        // Subscribe to get setup data
        this.subscribeData();
        // Form of Step 1
        this.StepOne();
        // Form of Step 2
        this.StepTwo();
        // Form of Step 3
        this.StepThree();
    }

    public noRoad: boolean = false;
    public onOptionSelected(event: MatAutocompleteSelectedEvent) {
        this.firstcreateProjectForm.get('road_id').setValue(event.option.value);
        this.checkRoad = true;
        this.roads.forEach((v: any) => {
            if (v.name == event.option.value) {
                this.provinces = [];
                this._projectService.getProvince(v.id).subscribe((res: any) => {
                    this.checkRoad = false;
                    this.provinces = res?.provinces;
                    if (this.provinces.length == 0) {
                        this._snackBar.openSnackBar('ផ្លូវនេះមិនទាន់មានការកំណត់ទីតាំង​(ខេត្ត)', GlobalConstants.error);
                    }
                    this.firstcreateProjectForm.get('province_id').setValue(null);
                }, (err: any) => {
                    console.log(err)
                })
            }
        });
    }
    checkRoads(): void {
        let j = 0;
        if (this.firstcreateProjectForm.get('road_id').value != null || this.firstcreateProjectForm.get('road_id').value != '') {
            this.checkRoad = true;
            this.roads.forEach((v: any) => {
                if (v.name == this.firstcreateProjectForm.get('road_id').value) {
                    j++;
                    this.provinces = [];
                    this._projectService.getProvince(v.id).subscribe((res: any) => {
                        this.checkRoad = false;
                        this.provinces = res?.provinces;
                        if (this.provinces.length == 0) {
                            this._snackBar.openSnackBar('ផ្លូវនេះមិនទាន់មានការកំណត់ទីតាំង​(ខេត្ត)', GlobalConstants.error);
                        }
                        this.firstcreateProjectForm.get('province_id').setValue(null);
                    }, (err: any) => {
                        console.log(err)
                    })
                } else {
                    this.provinces = [];
                    this.firstcreateProjectForm.get('province_id').setValue(null);
                }
            });
        }
        if (j == 0) {
            this.noRoad = true;
            this.checkRoad = false;
        } else {
            this.noRoad = false;
        }
    }


    public noYear: boolean = false;
    public year: Year;
    public onYearsSelected(event: MatAutocompleteSelectedEvent) {
        this.firstcreateProjectForm.get('budget_year_id').setValue(event.option.value);
        this.years.forEach((v: Year) => {
            if (v.year == event.option.value) {
                this.year = v;
            }
        });
    }
    checkYears(): void {
        let j = 0;
        if (this.firstcreateProjectForm.get('budget_year_id').value != null || this.firstcreateProjectForm.get('budget_year_id').value != '') {
            this.years.forEach((v: Year) => {
                if (v.year.includes(this.firstcreateProjectForm.get('budget_year_id').value)) {
                    j++;
                }
            });
        }
        if (j == 0) {
            this.noYear = true;
        } else {
            this.noYear = false;
        }
    }
    createYear(year: string) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = year;
        dialogConfig.autoFocus = false;
        dialogConfig.width = "550px";
        const dialogRef = this._dialog.open(CreateDialogComponent, dialogConfig);
        this._router.events.subscribe(() => {
            dialogRef.close();
        })
        dialogRef.componentInstance.CreateBudget.subscribe((response: any) => {
            this.noYear = false;
            delete response.created_at;
            delete response.updated_at;
            this.years.push(response);
            this.firstcreateProjectForm.get('budget_year_id').setValue(response?.year);
        });
    }

    public noExecutive: boolean = false;
    public executive: Executive;

    @ViewChild('executive') executiveDropdownComponent: DropdownComponent;

    public onExecutivesSelected(event: MatAutocompleteSelectedEvent) {
        this.firstcreateProjectForm.get('executive_director_id').setValue(event.option.value);
        this.executives.forEach((v: Executive) => {
            if (v.name == event.option.value) {
                this.executive = v;
            }
        });
    }
    checkExecutives(): void {
        let j = 0;
        if (this.firstcreateProjectForm.get('executive_director_id').value != null || this.firstcreateProjectForm.get('executive_director_id').value != '') {
            this.executives.forEach((v: Executive) => {
                if (v.name.includes(this.firstcreateProjectForm.get('executive_director_id').value)) {
                    j++;
                }
            });
        }
        if (j == 0) {
            this.noExecutive = true;
        } else {
            this.noExecutive = false;
        }
    }


    public noEntity: boolean = false;
    public entity: Entity;
    public onEntitiesSelected(event: MatAutocompleteSelectedEvent) {
        this.firstcreateProjectForm.get('entity_id').setValue(event.option.value);
        this.entities.forEach((v: Entity) => {
            if (v.name == event.option.value) {
                this.entity = v;
            }
        });
    }
    checkEntities(): void {
        let j = 0;
        if (this.firstcreateProjectForm.get('entity_id').value != null || this.firstcreateProjectForm.get('entity_id').value != '') {
            this.entities.forEach((v: Entity) => {
                if (v.name.includes(this.firstcreateProjectForm.get('entity_id').value)) {
                    j++;
                }
            });
        }
        if (j == 0) {
            this.noEntity = true;
        } else {
            this.noEntity = false;
        }
    }

    public noReviewer: boolean = false;
    public reviewer: Reviewer;
    public onReviewersSelected(event: MatAutocompleteSelectedEvent) {
        this.firstcreateProjectForm.get('reviewer_id').setValue(event.option.value);
        this.reviewers.forEach((v: Reviewer) => {
            if (v.name == event.option.value) {
                this.reviewer = v;
            }
        });
    }
    checkReviewers(): void {
        let j = 0;
        if (this.firstcreateProjectForm.get('reviewer_id').value != null || this.firstcreateProjectForm.get('reviewer_id').value != '') {
            this.reviewers.forEach((v: Reviewer) => {
                if (v.name.includes(this.firstcreateProjectForm.get('reviewer_id').value)) {
                    j++;
                }
            });
        }
        if (j == 0) {
            this.noReviewer = true;
        } else {
            this.noReviewer = false;
        }
    }

    public noProjectOwner: boolean = false;
    public projectOwner: ProjectOwner;
    public onProjectOwnersSelected(event: MatAutocompleteSelectedEvent) {
        this.firstcreateProjectForm.get('project_owner_id').setValue(event.option.value);
        this.project_owners.forEach((v: ProjectOwner) => {
            if (v.name == event.option.value) {
                this.projectOwner = v;
            }
        });
    }
    checkProjectOwners(): void {
        let j = 0;
        if (this.firstcreateProjectForm.get('project_owner_id').value != null || this.firstcreateProjectForm.get('project_owner_id').value != '') {
            this.project_owners.forEach((v: ProjectOwner) => {
                if (v.name.includes(this.firstcreateProjectForm.get('project_owner_id').value)) {
                    j++;
                }
            });
        }
        if (j == 0) {
            this.noProjectOwner = true;
        } else {
            this.noProjectOwner = false;
        }
    }

    public noFramework: boolean = false;
    public framework: Framework;
    public onFrameworksSelected(event: MatAutocompleteSelectedEvent) {
        this.firstcreateProjectForm.get('framework_id').setValue(event.option.value);
        this.frameworks.forEach((v: Framework) => {
            if (v.name == event.option.value) {
                this.framework = v;
            }
        });
    }
    checkFrameworks(): void {
        let j = 0;
        if (this.firstcreateProjectForm.get('framework_id').value != null || this.firstcreateProjectForm.get('framework_id').value != '') {
            this.frameworks.forEach((v: Framework) => {
                if (v.name.includes(this.firstcreateProjectForm.get('framework_id').value)) {
                    j++;
                }
            });
        }
        if (j == 0) {
            this.noFramework = true;
        } else {
            this.noFramework = false;
        }
    }

    createOrganization(value: string, type_id: number) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            kh_name: value,
            type_id: type_id
        }
        dialogConfig.autoFocus = false;
        dialogConfig.width = "550px";
        const dialogRef = this._dialog.open(DialogComponent, dialogConfig);
        dialogRef.componentInstance.createEmit.subscribe((res: any) => {
            if (res) {
                res.name = res?.kh_name
                delete res.kh_name;
                delete res.en_name;
                delete res.created_at;
                delete res.updated_at;
                if (type_id === Organization.Executive) {
                    this.noExecutive = false;
                    this.executives.push(res);
                    this.firstcreateProjectForm.get('executive_director_id').setValue(res.name);
                } else if (type_id === Organization.Entity) {
                    this.noEntity = false;
                    this.entities.push(res);
                    this.firstcreateProjectForm.get('entity_id').setValue(res.name);
                } else if (type_id === Organization.Reviewer) {
                    this.noReviewer = false;
                    this.reviewers.push(res);
                    this.firstcreateProjectForm.get('reviewer_id').setValue(res.name);
                } else if (type_id === Organization.Framework) {
                    this.noFramework = false;
                    this.frameworks.push(res);
                    this.firstcreateProjectForm.get('framework_id').setValue(res.name);
                } else if (type_id === Organization.ProjectOwner) {
                    this.noProjectOwner = false;
                    this.project_owners.push(res);
                    this.firstcreateProjectForm.get('project_owner_id').setValue(res.name);
                }
            }
        });
    }

    public check(): void {

        // console.log(this.executiveDropdownComponent.form.get('select').value); 

        // if(this.executiveDropdownComponent.form.get('select').value){


        //     this.firstcreateProjectForm.get('executive_director_id').setValue(this.executiveDropdownComponent.selected); 
        // }else{
        //     this.executiveDropdownComponent.markAsError();
        // }

        //===============================Check Road===============================\\
        this.checkRoad = true;
        this.roads.forEach((v: any) => {
            if (v.name == this.firstcreateProjectForm.get('road_id').value) {
                this.checkRoad = false,
                    this.wait = true;
                this.road = v;
            }
        });
        if (this.checkRoad) {
            this.road = null;
            this.wait = false;
            this._snackBar.openSnackBar('សូមបញ្ចូលផ្លូវឲ្យបានត្រឹមត្រូវ', 'error');
            return;
        }
        this._projectService.checkCode(this.firstcreateProjectForm.value.code).subscribe((res: boolean) => {
            if (!res) {
                this.stepper.previous();
                this.firstcreateProjectForm.get('code').setValue(this.firstcreateProjectForm.value.code);
                this.firstcreateProjectForm.get('code').setErrors(reportError);
                this.code_error = 'លេខកូដគម្រោងមានរួចហើយ';
                this._snackBar.openSnackBar('លេខកូដគម្រោងមានរួចហើយ', 'error');
                this.wait = false;
                return;
            }
            this.wait = true;
            this.stepper.next();
        })
        //===============================Check Year===============================\\
        let checkYear: boolean = true;
        this.years.forEach((v: Year) => {
            if (v.year == this.firstcreateProjectForm.get('budget_year_id').value) {
                this.year = v;
                checkYear = false;
            }
        })
        if (checkYear) {
            this._snackBar.openSnackBar('ឆ្នាំនៃជំពូកថវិកាមិនត្រឹមត្រូវ', 'error');
            return;
        }
        //===============================Check Executive===============================\\
        let checkExecutive: boolean = true;
        this.executives.forEach((v: Executive) => {
            if (v.name == this.firstcreateProjectForm.get('executive_director_id').value) {
                this.executive = v;
                checkExecutive = false;
            }
        })
        if (checkExecutive) {
            this._snackBar.openSnackBar('ការជ្រើសរើសប្រតិបត្តិករមិនត្រឹមត្រូវ', 'error');
            return;
        }
        //===============================Check Entity===============================\\
        if (this.categories.id === 2) {
            let checkEntity: boolean = true;
            this.entities.forEach((v: Entity) => {
                if (v.name == this.firstcreateProjectForm.get('entity_id').value) {
                    this.entity = v;
                    checkEntity = false;
                }
            })
            if (checkEntity) {
                this._snackBar.openSnackBar('ការជ្រើសរើសអង្គភាពគ្រប់គ្រងមិនត្រឹមត្រូវ', 'error');
                return;
            }
        }
        //===============================Check Reviewer===============================\\
        if (this.categories.id === 1) {
            let checkReviewer: boolean = true;
            this.reviewers.forEach((v: Reviewer) => {
                if (v.name == this.firstcreateProjectForm.get('reviewer_id').value) {
                    this.reviewer = v;
                    checkReviewer = false;
                }
            })
            if (checkReviewer) {
                this._snackBar.openSnackBar('ការជ្រើសរើសអង្គភាពត្រួតពិនិត្យមិនត្រឹមត្រូវ', 'error');
                return;
            }
        }
        //===============================Check Project Owner===============================\\
        let checkProjectOwner: boolean = true;
        this.project_owners.forEach((v: ProjectOwner) => {
            if (v.name == this.firstcreateProjectForm.get('project_owner_id').value) {
                this.projectOwner = v;
                checkProjectOwner = false;
            }
        })
        if (checkProjectOwner) {
            this._snackBar.openSnackBar('ការជ្រើសរើសម្ចាស់គម្រោងមិនត្រឹមត្រូវ', 'error');
            return;
        }
        //===============================Check Framework===============================\\
        let checkFramework: boolean = true;
        this.frameworks.forEach((v: Framework) => {
            if (v.name == this.firstcreateProjectForm.get('framework_id').value) {
                this.framework = v;
                checkFramework = false;
            }
        })
        if (checkFramework) {
            this._snackBar.openSnackBar('ការជ្រើសរើសអង្គក្របខណ្ឌមិនត្រឹមត្រូវ', 'error');
            return;
        }
    }

    public unawiat(): void {
        this.wait = false;
    }

    public checkCanStay(): void {
        this.checkRoad = true;
        this.roads.forEach((v: any) => {
            if (v.name == this.firstcreateProjectForm.get('road_id').value) {
                this.checkRoad = false,
                    this.wait = false;
                this.road = v;
            }
        });

        if (this.checkRoad) {
            this.road = null;
            this.wait = false;
            this._snackBar.openSnackBar('សូមបញ្ចូលផ្លូវឲ្យបានត្រឹមត្រូវ', 'error');
            return;
        }
        this._projectService.checkCode(this.firstcreateProjectForm.value.code).subscribe((res: boolean) => {
            if (!res) {
                this.stepper.previous();
                this.firstcreateProjectForm.get('code').setValue(this.firstcreateProjectForm.value.code);
                this.firstcreateProjectForm.get('code').setErrors(reportError);
                this.code_error = 'លេខកូដគម្រោងមានរួចហើយ';
                this._snackBar.openSnackBar('លេខកូដគម្រោងមានរួចហើយ', 'error');
            }
        })
    }

    public submit(): void {

        // Do nothing if the form is invalid
        if (this.firstcreateProjectForm.invalid && this.secondcreateProjectForm.invalid && this.thirdcreateProjectForm.invalid) return;

        this.firstcreateProjectForm.get('road_id').setValue(this.road.id);
        this.firstcreateProjectForm.get('budget_year_id').setValue(this.year.id);
        this.firstcreateProjectForm.get('executive_director_id').setValue(this.executive.id);
        if (this.categories.id === 1) {
            this.firstcreateProjectForm.get('reviewer_id').setValue(this.reviewer.id);
        } else if (this.categories.id === 1) {
            this.firstcreateProjectForm.get('entity_id').setValue(this.entity.id);
        }
        this.firstcreateProjectForm.get('project_owner_id').setValue(this.projectOwner.id);
        this.firstcreateProjectForm.get('framework_id').setValue(this.framework.id);

        if (this.thirdcreateProjectForm.get('project_start_date_contract').value != null)
            this.thirdcreateProjectForm.get('project_start_date_contract').setValue(moment(this.thirdcreateProjectForm.get('project_start_date_contract').value).format('YYYY-MM-DD'));
        else
            this.thirdcreateProjectForm.get('project_start_date_contract').setValue(null);

        if (this.thirdcreateProjectForm.get('project_end_date_contract').value != null)
            this.thirdcreateProjectForm.get('project_end_date_contract').setValue(moment(this.thirdcreateProjectForm.get('project_end_date_contract').value).format('YYYY-MM-DD'));
        else
            this.thirdcreateProjectForm.get('project_end_date_contract').setValue(null);

        if (this.thirdcreateProjectForm.get('delay_date').value != null)
            this.thirdcreateProjectForm.get('delay_date').setValue(moment(this.thirdcreateProjectForm.get('delay_date').value).format('YYYY-MM-DD'));
        else
            this.thirdcreateProjectForm.get('delay_date').setValue(null);

        if (this.thirdcreateProjectForm.get('close_project_date').value != null)
            this.thirdcreateProjectForm.get('close_project_date').setValue(moment(this.thirdcreateProjectForm.get('close_project_date').value).format('YYYY-MM-DD'));
        else
            this.thirdcreateProjectForm.get('close_project_date').setValue(null);

        const data = {
            ...this.firstcreateProjectForm.value,
            category_id: this.categories.id,
            ...this.secondcreateProjectForm.value,
            ...this.thirdcreateProjectForm.value
        };

        // Disable the form
        this.thirdcreateProjectForm.disable();
        // create
        const dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
        this._projectService.create(data).subscribe((res: any) => {
            // Re-enable the form
            this.thirdcreateProjectForm.enable();
            this._snackBar.openSnackBar(res.message, '');
            this._router.navigate(['/sup/projects/' + res?.id + '/info']);
            dialog.close();
        }, (err: any) => {
            // Re-enable the form
            this.firstcreateProjectForm.get('road_id').setValue(this.road.name);
            this.firstcreateProjectForm.get('budget_year_id').setValue(this.year.year);
            this.firstcreateProjectForm.get('executive_director_id').setValue(this.executive.name);
            if (this.categories.id === 1) {
                this.firstcreateProjectForm.get('reviewer_id').setValue(this.reviewer.name);
            } else if (this.categories.id === 1) {
                this.firstcreateProjectForm.get('entity_id').setValue(this.entity.name);
            }
            this.firstcreateProjectForm.get('project_owner_id').setValue(this.projectOwner.name);
            this.firstcreateProjectForm.get('framework_id').setValue(this.framework.name);
            this.thirdcreateProjectForm.enable();
            dialog.close();
            // console.log(err);
            let errors: any[] = [];
            errors = err.error.errors;
            let messages: any[] = [];
            let text: string = '';
            if (errors.length > 0) {
                errors.forEach((v: any) => {
                    messages.push(v.message)
                });
                if (messages.length > 1) {
                    text = messages.join('-');
                } else {
                    text = messages[0];
                }
            } else {
                text = err.error.message;
            }
            this._snackBar.openSnackBar(text, 'error');
        });
    }

    private StepOne(): void {
        this.firstcreateProjectForm = this._formBuilder.group({
            name: [null, Validators.required],
            code: [null, Validators.required],
            executive_director_id: [null, Validators.required],
            type_id: [null, Validators.required],
            budget_plan_id: [null, Validators.required],
            budget_year_id: [null, Validators.required],
            framework_id: [null, Validators.required],
            project_owner_id: [null, Validators.required],
            entity_id: [null, this.categories?.id == 2 ? Validators.required : []],
            province_id: [null],
            reviewer_id: [null, this.categories?.id == 1 ? Validators.required : []],
            road_id: [null, Validators.required],
            surface_id: [null, Validators.required]
        });
    }

    private StepTwo(): void {
        this.secondcreateProjectForm = this._formBuilder.group({
            bud_request_construction: ['0', [Validators.required, Validators.maxLength(26)]], //20 digit + 6 comma
            bud_request_check: ['0', [Validators.required, Validators.maxLength(26)]],
            bud_after_warranty_con: ['0', [Validators.required, Validators.maxLength(26)]],
            bud_after_warranty_check: ['0', [Validators.required, Validators.maxLength(26)]],
            bud_contract_construciton: ['0', [Validators.required, Validators.maxLength(26)]],
            bud_contract_check: ['0', [Validators.required, Validators.maxLength(26)]],
        });
        this.inputNumberSevice.init(this.form.nativeElement, this.secondcreateProjectForm);
    }

    private StepThree(): void {
        this.thirdcreateProjectForm = this._formBuilder.group({
            project_start_date_contract: [],
            project_end_date_contract: [],
            process_contract_duration: [, [Validators.pattern("^[0-9]*$")]],
            delay_date: [],
            warranty_duration: [, [Validators.pattern("^[0-9]*$")]],
            close_project_date: []
        });
        this.thirdcreateProjectForm.get('project_end_date_contract').disable();
    }

    public calculate(): void {
        this.thirdcreateProjectForm.get('project_end_date_contract').enable();
        if (this.thirdcreateProjectForm.get('project_start_date_contract').invalid || this.thirdcreateProjectForm.get('project_start_date_contract').value == null) {
            this.thirdcreateProjectForm.get('project_end_date_contract').setValue(null);
            this.thirdcreateProjectForm.get('project_end_date_contract').disable();
        }
        if (this.thirdcreateProjectForm.get('project_start_date_contract').valid) {
            let date1: any = moment(this.thirdcreateProjectForm.get('project_start_date_contract').value).format('DD/MM/YYYY');
            let date2: any = moment(this.thirdcreateProjectForm.get('project_end_date_contract').value).format('DD/MM/YYYY');
            let dateRegex = /\d+/g;
            let date1Array: any[] = date1.match(dateRegex);
            let date2Array: any[] = date2.match(dateRegex);


            let startDate: any;
            if (date1Array) {
                startDate = new Date(date1Array[2], date1Array[1], date1Array[0]);
            }
            let endDate: any;
            if (date2Array) {
                endDate = new Date(date2Array[2], date2Array[1], date2Array[0]);
            }
            if (date1Array && date2Array) {
                let diffResult = Math.round((endDate - startDate) / (1000 * 60 * 60 * 24));
                let months = Math.floor(diffResult / 30);
                if (months >= 0) {
                    this.thirdcreateProjectForm.get('process_contract_duration').setValue(months);
                } else {
                    ;
                    this.thirdcreateProjectForm.get('process_contract_duration').setValue(null);
                }
            }
        }
    }

    public calculate1(): void {
        if ((this.secondcreateProjectForm.get('bud_request_construction').value) == '') this.secondcreateProjectForm.get('bud_request_construction').setValue('0');
        if ((this.secondcreateProjectForm.get('bud_request_check').value) == '') this.secondcreateProjectForm.get('bud_request_check').setValue('0');
        let num1: any = this.secondcreateProjectForm.get('bud_request_construction').value;
        let num2: any = this.secondcreateProjectForm.get('bud_request_check').value;
        let numberFormat1 = num1.replaceAll(',', '');
        let numberFormat2 = num2.replaceAll(',', '');
        this.bud_request_total = Number(numberFormat1) + Number(numberFormat2);

    }
    public calculate2(): void {
        if ((this.secondcreateProjectForm.get('bud_after_warranty_con').value) == '') this.secondcreateProjectForm.get('bud_after_warranty_con').setValue('0');
        if ((this.secondcreateProjectForm.get('bud_after_warranty_check').value) == '') this.secondcreateProjectForm.get('bud_after_warranty_check').setValue('0');
        let num1: any = this.secondcreateProjectForm.get('bud_after_warranty_con').value;
        let num2: any = this.secondcreateProjectForm.get('bud_after_warranty_check').value;
        let numberFormat1 = num1.replaceAll(',', '');
        let numberFormat2 = num2.replaceAll(',', '');
        this.bud_after_warranty_total = Number(numberFormat1) + Number(numberFormat2);
    }
    public calculate3(): void {
        if ((this.secondcreateProjectForm.get('bud_contract_construciton').value) == '') this.secondcreateProjectForm.get('bud_contract_construciton').setValue('0');
        if ((this.secondcreateProjectForm.get('bud_contract_check').value) == '') this.secondcreateProjectForm.get('bud_contract_check').setValue('0');
        let num1: any = this.secondcreateProjectForm.get('bud_contract_construciton').value;
        let num2: any = this.secondcreateProjectForm.get('bud_contract_check').value;
        let numberFormat1 = num1.replaceAll(',', '');
        let numberFormat2 = num2.replaceAll(',', '');
        this.bud_contract_total = Number(numberFormat1) + Number(numberFormat2);
    }

    private subscribeData(): void {
        this._projectService.getDataSetup().subscribe((res: DataSetup) => {
            this.setup = res;
            this.types = this.setup.types;
            this.budget_plan = this.setup.budgets;
            this.years = this.setup.years;
            this.project_owners = this.setup.project_owners;
            this.entities = this.setup.entities;
            this.surfaces = this.setup.surfaces;
            this.frameworks = this.setup.frameworks;
            this.executives = this.setup.executives;
            this.reviewers = this.setup.reviewers;
            this.roads = this.setup.roads;
            this.roles = this.setup.roles;
            this.filteredRoads = this.firstcreateProjectForm.get('road_id').valueChanges.pipe(startWith(''), map(value => this._filterRoad(value || '')));
            this.filteredYears = this.firstcreateProjectForm.get('budget_year_id').valueChanges.pipe(startWith(''), map(value => this._filterYear(value || '')));
            this.filteredExecutives = this.firstcreateProjectForm.get('executive_director_id').valueChanges.pipe(startWith(''), map(value => this._filterExecutive(value || '')));
            this.filteredEntities = this.firstcreateProjectForm.get('entity_id').valueChanges.pipe(startWith(''), map(value => this._filterEntity(value || '')));
            this.filteredReviewers = this.firstcreateProjectForm.get('reviewer_id').valueChanges.pipe(startWith(''), map(value => this._filterReviewer(value || '')));
            this.filteredFrameworks = this.firstcreateProjectForm.get('framework_id').valueChanges.pipe(startWith(''), map(value => this._filterFramework(value || '')));
            this.filteredProjectOwners = this.firstcreateProjectForm.get('project_owner_id').valueChanges.pipe(startWith(''), map(value => this._filterProjectOwners(value || '')));
        }, (err: any) => {
            console.log(err);
            this._snackBar.openSnackBar('Something went wrong!. Please try agaan later.', 'error');
            this._router.navigate(['/sup/projects']);
        });
    }
    private _filterRoad(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.roads.filter((option: Road) => option.name.toLowerCase().includes(filterValue));
    }
    private _filterYear(value: string): string[] {
        const filterValue = typeof value === 'string' ? value : null;
        return this.years.filter((option: Year) => option.year.includes(filterValue));
    }
    private _filterExecutive(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.executives.filter((option: Executive) => option.name.toLowerCase().includes(filterValue));
    }
    private _filterEntity(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.entities.filter((option: Entity) => option.name.toLowerCase().includes(filterValue));
    }
    private _filterReviewer(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.reviewers.filter((option: Reviewer) => option.name.toLowerCase().includes(filterValue));
    }
    private _filterProjectOwners(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.project_owners.filter((option: ProjectOwner) => option.name.toLowerCase().includes(filterValue));
    }
    private _filterFramework(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.frameworks.filter((option: Framework) => option.name.toLowerCase().includes(filterValue));
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null)
        this._unsubscribeAll.complete()
    }
}