import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-choose-category',
    templateUrl: './choose-category.component.html',
    styleUrls: ['./choose-category.component.scss']
})
export class ChooseCategoryComponent {
    constructor(
        private dialogRef: MatDialogRef<ChooseCategoryComponent>
    ) { }
    close(result: any): void {
        this.dialogRef.close(result);
    }
}
