export enum Organization {
    ProjectOwner = 1,
    Entity = 2,
    Executive = 3,
    Framework = 4,
    Reviewer = 5
}

export interface DataSetup {
    types: Type[],
    categories: Category[],
    budgets: Budget[],
    project_owners: ProjectOwner[],
    entities: Entity[],
    frameworks: Framework[],
    executives: Executive[],
    reviewers: Reviewer[],
    roles: Role[],
    years: Year[],
    status: Status[],
    roads: Road[],
    surfaces: Surface[],
    progress_status: ProgressStatus[],
}

export interface Type {
    id: number,
    name: string
}

export interface Category {
    id: number,
    name: string
}

export interface Budget {
    id: number,
    name: string
}

export interface ProjectOwner {
    id: number,
    type_id: number,
    name: string,
    abbre: string
}

export interface Entity {
    id: number,
    type_id: number,
    name: string,
    abbre: string
}

export interface Framework {
    id: number,
    type_id: number,
    name: string,
    abbre: string
}

export interface Executive {
    id: number,
    type_id: number,
    name: string,
    abbre: string
}

export interface Reviewer {
    id: number,
    type_id: number,
    name: string,
    abbre: string
}

export interface Role {
    id: number,
    abbrivation: string,
    name: string,
    en_name?: string | null
}

export interface Year {
    id: number,
    year: string
}

export interface Status {
    id: number,
    kh_name: string,
    en_name: string,
    color: string
}

export interface Road {
    id: number,
    name: string
}


export interface Surface {
    id: number,
    name: string
}

export interface ProgressStatus {
    id: number,
    kh_name: string,
    en_name: string,
    color: string
}