import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SetupService {
    private _setup: ReplaySubject<any> = new ReplaySubject<any>(1);
    private _code: ReplaySubject<any> = new ReplaySubject<any>(1);

    /**
     * Constructor
     */
    constructor() { }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for data and setup
     *
     * @param value
     */
    set setup(value: any) {
        this._setup.next(value);
    }

    set code(value: any) {
        this._code.next(value);
    }

    /**
     * @returns setup & code
     */
    get setup$(): Observable<any> {
        return this._setup.asObservable();
    }

    get code$(): Observable<any> {
        return this._code.asObservable();
    }
}
