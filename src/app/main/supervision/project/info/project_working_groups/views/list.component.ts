import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { InfoService } from "../../info.service";
import { environment as env } from 'environments/environment';
import { UpdateDialogComponentUser } from "../update-dialog/dialog.component";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ConfirmDialogComponent } from "app/shared/confirm-dialog/confirm-dialog.component";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from "app/shared/services/snackbar.service";
@Component({
    templateUrl: './list.component.html',
    styleUrls: ['list.component.scss'],
    selector: 'list-view'
})
export class ListViewComponent implements OnInit {
    @Input() public users: any;
    @Input() public sysrole: any;
    @Input() public access_permission: any
    @Output() childEvent = new EventEmitter();

    public fileUrl = env.fileUrl;
    public projectId: any;
    public view = 'list';
    public Users: any = [];
    public displayedColumns: string[] = ['no', 'name', 'piu', 'sys_role', 'creator', 'create_at', 'action'];
    public dataSource: any;
    constructor(
        private route: ActivatedRoute,
        private service: InfoService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
    ) {
        this.route.paramMap.subscribe(paramMap => {
            this.projectId = paramMap.get('id');
        })
    }

    ngOnInit(): void {
        this.dataSource = this.users?.data;
    }


    delete(UserId: any, name: any) {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់លុបឈ្មោះ "' + name + '" នេះមែនឫទេ?' }
        });

        dialogRef.afterClosed().subscribe((res: any) => {
            if (res) {
                this.service.deleteUserFromProject(this.projectId, UserId).subscribe((res) => {
                    this._snackBar.openSnackBar('ទិន្នន័យត្រូវបានលុប!', '');
                    this.childEvent.emit()
                })
            }
        })

    }

    update(row: any) {
        const dia = this._dialog.open(UpdateDialogComponentUser, {
            width: '800px',
            data: {
                data: row,
                sysrole: this.sysrole
            }
        })

        dia.componentInstance.DateInfo.subscribe((res: any) => {
            this.childEvent.emit();
        })
    }

    preview(src: any): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: src,
            title: 'រូបភាព'
        };
        dialogConfig.width = "850px";
        this._dialog.open(PreviewImageComponent, dialogConfig);
    }

}