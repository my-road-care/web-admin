import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material/dialog";
import { DialogService } from "./create-dialog.service";
import { environment } from "environments/environment";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { InfoService } from "../../info.service";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { AccountService } from "app/main/supervision/authorities/account/account.service";
import { SnackbarService } from "app/shared/services/snackbar.service";
@Component({
    templateUrl: './create-dialog.component.html',
    styleUrls: ['./create-dialog.component.scss']
})
export class CreateRoleDialogComponent implements OnInit{

    constructor(
        public dialogRef: MatDialogRef<CreateRoleDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: DialogService,
        private _infoservice: InfoService,
        private _dialog: MatDialog,
        private service: AccountService,
        private _snackBar: SnackbarService,
    ){
        dialogRef.disableClose = true;
    }

    public src: string    = "";
    public fileUrl = environment.fileUrl;
    public User:any[]   = [];
    public UserData:any = [];
    public projectId:number;
    public isLoading:boolean = true;
    public roles:any = [];
    public sysId  :number;
    public roleId :number;
    public isDisable: boolean = false;
    public hide: boolean = false;
    public show: boolean  = false;
    public saving: boolean = false;
    public size: number   = 0;
    public type: string   = '';
    public canSaveList:checkSaveLists[]=[];
    public error_size: string = '';
    public error_type: string = '';
    public mode:string = "listing";
    public organizations:any = [];
    public organizationRoles:any = [];
    public searchValue:string = "";

    ngOnInit(): void {
        this.projectId = this.data?.projectId;

        // ==================> old code 
        this.getSys();
        // this.getOrganization();
        // this.getOrganizationRole();
        this.getUser();



        // ==================> new code 
        this.service.setUP().subscribe((res:any)=>{
            this.organizations=res.organizations;
            this.organizationRoles=res.roles;
          })

    }

    getUser():any{
        this.canSaveList=[];
        this.hide = true;
        this._service.getUsers(this.projectId).subscribe((res:any)=>{
            
            this.hide = false;
            this.isLoading = false;
            res.forEach((e:any) => {
                this.canSaveList.push({userId:e.id,check:false});
            });
            this.User = res ? res : [];
            
        })

    }

    // add user to project form
    formGroup = new FormGroup({
        role_id      : new FormControl(''),
        // sys_id       : new FormControl('',[Validators.required]),
        sys_id       : new FormControl(''),
        user_id      : new FormControl(''),
        responsible  : new FormControl('')
    });


    // create user form
    form = new FormGroup({
        name              : new FormControl('',[Validators.required]),
        email             : new FormControl('',[Validators.required,Validators.email,]),
        phone             : new FormControl('',[Validators.required]),
        password          : new FormControl('',[Validators.required]),
        organization_id   : new FormControl('',[Validators.required]),
        organization_role : new FormControl('',[Validators.required]),

        sys_id            : new FormControl('',[Validators.required]),
        role_id           : new FormControl(''),
        responsible       : new FormControl(''),
        image_uri         : new FormControl(''),
    });

    filterWorksType(){
    }

    Data = new EventEmitter();
    
    save(userId:any){
        this.saving = true;
        this.formGroup.get('user_id').setValue(userId);
    
        if(this.formGroup.invalid){
            this.saving = false;
            this._snackBar.openSnackBar('សូមជ្រើសរើសតួនាទីក្នុងប្រព័ន្ធជាមុនសិន !','error');
        }
        else{
            this._infoservice.createUserInProject(this.projectId,this.formGroup.value).subscribe((res:any)=>{
                this.Data.emit(res);
                this.saving = false;
                this.getUser();
                this.getSys();
                this.searchValue = '';
                this.src = "";
                this.formGroup.reset();
                this._snackBar.openSnackBar('បង្កើតអ្នកប្រើប្រាស់បានជោគជ័យ!', '');
            })
        }
    }

    getSys(){
        this.isLoading = true;
        this._service.getSystem().subscribe((res:any)=>{
            this.roles = res;
        })

    }

    changeMode(){
        if(this.mode == 'listing'){
            this.mode = 'create';
        }
        else{
            this.mode = 'listing';
        }
    }


    selectFile(): any {
        document.getElementById('portrait-file').click();
    }
    
    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.form.get('image_uri').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.form.get('image_uri').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.form.get('image_uri').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.form.get('image_uri').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }
    
    preview(){
        if(this.src == ""){
            document.getElementById('portrait-file').click();
        }
        else{
            this._dialog.open(PreviewImageComponent,{
                width: '850px',
                data: {
                  title: 'រូបភាពប្រភេទការងារ',
                  src: this.src
                }
              })
        }
    }

    getOrganization(){
        this.isLoading = true;
        this.service.getOrganization().subscribe((res:any)=>{
            this.organizations = res;
        })
    }
    
    getOrganizationRole(){
        // this.isLoading = true;
        // this.service.getOranizationRole().subscribe((res:any)=>{
        //     this.organizationRoles = res;
        // })
        
    }

    create(){
        this.isDisable = true;
        this.service.createUserProject(this.projectId,this.form.value).subscribe((res:any)=>{
            if(res?.status == 400){
                this._snackBar.openSnackBar(res?.message,'error');
                this.isDisable = false;
                return;
            }
            this.Data.emit(res);
            this._snackBar.openSnackBar('បង្កើតគណនីដោយជោគជ័យ','');
            this.getUser();
            this.form.reset();
            this.isDisable = false;
        })
    }

    findUser(){
        this.isLoading = true;
        this.canSaveList=[];
        if(this.searchValue == ''){
            this.getUser();
        }
        else{
            this.isLoading = false;
            this.User = this.User.filter(
                person =>
                person.phone.includes(this.searchValue) || 
                person.name.toLowerCase().includes(this.searchValue.toLowerCase())
            );
            this.User.forEach((e:any) => {
                this.canSaveList.push({userId:e.id,check:false});
            });
            
        }

    }
    checkSubmit(value){
        this.canSaveList.map((c:checkSaveLists)=>{if(c.userId == value.id) c.check = true})
    }
}
interface checkSaveLists{
    userId:number | string,
    check:boolean
}