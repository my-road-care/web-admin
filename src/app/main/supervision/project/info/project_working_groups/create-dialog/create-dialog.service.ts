import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DialogService {
    getUserInProject(projectId: any, param: any) {
        throw new Error("Method not implemented.");
    }

    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    /**
     * Constructor
     */
    constructor(private http: HttpClient) { }
    
    getUsers(projectId:any):any{
        return this.http.get(this.url+`/sup/projects/get-user?projectId=${projectId}`,this.httpOptions);
    }

    getSystem():any{
        return this.http.get(this.url+`/sup/projects/get-sys`,this.httpOptions);
    }

}