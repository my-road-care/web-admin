import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { InfoService } from '../info.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreateRoleDialogComponent } from './create-dialog/create-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { environment as env } from 'environments/environment';
import { DialogService } from './create-dialog/create-dialog.service';
@Component({
    selector: 'app-project_working_groups',
    templateUrl: './project_working_groups.component.html',
    styleUrls: ['./project_working_groups.component.scss']
})
export class ProjectWorkingGroupsComponent implements OnInit {

    @Input() public access_permission: any;
    public roles: any = [];
    public data: any = [];
    public users: any = [];
    public projectId: any;
    public fileUrl = env.fileUrl;
    public isLoading: boolean = true;
    public role_id: any;
    public view: string = "list";
    public sysRole: any = [];
    public total: number = 10;
    public limit: number = 10;
    public page: number = 1;

    constructor(
        private _service: InfoService,
        private _dialog: MatDialog,
        private route: ActivatedRoute,
        private service: DialogService
    ) {
        this.route.paramMap.subscribe(paramMap => {
            this.projectId = paramMap.get('id');
        })
    }

    ngOnInit(): void {
        this.getUser(this.limit, this.page);
        this.getSys();
    }

    viewMode(mode: string = 'list') {
        this.view = mode;
        this.getUser(this.limit, this.page);
    }

    getSys(): void {
        this.isLoading = true;
        this.service.getSystem().subscribe((res: any) => {
            this.sysRole = res;
            this.isLoading = false;
        })

    }

    getUser(_limit: number = 10, _page: number = 1): void {
        const param: any = {
            pageSize: _limit,
            page: _page,
        };
        if (this.page != 0) {
            param.page = this.page;
        }
        this.isLoading = true;
        this._service.getUserInProject(this.projectId, param).subscribe((res: any) => {
            this.isLoading = false;
            this.users = res;
            this.page = this.users?.current_page;
            this.limit = this.users?.per_page;
            this.total = this.users?.total;
        })
    }

    formGroup = new FormGroup({
        roleId: new FormControl(0),
    });

    filterWorksType(): void {

    }

    createDialog(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            roles: this.roles,
            projectId: this.projectId
        }
        dialogConfig.width = "1200px";
        dialogConfig.autoFocus = false;
        const dialog = this._dialog.open(CreateRoleDialogComponent, dialogConfig);

        dialog.componentInstance.Data.subscribe((res) => {
            this.getUser(this.limit, this.page);
        })

    }

    delete(UserId: any) {
        this._service.deleteUserFromProject(this.projectId, UserId).subscribe((res) => {
            this.getUser(this.limit, this.page);
        })
    }

    //=======================================>> On Page Changed
    onPageChanged(event: any): any {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.isLoading = true;
            this.getUser(this.limit, this.page);
        }
    }
}
