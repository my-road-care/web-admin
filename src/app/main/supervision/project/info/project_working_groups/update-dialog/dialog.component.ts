import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { AccountService } from "app/main/supervision/authorities/account/account.service";
import { environment as env } from "environments/environment";

import { MatDialog } from '@angular/material/dialog';
import { InfoService } from "../../info.service";
import { DialogService } from "../create-dialog/create-dialog.service";
@Component({
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})
export class UpdateDialogComponentUser implements OnInit{
    public sysRole = [];
    public isLoading : boolean = false;
    constructor(
        public dialogRef: MatDialogRef<UpdateDialogComponentUser>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: AccountService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
        private service: DialogService

        
    ){
        this.dialogRef.disableClose=true;
    }
    ngOnInit(): void {
        this.worksType = this.data?.worksType;
        this.worksGroup = this.data?.worksGroup;
        this.src = this.fileUrl+this.data?.data?.user?.avatar;
        this.formGroup.get('project_id').setValue(this.data?.data?.role?.id);
        this.formGroup.get('system_id').setValue(this.data?.data?.sys?.id);
        this.formGroup.get('responsible').setValue(this.data?.data?.responsible);

    }
    
    public fileUrl = env.fileUrl;
    public btnDisable: boolean = true;
    public route: string  = "";
    public src: string    = "";
    public title: string  = this.data.title;
    public worksType:any  = [];
    public worksGroup:any = []
    public show: boolean  = false;
    public saving: boolean = false;
    public size: number   = 0;
    public type: string   = '';
    public error_size: string = '';
    public error_type: string = '';
    public isloading = false;
    public type_id   :number;
    
    formGroup = new FormGroup({
        responsible      : new FormControl(''),
        system_id        : new FormControl(''),
        project_id       : new FormControl(''),

    })
    
    DateInfo = new EventEmitter();
    
    submit(){
        this.formGroup.disable();
        this.saving = true;
        this.isloading = true;
        this._service.updateUser(this.formGroup.value,this.data?.data?.id).subscribe((res:any)=>{
            this.isloading = false;
            this.saving = false;
            this.DateInfo.emit(res)
            this.formGroup.enable();
            this._snackBar.openSnackBar('ទិន្នន័យត្រូវបានកែប្រែ!','');
            this.dialogRef.close();
        })

    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }
    
    preview(){
        if(this.src == ""){
            document.getElementById('portrait-file').click();
        }
        else{
            this._dialog.open(PreviewImageComponent,{
                width: '850px',
                data: {
                  title: 'រូបភាពគណនី',
                  src: this.src
                }
              })
        }
    }

}