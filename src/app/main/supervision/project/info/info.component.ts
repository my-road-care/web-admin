import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { ProjectService } from '../project.service';
import { SetupService } from '../setup.service';
import { InfoService } from './info.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss']
})

export class InfoComponent implements OnInit, OnDestroy {

    private _unsubscribeAll: Subject<any> = new Subject<any>();
    private statistic: string = 'ព័ត៌មានស្ថិតិ';
    private overview: string = 'ព័ត៌មានទូទៅ';
    private procurement: string = 'ឯកសារលទ្ធកម្ម';
    private permission: string = 'អ្នកប្រើប្រាស់';
    public check_overview: boolean = true;
    public check_procurement: boolean = true;
    public check_permission: boolean = true;
    public check_statistic: boolean = true;
    public project_type_id: number = 1;
    public project_id: number = 0;
    public access_permission: any;
    public code_project: string = null;
    public checkQurey: boolean = true;
    constructor(
        private _route: ActivatedRoute,
        private _infoService: InfoService,
        private _projectService: ProjectService,
        private _setupService: SetupService,
        private _snackBarService: SnackbarService
    ) {
        this._route.paramMap.subscribe((params: any) => {
            this.project_id = params.get('id');
        });
    }

    ngOnInit(): void {
        // get project code
        this._setupService.code$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
            this.code_project = res;
            // console.log(this.code_project);
        });
        this.getCode();
        if (this.check_statistic) {
            this.getStatistic();
        }
    }

    checkTab(event: any): void {
        if (this.statistic === event.tab.textLabel) this.getStatistic();
        else if (this.overview === event.tab.textLabel) {
            if (this.check_overview) {
                this.getInfoGeneral();
            }
        } else if (this.procurement === event.tab.textLabel) {
            if (this.check_procurement) {
                this.getInfoProcurement();
            }
        } else if (this.permission === event.tab.textLabel) {
            // console.log(event.tab.textLabel);
        }
    }

    getInfoGeneral(): void {
        this._projectService.getDataSetup().subscribe((res: any) => {
            this._infoService.generalSetup = res;
        });
        this._infoService.getInfoGeneral(this.project_id).subscribe((res: any) => {
            this.check_overview = false;
            this._infoService.general = res;
        }, (err: any) => {
            this.check_overview = false;
            // console.log(err);
            this._snackBarService.openSnackBar(err.error.message, 'error');
        });
    }
    getInfoProcurement(): void {
        this._infoService.getInfoProcurement(this.project_id).subscribe((res: any) => {
            this.check_procurement = false;
            this._infoService.procurement = res.procurement;
        }, (err: any) => {
            this.check_procurement = false;
            // console.log(err);
            this._snackBarService.openSnackBar(err.error.message, 'error');
        });
    }
    getStatistic(): void {
        this.check_statistic = true;
        this._infoService.getStatistic(this.project_id).subscribe((res: any) => {
            this.check_statistic = false;
            this._infoService.statistic = res;
        }, (err: any) => {
            this.check_statistic = true;
            // console.log(err);
        });
    }
    clallBack(e: any): void {
        this._infoService.statistic = e;
    }



    // Get Code Project for Header
    getCode(): void {
        this.checkQurey = true;
        this._projectService.getCode(this.project_id).subscribe((res: any) => {
            if (res) {
                this.code_project = res.code;
                this._setupService.code = res.code;
                this.access_permission = res?.permission;
                this.checkQurey = false;
            }
        }, (err: any) => {
            //console.log(err);
        });
    }

    // Set Code Project for Header 
    setCode(code: string): void {
        this._setupService.code = code;
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
