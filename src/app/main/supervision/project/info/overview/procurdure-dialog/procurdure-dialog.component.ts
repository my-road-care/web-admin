import { Component, ElementRef, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { InfoService } from '../../info.service';
import { InputNumberFormatService } from 'app/shared/input-number';

@Component({
    selector: 'app-procurdure-dialog',
    templateUrl: './procurdure-dialog.component.html',
    styleUrls: ['./procurdure-dialog.component.scss']
})
export class ProcurdureDialogComponent implements OnInit {

    @ViewChild('form', { static: true }) form: ElementRef;
    @ViewChild('procurementInfoNgForm') procurementInfoNgForm: NgForm;
    ProcurementInfo = new EventEmitter();
    public saving: boolean = false;
    public data: any
    public procurementForm: UntypedFormGroup;
    public bud_request_total: number = 0;
    public bud_after_warranty_total: number = 0;
    public bud_contract_total: number = 0;
    constructor(
        @Inject(MAT_DIALOG_DATA) public dataDialog: any,
        private dialogRef: MatDialogRef<ProcurdureDialogComponent>,
        private _formBuilder: UntypedFormBuilder,
        private _infoService: InfoService,
        private snacBar: SnackbarService,
        private inputNumberSevice: InputNumberFormatService
    ) {
        dialogRef.disableClose = true;
    }

    ngOnInit(): void {
        this.data = this.dataDialog.data;
        this.bud_request_total = Number(this.data?.bud_request_construction) + Number(this.data?.bud_request_check);
        this.bud_after_warranty_total = Number(this.data?.bud_after_warranty_con) + Number(this.data?.bud_after_warranty_check);
        this.bud_contract_total = Number(this.data?.bud_contract_construciton) + Number(this.data?.bud_contract_check);
        this.formBuilder();
    }

    public calculate1(): void {
        if((this.procurementForm.get('bud_request_construction').value) == '') this.procurementForm.get('bud_request_construction').setValue('0');
        if((this.procurementForm.get('bud_request_check').value) == '') this.procurementForm.get('bud_request_check').setValue('0');
        let num1: any = this.procurementForm.get('bud_request_construction').value;
        let num2: any = this.procurementForm.get('bud_request_check').value;
        let numberFormat1 = num1.replaceAll(',', '');
        let numberFormat2 = num2.replaceAll(',', '');
        this.bud_request_total = Number(numberFormat1) + Number(numberFormat2);
    }
    public calculate2(): void {
        if((this.procurementForm.get('bud_after_warranty_con').value) == '') this.procurementForm.get('bud_after_warranty_con').setValue('0');
        if((this.procurementForm.get('bud_after_warranty_check').value) == '') this.procurementForm.get('bud_after_warranty_check').setValue('0');
        let num1: any = this.procurementForm.get('bud_after_warranty_con').value;
        let num2: any = this.procurementForm.get('bud_after_warranty_check').value;
        let numberFormat1 = num1.replaceAll(',', '');
        let numberFormat2 = num2.replaceAll(',', '');
        this.bud_after_warranty_total = Number(numberFormat1) + Number(numberFormat2);
    }
    public calculate3(): void {
        if((this.procurementForm.get('bud_contract_construciton').value) == '') this.procurementForm.get('bud_contract_construciton').setValue('0');
        if((this.procurementForm.get('bud_contract_check').value) == '') this.procurementForm.get('bud_contract_check').setValue('0');
        let num1: any = this.procurementForm.get('bud_contract_construciton').value;
        let num2: any = this.procurementForm.get('bud_contract_check').value;
        let numberFormat1 = num1.replaceAll(',', '');
        let numberFormat2 = num2.replaceAll(',', '');
        this.bud_contract_total = Number(numberFormat1) + Number(numberFormat2);
    }

    formBuilder(): void {
        this.procurementForm = this._formBuilder.group({
            bud_request_construction: [this.data?.bud_request_construction, [Validators.required, Validators.maxLength(26)]],
            bud_request_check: [this.data?.bud_request_check, [Validators.required, Validators.maxLength(26)]],
            bud_after_warranty_con: [this.data?.bud_after_warranty_con, [Validators.required, Validators.maxLength(26)]],
            bud_after_warranty_check: [this.data?.bud_after_warranty_check, [Validators.required, Validators.maxLength(26)]],
            bud_contract_construciton: [this.data?.bud_contract_construciton, [Validators.required, Validators.maxLength(26)]],
            bud_contract_check: [this.data?.bud_contract_check, [Validators.required, Validators.maxLength(26)]],
        });
        this.inputNumberSevice.init(this.form.nativeElement, this.procurementForm);
    }

    submit(): void {
        // Return if the form is invalid
        if (this.procurementForm.invalid) return;

        // Disable the form
        this.procurementForm.disable();


        // Saving
        this.saving = true;
        const data = {
            ...this.procurementForm.value,
            budget_plan_id: this.dataDialog.budget_plan_id,
        };

        this._infoService.updateProcedureInfo(this.dataDialog.project_id, data).subscribe((res: any) => {
            this.dialogRef.close();
            //use snack bar to opron message
            // console.log(res);
            this.ProcurementInfo.emit(res);
            this.snacBar.openSnackBar(res.message, '');
        }, () => {

            // Re-enable the form
            this.procurementForm.enable();

            // saved
            this.saving = false;

            // Reset the form
            this.procurementInfoNgForm.resetForm();
            this.dialogRef.close();
            this.snacBar.openSnackBar('Something went wrong!', 'error');
        });
    }
}
