import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, RequiredValidator, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { InfoService } from '../../info.service';
import { Observable, map, startWith } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ProjectService } from '../../../project.service';

@Component({
    selector: 'app-main-dialog',
    templateUrl: './main-dialog.component.html',
    styleUrls: ['./main-dialog.component.scss']
})
export class MainDialogComponent implements OnInit {
    @ViewChild('mainInfoNgForm') mainInfoNgForm: NgForm;
    public MainInfo = new EventEmitter();
    public mainForm: UntypedFormGroup;
    public saving: boolean = false;
    public hideForm: boolean = false;
    public data: any
    public setup: any;

    public province_id: any[] = [];
    public surface_id: any[] = [];
    public provinces: any;
    public categories: any;
    public entity_id: number = 0;
    public reviewer_id: number = 0;
    public project_id: number = 0;
    public filteredOptions: Observable<string[]>;
    
    constructor(
        @Inject(MAT_DIALOG_DATA) public dataDialog: any,
        private _dialogRef: MatDialogRef<MainDialogComponent>,
        private _formBuilder: UntypedFormBuilder,
        private _infoService: InfoService,
        private _projectService: ProjectService,
        private _snackBar: SnackbarService
    ) {
        _dialogRef.disableClose = true;
        this._projectService.getProvince(this.dataDialog?.data?.road?.id).subscribe((res: any) => {
            this.provinces = res?.provinces;
        });
    }

    ngOnInit(): void {
        this.setup = this.dataDialog.setup;
        this.data = this.dataDialog.data;
        if (this.data?.category?.id == 1) {
            this.hideForm = true;
            this.categories = {
                id: 1,
                name: 'ថវិការជាតិ'
            };
        }
        else if (this.data?.category?.id == 2) {
            this.hideForm = false;
            this.categories = {
                id: 2,
                name: 'កម្ចីឥទានក្រៅប្រទេស'
            };
        }
        this.data.surfaces.forEach((v: any) => {
            this.surface_id.push(v.id);
        });
        this.data.provinces.forEach((v: any) => {
            this.province_id.push(v.province_id);
        })
        this.formBuilder();
        this.filteredOptions = this.mainForm.get('road_id').valueChanges.pipe(startWith(''), map(value => this._filter(value || '')));
    }

    public onOptionSelected(event: MatAutocompleteSelectedEvent) {
        this.mainForm.get('road_id').setValue(event.option.value);
        this.setup?.roads.forEach((v: any) => {
            if (v.name == event.option.value) {
                this.provinces = [];
                this._projectService.getProvince(v.id).subscribe((res: any) => {
                    this.provinces = res?.provinces;
                    this.mainForm.get('province_id').setValue(null);
                });
            }
        });
    }

    private _filter(value: string): string[] {
        const filterValue = typeof value === 'string' ? value.toLowerCase() : null;
        return this.setup?.roads.filter((option: any) => option.name.toLowerCase().includes(filterValue));
    }

    public formBuilder(): void {
        this.mainForm = this._formBuilder.group({
            name: [this.data?.name, Validators.required],
            code: [this.data?.code, Validators.required],
            executive_director_id: [this.data?.executive_director?.id, Validators.required],
            type_id: [this.data?.type?.id, Validators.required],
            budget_plan_id: [this.data?.budget_plan?.id, Validators.required],
            budget_year_id: [this.data?.budget_plan?.budget_year_id, Validators.required],
            framework_id: [this.data?.framework?.id, Validators.required],
            project_owner_id: [this.data?.project_owner?.id, Validators.required],
            entity_id: [this?.data?.entity?.id, this.categories?.id == 2 ? Validators.required : []],
            reviewer_id: [this.data?.reviewer?.id, this.categories?.id == 1 ? Validators.required : []],
            road_id: [this.data?.road?.name, Validators.required],
            province_id: [this.province_id],
            surface_id: [this.surface_id, Validators.required]
        });
    }

    public checkRoad(): void {
        let canNotSubmit: boolean = true;
        this.setup?.roads.forEach((v: any) => {
            if (v.name === this.mainForm.get('road_id').value) {
                canNotSubmit = false;
                this.provinces = [];
                this._projectService.getProvince(v.id).subscribe((res: any) => {
                    this.provinces = res?.provinces;
                    this.mainForm.get('province_id').setValue(null);
                })
            }
        });

        if (canNotSubmit) {
            this.provinces = [];
            this.mainForm.setErrors(RequiredValidator);
            this._snackBar.openSnackBar('សូមបញ្ចូលផ្លូវឲ្យបានត្រឹមត្រូវ', 'error');
            return;
        }
    }

    public submit(): void {
        // Return if the form is invalid
        if (this.mainForm.invalid) return;

        let canNotSubmit: boolean = true;
        let road: any = null;

        this.setup?.roads.forEach((v: any) => {
            if (v.name === this.mainForm.get('road_id').value) {
                canNotSubmit = false;
                road = v;
            }
        });

        if (canNotSubmit) {
            this._snackBar.openSnackBar('សូមបញ្ចូលផ្លូវឲ្យបានត្រឹមត្រូវ', 'error');
            road = null;
            return;
        }

        // Disable the form
        this.mainForm.disable();

        // Saving
        this.saving = true;

        let data: object = {
            name: this.mainForm.get('name').value,
            code: this.mainForm.get('code').value,
            executive_director_id: this.mainForm.get('executive_director_id').value,
            type_id: this.mainForm.get('type_id').value,
            budget_plan_id: this.mainForm.get('budget_plan_id').value,
            budget_year_id: this.mainForm.get('budget_year_id').value,
            framework_id: this.mainForm.get('framework_id').value,
            project_owner_id: this.mainForm.get('project_owner_id').value,
            entity_id: this.categories?.id == 2 ? this.mainForm.get('entity_id').value : null,
            reviewer_id: this.categories?.id == 1 ? this.mainForm.get('reviewer_id').value : null,
            category_id: this.categories.id,
            road_id: road?.id,
            province_id: this.mainForm.get('province_id').value,
            surface_id: this.mainForm.get('surface_id').value
        }

        this._infoService.updateMainInfo(this.dataDialog.project_id, data).subscribe((res: any) => {
            this._dialogRef.close();
            this.MainInfo.emit(res);
            this._snackBar.openSnackBar(res.message, '');
        }, (err: any) => {

            this.mainForm.get('code').patchValue('');
            this.mainForm.get('code').setErrors(reportError);
            // Re-enable the form
            this.mainForm.enable();
            // saved
            this.saving = false;
            let errors: any[] = [];
            errors = err.error.errors;
            let messages: any[] = [];
            let text: string = '';
            if (errors.length > 0) {
                errors.forEach((v: any) => {
                    messages.push(v.message)
                });
                if (messages.length > 1) {
                    text = messages.join('-');
                } else {
                    text = messages[0];
                }
            } else {
                text = err.error.message;
            }
            this._snackBar.openSnackBar(text, 'error');
        });
    }
}
