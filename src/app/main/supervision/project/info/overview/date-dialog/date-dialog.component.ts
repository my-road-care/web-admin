import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import * as _moment from 'moment';
const moment = _moment;
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { InfoService } from '../../info.service';
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};
@Component({
    selector: 'app-date-dialog',
    templateUrl: './date-dialog.component.html',
    styleUrls: ['./date-dialog.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class DateDialogComponent implements OnInit {
    @ViewChild('dateInfoNgForm') dateInfoNgForm: NgForm;
    DateInfo = new EventEmitter();
    public saving: boolean = false;
    public data: any
    public dateForm: UntypedFormGroup;
    constructor(
        @Inject(MAT_DIALOG_DATA) public dataDialog: any,
        private dialogRef: MatDialogRef<DateDialogComponent>,
        private _formBuilder: UntypedFormBuilder,
        private _infoService: InfoService,
        private snackBar: SnackbarService
    ) {
        dialogRef.disableClose = true;
    }

    ngOnInit(): void {
        // console.log(this.dataDialog);
        this.data = this.dataDialog.data;
        this.formBuilder();
    }

    formBuilder(): void {
        this.dateForm = this._formBuilder.group({
            project_start_date_contract: [this.data?.project_start_date_contract],
            project_end_date_contract: [this.data?.project_end_date_contract],
            process_contract_duration: [this.data?.process_contract_duration, Validators.pattern("^[0-9]*$")],
            delay_date: [this.data?.delay_date],
            warranty_duration: [this.data?.warranty_duration, Validators.pattern("^[0-9]*$")],
            close_project_date: [this.data?.close_project_date]
        });
        if(this.dateForm.get('project_end_date_contract').value == null) this.dateForm.get('project_end_date_contract').disable();
    }

    public calculate(): void {
        if (this.dateForm.get('project_start_date_contract').valid) this.dateForm.get('project_end_date_contract').enable();
        else this.dateForm.get('project_end_date_contract').disable();

        if (this.dateForm.get('project_start_date_contract').valid) {
            let date1: any = moment(this.dateForm.get('project_start_date_contract').value).format('DD/MM/YYYY');
            let date2: any = moment(this.dateForm.get('project_end_date_contract').value).format('DD/MM/YYYY');
            let dateRegex = /\d+/g;
            let date1Array: any[] = date1.match(dateRegex);
            let date2Array: any[] = date2.match(dateRegex);

            let startDate: any;
            if (date1Array) {
                startDate = new Date(date1Array[2], date1Array[1], date1Array[0]);
            }
            let endDate: any;
            if (date2Array) {
                endDate = new Date(date2Array[2], date2Array[1], date2Array[0]);
            }

            if (date1Array && date2Array) {
              // console.log(date1Array,date2Array)
                let diffResult = Math.round((endDate - startDate) / (1000 * 60 * 60 * 24));
                let months = Math.floor(diffResult / 30);
                // console.log(diffResult / 30)
                if (months >= 0) {
                    this.dateForm.get('process_contract_duration').setValue(months);
                } else {
                    this.dateForm.get('project_end_date_contract').setValue(null);
                    this.dateForm.get('process_contract_duration').setValue(null);
                }
            }


        }
    }
    startDate(){
      // console.log("w1")
      document.getElementById('project_start_date').click();
    }
    endDate(){
      // console.log("w2")
      document.getElementById('project_end_date').click();
    }
    close_project_Date(){
      document.getElementById('closeDate').click();
    }
    delayDate(){
      document.getElementById('delay_date').click();
    }
    submit(): void {
        // Return if the form is invalid
        if (this.dateForm.invalid) {
            return;
        }

        // Disable the form
        this.dateForm.disable();


        // Saving
        this.saving = true;

        if (this.dateForm.get('project_start_date_contract').value != null)
            this.dateForm.get('project_start_date_contract').setValue(moment(this.dateForm.get('project_start_date_contract').value).format('YYYY-MM-DD'));
        else
            this.dateForm.get('project_start_date_contract').setValue(null);

        if (this.dateForm.get('project_end_date_contract').value != null)
            this.dateForm.get('project_end_date_contract').setValue(moment(this.dateForm.get('project_end_date_contract').value).format('YYYY-MM-DD'));
        else
            this.dateForm.get('project_end_date_contract').setValue(null);

        if (this.dateForm.get('delay_date').value != null)
            this.dateForm.get('delay_date').setValue(moment(this.dateForm.get('delay_date').value).format('YYYY-MM-DD'));
        else
            this.dateForm.get('delay_date').setValue(null);

        if (this.dateForm.get('close_project_date').value != null)
            this.dateForm.get('close_project_date').setValue(moment(this.dateForm.get('close_project_date').value).format('YYYY-MM-DD'));
        else
            this.dateForm.get('close_project_date').setValue(null);

        // Sign in
        this._infoService.updateDateInfo(this.dataDialog.project_id, this.dateForm.value).subscribe(
            (res: any) => {
                this.dialogRef.close();
                //use snack bar to opron message
                // console.log(res);
                this.DateInfo.emit(res);
                this.snackBar.openSnackBar(res.message, '');
            },
            (err: any) => {

                // Re-enable the form
                this.dateForm.enable();

                // saved
                this.saving = false;

                // Reset the form
                this.dateInfoNgForm.resetForm();
                this.dialogRef.close();
                let errors: any[] = [];
                errors = err.error.errors;
                let messages: any[] = [];
                let text: string = '';
                if (errors.length > 0) {
                    errors.forEach((v: any) => {
                        messages.push(v.message)
                    });
                    if (messages.length > 1) {
                        text = messages.join('-');
                    } else {
                        text = messages[0];
                    }
                } else {
                    text = err.error.message;
                }
                this.snackBar.openSnackBar(text, 'error');
            }
        );
    }
}
