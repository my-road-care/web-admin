import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { InfoService } from '../info.service';
import { DateDialogComponent } from './date-dialog/date-dialog.component';
import { MainDialogComponent } from './main-dialog/main-dialog.component';
import { ProcurdureDialogComponent } from './procurdure-dialog/procurdure-dialog.component';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnDestroy {

    @Input() public isLoading: boolean = false;
    @Input() public id: number;
    @Input() public access_permission: any;
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    public data: any = null;
    public setup: any;
    public surfaces: any[] = [];
    public provinces: any[] = [];
    public textSurface: string = '';
    public textProvince: string = '';
    public bud_request_total: number = 0;
    public bud_after_warranty_total: number = 0;
    public bud_contract_total: number = 0;

    constructor(
        private _infoService: InfoService,
        private _dialog: MatDialog,
    ) { }

    ngOnInit(): void {
        // Subscribe to get general data
        this._infoService.general$.pipe(takeUntil(this._unsubscribeAll)).subscribe((general: any) => {
            this.data = general.data;
            this.bud_request_total = Number(this.data?.procurement?.bud_request_construction) + Number(this.data?.procurement?.bud_request_check);
            this.bud_after_warranty_total = Number(this.data?.procurement?.bud_after_warranty_con) + Number(this.data?.procurement?.bud_after_warranty_check);
            this.bud_contract_total = Number(this.data?.procurement?.bud_contract_construciton) + Number(this.data?.procurement?.bud_contract_check);
            this.surfaces = this.data?.main?.surfaces;
            this.provinces = this.data?.main?.provinces;
            if (this.surfaces.length > 0) {
                let copy: any[] = [];
                this.surfaces.forEach((v: any) => {
                    copy.push(v.name);
                });
                this.textSurface = copy.join(', ');
            };
            if (this.provinces.length > 0) {
                let copy: any[] = [];
                this.provinces.forEach((v: any) => {
                    copy.push(v?.provinces?.name);
                });
                this.textProvince = copy.join(', ');
            };
        });

        this._infoService.generalSetup$.pipe(takeUntil(this._unsubscribeAll)).subscribe((setup: any) => {
            this.setup = setup;
        });
    }

    updateMain(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            data: this.data.main,
            setup: this.setup,
            project_id: this.id
        }
        dialogConfig.width = "850px";
        dialogConfig.autoFocus = false;
        const dialogRef = this._dialog.open(MainDialogComponent, dialogConfig);
        dialogRef.componentInstance.MainInfo.subscribe((response: any) => {
            let data = response.data;
            this.surfaces = data?.surfaces;
            this.provinces = data?.provinces;
            if (this.surfaces.length > 0) {
                let copy: any[] = [];
                this.surfaces.forEach((v: any) => {
                    copy.push(v.name);
                });
                this.textSurface = copy.join(', ');
            };
            if (this.provinces.length > 0) {
                let copy: any[] = [];
                this.provinces.forEach((v: any) => {
                    copy.push(v?.provinces?.name);
                });
                this.textProvince = copy.join(', ');
            };
            this.data.main = data;
        });
    }

    updateProcurement(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            data: this.data.procurement,
            budget_plan_id: this.data.main.budget_plan.id,
            project_id: this.id
        }
        dialogConfig.width = "850px";
        dialogConfig.autoFocus = false;
        const dialogRef = this._dialog.open(ProcurdureDialogComponent, dialogConfig);
        dialogRef.componentInstance.ProcurementInfo.subscribe((response: any) => {
            this.data.procurement = response.data;
        });
    }

    updateDate(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            data: this.data.date,
            project_id: this.id
        }
        dialogConfig.width = "850px";
        dialogConfig.autoFocus = false;
        const dialogRef = this._dialog.open(DateDialogComponent, dialogConfig);
        dialogRef.componentInstance.DateInfo.subscribe((response: any) => {
            this.data.date = response.data;
        });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
