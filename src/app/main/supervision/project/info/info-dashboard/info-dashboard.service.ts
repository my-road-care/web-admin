import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class InfoDashboardService {

    private url = env.apiUrl;
    private httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(private http: HttpClient) { }
    approve(project_id: number = 0): any {
        return this.http.get(this.url + '/sup/projects/' + project_id + '/approve', this.httpOptions);
    }
}