import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { ApexOptions } from 'ng-apexcharts';
import { Subject, takeUntil } from 'rxjs';
import { InfoService } from '../info.service';
import { AlertType } from 'helpers/components/alert';
import { Animations } from 'helpers/animations';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { InfoDashboardService } from './info-dashboard.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import * as _moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as FileSaver from 'file-saver';
import { ReportWorkingTableComponent } from 'app/shared/report-working-table/report-working-table.component';
import { ProjectReportsComponent } from 'app/shared/project-reports/project-reports.component';
import { SystemReportService } from '../../report/system-report/system-report.service';
import { GlobalConstants } from 'app/shared/global-constants';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};

interface Overview {
    code: string,
    name: string,
    pk_start_end?: string | null,
    surfaces: { id: number, name: string }[],
    status: { name: string, color: string },
    project_start_date?: string | null,
    project_end_date?: string | null
}

@Component({
    selector: 'app-info-dashboard',
    templateUrl: './info-dashboard.component.html',
    styleUrls: ['./info-dashboard.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: Animations,
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class InfoDashboardComponent implements OnInit, OnDestroy {
    public total: number = 0;
    public strokeDasharrayArray: any[] = [95, 5];
    public strokeDasharray: string = this.strokeDasharrayArray.join(', ');
    public attrStrokeDasharray: string = this.strokeDasharrayArray.join(' ');
    public workProgress: ApexOptions = {};
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    @Input() public loading: boolean = false;
    @Input() public id: number;
    @Output() public BackToParent = new EventEmitter<any>();
    @Input() public access_permission: any;
    @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
    public approved: any = undefined;
    public saving: boolean = false;
    public isSaving: boolean = false;
    public show: boolean = false;
    alert: { type: AlertType; message: string } = {
        type: 'warning',
        message: 'មិនទាន់មានគោលការណ៍អនុញ្ញាតិអោយអ្នកបច្ចេកទេសធ្វើវឌ្ឍនភាពការងារ និងធ្វើតេស្តត្រួតពិនិត្យគុណភាព!'
    };
    public overview: Overview = undefined;
    public surface: string;

    public data: any = {
        process: {
            series: [45, 30, 13, 12],
            labels: [
                'បានពិសោធន៍រួច',
                'បញ្ចប់ការងារ',
                'កំពុងអនុវត្ត',
                'មិនទាន់អនុវត្ត',
            ],
            colors: [
                "#48D03E",
                "#FFCD52",
                "#F6E5BA",
                "#D4D4D4"
            ]
        },
        overview: []
    };

    constructor(
        private _infoService: InfoService,
        private _infoDashboardService: InfoDashboardService,
        private _service: SystemReportService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService
    ) { }

    ngOnInit(): void {
        console.log(this.access_permission)
        this._infoService.statistic$.pipe(takeUntil(this._unsubscribeAll)).subscribe((statistic: any) => {
            this.data = statistic?.data;
            this.overview = this.data?.overview as Overview;
            if (this.overview) {
                let surfaces: any[] = [];
                this.overview.surfaces.forEach(v => {
                    surfaces.push(v.name);
                })
                this.surface = surfaces.join(', ');
            }

            if (statistic?.technical_approved != null) {
                this.show = true;
                this.alert = {
                    type: 'success',
                    message: 'ការងារបច្ចេកទេសត្រូវបានផ្តល់គោលការណ៏ដាក់ឲ្យប្រើប្រាស់ដោយៈ'
                };
                this.approved = statistic?.technical_approved;
            }
            this._prepareChartData();
            this.total = this.data?.test_quality?.series[0];
            this.strokeDasharray = this.data?.test_quality?.series.join(', ');
            this.attrStrokeDasharray = this.data?.test_quality?.series.join(' ');
        }, (err: any) => {
            console.log(err);
        });
    }

    submit(): void {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើការអនុម័តនេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((result) => {
            // console.log(result);
            if (result) {
                this.loading = true
                this._infoDashboardService.approve(this.id).subscribe((res: any) => {
                    this.loading = false;
                    this.show = true;
                    let obj = {
                        data: this.data,
                        technical_approved: res?.data
                    }
                    this.BackToParent.emit(obj);
                    this._snackBar.openSnackBar(res.message, '');
                }, (err: any) => {
                    this.loading = false;
                    this._snackBar.openSnackBar(err.error ? err.error.message : GlobalConstants.genericError, GlobalConstants.error);
                });
            }
        });
    }

    download(report: number) {
        let obj = {
            report: report
        }
        this.isSaving = true;
        if (report == 1) {
            const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            const day = ("0" + currentDate.getDate()).slice(-2);
            const hours = ("0" + currentDate.getHours()).slice(-2);
            const minutes = ("0" + currentDate.getMinutes()).slice(-2);
            const seconds = ("0" + currentDate.getSeconds()).slice(-2);
            const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
            let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            this._service.getjsReport(this.id, obj).subscribe((res: any) => {
                this.isSaving = false;
                let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
                FileSaver.saveAs(blob, this.overview.code + '-របាយការណព័ត៌មានគម្រោង និងស្ថិតិ-' + formattedDate + '.pdf');
                dialog.close();
            }, (err: any) => {
                this.isSaving = false;
                console.log(err);
                dialog.close();
            })
        }
        if (report === 2) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.data = {
                project_id: this.id
            };
            dialogConfig.width = "750px"
            dialogConfig.autoFocus = false
            dialogConfig.disableClose = true
            this._dialog.open(ReportWorkingTableComponent, dialogConfig);
        }
        if (report === 3) {
            // let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            // this._service.getjsReport(this.id, obj).subscribe((res: any) => {
            //     this.isSaving = false;
            //     let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
            //     FileSaver.saveAs(blob, 'របាយការណ៏ស្តង់ដាតេស្ត' + '.pdf');
            //     dialog.close();
            // }, (err: any) => {
            //     this.isSaving = false;
            //     console.log(err);
            //     dialog.close();
            // });
        }
        if (report === 4) {
            // let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            // this._service.getjsReport(this.id, obj).subscribe((res: any) => {
            //     this.isSaving = false;
            //     let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
            //     FileSaver.saveAs(blob, 'របាយការណ៏ប្លង់' + '.pdf');
            //     dialog.close();
            // }, (err: any) => {
            //     this.isSaving = false;
            //     console.log(err);
            //     dialog.close();
            // });
        }
        if (report === 5) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.data = {
                report: 8,
                title: 'របាយការណ៏វឌ្ឍនភាពការងារ',
                project_id: this.id,
                code: this.overview.code
            };
            dialogConfig.width = "550px"
            dialogConfig.autoFocus = false
            this._dialog.open(ProjectReportsComponent, dialogConfig);
        }
        if (report === 6) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.data = {
                report: 9,
                title: 'របាយការណ៏ធ្វើតេស្ត',
                project_id: this.id,
                code: this.overview.code
            };
            dialogConfig.width = "750px"
            dialogConfig.autoFocus = false
            this._dialog.open(ProjectReportsComponent, dialogConfig);
        }
    }
    // =================================>> Convert base64 to blob 
    b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Prepare the chart data from the data
     *
     * @private
     */
    private _prepareChartData(): void {

        // Progress of Project
        this.workProgress = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                height: '100%',
                type: 'donut',
                sparkline: {
                    enabled: true
                }
            },
            colors: this.data?.process?.colors,
            labels: this.data?.process?.labels,
            plotOptions: {
                pie: {
                    customScale: 1,
                    expandOnClick: false,
                    donut: {
                        size: '82%',
                    }
                }
            },
            series: this.data?.process?.series,
            states: {
                hover: {
                    filter: {
                        type: 'none'
                    }
                },
                active: {
                    filter: {
                        type: 'none'
                    }
                }
            },
            tooltip: {
                enabled: true,
                fillSeriesColor: false,
                theme: 'dark',
                custom: ({
                    seriesIndex,
                    w
                }): string =>
                    `<div class="flex items-center h-8 min-h-8 max-h-8 px-3">
                        <div class="w-3 h-3 rounded-full" style="background-color: ${w.config.colors[seriesIndex]};"></div>
                        <div class="ml-2 text-md leading-none">${w.config.labels[seriesIndex]}:</div>
                        <div class="ml-2 text-md font-bold leading-none">${w.config.series[seriesIndex]}%</div>
                    </div>`
            }
        };
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._infoService.statistic = null;
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
