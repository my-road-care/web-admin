import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Subject, takeUntil } from 'rxjs';
import { InfoService } from '../info.service';
import { CreateDialogComponent } from './create-dialog/create-dialog.component';
import { UpdateDialogComponent } from './update-dialog/update-dialog.component';
import { environment as env } from 'environments/environment';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';

@Component({
    selector: 'app-procurement',
    templateUrl: './procurement.component.html',
    styleUrls: ['./procurement.component.scss']
})
export class ProcurementComponent implements OnInit, OnDestroy {

    @Output() output = new EventEmitter<any>();
    @Input() public isLoading: boolean = false;
    @Input() public id: number;
    @Input() public access_permission:any;

    public fileUrl: any = env.fileUrl;
    public toggle: boolean = true;

    public displayedColumns: string[] = ['no','code', 'title','image','description','date','action'];
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    public dataSource: any;
    public data: any[] = [];
    public isClosing: boolean = false;
    public project_id: number = 0;
    public src: string = 'assets/images/avatars/pdf.png';
    /**
     * Constructor
     */
    constructor(
        private _infoService: InfoService,
        private _dialog: MatDialog,
        private _router: Router,
        private _snackBar: SnackbarService,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
   * On init
   */
    ngOnInit(): void {
        this._infoService.procurement$.pipe(takeUntil(this._unsubscribeAll)).subscribe((procurement: any) => {
            this.data = procurement.data;
            // console.log(this.data);
            this.dataSource = new MatTableDataSource(this.data);
        });
        console.log(this.access_permission);
    }

    create() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = this.id;
        dialogConfig.width = "650px";
        dialogConfig.autoFocus = false;
        const dialogRef = this._dialog.open(CreateDialogComponent, dialogConfig);
        dialogRef.componentInstance.CreatePro.subscribe((response: any) => {
              // =====>> when create up
              let copy: any[] = [];
              copy.push(response.data);
              this.data.forEach((row: any) => {
                  copy.push(row);
              })
              this.data = copy;
              // console.log(this.data)
              // ======>> when create down
              // this.data.push(response.data);
              this.dataSource = new MatTableDataSource(this.data);
        });
    }

    updatePro(row: any) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            data: row,
            project_id: this.id
        }
        dialogConfig.width = "650px";
        const dialogRef = this._dialog.open(UpdateDialogComponent, dialogConfig);
        this._router.events.subscribe(() => {
            dialogRef.close();
        })
        dialogRef.componentInstance.UpdatePro.subscribe((response: any) => {
            let copy: any[] = [];
            this.data.forEach((obj: any) => {
                if (obj.id == response.data.id) {
                    obj = response.data;
                }
                copy.push(obj);
            })
            this.data = copy
            // console.log(this.data);
            this.dataSource = new MatTableDataSource(this.data);
        });
    }

    deletePro(row: any) {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this._infoService.viewDeletepro(this.id, row.id).subscribe((res: any) => {
                    this.isClosing = false;
                    this._snackBar.openSnackBar(res.message, '');
                    let copy: any[] = [];
                    this.data.forEach((obj: any) => {
                        if (obj.id !== row.id) {
                            copy.push(obj);
                        }
                    });
                    this.data = copy;
                    this.dataSource = new MatTableDataSource(this.data);
                }, (err: any) => {
                    this.isClosing = false;
                    this._snackBar.openSnackBar('Something went wrong.', 'error');
                });
            }
        });
    }
    openPDF(data){
        window.open(this.fileUrl+'v3/get-file/'+data.file);
        console.log(data);
    }
    preview(src: any): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: src,
            title: 'រូបភាពនៃឯកសារលទ្ធកម្ម'
        };
        dialogConfig.width = "850px";
        this._dialog.open(PreviewImageComponent, dialogConfig);
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    changeView(): void{
        this.toggle = !this.toggle;
    }
}
