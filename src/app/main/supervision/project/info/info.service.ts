import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { environment as env } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class InfoService {
    private _general: ReplaySubject<any>        = new ReplaySubject<any>(1);
    private _generalSetup: ReplaySubject<any>   = new ReplaySubject<any>(1);
    private _procurement: ReplaySubject<any>    = new ReplaySubject<any>(1);
    private _statistic: BehaviorSubject<any> = new BehaviorSubject(null);

    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    /**
     * Constructor
     */
    constructor(private http: HttpClient) { }



    /**
     |---------------------------------------------------------------------------------------
     | project_working_groups
     |---------------------------------------------------------------------------------------
     */
    getRoles():any{
        return this.http.get(this.url+`/sup/projects/get-role`,this.httpOptions);
    }

    searchUser(projectId: any){
        return this.http.get(this.url+`/sup/projects/get-user?projectId=${projectId}`);
    }

    getUserInProject(projectId:any, params = {}){
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url+`/sup/projects/${projectId}/info/working-structure`, httpOptions);
    }

    createUserInProject(projectId:any,data:any){

        return this.http.post(this.url+`/sup/projects/${projectId}/info/working-structures`,data,this.httpOptions);
    }

    deleteUserFromProject(ProjectId,UserId:any){
        return this.http.post(this.url+`/sup/projects/delete?userId=${UserId}&projectId=${ProjectId}`,this.httpOptions);
    }

    updateRoleUser(data:any){
        return this.http.post(this.url+`/sup/projects/update`,data,this.httpOptions);
    }






    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    getInfo(id: any = ''): any {
        return this.http.get(this.url + '/sup/projects/'+ id +'/info', this.httpOptions);
    }

    /**
     |---------------------------------------------------------------------------------------
     | Info General
     |---------------------------------------------------------------------------------------
     */
    //===========================================================================================>>  View Project Info General
    getInfoGeneral(id: any = ''): any {
        return this.http.get(this.url + '/sup/projects/'+ id +'/info/general', this.httpOptions);
    }
    //===========================================================================================>>  Update Project Main Info
    updateMainInfo(id: any = '',data: any = {}): any {
        const httpOptions = {};
        return this.http.post(this.url + '/sup/projects/'+ id +'/info/main',data, httpOptions);
    }
    //===========================================================================================>>  Update Project Procedure Info
    updateProcedureInfo(id: any = '',data: any = {}): any {
        const httpOptions = {};
        return this.http.post(this.url + '/sup/projects/'+ id +'/info/procedure',data, httpOptions);
    }
    //===========================================================================================>>  Update Project Date Info
    updateDateInfo(id: any = '',data: any = {}): any {
        const httpOptions = {};
        return this.http.post(this.url + '/sup/projects/'+ id +'/info/date',data, httpOptions);
    }
    //===========================================================================================>> End ViewInfo for General


    /**
     |---------------------------------------------------------------------------------------
     | Info Procurement
     |---------------------------------------------------------------------------------------
     */
    //===========================================================================================>>  View Project Info General
    getInfoProcurement(id: any = ''): any {
        return this.http.get(this.url + '/sup/projects/'+ id +'/info/procurement', this.httpOptions);
    }
    //===========================================================================================>> update procurement
    viewUpdatePro( data: any = {}, project_id:any='', id:any='' ): any {
      const httpOptions:Object={
        headers: new HttpHeaders({
            'Accept': 'application/json',
            'withCredentials': 'true',
        })

        }
        let formdata=new FormData();
        formdata.append('file', data.file);
        formdata.append('description',data.description);
        formdata.append('docu_code', data.docu_code);
        formdata.append('title', data.title);
        return this.http.post(this.url + '/sup/projects/'+ project_id +'/info/procurement/'+ id , formdata , httpOptions);
    }
    //===========================================================================================>> create procurement
    viewCreatepro(data: any = {}, id: any = ''): any {
      const httpOptions:Object={
        headers: new HttpHeaders({
            'Accept': 'application/json',
            'withCredentials': 'true',
        })

        }
        let formdata=new FormData();
        formdata.append('file', data.file);
        formdata.append('description',data.description);
        formdata.append('docu_code', data.docu_code);
        formdata.append('title', data.title);
        return this.http.post(this.url + '/sup/projects/'+ id +'/info/procurements', formdata, httpOptions);
    }
    //===========================================================================================>> Delete procurement
    viewDeletepro( project_id:number = 0 , id: number = 0 ): any {
        return this.http.delete(this.url+'/sup/projects/'+project_id+'/info/procurement/' +id ,this.httpOptions);
    }


     //===========================================================================================>> get Statistic
    getStatistic(projectId: any): any {
        return this.http.get(this.url + `/sup/projects/${projectId}/info/statistic`, this.httpOptions);
    }


    /**
     * Setter & getter for data and setup
     *
     * @param value
     */
    set general(value: any) {
        this._general.next(value);
    }
    set generalSetup(value: any) {
        this._generalSetup.next(value);
    }

    set procurement(value:any){
        this._procurement.next(value);
    }

    set statistic(value: any) {
        this._statistic.next(value);
    }
    /**
     * @returns data
     */
    get general$(): Observable<any> {
        return this._general.asObservable();
    }
    get generalSetup$(): Observable<any> {
        return this._generalSetup.asObservable();
    }
    get procurement$(): Observable<any> {
        return this._procurement.asObservable();
    }
    get statistic$(): Observable<any> {
        return this._statistic.asObservable();
    }

}
