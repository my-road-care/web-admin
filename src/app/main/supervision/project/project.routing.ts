import { Route } from "@angular/router";
import { CreateComponent } from "./create/create.component";
import { InfoComponent } from "./info/info.component";
import { ListingComponent } from "./listing/listing.component";
import { ReportComponent } from "./report/report.component";
import { TechnicalComponent } from "./technical/technical.component";

export const projectRoutes: Route[] = [{
    path: '',
    children: [
        { path  : '',  component: ListingComponent },
        { path  : 'create/:category_id', component: CreateComponent },
        { path  : ':id/info', component: InfoComponent },
        { path  : ':id/technical', component: TechnicalComponent },
        { path  : ':id/report', component: ReportComponent }
    ]
}];