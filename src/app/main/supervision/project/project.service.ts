import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class ProjectService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) { }
    getUser(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/users', httpOptions);
    }

    getRoles(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/users/getRole', httpOptions);
    }

    /**
     |-------------------------------------------------------------------------------------
     | ProjectListingController
     |-------------------------------------------------------------------------------------
     | Date: 3/15/2023 11:55:55 PM, By Yim Klok 
     */
    //==================================================================================>> Get Data Setup
    getDataSetup(): any {
        return this.http.get(this.url + '/sup/projects/data-setup', this.httpOptions);
    }

    //==================================================================================>> Get PIU Setup
    getSetup(): any {
        return this.http.get(this.url + '/sup/projects/setup', this.httpOptions);
    }

    //==================================================================================>> Get PIU Setup
    getProvince(road_id: number = 0): any {
        return this.http.get(this.url + '/sup/projects/setup/provinces/' + road_id, this.httpOptions);
    }

    //==================================================================================>> Listing Project
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/projects', httpOptions);
    }

    //==================================================================================>>  Create Project
    create(data: any = {}): any {
        return this.http.post(this.url + '/sup/projects/create', data, this.httpOptions);
    }

    //==================================================================================>>  Create Project
    checkCode(code: string = ''): any {
        return this.http.get(this.url + '/sup/projects/check-code?code=' + code, this.httpOptions);
    }

    //==================================================================================>>  Get Code Project
    getCode(project_id: number = 0): any {
        // return this.http.get(this.url + '/sup/projects/get-code/'+project_id, this.httpOptions);
        return this.http.get(this.url + '/sup/projects/' + project_id + '/permission', this.httpOptions);

    }

    //==================================================================================>>  Delete Project
    deleteProject(project_id: number = 0): any {
        return this.http.delete(this.url + '/sup/projects/' + project_id + '/delete', this.httpOptions);
    }

    /**
     |-------------------------------------------------------------------------------------
     | End of route ProjectListingController
     |-------------------------------------------------------------------------------------
     */
}
