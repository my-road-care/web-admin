// ===================================================================>> Core Library
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// ===================================================================>> Third Library

import { SharedModule } from '../../../shared/shared.module';
// ===================================================================>> Custom Library
import { ListingComponent } from '../project/listing/listing.component';
import { CreateComponent } from './create/create.component';
import { projectRoutes } from './project.routing';
import { CurrencyPipe } from '@angular/common';
import { ReportComponent } from './report/report.component';
import { OverviewComponent } from './info/overview/overview.component';
import { InfoComponent } from './info/info.component';
import { ProcurementComponent } from './info/procurement/procurement.component';
import { ReferencesReportComponent } from './report/references-report/references-report.component';
import { SystemReportComponent } from './report/system-report/system-report.component';
import { MainDialogComponent } from './info/overview/main-dialog/main-dialog.component';
import { DateDialogComponent } from './info/overview/date-dialog/date-dialog.component';
import { ProcurdureDialogComponent } from './info/overview/procurdure-dialog/procurdure-dialog.component';
import { TestingStandardComponent } from './technical/testing-standard/testing-standard.component';
import { WorkingTableComponent } from './technical/working-table/working-table.component';
import { PlanComponent } from './technical/plan/plan.component';
import { InfoDashboardComponent } from './info/info-dashboard/info-dashboard.component';
import { PkDialogComponent } from './technical/working-table/pk-dialog/pk-dialog.component';
import { UpdateDialogComponent } from './info/procurement/update-dialog/update-dialog.component';
import { CreateDialogComponent } from './info/procurement/create-dialog/create-dialog.component';
import { CreateTestingDialogComponent } from './technical/testing-standard/create-dialog/create-dialog.component';
import { UpdateTestingDialogComponent } from './technical/testing-standard/update-dialog/update-dialog.component';
import { CreateDialogPlanComponent } from './technical/plan/create-dialog/create-dialog.component';
import { UpdateDialogPlanComponent } from './technical/plan/update-dialog/update-dialog.component';
import { ScrollbarModule } from 'helpers/directives/scrollbar';
import { PictureComponent } from './report/picture/picture.component';
import { LetterComponent } from './report/letter/letter.component';
import { ProjectWorkingGroupsComponent } from './info/project_working_groups/project_working_groups.component';
import { TestingReportComponent } from './report/testing-report/testing-report.component';
import { TechnicalComponent } from './technical/technical.component';
import { ArtisticWorkDialogComponent } from './technical/working-table/artistic-work-dialog/artistic-work-dialog.component';
import { WorkSideDialogComponent } from './technical/working-table/work-side-dialog/work-side-dialog.component';
import { ProgressComponent } from './technical/working-table/work-side-dialog/progress/progress.component';
import { QualityComponent } from './technical/working-table/work-side-dialog/quality/quality.component';
import { MapComponent } from './technical/map/map.component';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { AgmDirectionModule } from 'agm-direction';
import { CreateRoleDialogComponent } from './info/project_working_groups/create-dialog/create-dialog.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AlertModule } from 'helpers/components/alert';
import { ListViewComponent } from './info/project_working_groups/views/list.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { UpdateDialogComponentUser } from './info/project_working_groups/update-dialog/dialog.component';
import { CreateReportDialogComponent } from './report/references-report/create-dialog/create-dialog.component';
import { UpdateRefernceDialogComponent } from './report/references-report/update-dialog/update-dialog.component';
import { TesingReportDialogComponent } from './report/testing-report/tesing-report-dialog/tesing-report-dialog.component';
import { CreateLetterDialogComponent } from './report/letter/create-letter-dialog/create-letter-dialog.component';
import { UpdateLetterDialogComponent } from './report/letter/update-letter-dialog/update-letter-dialog.component';
import { ChooseCategoryComponent } from './create/choose-category/choose-category.component';
import { FilterComponent } from './listing/filter/filter.component';
import { OverviewDialogComponent } from './report/picture/overview-dialog/overview-dialog.component';
import { DropdownComponent } from './shared/dropdown/dropdown.component';
import { OrganizationComponent } from './shared/organization/organization.component';

@NgModule({
  declarations: [
    ListingComponent,
    CreateComponent,
    InfoComponent,
    OverviewComponent,
    PlanComponent,
    ProcurementComponent,
    ReportComponent,
    ReferencesReportComponent,
    SystemReportComponent,
    MainDialogComponent,
    DateDialogComponent,
    ProcurdureDialogComponent,
    TechnicalComponent,
    TestingStandardComponent,
    WorkingTableComponent,
    InfoDashboardComponent,
    PkDialogComponent,
    UpdateDialogComponent,
    CreateDialogComponent,
    ProjectWorkingGroupsComponent,
    TestingReportComponent,
    LetterComponent,
    PictureComponent,
    CreateTestingDialogComponent,
    UpdateTestingDialogComponent,
    CreateDialogComponent,
    UpdateDialogComponent,
    CreateDialogPlanComponent,
    UpdateDialogPlanComponent,
    ArtisticWorkDialogComponent,
    WorkSideDialogComponent,
    ProgressComponent,
    QualityComponent,
    MapComponent,
    CreateRoleDialogComponent,
    ListViewComponent,
    UpdateDialogComponentUser,
    CreateReportDialogComponent,
    UpdateRefernceDialogComponent,
    TesingReportDialogComponent,
    CreateLetterDialogComponent,
    UpdateLetterDialogComponent,
    ChooseCategoryComponent,
    FilterComponent,
    OverviewDialogComponent,
    DropdownComponent,
    OrganizationComponent
  ],
  exports: [
    DropdownComponent
  ],
  imports: [
    SharedModule,
    ScrollbarModule,
    NgxDropzoneModule,
    NgApexchartsModule,
    RouterModule.forChild(projectRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAeOPNbz8UGspqu4TqT2vAA7XFYBClQpnU'
  }),
  AgmDirectionModule,
  AgmJsMarkerClustererModule,
  AlertModule
  ],
  providers: [
    CurrencyPipe
  ]
})
export class ProjectModule { }
