import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { ProjectService } from '../project.service';
import { SetupService } from '../setup.service';
import { TechnicalService } from './technical.service';

@Component({
    selector: 'app-technical',
    templateUrl: './technical.component.html',
    styleUrls: ['./technical.component.scss'],
})
export class TechnicalComponent implements OnInit, OnDestroy {

    private _unsubscribeAll: Subject<any> = new Subject<any>();
    //----------------------------------------------------|
    private testingStandard: string = 'កំណត់ស្តង់ដា';
    private plan: string = 'ប្លង់';
    private testingReport: string = 'របាយការណ៍ធ្វើតេស្ត';
    private letter: string = 'លិខិតណែនាំ';
    private picture: string = 'រូបភាព';
    public check_workingTable: boolean = true;
    public check_testingStandard: boolean = true;
    public check_plan: boolean = true;
    public mapLoaded: boolean = false;
    public checkQurey:boolean = true;

    public access_permission:any;

    //----------------------------------------------------|
    public code_project: string = undefined;
    public project_type_id: number = 2;
    public project_id: number = 0;

    constructor(
        private _route: ActivatedRoute,
        private _technicalService: TechnicalService,
        private _projectService: ProjectService,
        private _setupService: SetupService,
    ) {
        this._route.paramMap.subscribe((params: any) => {
            this.project_id = params.get('id');
        });
        this._technicalService.technical = {
            workingTable: 'can_loading',
            testingStandard: 'cannot_loading',
            plan: 'connot_loading',
            map: 'connot_loading'
        }
    }

    ngOnInit(): void {
        // get project code
        this._setupService.code$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => this.code_project = res);
        this.getCode();
    }

    checkTab(event: any): void {
        if (event.tab.textLabel === 'តារាងការងារ') {
            this._technicalService.technical = {
                workingTable: 'can_loading',
                testingStandard: 'cannot_loading',
                plan: 'cannot_loading',
                map: 'cannot_loading'
            }
        } else if (event.tab.textLabel === 'កំណត់ស្តង់ដា') {
            this._technicalService.technical = {
                workingTable: 'cannot_loading',
                testingStandard: 'can_loading',
                plan: 'cannot_loading',
                map: 'cannot_loading'
            }
        } else if (event.tab.textLabel === 'ប្លង់') {
            this._technicalService.technical = {
                workingTable: 'cannot_loading',
                testingStandard: 'cannot_loading',
                plan: 'can_loading',
                map: 'cannot_loading'
            }
        } else if (event.tab.textLabel === 'ផែនទី' && !this.mapLoaded) {
            this._technicalService.technical = {
                workingTable: 'cannot_loading',
                testingStandard: 'cannot_loading',
                plan: 'cannot_loading',
                map: 'can_loading'
            }
            this.mapLoaded = true;
        }
    }

    // Get Code Project for Header
    getCode(): void {
        this.checkQurey = true;
        this._projectService.getCode(this.project_id).subscribe((res: any) => {
            if (res) {
                this.code_project = res.code;
                this._setupService.code = res.code;
                this.access_permission = res?.permission;
                this.checkQurey = false;
            }
        }, (err: any) => {
            console.log(err);
        });
    }

    // Set Code Project for Header
    setCode(code: string): void {
        this._setupService.code = code;
    }

    // Unsubscribe from all subscriptions
    ngOnDestroy(): void {
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
