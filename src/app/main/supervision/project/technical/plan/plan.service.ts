import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class PlanService {

    private _project_plan: ReplaySubject<any> = new ReplaySubject<any>(1);
    private url: string = env.apiUrl;
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) { }

    /**
     |**************** Plan ****************|
     |--------------------------------------------------|
     */

    listing(params = {}, project_id: number = 0): any {
        const httpOptions: object = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/plans', httpOptions);
    }

    create(data: any, project_id: number = 0): any {
      // title: [this.data.title, Validators.required],
      //       docu_code: [this.data.docu_code, []],
      //       image_uri: [this.data.image_uri, Validators.required],
      //       description: [this.data.description]
      const httpOptions:Object={
        headers: new HttpHeaders({
            'Accept': 'application/json',
            'withCredentials': 'true',
        })

        }
        let formdata=new FormData();
        formdata.append('file', data.image_uri);
        formdata.append('docu_code',data.docu_code);
        formdata.append('title', data.title);
        formdata.append('description', data.description);
        return this.http.post(this.url + '/sup/projects/' + project_id + '/technical/plans', formdata, httpOptions);
    }

    update(data: any , project_id: number = 0, id: number = 0): any {
      const httpOptions:Object={
        headers: new HttpHeaders({
            'Accept': 'application/json',
            'withCredentials': 'true',
        })

        }
        let formdata=new FormData();
        formdata.append('file', data.image_uri);
        formdata.append('docu_code',data.docu_code);
        formdata.append('title', data.title);
        formdata.append('description', data.description);
        return this.http.post(this.url + '/sup/projects/' + project_id + '/technical/plans/' + id, formdata, httpOptions);
    }

    delete(project_id: number = 0, id: number = 0): any {
        return this.http.delete(this.url + '/sup/projects/' + project_id + '/technical/plans/' + id, this.httpOptions);
    }

    set project_plan(value: any) {
        this._project_plan.next(value);
    }

    get project_plan$(): Observable<any> {
        return this._project_plan.asObservable();
    }
}
