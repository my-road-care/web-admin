import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { CreateDialogPlanComponent } from './create-dialog/create-dialog.component';
import { UpdateDialogPlanComponent } from './update-dialog/update-dialog.component';
import { environment as env } from 'environments/environment';
import { PlanService } from './plan.service';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { Subject, takeUntil } from 'rxjs';
import { TechnicalService } from '../technical.service';

@Component({
    selector: 'app-plan',
    templateUrl: './plan.component.html',
    styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {

    private _unsubscribeAll: Subject<any> = new Subject<any>();
    @Input() public id: number;
    @Input() public access_permission:any;

    public fileUrl: any = env.fileUrl;
    public listview = true;
    public gridView = false;
    public displayedColumns: string[] = ['no', 'title', 'image_uri', 'docu_code', 'creator', 'date', 'description', 'action'];
    public dataSource: any;
    public data: any[] = [];
    public total: number = 10;
    public limit: number = 10;
    public page: number = 1;
    public isLoading: boolean = true;
    public isClosing: any;

    constructor(
        private _planService: PlanService,
        private _dialog: MatDialog,
        private _technicalService: TechnicalService,
        private _snackBar: SnackbarService,
    ) { }

    ngOnInit(): void {
        console.log("plan"+this.access_permission.create);
        this._technicalService.technical$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
            if (res.plan == 'can_loading' && this.isLoading) this.listing(this.limit, this.page);
        });
    }

    //===================================>> List
    listing(_limit: number = 10, _page: number = 1): any {
        const param: any = {
            pageSize: _limit,
            page: _page,
        };
        if (this.page != 0) {
            param.page = this.page;
        }
        this.isLoading = true;
        this._planService.listing(param, this.id).subscribe((res: any) => {
            this.isLoading = false;
            this.data = res?.data?.plans;
            this.total = res?.data?.pagination?.total;
            this.page = res?.data?.pagination?.current_page;
            this.limit = res?.data?.pagination?.per_page;
            this.dataSource = new MatTableDataSource(this.data);
        }, (err: any) => {
            this.isLoading = false;
            // // console.log(err);
        });
    }

    //===================================>> Create
    create() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = this.id;
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(CreateDialogPlanComponent, dialogConfig);
        dialogRef.componentInstance.CreatePro.subscribe((response: any) => {
            let copy: any[] = [];
            copy.push(response.data);
            this.data.forEach((row: any) => {
                copy.push(row);
            })
            this.data = copy;
            this.dataSource = new MatTableDataSource(this.data);
        });
    }

    //===================================>> Update
    update(row: any) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            data: row,
            project_id: this.id
        }
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(UpdateDialogPlanComponent, dialogConfig);
        dialogRef.componentInstance.UpdatePlan.subscribe((response: any) => {
            let copy: any[] = [];
            this.data.forEach((obj: any) => {
                if (obj.id == response.data.id) {
                    obj = response.data;
                }
                copy.push(obj);
            })
            this.data = copy
            // // console.log(this.data);
            this.dataSource = new MatTableDataSource(this.data);
        });
    }

    //===================================>> Delete
    delete(id: number = 0) {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((result) => {
            // // console.log(result);
            if (result) {
                this._planService.delete(this.id, id).subscribe((res: any) => {
                    this.isClosing = false;
                    this._snackBar.openSnackBar(res.message, '');
                    let copy: any[] = [];
                    this.data.forEach((obj: any) => {
                        if (obj.id !== id) {
                            copy.push(obj);
                        }
                    });
                    this.data = copy;
                    this.dataSource = new MatTableDataSource(this.data);
                }, (err: any) => {
                    this.isClosing = false;
                    this._snackBar.openSnackBar('Something went wrong.', 'error');
                });
            }
        });
    }

    preview(src: any): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: src,
            title: 'រូបភាពនៃគម្រោង'
        };
        dialogConfig.width = "850px";
        this._dialog.open(PreviewImageComponent, dialogConfig);
    }
    openPDF(data){

        window.open(this.fileUrl+'v3/get-file/'+data.image_uri);
     
    }
    //=======================================>> On Page Changed
    onPageChanged(event: any): any {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }
    }
    changeView(event: any) {
        if (event == 'grid') {
            this.gridView = false;
            this.listview = true;
        } else {
            this.gridView = true;
            this.listview = false;
        }
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
