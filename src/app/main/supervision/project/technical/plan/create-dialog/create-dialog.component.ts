import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Subject } from 'rxjs';
import { PlanService } from '../plan.service';

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.scss']
})
export class CreateDialogPlanComponent implements OnInit{
    @ViewChild('CreatePlanNgForm') CreatePlanNgForm: NgForm;
    CreatePro = new EventEmitter();
    public isLoading : boolean = false;
    public data: any;
    public planForm: UntypedFormGroup;
    public src: string = null;
    public show: boolean = false;
    public saving: boolean = false;
    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';


    constructor(
        @Inject(MAT_DIALOG_DATA) public id: any,
        private dialogRef: MatDialogRef<CreateDialogPlanComponent>,
        private _planService: PlanService,
        private _formBuilder: UntypedFormBuilder,
        private _snackBar: SnackbarService,
        private _dialog: MatDialog,
    )
    {
        dialogRef.disableClose = true;
    }

    ngOnInit(): void {
        // console.log(this.id);
        this.formBuilder();
    }

    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
              this.src = '../../../../../../../assets/images/logo/PDF_file_icon.svg.png';
              this.show=false;

                this.planForm.get('image_uri').setValue(e?.target?.files[0]);
            }
        //     if (this.size > 3145728) { //3mb
        //         this.saving = true;
        //         this.planForm.get('image_uri').setValue('');
        //         this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
        //     }
        //     if (this.type.substring(0, 5) !== 'image') {
        //         this.saving = true;
        //         this.src = '';
        //         this.show = true;
        //         this.error_size = '';
        //         this.planForm.get('image_uri').setValue(this.src);
        //         this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        //     }
        // } else {
        //     this.saving = true;
        //     this.src = '';
        //     this.show = true;
        //     this.error_size = '';
        //     this.planForm.get('image_uri').setValue(this.src);
        //     this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        //     //it work when user cancel select file
        }
    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }

    preview(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            src: this.src,
            title: 'រូបភាពនៃប្លង់'
        };
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(PreviewImageComponent, dialogConfig);
    }


    formBuilder(): void {
        this.planForm = this._formBuilder.group({
            title: ['', Validators.required],
            docu_code: ['',[] ],
            image_uri: ['',Validators.required],
            description: ['' ],
        });
    }

    submit(){
        // console.log(this.planForm.value);
        if(this.planForm.value){
            this.planForm.disable();
            this.saving = true;
            this._planService.create(this.planForm.value, this.id).subscribe((res:any) =>{
                this.isLoading = false;
                this.dialogRef.close();
                this.CreatePro.emit(res);
                this._snackBar.openSnackBar(res.message, '');
            },err =>{
                this.planForm.enable();
                this.saving = false;
                this.dialogRef.close();
                for(let key in err.error.errors){
                let control = this.planForm.get(key);
                control.setErrors({'servererror':true});4
                control.errors.servererror = err.error.errors[key][0];
                }
            });
        }
        else{
            this._snackBar.openSnackBar('Please check your input.', 'error');
        }
    }
}
