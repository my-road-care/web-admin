import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { environment as env } from 'environments/environment';
import { PlanService } from '../plan.service';

@Component({
    selector: 'app-update-dialog',
    templateUrl: './update-dialog.component.html',
    styleUrls: ['./update-dialog.component.scss']
})
export class UpdateDialogPlanComponent implements OnInit {

    @ViewChild('UpdatePlanForm') UpdatePlanForm: NgForm;
    UpdatePlan = new EventEmitter();
    public fileUrl: any = env.fileUrl;
    public saving: boolean = false;
    public data: any;
    public UpdateForm: UntypedFormGroup;
    public isLoading: boolean = false;
    public src: string = null;
    public show: boolean = false;
    public project_id: number = 0;
    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';

    constructor(
        @Inject(MAT_DIALOG_DATA) public dataDialog: any,
        private dialogRef: MatDialogRef<UpdateDialogPlanComponent>,
        private _formBuilder: UntypedFormBuilder,
        private _planService: PlanService,
        private _snacBar: SnackbarService,
        private _dialog: MatDialog
    ) {
        dialogRef.disableClose = true;
    }

    ngOnInit(): void {
        this.data = this.dataDialog.data;
        this.project_id = this.dataDialog.project_id;
        this.formBuilder();
        this.getBase64FromUrl('https://lh3.googleusercontent.com/i7cTyGnCwLIJhT1t2YpLW-zHt8ZKalgQiqfrYnZQl975-ygD_0mOXaYZMzekfKW_ydHRutDbNzeqpWoLkFR4Yx2Z2bgNj2XskKJrfw8').then(console.log)
        // // console.log(this.data);
        // if (this.data?.image_uri) {
        //     this.src = this.fileUrl + this.data.image_uri;
        // } else {
        //     this.src = null;
        // }
        this.src = '../../../../../../../assets/images/logo/PDF_file_icon.svg.png';
    }
    getBase64FromUrl = async (url) => {
        const data = await fetch(url,{ mode: 'no-cors'});
        const blob = await data.blob();
        return new Promise((resolve) => {
          const reader = new FileReader();
          reader.readAsDataURL(blob);
          reader.onloadend = () => {
            const base64data = reader.result;
            resolve(base64data);
          }
        });
      }

    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
          this.error_size = '';
          this.error_type = '';
          this.show = false;
          this.saving = false;
          this.type = e?.target?.files[0]?.type;
          this.size = e?.target?.files[0]?.size;
          var reader = new FileReader();
          reader.readAsDataURL(e?.target?.files[0]);
          reader.onload = (event: any) => {
            this.UpdateForm.get('image_uri').setValue(e?.target?.files[0]);
          }
        //   if (this.size > 3145728) { //3mb
        //     this.saving = true;
        //     this.UpdateForm.get('image_uri').setValue('');
        //     this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
        //   }
        //   if (this.type.substring(0, 5) !== 'image') {
        //     this.saving = true;
        //     this.src = '';
        //     this.show = true;
        //     this.error_size = '';
        //     this.UpdateForm.get('image_uri').setValue(this.src);
        //     this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        //   }
        // } else {
        //   this.saving = true;
        //   this.src = '';
        //   this.show = true;
        //   this.error_size = '';
        //   this.UpdateForm.get('image_uri').setValue(this.src);
        //   this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
        //   //it work when user cancel select file
        }
      }

    selectFile(): any {
        document.getElementById('portrait-file').click();
      }

      preview(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
          src: this.src,
          title: 'រូបភាពនៃប្លង់'
        };
        dialogConfig.width = "850px";
        this._dialog.open(PreviewImageComponent, dialogConfig);
      }

    formBuilder(): void {

        this.UpdateForm = this._formBuilder.group({
            title: [this.data.title ? this.data.title :"", Validators.required],
            docu_code: [this.data.docu_code? this.data.docu_code:"", []],
            image_uri: ['',[]],
            description: [this.data.description ? this.data.description != "null" ? this.data.description:"" :" " ]
        });
    }

    submit(): void {
        if (this.UpdateForm.invalid) {
            return;
        }
        this.UpdateForm.disable();
        this.saving = true;
        this._planService.update(this.UpdateForm.value, this.project_id, this.data.id).subscribe(
            (res: any) => {
                this.dialogRef.close();
                // console.log(res);
                this.UpdatePlan.emit(res);
                this._snacBar.openSnackBar(res.message, '');
            },
            () => {
                this.UpdateForm.enable();
                this.saving = false;
                this.UpdatePlanForm.resetForm();
                this.dialogRef.close();
                this._snacBar.openSnackBar('Something went wrong!', 'error');
            }
        );
    }
}
