import { MouseEvent } from '@agm/core';
import { GoogleMap, LatLngBounds, Polyline, PolylineOptions } from '@agm/core/services/google-maps-types';
import { Component, Input, OnInit } from '@angular/core';
import styles from './map-styles/grey.json'
import svg from './marker.json'
import { RoadAllService } from 'app/main/cp/nationalRoad/all/all.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingDialogComponent } from 'app/shared/loading-dialog/loading-dialog.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() public project_id: number = 0;
  road_id: number = 0

  // Custom polylines
  public polylines: Array<Polyline> = [];

  map: GoogleMap
  directions: Direction[] = []
  routeMarkers: marker[] = []
  markers: marker[] = []
  styles = styles

  constructor(
    private _service: RoadAllService,
    private matDialog: MatDialog,
  ) { }

  ngOnInit(): void {
    try {
      const project_road_ids = JSON.parse(localStorage.getItem('project_road_ids_mapping'))
      this.road_id = project_road_ids.find(({ id, road_id }) => this.project_id == id)?.road_id
    } catch (error) {
      window.location.href = '/#/sup/projects'
    }
  }

  clickedMarker(label: string, index: number) {
    // console.log(`clicked the marker: ${label || index}`)
    // console.log(this.routeMarkers[index])
  }

  filterParentheses(s: string) {
    return s.includes('(') ? s.substring(0, s.indexOf('(')) : s
  }

  calculator(markers, numStyles) {
    return {
      text: '',
      index: 2
      // title: count
    };
  }

  getPKs(id: number | string, mainRoad: boolean = false) {

    const dialog = this.matDialog.open(LoadingDialogComponent, {
      data: {
        message: 'សូមមេត្តារង់ចាំ...'
      }
    })

    this._service.viewPk(id, { limit: 173, with_potholes: 1 }).subscribe(res => {
      this.setPoliline(res);
      dialog.close();
    })
  }

  onResponse(event) {
    // console.log('res', event)
    // Default style
    const polylineOptions: PolylineOptions = {
      strokeWeight: 6,
      strokeOpacity: .5,
      icons: [
        // {
        //   icon: {
        //     path: 0,
        //     fillColor: '#000'
        //   },
        //   offset: '0%',
        //   repeat: '10%'
        // }
      ]
    };

    // Polylines strokeColor
    const colors = ['#FF0000', '#00FF00', '#0000FF'];

    // Clear exist polylines
    this.polylines.forEach(polyline => polyline.setMap(null));
    const { legs } = event.routes[0];

    this.directions = []
    const markers = this.routeMarkers
    const map = this.map
    class PLine extends google.maps.Polyline {
      GetPointAtDistance(metres) {
        // some awkward special cases
        if (metres == 0) return this.getPath().getAt(0);
        if (metres < 0) return null;
        if (this.getPath().getLength() < 2) return null;
        var dist = 0;
        var olddist = 0;
        var i = 0
        for (i = 1; (i < this.getPath().getLength() && dist < metres); i++) {
          olddist = dist;
          dist += google.maps.geometry.spherical.computeDistanceBetween(
            this.getPath().getAt(i),
            this.getPath().getAt(i - 1)
          );
        }

        if (dist < metres) {
          return null;
        }
        var p1 = this.getPath().getAt(i - 2);
        var p2 = this.getPath().getAt(i - 1);
        var m = (metres - olddist) / (dist - olddist);

        return new google.maps.LatLng(p1.lat() + (p2.lat() - p1.lat()) * m, p1.lng() + (p2.lng() - p1.lng()) * m);
      }

      renderPoints(m) {
        let od = 0
        let p, op
        let d = 0
        while (p = this.GetPointAtDistance(od += m)) {
          const { lat, lng } = p.toJSON()
          const distance = d += m
          const a = Math.floor(distance / 1000)
          const b = (distance % 1000).toFixed()

          const icon: google.maps.Symbol = {
            path: 0,
            scale: 3,
            fillColor: 'black',
            fillOpacity: 1,
            labelOrigin: new google.maps.Point(10, 1)
          }

          markers.push({
            lat: lat,
            lng: lng,
            label: { text: `${a}+${Number(b) || '000'}` },
            // label: 'ABC',
            icon,
            info: distance,
            draggable: false
          })
          op = p
        }
        const start = this.getPath().getAt(0).toJSON()
        const end = this.getPath().getAt(this.getPath().getLength() - 1).toJSON()

        const ms = new google.maps.Marker
        const me = new google.maps.Marker

        ms.setPosition(start)
        me.setPosition(end)

        ms.setLabel('A')
        me.setLabel('B')

        ms.setMap(map as any)
        me.setMap(map as any)

        // markers.push({
        //   lat,
        //   lng,
        //   icon: {
        //     path: 0,
        //     scale: 4
        //   },
        //   info: 'x',
        //   draggable: false
        // })
      }
    }

    let points = []

    legs.forEach((leg) => {
      leg.steps.forEach((step, index) => {
        points = [...points, ...step.path]
        const stepPolyline: PLine = new PLine(polylineOptions as any);

        // Custom color
        stepPolyline.setOptions({ strokeColor: colors[index % 3] });
        stepPolyline.setPath(step.path)
        stepPolyline.setMap(this.map as any);
        // stepPolyline.renderPoints(100)
        this.polylines.push(stepPolyline as any);
      });
    });

    // render markers 1/100m
    const pline: PLine = new PLine(polylineOptions as any)
    pline.setPath(points)
    pline.renderPoints(100)

    // console.log(this.routeMarkers.map(({ lat, lng, info }) => ({ lat, lng, info })))
  }

  mapReady(map: GoogleMap) {
    this.map = map
    map.setOptions({
      // center: { lat: 12.71642962743061, lng: 104.92398335602526 },
      // zoom: 7.8
      restriction: {
        latLngBounds: {
          south: 10.069775255302284,
          west: 101.801119586494,
          north: 14.746719947756558,
          east: 108.0468471255565
        },
        strictBounds: false,
      }
    })

    this.getPKs(this.road_id)

    // this.directions.push({
    //   origin: 'វត្តភ្នំ',
    //   destination: 'បាវិត',
    //   renderOptions: {
    //     draggable: false
    //   }
    // })

    // new google.maps.MapLabel({
    //   text: 'Test',
    //   position: new google.maps.LatLng(34.515233, -100.918565),
    //   map: map,
    //   fontSize: 35,
    //   align: 'right'
    // })

    window['map'] = map
  }

  setPoliline(res: any) {
    this.polylines.forEach(l => l.setMap(null))
    this.routeMarkers = [];
    const pkPaths = res.data
    const origin = pkPaths[0].points[0].latlng
    const destination = pkPaths[pkPaths.length - 1].points[0].latlng
    const colorMainRoad = res.mainRoad == 1 ? 'green' : 'orange'

    const bounds = new google.maps.LatLngBounds() as LatLngBounds
    bounds.extend(origin)
    bounds.extend(destination)
    this.map.fitBounds(bounds)
    this.map.fitBounds(bounds)

    const stepPolyline = pkPaths.map(({ code, points }) => {
      const line = new google.maps.Polyline({ strokeColor: colorMainRoad, strokeOpacity: .4, strokeWeight: res.mainRoad == 1 ? 6 : 4 })

      points.forEach(({ latlng, meter, n_of_potholes, potholes }) => {
        const info = potholes.length ? { ...potholes[0], code, meter, n_of_potholes } : null
        const color = info ? 'red' : 'black'
        const icon: google.maps.Symbol = {
          path: 0,
          scale: 3,
          fillColor: color,
          strokeColor: color,
          labelOrigin: new google.maps.Point(11, 1)
        }

        this.routeMarkers.push({
          ...latlng, label: `${code}+${meter}`, icon, info
        })
      })
      line.setPath(points.map(({ latlng }) => latlng))
      line.setMap(this.map as any)
      return line
    })
    this.polylines = stepPolyline
  }

  restrictMap() {
    this.map.setOptions({
      // center: { lat: 12.71642962743061, lng: 104.92398335602526 },
      // zoom: 7.8
      restriction: {
        latLngBounds: {
          south: 10.069775255302284,
          west: 101.801119586494,
          north: 14.746719947756558,
          east: 108.0468471255565
        },
        strictBounds: true,
      },
    })
  }

  mapClicked($event: MouseEvent) {
    // console.log('clicked', $event.coords)
    // if (this.markers.length) this.markers.pop()
    // else
    // this.markers.push({
    //   lat: $event.coords.lat,
    //   lng: $event.coords.lng,
    //   icon: { url: 'data:image/svg+xml;charset=UTF-8,' + svg.mpwt, scaledSize: new google.maps.Size(.1, .1) },
    //   draggable: true
    // });
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    // console.log('dragEnd', m, $event);
  }

}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: any;
  info?: any;
  icon?: any;
  draggable: boolean;
}

interface Direction {
  origin: string | { lat: number, lng: number }
  destination: string | { lat: number, lng: number }
  renderOptions: {
    draggable: Boolean
  }
}