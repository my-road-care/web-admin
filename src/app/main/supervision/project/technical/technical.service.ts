import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
@Injectable({
    providedIn: 'root',
})
export class TechnicalService {
    private _getSetUp:ReplaySubject<any>=new ReplaySubject<any>(1);
    private url: string = env.apiUrl;
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) { }

    setuptestingStandard(project_id: any): any {
        return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/testing-standards/data-setup-testing-standards', this.httpOptions);
    }
    set setDataSetUP(valuse){
        this._getSetUp.next(valuse);
    }
    get getDataSetUP():Observable<any>{
        return this._getSetUp.asObservable();
    }

    //Test this one
    private _technical: BehaviorSubject<any> = new BehaviorSubject<any>(1);
    set technical(value: object) {
        this._technical.next(value);
    }
    get technical$(): Observable<any> {
        return this._technical.asObservable();
    }
}
