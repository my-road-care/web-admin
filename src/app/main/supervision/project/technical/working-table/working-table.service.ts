import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { environment as env } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class WorkingTableService {

    private _listConstructionWork: ReplaySubject<any> = new ReplaySubject<any>(1);

    private url: string = env.apiUrl;
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) { }

    getDiagram(project_id: number = 0): any {
        return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/diagram', this.httpOptions);
    }
    listConstructionWork(project_id: number = 0): any {
        return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/diagram/contrction-works', this.httpOptions);
    }
    addAndUpdatePKWork(project_id: number = 0, data: any = {}): any {
        return this.http.post(this.url + '/sup/projects/' + project_id + '/technical/diagram', data, this.httpOptions);
    }
    previewSide(project_id: number = 0, side_id: number = 0): any {
        return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/diagram/side/' + side_id, this.httpOptions);
    }
    updateProgress(project_id: number, side_id: number = 0, data: object = {}): any {
        return this.http.post(this.url + '/sup/projects/' + project_id + '/technical/diagram/side/' + side_id + '/update-progress', data, this.httpOptions);
    }
    testQuality(project_id: number, side_id: number = 0, data: object = {}): any {
        return this.http.post(this.url + '/sup/projects/' + project_id + '/technical/diagram/side/' + side_id + '/add-quality-test', data, this.httpOptions);
    }

    set listConstruction(value: any) {
        this._listConstructionWork.next(value);
    }
    get listConstruction$(): Observable<any> {
        return this._listConstructionWork.asObservable();
    }

}
