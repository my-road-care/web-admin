import { Component, EventEmitter, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Subject, takeUntil } from 'rxjs';
import { WorkingTableService } from '../working-table.service';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { MatCheckboxChange } from '@angular/material/checkbox';

interface Data {
    project_id: number,
    pk_start?: null | number,
    pk_end?: null | number,
    station_start?: null | number,
    station_end?: null | number,
    stationScale?: null | number,
    data_update?: null | number[]
}
@Component({
    selector: 'app-pk-dialog',
    templateUrl: './pk-dialog.component.html',
    styleUrls: ['./pk-dialog.component.scss']
})
export class PkDialogComponent implements OnInit, OnDestroy {
    public PkDialog = new EventEmitter()
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    public data_create: any = [];
    public datas: any = [];
    public scales: any[] = [25, 50, 100, 200, 250];
    public stations: number[] = [];
    public works_id: any[] = [];
    public canSubmit: boolean = true;
    public saving: boolean = false;
    public timeOut: any;
    public timeout1: any;
    public pkForm: UntypedFormGroup;
    public errorPkstart: any = '';
    public errorPkend: any = '';
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: Data,
        private dialogRef: MatDialogRef<PkDialogComponent>,
        private _workingTableService: WorkingTableService,
        private _formBuilder: UntypedFormBuilder,
        private _snackbarService: SnackbarService
    ) {
        if (this.data.data_update) {
            this.data.data_update.forEach((v: any) => this.works_id.push(v));
        }
        if (data.stationScale) {
            this.stations = [];
            const interval = 1000 - data.stationScale;
            let i = 0;
            while (i <= interval) {
                this.stations.push(i);
                i += data.stationScale;
            }
        }
    }

    ngOnInit(): void {
        this.pkFormBuilder();
        this._workingTableService.listConstruction$.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: any) => {
            this.data_create = Object.keys(data);
            Object.values(data).forEach((v: any) => {
                if (this.data.data_update.length == 0) {
                    v.forEach((word: any) => word.check = false);
                    this.datas.push(v);
                } else {
                    v.forEach((d: any) => {
                        this.works_id.forEach((id: any) => {
                            if (id == d.id) d.check = true;
                        })
                    })
                    this.datas.push(v);
                }
            })
        });
        document.getElementById('select').click();
    }

    pkFormBuilder(): void {
        this.pkForm = this._formBuilder.group({
            pk_start: [this.data.pk_start, Validators.required],
            station_start: [this.data.station_start, Validators.required],
            pk_end: [this.data.pk_end, Validators.required],
            station_end: [this.data.station_end, Validators.required],
            works_id: [this.works_id, Validators.required],
            stationScale: [this.data.stationScale, Validators.required],
        });
    }

    keyup(): void {
        clearTimeout(this.timeOut);
        if (this.works_id.length > 0 && (parseInt(this.pkForm.get('pk_start').value) < parseInt(this.pkForm.get('pk_end').value) && this.pkForm.get('stationScale').value != null)) {
            this.canSubmit = false;
            console.log(this.pkForm.get('pk_start').value)
        } else {
            this.canSubmit = true;
            if ((this.pkForm.get('stationScale').value == null || typeof (this.pkForm.get('stationScale').value) == "undefined") && this.errorPkend == '' && this.errorPkstart == '') {
                this.timeOut = setTimeout(() => {
                    this.pkForm.get('stationScale').markAsTouched();
                }, 2000);
            }
        }
    }
    pkStat() {
        this.errorPkstart = '';
        clearTimeout(this.timeout1);
        if (this.pkForm.get('pk_end').value == '') {
            this.keyup();
        } else {
            if (parseInt(this.pkForm.get('pk_start').value) >= parseInt(this.pkForm.get('pk_end').value)) {
                this.timeout1 = setTimeout(() => {
                    this.errorPkstart = "សូមបញ្ចូល PK_Start តូចជាង PK_End";
                    this.keyup();
                }, 1600)
            }
        }
        this.keyup();
    }
    pkend() {
        this.errorPkend = '';
        clearTimeout(this.timeout1);
        if (this.pkForm.get('pk_start').value == '') {
            this.keyup();
        } else {
            if (parseInt(this.pkForm.get('pk_start').value) >= parseInt(this.pkForm.get('pk_end').value)) {
                this.timeout1 = setTimeout(() => {
                    this.errorPkend = "សូមបញ្ចូល PK_End ធំជាង PK_Start ";
                    this.keyup();
                }, 1600)
            }
        }
        this.keyup()
    }
    checkbox(event: MatCheckboxChange, id: number): void {
        event.checked === true ? this.works_id.push(id) : this.works_id = this.works_id.filter((v: number) => v != id);
        this.pkForm.get('works_id').setValue(this.works_id);
        this.keyup();
    }
    changeScale(e: MatSelectChange): void {
        this.stations = [];
        const interval = 1000 - e.value;
        let i = 0;
        while (i <= interval) {
            this.stations.push(i);
            i += e.value;
        }
        this.keyup();
    }
    submit(): void {
        this.saving = true;
        this._workingTableService.addAndUpdatePKWork(this.data.project_id, this.pkForm.value).subscribe((res: any) => {
            this.saving = false;
            this.dialogRef.close();
            this.PkDialog.emit();
            this._snackbarService.openSnackBar(res.message, '');
        }, (err: any) => {
            console.log(err);
            this.saving = false;
            this.dialogRef.close();
            this._snackbarService.openSnackBar('Something went wrong', 'error');
        });
    }
    ngOnDestroy(): void {
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
