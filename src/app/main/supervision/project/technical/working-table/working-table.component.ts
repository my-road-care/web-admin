import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ArtisticWorkDialogComponent } from './artistic-work-dialog/artistic-work-dialog.component';
import { PkDialogComponent } from './pk-dialog/pk-dialog.component';
import { WorkSideDialogComponent } from './work-side-dialog/work-side-dialog.component';
import { WorkingTableService } from './working-table.service';
import { AlertType } from 'helpers/components/alert';
import { Animations } from 'helpers/animations';
import { Subject, takeUntil } from 'rxjs';
import { TechnicalService } from '../technical.service';
import { ReportWorkingTableComponent } from 'app/shared/report-working-table/report-working-table.component';

@Component({
    selector: 'app-working-table',
    templateUrl: './working-table.component.html',
    styleUrls: ['./working-table.component.scss'],
    animations: Animations
})
export class WorkingTableComponent implements OnInit {
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    @ViewChild('table') table: ElementRef;
    @Input() public project_id: number = 0;
    @Input() public access_permission: any;
    public isLoading: boolean = false;
    public user: any = undefined;
    public works: any[] = [];
    public pk_stations: any[] = [];
    public data_update: any[] = [];
    public pk_start: number = null;
    public station_start: number = null;
    public pk_end: number = null;
    public station_end: number = null;
    public stationScale: number = null;
    public left: any[] = [];
    public rigth: any[] = [];
    public showAlert: boolean = true;
    alert: { type: AlertType; message: string } = {
        type: 'warning',
        message: 'មិនទាន់មានគោលការណ៍អនុញ្ញាតិអោយអ្នកបច្ចេកទេសធ្វើវឌ្ឍនភាពការងារ និងធ្វើតេស្តត្រួតពិនិត្យគុណភាព!'
    };
    constructor(
        private _router: Router,
        private _dialog: MatDialog,
        private _workingTableService: WorkingTableService,
        private _technicalService: TechnicalService,
        private _snackBarService: SnackbarService
    ) { }

    ngOnInit(): void {
        this._technicalService.technical$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
            if (res.workingTable == 'can_loading') {
                this.listing();
                if (this.access_permission?.add_update_work) this.getConstructionWork();
            };
        });
    }

    download(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            project_id: this.project_id
        };
        dialogConfig.width = "750px"
        dialogConfig.autoFocus = false
        dialogConfig.disableClose = true
        this._dialog.open(ReportWorkingTableComponent, dialogConfig);
    }

    openWorkSideDialog(e: any, work_index: number = 0, side: string = "L", side_index: number, side_id: number): void {
        if (this.user?.approved_by == null) {
            this._snackBarService.openSnackBar('មិនទាន់មានគោលការណ៍អនុញ្ញាតិអោយអ្នកបច្ចេកទេសធ្វើវឌ្ឍនភាពការងារ និងធ្វើតេស្តត្រួតពិនិត្យគុណភាព!', 'error');
            return;
        }
        if (this.access_permission.udpate_progress != 1) {
            this._snackBarService.openSnackBar('អ្នកមិនមានសិទ្ធទេ !', 'error');
            return;
        }
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            project_id: this.project_id,
            side_id: side_id
        };
        dialogConfig.width = "750px";
        dialogConfig.autoFocus = false;
        const dialogRef = this._dialog.open(WorkSideDialogComponent, dialogConfig);
        dialogRef.componentInstance.progressId.subscribe((response: any) => {
            if (side == 'R') {
                this.works[work_index].r[side_index].progress = response;
            } else {
                this.works[work_index].l[side_index].progress = response;
            }
        });
    }

    getConstructionWork(): void {
        this._workingTableService.listConstructionWork(this.project_id).subscribe((res: any) => {
            this._workingTableService.listConstruction = res;
        });
    }

    listing(): void {
        this.isLoading = true;
        this._workingTableService.getDiagram(this.project_id).subscribe((res: any) => {
            this.isLoading = false;
            this.pk_stations = res.pk_stations;
            if (this.pk_stations.length > 0) {
                let start = this.pk_stations[0].trx;
                let afterStart = this.pk_stations[1].trx;
                var numbersafterStart = afterStart.split('+');
                let scale = Number(numbersafterStart[1]);
                let end = this.pk_stations[this.pk_stations.length - 1].trx;
                var numbers = start.split('+');
                this.pk_start = Number(numbers[0]);
                this.station_start   = Number(numbers[1]);
                var numbers = end.split('+');
                this.pk_end = Number(numbers[0]);
                this.station_end   = Number(numbers[1]);
                this.stationScale = scale - this.station_start;
            }
            this.user = res?.user ? res?.user : undefined;
            if (this.user?.approved_by != null) this.showAlert = false;

            this.works = res.works;
            this.data_update = [];
            if (this.works.length > 0) {
                this.works.forEach((v: any) => {
                    this.data_update.push(v.id);
                })
            }
        }, (err: any) => {
            this._snackBarService.openSnackBar('Something went worng', 'error');
            console.log(err);
            this._router.navigateByUrl('/sup/projects');
        });
    }

    artisticWork(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            project_id: this.project_id,
        };
        dialogConfig.width = "850px";
        dialogConfig.autoFocus = false;
        const dialogRef = this._dialog.open(ArtisticWorkDialogComponent, dialogConfig);
        dialogRef.componentInstance.ArtisticWork.subscribe((response: any) => {
            window.alert("I don't understand artistic");
        });
    }

    createPK(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            project_id: Number(this.project_id),
            data_update: this.data_update,
            pk_start: this.pk_start,
            station_start: this.station_start,
            station_end: this.station_end,
            pk_end: this.pk_end,
            stationScale: this.stationScale
        };
        dialogConfig.autoFocus = false;
        dialogConfig.disableClose = true;
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(PkDialogComponent, dialogConfig);
        dialogRef.componentInstance.PkDialog.subscribe(() => {
            this.listing();
        });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
