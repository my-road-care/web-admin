import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { WorkingTableService } from '../working-table.service';

@Component({
    selector: 'app-work-side-dialog',
    templateUrl: './work-side-dialog.component.html',
    styleUrls: ['./work-side-dialog.component.scss']
})
export class WorkSideDialogComponent implements OnInit {

    public progressId = new EventEmitter();
    public loading: boolean = false;
    public response: any = null;
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<WorkSideDialogComponent>,
        private workingTableService: WorkingTableService,
        private snackBarService: SnackbarService
    ) {

    }

    ngOnInit(): void {
        this.progress();
    }

    progress(): void {
        this.loading = true;
        this.workingTableService.previewSide(this.data.project_id, this.data.side_id).subscribe((res: any) => {
            this.loading = false;
            this.response = res;
        }, (err: any) => {
            // console.log(err);
            this.loading = false;
            this.dialogRef.close();
            this.snackBarService.openSnackBar(err?.error?.message, 'error');
        });
    }

    getNewSide(value: any): void {
        // console.log(value);
        this.progressId.emit(value);
        this.dialogRef.close();
        this.snackBarService.openSnackBar('ទិន្នន័យត្រូវបានកែប្រែ', '');
    }

    testQuality(value: boolean): void {
        if (value) {
            this.dialogRef.close();
        }
    }
}
