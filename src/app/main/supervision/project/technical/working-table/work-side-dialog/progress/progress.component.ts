import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { WorkingTableService } from '../../working-table.service';
import ExifReader from 'exifreader'; //for check GPS
// import heic2any from "heic2any"; //for convert heic or heif to jpeg
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { MatDialog } from '@angular/material/dialog';
import { environment as env } from 'environments/environment';

@Component({
    selector: 'app-progress',
    templateUrl: './progress.component.html',
    styleUrls: ['./progress.component.scss']
})
export class ProgressComponent implements OnInit {

    @Output() updateSide = new EventEmitter();
    @Input() data: any;
    @Input() id: any;
    @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
    public fileUrl: any = env.fileUrl;
    public progressForm: UntypedFormGroup;
    public loading: boolean = false;
    constructor(
        private _formBuilder: UntypedFormBuilder,
        private workingTableService: WorkingTableService,
        private _snankBarService: SnackbarService,
        private _dialog: MatDialog,
    ) { }

    ngOnInit(): void {
        this.formBuilder();
    }

    formBuilder(): void {
        this.progressForm = this._formBuilder.group({
            message: [],
        });
    }

    files: File[] = [];

    async onSelect(event: any) {
        // console.log(event);
        if (event.addedFiles.length == 0) {
            this._snankBarService.openSnackBar('សូមជ្រើសរើសរូបភាពប្រភេទៈ png, jpg and jpeg', 'error');
            return;
        }
        if (event.addedFiles[0].name.toLowerCase().endsWith('heic')) {
            // this._snankBarService.openSnackBar('សូមជ្រើសរើសរូបភាព', 'error');
            // return;



            let f: File = event.addedFiles[0];
            let blob: Blob = f;
            let file: File = f;
            let convProm: Promise<any>;
            //CONVERT HEIC TO JPG
            const dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            // convProm = heic2any({ blob, toType: "image/jpeg", quality: 1 }).then(async (jpgBlob: Blob) => {
            //     //Change the name of the file according to the new format
            //     let newName = f.name.replace(/\.[^/.]+$/, ".jpeg");
            //     //Convert blob back to file
            //     file = this.blobToFile(jpgBlob, newName);
            //     // this.files.push(file);
            //     let v: any;
            //     v = await ExifReader.load(file);
            //     // console.log(v);
            //     if (v.GPSLatitude) {
            //         // // console.log(v);
            //     } else {
            //         this._snankBarService.openSnackBar('After converted this image,It does not have GPS. You must upload images that have extensions png, jpg and jpeg.', 'error');
            //     }
            //     dialog.close();


            // }).catch(err => {
            //     dialog.close();
            //     //Handle error
            //     // console.log(err);
            // });

        } else {
            let v: any;
            v = await ExifReader.load(event.addedFiles[0]);
            if (v.GPSLatitude) {
                this.files.push(...event.addedFiles);
            } else {
                this._snankBarService.openSnackBar('This image does not have GPS.', 'error');
            }
        }

    }

    onRemove(event: any) {
        // console.log(event);
        this.files.splice(this.files.indexOf(event), 1);
        // console.log(this.files);
    }

    submit(): void {

        if (this.files.length == 0) {
            // this._snankBarService.openSnackBar('សូមជ្រើសរើសរូបភាពយ៉ាងតិចមួយសន្លឹក', 'error');
            let data: any = {
                status_id: this.data?.progress?.next_progress?.id,
                images: null,
                message: this.progressForm.value?.message
            }
            // console.log(data.image);
            this.progressForm.disable();
            this.loading = true;
            this.workingTableService.updateProgress(this.id.project_id, this.id.side_id, data).subscribe((res: any) => {
                this.loading = false;
                // console.log(res);
                this.updateSide.emit(this.data?.progress?.next_progress?.id);
            }, (err: any) => {
                this.loading = false;
                // console.log(err);
                this.progressForm.enable();
            });
            return;
        }

        const images: ImagePayload[] = [];

        const startSubmit = () => {
            let data: any = {
                status_id: this.data?.progress?.next_progress?.id,
                images: JSON.stringify(images),
                message: this.progressForm.value?.message
            }
            // console.log(data.image);
            this.progressForm.disable();
            this.loading = true;
            this.workingTableService.updateProgress(this.id.project_id, this.id.side_id, data).subscribe((res: any) => {
                this.loading = false;
                // console.log(res);
                this.updateSide.emit(this.data?.progress?.next_progress?.id);
            }, (err: any) => {
                this.loading = false;
                // console.log(err);
                this.progressForm.enable();
            });
        }

        // console.log(this.files);

        this.files.forEach(async (v: any) => {
            const data = await ExifReader.load(v);
            var reader = new FileReader();
            reader.readAsDataURL(v);
            reader.onload = (event: any) => {
                images.push({
                    img: event.target.result,
                    lat: Number(data.GPSLatitude.description),
                    lng: Number(data.GPSLongitude.description)
                })

                if (images.length === this.files.length) {
                    startSubmit()
                }
            }
        });
    }

    private blobToFile = (theBlob: Blob, fileName: string): File => {
        return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
    }


    timeLine = [{ year: 'មិនទាន់អនុវត្ត', detail: 'Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.' },
    { year: 'កំពុងអនុវត្ត', detail: 'Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.' },
    { year: 'បញ្ចប់ការងារ', detail: 'Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.' },
    { year: 'បានចែកកូដរួច', detail: 'Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.' },
    { year: 'បានពិសោធន៍រួច', detail: 'Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.' }

    ]

}


interface ImagePayload {
    img: string,
    lat: number,
    lng: number
}
