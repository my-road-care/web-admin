import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WorkingTableService } from '../../working-table.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { ImageGPS, ImageGpsService } from 'app/shared/services/image-gps.service';
import { Animations } from 'helpers/animations';
import { environment as env } from 'environments/environment';

@Component({
    selector: 'app-quality',
    templateUrl: './quality.component.html',
    styleUrls: ['./quality.component.scss'],
    animations: Animations
})
export class QualityComponent implements OnInit {

    @Input() data: any;
    @Input() id: any;
    @Output() updateTestQuality = new EventEmitter();
    public fileUrl: any = env.fileUrl;
    public saving: boolean = false;
    public standards: any[] = [];
    public message: string = '';
    public result: number = null;
    public last_row: number = null;
    public validator: number = null;
    files: File[] = [];
    warningText = null;

    public selectedTestingMethod: any = null;

    constructor(
        private workingTableService: WorkingTableService,
        private _snankBarService: SnackbarService,
        private imageGPS: ImageGpsService
    ) {
    }

    ngOnInit(): void {
        this.standards = this.data?.quality_check ? this.data?.quality_check?.standards : [];
    }

    validate(): void {
        // console.log(this.result);
        if(this.result != null){
            if(this.result >= Number(this.selectedTestingMethod.requirment)){
                this.validator = 1;
            }else if(this.result < Number(this.selectedTestingMethod.requirment)){
                this.validator = 0;
            }
        }
    }

    selectionChange($event: any = null) {
        this.selectedTestingMethod = $event.value;
        this.last_row = this.selectedTestingMethod?.parameters.length + 1;
        this.validate();
    }

    async onSelect(event: any) {
        // console.log(event);
        if (event.rejectedFiles.length) {
            this._snankBarService.openSnackBar('សូមជ្រើសរើសរូបភាពប្រភេទៈ png, jpg and jpeg', 'error');
            return;
        }
        if (event.addedFiles.length > 3 || this.files.length >= 3) {
            this.warningText = '😒 រូបភាព មិនអាចលើសពី ៣! សូមអរគុណ។ 😝'
            setTimeout(() => this.warningText = null, 5000)
            return
        }
        event.addedFiles.forEach(async (file: File) => {
            try {
                const { img } = await this.imageGPS.processAnyImage(file)

                if (file.name.toLocaleLowerCase().endsWith('heic')) {
                    const newName = file.name.replace(/\.[^/.]+$/, ".jpeg");
                    this.files.push(await this.imageGPS.createImageFile(img, newName))
                } else this.files.push(file)
            } catch (error) {
                this._snankBarService.openSnackBar(error, 'error');
            }
        })

    }

    onRemove(event: any) {
        // console.log(event);
        this.files.splice(this.files.indexOf(event), 1);
        // console.log(this.files);
    }

    submit() {
        // console.log(this.selectedTestingMethod);
        let parameters = this.selectedTestingMethod ? this.selectedTestingMethod.parameters : null;
        let obj: any[] = [];
        if (parameters.length != 0) {
            parameters.forEach((data: any) => {
                obj.push({
                    parameter_id: data.id,
                    value: data?.value ? data.value : null
                })
            })
        }

        const send = (data: any) => {
            this.saving = true;
            this.workingTableService.testQuality(this.id.project_id, this.id.side_id, data).subscribe((res: any) => {
                this.saving = false;
                // console.log(res);
                this.updateTestQuality.emit(true);
                this._snankBarService.openSnackBar(res?.message, '');

            }, (err: any) => {
                this.saving = false;
            });
        }

        if (!this.files.length) return send({
            standard_test_id: this.selectedTestingMethod?.standard_test_id,
            result: this.result,
            message: this.message,
            images: null,
            parameters: JSON.stringify(obj)
        })

        const images: ImageGPS[] = [];

        this.files.forEach(async (file: File) => {
            images.push(await this.imageGPS.processAnyImage(file))
            if (images.length === this.files.length) send({
                standard_test_id: this.selectedTestingMethod?.standard_test_id,
                result: this.result,
                message: this.message,
                images: JSON.stringify(images),
                parameters: JSON.stringify(obj)
            })
        });
    }

    Number(a) {
        return Number(a)
    }
}
