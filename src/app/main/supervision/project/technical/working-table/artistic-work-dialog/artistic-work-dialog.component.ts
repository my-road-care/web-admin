import { Component, EventEmitter, OnInit } from '@angular/core';

@Component({
    selector: 'app-artistic-work-dialog',
    templateUrl: './artistic-work-dialog.component.html',
    styleUrls: ['./artistic-work-dialog.component.scss']
})
export class ArtisticWorkDialogComponent implements OnInit {

    public ArtisticWork = new EventEmitter();
    constructor() { }

    ngOnInit(): void {
        this.ArtisticWork.emit('Hello Kon Papa');
    }

}
