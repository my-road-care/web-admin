import { Component, EventEmitter,Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Subject } from 'rxjs';
import { TestingStandardService } from '../testing-standard.service';

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.scss'],
})
export class CreateTestingDialogComponent implements OnInit , OnDestroy{
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    @ViewChild('CreateStandardNgForm') CreateStandardNgForm: NgForm;
    CreateStandard = new EventEmitter();

    public StandardForm: UntypedFormGroup;
    public saving: boolean = false;
    public isLoading: boolean =false;
    public methods: any[] = [];
    public layers: any[] = [];
    public pks: any[] = [];
    public pk_start: any[] = [];
    public station_start: any[] = [];
    public pk_end: any[] = [];
    public station_end: any[] = [];
    public data : any;
    public unit :any = '';
    public unitShow:boolean=false;
    public methodError='';
    public showMethodError:boolean=false;
    public project_id :any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
    private dialogRef: MatDialogRef<CreateTestingDialogComponent>,
    private _testingStandartService: TestingStandardService,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
  )
  {
    dialogRef.disableClose = true;
  }

    ngOnInit(): void {

        this.data       = this.dataDialog.data;
        this.pks        = this.dataDialog.setup?.pks;
        this.layers     = this.dataDialog.setup?.layers;
        // this.methods     = this.dataDialog.setup?.methods;
        this.project_id = this.dataDialog.project_id;
        this.pks.forEach((v: any)=>{
            this.pk_start.push(v.pk);
            if(this.dataDialog.setup?.pks[this.dataDialog.setup?.pks.length-1].pk == v.pk){
              v.stations.forEach(s=>{
                this.station_end.push(s.number);
              })

            }
            if(this.dataDialog.setup?.pks[0].pk == v.pk){
              v.stations.forEach((station: any,i)=>{
                this.station_start.push({'id':i,'name':station.number});
            });
            }
            this.pk_end.push(v.pk);
        });
        this.formBuilder();
        this.layers.forEach(l=>{
          if(l.id==this.dataDialog.layerid){
            this.StandardForm.get('type_id').setValue(l.id);
            this.checkLayer(l.id);
          }
        })
        // this.StandardForm.get('testing_id').disable();
        this.StandardForm.get('standard').disable();
    }

    check(): void {
        this.pk_end = [];
        this.pks.forEach((v: any,i)=>{
            if(v.pk === this.StandardForm.value.pk_start){
                this.StandardForm.get('station_start').setValue('');
                this.station_start = [];
                this.StandardForm.get('station_end').setValue('');
                this.station_end = [];
                v.stations.forEach((station: any,i)=>{
                    this.station_start.push({'id':i,'name':station.number});
                });
            }
            if(v.pk >= this.StandardForm.value.pk_start){
                this.StandardForm.get('pk_end').setValue('');
                this.pk_end.push(v.pk);
            }
        });
        this.StandardForm.get('pk_end').enable();
        this.StandardForm.get('station_start').enable();
        this.StandardForm.get('station_end').disable();
    }
    checkEnd(): void {

      if(this.StandardForm.value.pk_start==this.StandardForm.value.pk_end){

        this.pks.forEach((v: any)=>{
          if(v.pk === this.StandardForm.value.pk_end){
         
              this.StandardForm.get('station_end').setValue('');
              this.station_end = [];
     
              v.stations.forEach((station: any,i)=>{
    
                  if(parseInt(station.number) > parseInt(this.StandardForm.value.station_start)){

                    this.station_end.push(station.number);
                }
              });
          }
      });
      }else{
        this.pks.forEach((v: any)=>{
          if(v.pk === this.StandardForm.value.pk_end){

              this.StandardForm.get('station_end').setValue('');
              this.station_end = [];

              v.stations.forEach((station: any,i)=>{
                    this.station_end.push(station.number);
              });
          }
      });
      }
      if(this.StandardForm.get('station_start').value!=''){
        this.StandardForm.get('station_end').enable();
      }
    }

    formBuilder(): void {
        this.StandardForm = this._formBuilder.group({
            pk_start:       [this.dataDialog.setup?.pks ? this.dataDialog.setup?.pks[0].pk : '', Validators.required],
            station_start:  ['', Validators.required],
            pk_end:         [this.dataDialog.setup?.pks ?  this.dataDialog.setup?.pks[this.dataDialog.setup?.pks.length-1].pk :'', Validators.required],
            station_end:    ['', Validators.required],
            type_id:        ['', Validators.required],
            testing_id:     ['', Validators.required],
            standard:       ['', Validators.required],
        });
    }
    oninput(event){
      if(event.target.value === ''){
        this.unitShow=false;
      }else{
        this.unitShow=true;
      }
    }
    checkmethodError(){
      if(this.methods.length === 0 && this.StandardForm.value.type_id != '' ){
        this.methodError="ស្រទាប់នេះមិនមានវិធីសាស្ត្រតេស្តទេ!";
        this.saving=true;
        this.showMethodError=true;
        this.StandardForm.get('testing_id').disable();
        this.StandardForm.get('standard').disable();
      }
      else{
      this.showMethodError=false;
      this.saving=false;
    }
  }
    checkmethod(event){
      this.StandardForm.get('standard').enable();
      this.unit=event.unit;
      this.unitShow=true;
      // let method=this.dataDialog.setup?.methods.filter(r=>r.id == this.StandardForm.get('testing_id').value);
      this.StandardForm.get('standard').setValue(event.value);
      
    }
    submit(){
        if(this.StandardForm.value){
          this.StandardForm.get('station_end').setValue(this.StandardForm.get('station_end').value.replace(',',''));
          this.StandardForm.get('station_start').setValue(this.StandardForm.get('station_start').value.replace(',',''));
            this.StandardForm.disable();
            this.saving = true;
            this._testingStandartService.create(this.StandardForm.value, this.project_id).subscribe((res:any) =>{
                this.isLoading = false;
                if(res.statusCode===200){
                  this.saving = false;
                  this.dialogRef.close();
                  this.CreateStandard.emit(res);
                  this._snackBar.openSnackBar(res.message, '');
                }else{
                  this.StandardForm.enable();
                  this.saving = false;
                  this._snackBar.openSnackBar(res.message, 'error');
                }

            },(err: any) =>{


                this.dialogRef.close();
                for(let key in err.error.errors){
                let control = this.StandardForm.get(key);
                control.setErrors({'servererror':true});
                control.errors.servererror = err.error.errors[key][0];
                this._snackBar.openSnackBar(err.error.message.message, '');
                }
            });
        }
        else{
            this._snackBar.openSnackBar('Please check your input.', 'error');
        }
    }
    checkSta_start():void{

      let lastindex=0;
      lastindex=this.station_start.length-1;
        this.station_start.forEach((v:any,i)=>{
          if(this.StandardForm.value.station_start === v.name){

              if(v.id==lastindex){

                  if(parseInt(this.StandardForm.value.pk_start)==parseInt(this.StandardForm.value.pk_end)){

                      this.pk_end=[];
                      this.StandardForm.get('pk_end').setValue('');
                      this.station_end=[];
                      this.StandardForm.get('station_end').setValue('');
                      this.pks.forEach((pk: any)=>{

                      if(pk.pk > this.StandardForm.value.pk_start){
                      this.pk_end.push(pk.pk);
                      }
                  })
                  }else{
                    this.pk_end=[];
                    this.pks.forEach((pk: any)=>{
                      if(pk.pk > this.StandardForm.value.pk_start){
                      this.pk_end.push(pk.pk);
                      }
                    })
                  }
                 }else{

               if(this.StandardForm.value.pk_start==this.StandardForm.value.pk_end){


                if(parseInt(this.StandardForm.value.station_start) >= parseInt(this.StandardForm. value.station_end)){
                  if(this.StandardForm.value.pk_end != ''){
                    this.pks.forEach((v: any)=>{
                      if(v.pk == this.StandardForm.value.pk_end){
                        this.station_end = [];
                        v.stations.forEach((station: any,i)=>{
                          if(parseInt(station.number) > parseInt(this.StandardForm.value.station_start)){
                                this.station_end.push(station.number);
                            }
                        })
                      }
                    })
                    }
                }else{

                 if(this.StandardForm.value.pk_end != ''){
                  this.pks.forEach((v: any)=>{

                    if(v.pk == this.StandardForm.value.pk_end){
                      this.station_end = [];
                      v.stations.forEach((station: any,i)=>{
                        if(parseInt(station.number) > parseInt(this.StandardForm.value.station_start)){
                                    this.station_end.push(station.number);
                          }
                      })
                    }
                });

                 }
                }
               }else{

                this.pk_end=[];
                this.pks.forEach((pk: any)=>{
                  if(pk.pk >= this.StandardForm.value.pk_start){
                  this.pk_end.push(pk.pk);
                }
              });
               }
          }
          }
        })
       if( this.StandardForm.get('pk_end').value != ''){
        this.StandardForm.get('station_end').enable();
       }
    }
    checkLayer(id:any){

       this.StandardForm.get('testing_id').enable();
        this.methods=[];
        this.StandardForm.get('testing_id').setValue('');
        this.layers?.forEach((e:any)=>{

          if(e?.id===id){
            e.standard.forEach(s=>{
              if(s.method != null ){
        
                this.methods.push({"id":s.method.testing_method_id,'name':s.method.method.name,'testing_method_type':s.method.method.testing_method_type,'unit':s.method.method.unit,"value":s.value});
              }
            })
            
          }
        });
        this.checkmethodError();


    }
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }





}
