import { Component, EventEmitter, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { Subject } from 'rxjs';
import { UpdateDialogComponent } from '../../../info/procurement/update-dialog/update-dialog.component';
import { TestingStandardService } from '../testing-standard.service';

@Component({
  selector: 'app-update-dialog',
  templateUrl: './update-dialog.component.html',
  styleUrls: ['./update-dialog.component.scss']
})
export class UpdateTestingDialogComponent implements OnInit, OnDestroy {

  private _unsubscribeAll: Subject<any> = new Subject<any>();
  @ViewChild('UpdateStaNgForm') UpdateStaNgForm: NgForm;
  UpdateSta = new EventEmitter();
  public updateForm: UntypedFormGroup;
  public saving: boolean = false;
  public isLoading: boolean = false;
  public test_types: any[] = [];
  public layers: any[] = [];
  public pks: any[] = [];
  public pk_start: any[] = [];
  public station_start: any[] = [];
  public pk_end: any[] = [];
  public station_end: any[] = [];
  public data: any;
  public unit :any = '';
  public unitShow:boolean=false;
  public project_id: any;
  constructor(
    @Inject(MAT_DIALOG_DATA) public dataDialog: any,
    @Inject(MAT_DIALOG_DATA) public id: any,
    private dialogRef: MatDialogRef<UpdateDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _testingStandartService: TestingStandardService,
    private _snacBar: SnackbarService,
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.data = this.dataDialog.data;
    this.pks = this.dataDialog.setup?.pks;
    this.layers = this.dataDialog.setup?.layers;

    // this.layers?.forEach((e:any)=>{
    //   if(e?.id===this.data?.work_type?.id){
    //     this.test_types=e?.testing_methods;
    //   }
    // });
    this.layers?.forEach((e:any)=>{
      if(e?.id===this.data?.work_type?.id){
        e.standard.forEach(s=>{
          if(s.method != null ){
            this.test_types.push({"id":s.method.testing_method_id,'name':s.method.method.name,'testing_method_type':s.method.method.testing_method_type,'unit':s.method.method.unit,"value":s.value});
          }
        })
        
      }
    });

    this.project_id = this.dataDialog.project_id;
    this.unit=this.dataDialog.setup?.methods?.unit;
    this.unitShow=true;
    this.unit=this.data?.method?.unit;
    this.pks.forEach((v: any) => {
      this.pk_start.push(v.pk);
      this.pk_end.push(v.pk);

      if(parseInt(this.data.pk_start)===parseInt(v.pk)){

            v.stations.forEach((station:any,i)=>{
              this.station_start.push({'id':i,'name':station.number});
            })

      }
      if(parseInt(this.data.pk_end)===parseInt(v.pk)){
        v.stations.forEach((station:any)=>{
          if(parseInt(this.data.pk_end) == parseInt(this.data.pk_start)){
            if(parseInt(station.number.replace(',','')) > parseInt(this.data?.station_start.replace(',',''))){
              this.station_end.push(station.number);
           }
          }else{
            this.station_end.push(station.number);
          }
        
        })

  }
  
    });
    this.formBuilder();
   
  }
  oninput(event){
    if(event.target.value === ''){
      this.unitShow=false;
    }else{
      this.unitShow=true;
    }
  }
  checkmethod(event){
    this.unit=event.unit;
    this.updateForm.get('standard').setValue(event.value);
    this.updateForm.get('standard').markAsTouched();
  }
  check(): void {
    this.pk_end = [];
    this.pks.forEach((v: any,i)=>{
        if(v.pk === this.updateForm.value.pk_start){
            this.updateForm.get('station_start').setValue('');
            this.station_start = [];
            this.updateForm.get('station_end').setValue('');
            this.station_end = [];
            v.stations.forEach((station: any,i)=>{
                this.station_start.push({'id':i,'name':station.number});
            });
        }
        if(v.pk >= this.updateForm.value.pk_start){
            this.updateForm.get('pk_end').setValue('');
            this.pk_end.push(v.pk);
        }
    });
}

checkEnd(): void {
  if(this.updateForm.value.pk_start==this.updateForm.value.pk_end){
    this.pks.forEach((v: any)=>{
      if(v.pk === this.updateForm.value.pk_end){
          this.updateForm.get('station_end').setValue('');
          this.station_end = [];
          v.stations.forEach((station: any,i)=>{
              if(parseInt(station.number) > parseInt(this.updateForm.value.station_start)){

                this.station_end.push(station.number);
            }
          });
      }
  });
  }else{
    this.pks.forEach((v: any)=>{
      if(v.pk === this.updateForm.value.pk_end){
          this.updateForm.get('station_end').setValue('');

          this.station_end = [];
          v.stations.forEach((station: any,i)=>{
                this.station_end.push(station.number);
          });
      }
  });
  }

}
checkSta_start():void{
  let lastindex=0;
  lastindex=this.station_start.length-1;
    this.station_start.forEach((v:any,i)=>{
      if(this.updateForm.value.station_start === v.name){
    
          if(v.id==lastindex){
  
              if(parseInt(this.updateForm.value.pk_start)==parseInt(this.updateForm.value.pk_end)){
                
                  this.pk_end=[];
                  this.updateForm.get('pk_end').setValue('');
                  this.station_end=[];
                  this.updateForm.get('station_end').setValue('');
                  this.pks.forEach((pk: any)=>{

                  if(pk.pk > this.updateForm.value.pk_start){
                  this.pk_end.push(pk.pk);
                  }
              })
              }else{
                this.pk_end=[];
                this.pks.forEach((pk: any)=>{
                  if(pk.pk > this.updateForm.value.pk_start){
                  this.pk_end.push(pk.pk);
                  }
                })
              }
             }else{
         
           if(this.updateForm.value.pk_start==this.updateForm.value.pk_end){
         
            if(parseInt(this.updateForm.value.station_start) >= parseInt(this.updateForm. value.station_end)){
              if(this.updateForm.value.pk_end != ''){
                this.pks.forEach((v: any)=>{
                  if(v.pk == this.updateForm.value.pk_end){
                    this.station_end = [];
                    v.stations.forEach((station: any,i)=>{
                      if(parseInt(station.number) > parseInt(this.updateForm.value.station_start)){
                            this.station_end.push(station.number);
                        }
                    })
                  }
                })
                }
            }else{
            
             if(this.updateForm.value.pk_end != ''){
              this.pks.forEach((v: any)=>{

                if(v.pk == this.updateForm.value.pk_end){
                  this.station_end = [];
                  v.stations.forEach((station: any,i)=>{
                    if(parseInt(station.number) > parseInt(this.updateForm.value.station_start)){
                                this.station_end.push(station.number);
                      }
                  })
                }
            });

             }
            }
           }else{
           
            this.pk_end=[];
            this.pks.forEach((pk: any)=>{
              if(pk.pk >= this.updateForm.value.pk_start){
              this.pk_end.push(pk.pk);
            }
          });
           }
      }
      }
    })


}

  formBuilder(): void {

    this.updateForm = this._formBuilder.group({
      pk_start: [this.data?.pk_start, Validators.required],
      station_start:  [this.data?.station_start, Validators.required],
      pk_end: [this.data?.pk_end, Validators.required],
      station_end:[this.data?.station_end,Validators.required ],
      type_id: [this.data?.work_type?.id, Validators.required],
      testing_id: [this.data?.method?.id, Validators.required],
      standard: [this.data?.standard, Validators.required],
    });
  }

  submit(): void {
    if (this.updateForm.invalid) {
      return;
    }
    this.updateForm.disable();
    this.updateForm.get('station_end').setValue(this.updateForm.get('station_end').value.replace(',',''));
    this.updateForm.get('station_start').setValue(this.updateForm.get('station_start').value.replace(',',''));
    this.saving = true;
    this._testingStandartService.update(this.updateForm.value, this.project_id, this.data.id).subscribe(
      (res: any) => {

        if(res.statusCode===200){
          this.saving = false;
          this.dialogRef.close();
       
            this.UpdateSta.emit(res);
            this._snacBar.openSnackBar(res.message, '');
        }else{
          this.updateForm.enable();
          this.saving = false;
          this._snacBar.openSnackBar(res.message, 'error');
        }
      },
      () => {
        this.updateForm.enable();
        this.saving = false;
        this.UpdateStaNgForm.resetForm();
        this.dialogRef.close();
        this._snacBar.openSnackBar('Something went wrong!', 'error');
      }
    );
  }
//   checkLayer(id:any){
//     this.test_types=[];
//     this.updateForm.get('testing_id').setValue('');
//     this.layers?.forEach((e:any)=>{
//       if(e?.id===id){
//         e.standard.forEach(s=>{
//           if(s.method != null ){
//             this.test_types.push({"id":s.method.testing_method_id,'name':s.method.method.name,'testing_method_type':s.method.method.testing_method_type,'unit':s.method.method.unit,"value":s.value});
//           }
//         })
        
//       }
//     });
//     // this.layers?.forEach((e:any)=>{
//     //   if(e?.id===id){
//     //     this.test_types=e?.testing_methods;
//     //   }
//     // });


// }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }



}
