import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class TestingStandardService {
    shortList(id: number, by: any, param: any) {
      throw new Error('Method not implemented.');
    }

    private url: string = env.apiUrl;
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) { }

    /**
     |**************** Testing Standard ****************|
     |--------------------------------------------------|
     */

    listing(params = {}, project_id: number = 0): any {
        const httpOptions: object = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        };
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/testing-standards', httpOptions);
    }

    create(data: any = {}, project_id: number = 0): any {
        return this.http.post(this.url + '/sup/projects/' + project_id + '/technical/testing-standards', data, this.httpOptions);
    }

    update(data: any = {}, project_id: number = 0, id: number = 0): any {
        return this.http.post(this.url + '/sup/projects/' + project_id + '/technical/testing-standards/' + id, data, this.httpOptions);
    }

    delete(project_id: number = 0, id: number = 0): any {
        return this.http.delete(this.url + '/sup/projects/' + project_id + '/technical/testing-standards/' + id, this.httpOptions);
    }
    sortList(project_id: number = 0,params:any):Observable<any>{
      const httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
    };
    httpOptions['params'] = params;
      return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/testing-standards/sort',httpOptions);
    }
    setup(project_id: any): any {
      return this.http.get(this.url + '/sup/projects/' + project_id + '/technical/testing-standards/data-setup-testing-standards', this.httpOptions);
    }
    getjsReport(id: any, body: any): any {
        return this.http.post(this.url + '/sup/projects/' + id + '/report/system-report', body, this.httpOptions);
    }
    private _testingStandard: BehaviorSubject<any> = new BehaviorSubject<any>(1);
    set standard(value: object) {
        this._testingStandard.next(value);
    }
    get standard$(): Observable<any> {
        return this._testingStandard.asObservable();
    }
}
