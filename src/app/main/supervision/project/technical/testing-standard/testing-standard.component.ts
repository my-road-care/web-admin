import { Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as FileSaver from 'file-saver';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { CreateTestingDialogComponent } from './create-dialog/create-dialog.component';
import { TestingStandardService } from './testing-standard.service';
import { UpdateTestingDialogComponent } from './update-dialog/update-dialog.component';
import { TechnicalService } from '../technical.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
    selector: 'app-testing-standard',
    templateUrl: './testing-standard.component.html',
    styleUrls: ['./testing-standard.component.scss']
})
export class TestingStandardComponent implements OnInit, OnDestroy {
    @ViewChild('loadingTemplate', { static: true }) private loadingTemplate: TemplateRef<any>;
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    @Input() public id: number;
    @Input() public code:number;
    @Input() public access_permission: any;
    public setup: any;
    public isSearching: boolean = false;
    public isSaving: boolean = false;
    public displayedColumns: string[] = ['no', 'pks', 'work_type', 'method', 'standard', 'create_at', 'created_by', 'action'];
    public dataSource: any;
    public data: any[] = [];
    public filename: any;
    public total: number = 10;
    public limit: number = 1000;
    public page: number = 1;
    public isLoading: boolean = true;
    public isClosing: boolean = false;
    public project_id: any;
    public setupList: any[] = [];
    public work_type: any = '';
    public method: any = '';
    public methos: any = [];
    Alldata: any[] = []
    constructor(
        private _testingStandartService: TestingStandardService,
        private _technicalService: TechnicalService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
    ) { }

    ngOnInit(): void {
       
        this._technicalService.technical$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
            // if (res.testingStandard == 'can_loading' && this.isLoading) {}
            if (res.testingStandard == 'can_loading') this._testingStandartService.setup(this.id).subscribe((res: any) => {
                this.setup = res;
                this.listing(this.limit, this.page);
            });
        });
        this.methos = [];

    }

    //===================================>> List
    listing(_limit: number = 1000, _page: number = 1): any {
        this.Alldata = []
        if (this.setup?.layers?.length === 0 || this.setup?.pks?.length === 0) {
            this._snackBar.openSnackBar('សូមបង្កើតតារាងការងារជាមុនសិន​!', 'error');
            this.isLoading = false;
        } else {
            const param: any = {
                pageSize: _limit,
                page: _page,
            };
            if (this.page != 0) {
                param.page = this.page;
            }
            if (this.work_type != '') {
                param.workid = this.work_type;
            }
            let setups = [];
            this.setup?.layers.forEach(l => {
                l.item = []
                l.item.sub = [];
            })
            this.isLoading = true;
            this._testingStandartService.listing(param, this.id).subscribe((res: any) => {
                this.isLoading = false;
                this.data = res?.data?.testing_standard;
                this.setup.layers.forEach(l => {
                    this.data.forEach(e => {
                        if (l.id == e.work_type.id) {
                            if (l.item.sub.length == 0) {
                                l.item.sub.push({ "name": e.method.name, methodid: e.method.id, "data": [e] });
                            } else {

                                if (l.item.sub.filter(r => r.methodid == e.method.id).length == 0) {
                                    l.item.sub.push({ "name": e.method.name, methodid: e.method.id, "data": [e] });
                                } else {
                                    l.item.sub.forEach(s => {
                                        if (e.method.id == s.methodid) {
                                            s.data.push(e);
                                        }
                                    });
                                }
                            }
                            l.item.push(e);
                        }
                    })
                    this.Alldata.push(l);
                })
                this.total = res?.data?.pagination?.total;
                this.page = res?.data?.pagination?.current_page;
                this.limit = res?.data?.pagination?.per_page;
                this.dataSource = new MatTableDataSource(this.data);

            }, (err: any) => {
                this.isLoading = false;
               
            });
        }
    }

    //===================================>> Create
    create(layer: any, id: any) {
       
        if (this.setup?.layers?.length === 0 || this.setup?.pks?.length === 0) {
            this._snackBar.openSnackBar('សូមបង្កើតតារាងការងារជាមុនសិន​!', 'error');
            this.isLoading = false;
        } else {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.data = {
                data: this.id,
                setup: this.setup,
                project_id: this.id,
                layer: layer,
                layerid: id
            }
            dialogConfig.autoFocus = false;
            dialogConfig.width = "850px";
            const dialogRef = this._dialog.open(CreateTestingDialogComponent, dialogConfig);
            dialogRef.componentInstance.CreateStandard.subscribe((response: any) => {
                this.listing();
            });
        }

    }

    //===================================>> Update
    update(row: any, layer: any) {
       
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            data: row,
            setup: this.setup,
            project_id: this.id,
            layer: layer
        }
        dialogConfig.autoFocus = false;
        dialogConfig.width = "850px";
        const dialogRef = this._dialog.open(UpdateTestingDialogComponent, dialogConfig);
        dialogRef.componentInstance.UpdateSta.subscribe((response: any) => {
            this.listing();
            this.dataSource = new MatTableDataSource(this.data);
        });
    }

    //===================================>> Delete
    delete(id: any) {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
        });
        dialogRef.afterClosed().subscribe((result) => {

            if (result) {
                this._testingStandartService.delete(this.id, id).subscribe((res: any) => {
                    this.isClosing = false;
                    this._snackBar.openSnackBar(res.message, '');

                    this.listing();
                }, (err: any) => {
                    this.isClosing = false;
                    let message = err?.error?.message ? err.error.message : "Something went wrong.";
                    this._snackBar.openSnackBar(message, 'error');
                });
            }
        });
    }

    //=======================================>> On Page Changed
    onPageChanged(event: any): any {
        if (event && event.pageSize) {
            this.limit = event.pageSize;
            this.page = event.pageIndex + 1;
            this.listing(this.limit, this.page);
        }
    }

    onChangeLayer(type: any) {

        this.method = [];
        if (this.work_type == 0) {
            this.methos = [];
            this.method = '';
        }
        else {
            this.setup?.layers.forEach((e: any) => {
                if (e?.id === this.work_type) {
                    this.methos = e?.testing_methods;
                }
            })

        }
        if (this.setup?.layers?.length === 0 || this.setup?.pks?.length === 0) {
            this._snackBar.openSnackBar('សូមបង្កើតតារាងការងារជាមុនសិន​!', 'error');
        } else {
            const param: any = {
                pageSize: this.limit
            };
            if (this.page != 0) {
                param.page = this.page;
            }
            // if (this.method != '') {
            //     param.methodid = this.method;

            // }
            if (this.work_type != '') {
                param.workid = this.work_type;
            }
            this.isLoading = true;
            this._testingStandartService.sortList(this.id, param).subscribe((res: any) => {
                this.isLoading = false;
                this.data = res?.data?.testing_standard;
                this.data.forEach((e, i) => {
                    if (!e.locat) {
                        if (i % 2 == 1) {
                            e.locat = 'ការដ្ឋាន';
                        } else {
                            e.locat = 'មន្ទីរ​ពិសោធន៍';
                        }
                    }
                })
                this.total = res?.data?.pagination?.total;
                this.page = res?.data?.pagination?.current_page;
                this.limit = res?.data?.pagination?.per_page;
                this.dataSource = new MatTableDataSource(this.data);
               
            }, (err: any) => {
                this.isLoading = false;
            
            });
        }
    }
    onChangeTest(type: any) {

        if (this.setup?.layers?.length === 0 || this.setup?.pks?.length === 0) {
            this._snackBar.openSnackBar('សូមបង្កើតតារាងការងារជាមុនសិន​!', 'error');
        } else {
            const param: any = {
                pageSize: this.limit
            };
            if (this.page != 0) {
                param.page = this.page;
            }
            if (this.method != '') {
                param.methodid = this.method;

            }
            if (this.work_type != '') {
                param.workid = this.work_type;
            }
            this.isLoading = true;
            this._testingStandartService.sortList(this.id, param).subscribe((res: any) => {
                this.isLoading = false;
                this.data = res?.data?.testing_standard;
                this.data.forEach((e, i) => {
                    if (!e.locat) {
                        if (i % 2 == 1) {
                            e.locat = 'ការដ្ឋាន';
                        } else {
                            e.locat = 'មន្ទីរ​ពិសោធន៍';
                        }
                    }
                })
                this.total = res?.data?.pagination?.total;
                this.page = res?.data?.pagination?.current_page;
                this.limit = res?.data?.pagination?.per_page;
                this.dataSource = new MatTableDataSource(this.data);
               
            }, (err: any) => {
                this.isLoading = false;
               
            });
        }
    }
    downloadjsreport(report: number) {
        let obj = {
            report: report,
        }
        if (report === 7) {
            const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            const day = ("0" + currentDate.getDate()).slice(-2);
            const hours = ("0" + currentDate.getHours()).slice(-2);
            const minutes = ("0" + currentDate.getMinutes()).slice(-2);
            const seconds = ("0" + currentDate.getSeconds()).slice(-2);
            const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
            let dialog = this._dialog.open(this.loadingTemplate, { width: 'auto', height: 'auto', disableClose: true });
            this._testingStandartService.getjsReport(this.id, obj).subscribe((res: any) => {
                this.isSaving = false;
                let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
                FileSaver.saveAs(blob, this.code + '-របាយការណ៍ស្តង់ដារតេស្ត-' + formattedDate + '.pdf');
                dialog.close();
            }, (err: any) => {
                this.isSaving = false;
                console.log(err);
                dialog.close();
            });
        }
    }
    b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
    ngOnDestroy(): void {
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
