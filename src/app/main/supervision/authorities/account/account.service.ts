import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AccountService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}

    //==================================================================================>> Listing Project
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/authority/user/supervision', httpOptions);
    }

    type(params = {}){
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/authority/user/type', httpOptions);
    }


    search(data:any,params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + `/sup/users?key=${data}`, httpOptions);
    }

    getOrganization(){
        return this.http.get(this.url + '/sup/authority/user/organization');
    }

    setUP(){
        return this.http.get(this.url + '/sup/authority/user/setup',this.httpOptions);
    }

    // getOranizationRole(){
    //     return this.http.get(this.url + '/sup/authority/user/oranization-role');
    // }


    create(data:any,params = {}):any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.post(this.url + '/sup/authority/user',data, httpOptions);
    }

    createUserProject(projectId:any,data:any,params = {}):any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.post(this.url + `/sup/projects/${projectId}/info/working-structures-user`,data, httpOptions);
    }

    update(data:any,userId:any):any{

        return this.http.post(this.url + `/sup/authority/user/update?id=${userId}`,data, this.httpOptions);
    }

    delete(id:any,params = {}){
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.delete(this.url + `/sup/authority/user?id=${id}`,httpOptions);
    }

    view(id:any,params = {}){
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + `/sup/users/${id}`, httpOptions);
    }
    
    updateUser(data:any,id:any){
        return this.http.post(this.url+ `/sup/projects/update?id=${id}`,data);
    }

    getSys(){
        return this.http.get(this.url + `/sup/authority/user/type`);
    }
}
