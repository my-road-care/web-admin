import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { ViewComponent } from './view/view.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { accountRoutes } from './account.routing';
import { DialogComponent } from './create-dialog/dialog.component';
import { UpdateDialogComponent } from './update-dialog/dialog.component';
import { ViewProjectComponent } from './view-project/view-project.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(accountRoutes)
    ],
    declarations: [
      ListingComponent,
      ViewComponent,
      DialogComponent,
      UpdateDialogComponent,
      ViewProjectComponent
    ]
})

export class AccountModule{}