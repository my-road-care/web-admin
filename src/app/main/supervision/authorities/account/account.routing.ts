import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";
import { ViewComponent } from "./view/view.component";

export const accountRoutes: Route[] = [{
    path: 'accounts',
    children: [
        { path  : '',  component: ListingComponent },
        { path  : ':id', component: ViewComponent }
    ]
}];