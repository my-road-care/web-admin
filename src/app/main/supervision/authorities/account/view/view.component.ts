import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  constructor(
    private _service: AccountService,
    private _route  : ActivatedRoute
  ) { }
  public isSearching = false;
  public name:string;
  public id  :number;

  ngOnInit(): void {
    this.id = this._route.snapshot.params['id'];
    this.view(this.id);
  }

  view(id:any):any{
    this.isSearching = true;
    this._service.view(this.id).subscribe((res:any)=>{
      this.name = res.name;
      this.isSearching = false;
    })
  }

}
