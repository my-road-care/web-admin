import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { AccountService } from '../account.service';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { DataSetupService } from '../dataSetup.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogComponent } from '../create-dialog/dialog.component';
import { environment as env } from 'environments/environment';
import { UpdateDialogComponent } from '../update-dialog/dialog.component';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { LoadingService } from 'helpers/services/loading';
import { ViewProjectComponent } from '../view-project/view-project.component';
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id','name','organization','type','status','nPro','action'];
  public isClosing :boolean = false
  public data : any[]   = [];
  public total: number = 10;
  public limit: number = 10;
  public page:  number = 1;
  public dataSource:any = [];
  public searchValue: String = "";
  public searchBtn: boolean = true;
  public organizations:any = [];
  public organizationRoles:any = [];
  public fileUrl = env.fileUrl;
  public type:any = [];
  constructor(
    private _service: AccountService,
    private _snackbar: SnackbarService,
    private _router  : Router,
    private _dataSetup: DataSetupService,
    private _dialog   : MatDialog,
    private _loadingService: LoadingService,

  ) { }

  public isSearching: boolean = false;
  // public data : any[]   = [];
  
  
  ngOnInit(): void {
    this.isSearching = true;
    // this.getOrganization();
    // this.getOrganizationRole();
    this._service.setUP().subscribe((res:any)=>{
     
      this.organizations=res.organizations;
      this.organizationRoles=res.roles;
      this.type=res.types;
    })
    this.listing(this.limit,this.page);
    // this.getType();
  }

  viewProject(data:any){
    const dia = this._dialog.open(ViewProjectComponent,{
      width: '900px',
      data: data
    })
  }

  getType(){
    this._service.type().subscribe((res:any)=>{
      this.type = res;
    })
  }

  getOrganization(){
    this._service.getOrganization().subscribe((res:any)=>{
      this.organizations = res?.data;
    })
  }

  // getOrganizationRole(){
  //   this._service.getOranizationRole().subscribe((res:any)=>{
  //     this.organizationRoles = res;
  //   })
  // }

  create(){

    const dia = this._dialog.open(DialogComponent,{
      width: '900px',
      data: {
        title: 'បង្កើតគណនី',
        organizations    : this.organizations,
        organizationRoles: this.organizationRoles,
        type             : this.type 
      }
    });

    dia.componentInstance.DateInfo.subscribe((res:any)=>{
      this.listing();
    })

  }

  checkBtn(){
    if(this.searchValue == ""){
      this.searchBtn = true; 
    }
    else{
      this.searchBtn = false;
    }
  }

  listing(_limit: number = 10, _page: number = 1){
    this.isSearching = true;
    const params: any = {
      limit: _limit,
      page : _page
    };
    this._service.listing(params).subscribe((res:any)=>{

      this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data   = res.data;
      let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        this.dataSource = new MatTableDataSource(this.data);
    })
  }

  onPageChanged(event:any){
    this.isSearching = true;
    if(event && event.pageSize){
      this.limit = event.pageSize;
      this.page  = event.pageIndex+1;
      this.listing(this.limit,this.page);
    }
  }

  delete(data:any = null,id:any):any{
    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res:any)=>{
      if(res){
        this.isClosing = true;
        this._loadingService.show();
        this._service.delete(id).subscribe((res:any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackbar.openSnackBar(res.message,'');
            let copy: any[] = [];
            this.data.forEach((obj: any) => {
                if (obj.id !== data.id) {
                    copy.push(obj);
                }
            });
            this.data = copy;
            this.total -= 1;
            this.dataSource = new MatTableDataSource(this.data);
          },(err: any)=>{
            this.isClosing = false;
            this._loadingService.hide();
            this._snackbar.openSnackBar('Something went wrong.','error');
            // console.log(err);
          }
        );
      }
    });
    // this._service.delete(id).subscribe((res:any)=>{
    //   this._snackbar.openSnackBar(res.message,'')
    //   this.listing(this.limit,this.page);
    // })
  }

  search():any{
    this.isSearching = true;
    const params: any = {
      limit: this.limit,
      page : this.page,
      key  : this.searchValue
    };
    this._service.listing(params).subscribe((res:any)=>{
      this.isSearching = false;
      this.total  = res.total;
      this.page   = res.current_page;
      this.limit  = res.per_page;
      this.data   = res.data;
      let copy: any[] = [];
        let j = res?.from;
        this.data.forEach((v:any)=>{
           v = {
            ...v,
            rn: j,
          }
          j++;
          copy.push(v);
        });
        this.data = copy;
        this.dataSource = new MatTableDataSource(this.data);
    })
  }

  update(row:any){
    const dia = this._dialog.open(UpdateDialogComponent,{
      width: '900px',
      data: {
        title: 'កែប្រែគណនី',
        row: row,
        organizations: this.organizations,
        organizationRoles: this.organizationRoles,
        type: this.type
      }
    })
    dia.componentInstance.DateInfo.subscribe((res:any)=>[
      this.listing()
    ]);
  } 

  detail(id:any){
    this._router.navigate([`/sup/authorities/accounts/${id}`]);
  }

  preview(src: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
        src: src,
        title: 'រូបភាព'
    };
    dialogConfig.width = "850px";
    this._dialog.open(PreviewImageComponent, dialogConfig);
  }

}
