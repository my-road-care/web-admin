import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { AccountService } from "../account.service";
import { environment as env } from "environments/environment";

import { MatDialog } from '@angular/material/dialog';
@Component({
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})
export class UpdateDialogComponent implements OnInit{
    constructor(
        public dialogRef: MatDialogRef<UpdateDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: AccountService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
        
    ){
        dialogRef.disableClose = true;
    }
    ngOnInit(): void {
        this.worksType = this.data?.worksType;
        this.worksGroup = this.data?.worksGroup;
        this.src = this.fileUrl+this.data?.row?.avatar;
        this.formGroup.get('name').setValue(this.data?.row?.name);
        this.formGroup.get('organization_id').setValue(this.data?.row?.organization.id);
        this.formGroup.get('organization_role').setValue(this.data?.row?.oranization_role.id);
        this.formGroup.get('type').setValue(this.data?.row?.type?.id);
        // console.log(this.data);
    }
    
    public fileUrl = env.fileUrl;
    public btnDisable: boolean = true;
    public route: string  = "";
    public src: string    = "";
    public title: string  = this.data.title;
    public worksType:any  = [];
    public worksGroup:any = []
    public show: boolean  = false;
    public saving: boolean = false;
    public size: number   = 0;
    public type: string   = '';
    public error_size: string = '';
    public error_type: string = '';
    public isloading = false;
    public type_id   :number;
    public role = localStorage.getItem('access');
    
    formGroup = new FormGroup({
        name              : new FormControl('',[Validators.required, Validators.maxLength(100)]),
        organization_id   : new FormControl('',[Validators.required]),
        organization_role : new FormControl('',[Validators.required]),
        image_uri         : new FormControl(''),
        type              : new FormControl('',[Validators.required]),
    })
    
    DateInfo = new EventEmitter();
    
    create(){
        this.formGroup.disable();
        this.saving = true;
        this.isloading = true;

        if(this.src == this.fileUrl+this.data?.row?.avatar){
            this.formGroup.get('image_uri').setValue(null);
        }
        
        this._service.update(this.formGroup.value,this.data?.row?.id).subscribe((res:any)=>{
            this.isloading = false;
            this.saving = false;
            this.DateInfo.emit(res)
            this.formGroup.enable();
            this._snackBar.openSnackBar(this.title+'ដោយជោគជ័យ','');
            this.dialogRef.close();
        })

    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }
    
    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.formGroup.get('image_uri').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.formGroup.get('image_uri').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.formGroup.get('image_uri').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.formGroup.get('image_uri').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }
    
    preview(){
        if(this.src == ""){
            document.getElementById('portrait-file').click();
        }
        else{
            this._dialog.open(PreviewImageComponent,{
                width: '850px',
                data: {
                  title: 'រូបភាពប្រភេទការងារ',
                  src: this.src
                }
              })
        }
    }

}