import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { AccountService } from "../account.service";
import { Router } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
@Component({
    templateUrl: './view-project.component.html',
    styleUrls: ['./view-project.component.scss']
})
export class ViewProjectComponent implements OnInit{
    constructor(
        public dialogRef: MatDialogRef<ViewProjectComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: AccountService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
        private _route: Router,
        
    ){
        dialogRef.disableClose = true;
    }
    ngOnInit(): void {
        // console.log(this.data);
    }

    
    goToProject(projectID:number){
        
        this.dialogRef.close();
        this._route.navigate(['/sup/projects/'+projectID+'/info']);
    }
    
    
    DateInfo = new EventEmitter();
    
   

}