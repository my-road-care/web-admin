import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DataSetupService {
    private _types: ReplaySubject<any> = new ReplaySubject<any>(1);
    private _organization: ReplaySubject<any> = new ReplaySubject<any>(2);
    

    /**
     * Constructor
     */
    constructor() { }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for data and setup
     *
     * @param value
     */
    set types(value: any) {
        // console.log(value);
        this._types.next(value);
    }
    set organization(value: any) {
        this._organization.next(value);
    }

    /**
     * @returns data
     */
    get types$(): Observable<any> {
        return this._types.asObservable();
    }
    get organization$(): Observable<any> {
        return this._organization.asObservable();
    }
}
