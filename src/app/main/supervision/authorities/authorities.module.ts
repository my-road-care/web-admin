import { NgModule } from '@angular/core';
import { AccountModule } from './account/account.module';
import { PermissionsModule } from './permissions/permissions.module';
import { TypesModule } from './types/types.module';
import { ProjectRoleModule } from './project-role/project.module';
@NgModule({
    imports: [
        AccountModule,
        TypesModule,
        PermissionsModule,
        ProjectRoleModule
    ],
    exports: [
        AccountModule,
        TypesModule,
        PermissionsModule,
        ProjectRoleModule
    ]
})
export class AuthoritiesModule{}