import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { TypeService } from '../type.service';
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['id','name','abbre','description','date','qty'];
  public data:  any = [];
  public dataSource = [];
  public searchValue: String;
  public total:number;
  constructor(
    private _service: TypeService,
    private _snackbar: SnackbarService,
    private _router  : Router,

  ) { }
  public isSearching: boolean = false;

  ngOnInit(): void {
    this.listing();
  }


  listing(){
    this.isSearching = true;
    this._service.listing().subscribe((res:any)=>{
      this.data       = res?.data;
      this.dataSource = res?.data;
      this.isSearching = false;
      this.total = res?.total
    })
  }
  search():any{
    this.isSearching = true;
    this._service.search(this.searchValue).subscribe((res:any)=>{
      this.data       = res.data;
      this.dataSource = res.data;
      this.isSearching = false
    })
  }
}
