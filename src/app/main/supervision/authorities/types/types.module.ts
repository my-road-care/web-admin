import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { typesRoutes } from './types.routing';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(typesRoutes)
    ],
    declarations: [
      ListingComponent,
      CreateComponent,
      ViewComponent
    ]
})
export class TypesModule{}