import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../permission.service';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { DialogContentDialog } from '../dialog/dialog.component';
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  constructor(
    private _service: PermissionService,
    private _dialog : MatDialog
  ) { }
  public header :any;
  public body   :any;
  public actions :any;
  public isSearching: boolean = false;
  ngOnInit(): void {
    this.listing();
  }

  listing():any{
    this.isSearching = true;
    this._service.listing().subscribe((res:any)=>{
      this.header = res.headers;
      this.body   = res.body;
      this.actions = res.actions;
      this.isSearching = false;
    })
  }


  update(partId:number = 0, roleId:number = 0, actionId:number = 0){
    
    this._service.update(partId, roleId, actionId).subscribe((res:any)=>{
      // console.log(res)
    })
    
  }
  openDialog(): void{
    const DiaLogMain = this._dialog.open(DialogContentDialog,{
      width: "800px"
    });
    
    DiaLogMain.componentInstance.DateInfo.subscribe((res:any)=>{
      this.header = res.headers;
      this.body   = res.body;
      this.actions = res.actions;
    });
  }


}