import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { permissionsRoutes } from './permissions.routing';
import { DialogContentDialog } from './dialog/dialog.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(permissionsRoutes)
    ],
    declarations: [
      ListingComponent,
      CreateComponent,
      ViewComponent,
      DialogContentDialog
    ]
})
export class PermissionsModule{}