import { Route } from "@angular/router";
import { CreateComponent } from "./create/create.component";
import { ListingComponent } from "./listing/listing.component";
import { ViewComponent } from "./view/view.component";

export const permissionsRoutes: Route[] = [{
    path: 'permissions',
    children: [
        { path  : '',  component: ListingComponent },
        { path  : 'create', component: CreateComponent },
        { path  : ':id', component: ViewComponent }
    ]
}];