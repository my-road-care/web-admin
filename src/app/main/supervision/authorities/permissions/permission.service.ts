import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class PermissionService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    params = {};

    constructor(private http: HttpClient) {}

    //==================================================================================>> Listing Project
    listing(params = {}): any {
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url + '/sup/authority/permission', httpOptions);
    }

    update(partId,roleId,actionId): any {
        const httpOptions = {};
        httpOptions['params'] = this.params;
        return this.http.post(this.url + `/sup/authority/permission?partId=${partId}&roleId=${roleId}&actionId=${actionId}`, httpOptions);
    }

    create(section,part,name,slug){
        const httpOptions = {};
        httpOptions['params'] = this.params;
        return this.http.post(this.url + `/sup/create?section=${section}&part=${part}&name=${name}&slug=${slug}`, httpOptions);
    }
   
    
}
