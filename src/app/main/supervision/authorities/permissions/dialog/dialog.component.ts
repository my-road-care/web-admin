import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { PermissionService } from "../permission.service";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef,MAT_DIALOG_DATA  } from "@angular/material/dialog";
import { SnackbarService } from 'app/shared/services/snackbar.service';
@Component({
    selector: 'dialog-content-example-dialog',
    styleUrls: ['./dialog.component.scss'],
    templateUrl: 'dialog.component.html',
  })
  export class DialogContentDialog{
    constructor(
        public _service  : PermissionService,
        public _matDialog: MatDialogRef<DialogContentDialog>,
        private _snackbar: SnackbarService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ){}
    DateInfo = new EventEmitter();
    public returnData:any;

    
    create(){
        if(this.formGroup.valid){
            this._service.create(this.formGroup.value.section,this.formGroup.value.part,this.formGroup.value.name,this.formGroup.value.slug).subscribe((res)=>{
                
                this.DateInfo.emit(res);
                this._snackbar.openSnackBar("បន្ថែមផ្នែកដោយជោគជ័យ",'');

                this._matDialog.close();
            });
        }
        else{
            alert("Required")
        }
        
    }
    formGroup = new FormGroup({
        section :           new FormControl('',[Validators.required]),
        part:           new FormControl('',[Validators.required]),
        name:           new FormControl('',[Validators.required]),
        slug:         new FormControl('',[Validators.required])
    });
  }