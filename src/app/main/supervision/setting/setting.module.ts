import { NgModule } from '@angular/core';
import { WorkModule } from './work/work.module';
import { TestingMethodModule } from './testing-method/testing-method.module';  
import { BudgetModule } from './budget-year/budget-year.module';
import { TesingStandardModule } from './testing-standard/tesing-standard.module';
@NgModule({
    imports: [
        WorkModule,
        TesingStandardModule,
        TestingMethodModule,
        BudgetModule
    ],
    exports: [
        WorkModule,
        TesingStandardModule,
        TestingMethodModule,
        BudgetModule
        
    ],
})
export class SettingModule{}