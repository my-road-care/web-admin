import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { DialogComponent } from '../create-dialog/dialog.component';
import { SettingService } from '../../setting.service';
import { WorkTypeService } from '../../work/work.service';
import { environment as env} from 'environments/environment';
import { UpdateDialogComponent } from '../update-dialog/dialog.component';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { LoadingService } from 'helpers/services/loading';
@Component({
  selector: 'app-listing',
  templateUrl: './listing-method.component.html',
  styleUrls: ['./listing-method.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['no','name','description','unit','type','place','image','action'];

  public data:  any = [];
  public TestsType:any = [];
  public total: number = 10;
  public limit: number = 10;
  public page:  number = 1;
  public dataSource : any;
  public searchValue: String;
  public type_id: number = 1;
  public view:string = "list";
  public fileUrl = env.fileUrl;
  public works:any = [];
  public isClosing :boolean = false;
  constructor(
    private _service : WorkTypeService,
    private _dialog  : MatDialog,
    private _settingService: SettingService,
    private _snackBar: SnackbarService, 
    private _loadingService: LoadingService,
  ) { }

  public isSearching: boolean = false;
  
  
  ngOnInit(): void {
    // this.filterWorksType();
    this.getTestsType();
    this.listing();
    
  }
  

  // filterWorksType(): void{
  //   this.isSearching = true;
  //   this._settingService.filterListTestingMethodType(this.type_id).subscribe((res):any=>{
  //     this.data = res;
  //     console.log(this.data);
  //     this.dataSource = res;
  //     this.isSearching = false;
  //   })
  // }

  getTestsType(){
    this._settingService.listTestingMethodType().subscribe((res):any =>{
      this.TestsType = res;
    })
  }

  onPageChanged(event:any){
    this.isSearching = true;
    if(event && event.pageSize){
      this.limit = event.pageSize;
      this.page  = event.pageIndex+1;
      this.listing();
    }
  }

  listing():any{
    this.isSearching = true;
    this._settingService.filterListTestingMethodType(this.type_id).subscribe((res:any)=>{
      this.data       = res;
      this.dataSource = new MatTableDataSource(this.data);
      this.total      = res.total;
      this.isSearching = false;
    })
  }

  openDialog(){
    const dia = this._dialog.open(DialogComponent,{
      width: "750px",
      data: {
        title: "បន្ថែមវិធីសាស្ត្រតេស្ត",
        TestsType: this.TestsType,
        works: this.works,
        type_id: this.type_id
      }
    });
    dia.componentInstance.DateInfo.subscribe((res:any)=>{
      if(this.type_id != 0){
        this.listing(); // i change function change 1
      }
      else{
        this.listing();
      }
      
    });
    
  }



  delete(id:any){ 
   const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._settingService.deleteTestingMethod(id).subscribe((res:any)=>{
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            this.listing();

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
          }
        );
      }
    });
  }

  update(work_id:any,description:any,parameters:any,unit:any,id:any,name:any,typeId:any,file:any){

    const dia = this._dialog.open(UpdateDialogComponent,{
      width: "850px",
      data: {
        title: "កែប្រែវិធីសាស្ត្រតេស្ត",
        typeId: typeId,
        name : name,
        id   : id,
        unit : unit,
        file  : file,
        testsType: this.TestsType,
        parameters: parameters,
        description: description,
        works: this.works,
        work_id: work_id
      }
    });
    dia.componentInstance.DateInfo.subscribe((res:any)=>{
      if(this.type_id != 0){
        this.listing(); // i change function change 2
      }
      // else{
      //   this.listing();
      // }
    })

    // dia.componentInstance.DateInfo.subscribe((res:any)=>{
    //   this.listing(this.limit,this.page);
      
    // });
  }

  searchLike(_limit: number = 10, _page: number = 1){
    if(this.searchValue == undefined){
      this.searchValue = ""; 
    }
    const params: any = {
      limit: _limit,
      page : _page,
      key  : this.searchValue
    };
    this.isSearching = true;
    this._service.listing(params).subscribe((res:any)=>{
      this.dataSource = res.data;
      this.data       = res.data;
      this.total      = res.total;
      this.isSearching = false;
    })
  }

  preview(name:any,image:any){
    this._dialog.open(PreviewImageComponent,{
      width: '850px',
      data: {
        title: name,
        src: image
      }
    })
  }

  viewAs(){
    if(this.view == "list"){
      this.view = "grid";
    }
    else{
      this.view = "list";
    }
  }

  // getWorks(){
  //   this._service.listing().subscribe((res:any)=>{
  //     this.works = res;
  //     console.log(this.works);
  //   })
  // }

}
