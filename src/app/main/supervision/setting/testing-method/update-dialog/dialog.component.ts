import { Component, EventEmitter, Inject, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators, NgForm } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { SettingService } from "../../setting.service";

import { MatDialog } from '@angular/material/dialog';
import { WorkTypeService } from "../../work/work.service";
@Component({
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})
export class UpdateDialogComponent implements OnInit{
    @ViewChild('updatedataNgForm') updatedataNgForm: NgForm;
    constructor(

        public dialogRef: MatDialogRef<UpdateDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: SettingService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
        private _workService: WorkTypeService

    ){
        dialogRef.disableClose = true;
    }
    public btnDisable: boolean = false;
    public route: string = "";
    public src: string = "";
    public title: string = this.data.title;
    public testsType:any = [];
    public show: boolean = false;
    public saving: boolean = false;
    public MethodId:any;

    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';
    public isloading = false;
    public listing:any = [];
    public nameTmp:string = '';
    public isSpinner = false;
    public unit = "";
    public isSearching = true;
    public works:any = [];
    public listWork:any = [];


    ngOnInit(): void {
        this.getWork();
        this.testsType = this.data?.testsType;
        this.src = this.data?.file;
        this.formGroup.get('image_uri').setValue( this.data?.file);
        this.formGroup.get('unit').setValue( this.data?.unit);
        this.formGroup.get('name').setValue( this.data?.name);
        this.formGroup.get('id').setValue( this.data?.id);
        this.formGroup.get('type_id').setValue( this.data?.typeId);
        this.formGroup.get('description').setValue( this.data?.description);
        // this.formGroup.get('work_id').setValue(this.data?.work_id);
        this.listing = this.data?.parameters;
        this.MethodId = this.data?.id;
        this.filterWork();
        this.listingWork();

    }

    formGroup = new FormGroup({
        id    : new FormControl(''),
        name  : new FormControl('',[Validators.required, Validators.maxLength(100)]),
        unit  : new FormControl('',[]),
        image_uri: new FormControl(''),
        type_id : new FormControl('',[Validators.required]),
        description: new FormControl(''),
    })

    form = new FormGroup({
        testing_method_id       : new FormControl(this.data?.id),
        work_id                 : new FormControl('',[Validators.required])
    });

    DateInfo = new EventEmitter();


    filterWork(){
      console.log(this.data?.work_id);
        var works = [];
        this.data?.work_id.forEach((res:any)=>{
           if(res != null){
            works.push(res?.work_id);
           }
        })
    }

    addwork(){

        this.isSearching = true;
        this.btnDisable = true;
        this._service.workTestingMethod(this.form.value).subscribe((res)=>{
            this.getWork();
            this.listingWork();
            this.DateInfo.emit(res);
        })
    }


    deleteWork(row:any){
      console.log(row);
        this.form.get('work_id').setValue(row?.id);
        this.isSearching = true;
        this.btnDisable = true;
        this._service.workTestingMethod(this.form.value).subscribe((res)=>{
            this.getWork();
            this.listingWork();
            this.DateInfo.emit(res);
        })
    }

    getWork(){
        this.isSearching = true;
        this._workService.listing(this.data?.id).subscribe((res:any)=>{
            this.works = res;
            this.isSearching = false;
            this.btnDisable = false;
        })
    }

    listingWork(){
        this.isSearching = true;
        let listWorks=[];
        this._workService.listWork(this.data?.id).subscribe((res:any)=>{
            if(res.length != 0){
              res.forEach(e=>{
                if(e != null){
                  listWorks.push(e);
                }
              })
              this.listWork=listWorks;
            }

            this.isSearching = false;
            this.btnDisable = false;
        })
    }

    filterWorksType(){

    }

    addParam(){

        this.isSpinner = true;

        let data = {
            method_id:this.MethodId,
            name: this.nameTmp,
            unit: this.unit
        };

        this._service.addParam(data).subscribe((res:any)=>{
            this.listing.push({id: res?.id, name: res?.name, unit:res?.unit})
            this.nameTmp = "";
            this.unit    = "";
            this.DateInfo.emit(res);
        })
        this.isSpinner = false;

    }



    create(){
        this.formGroup.disable();
        this.saving = true;
        this.isloading = true;

        if(this.data.title == "កែប្រែការងារ"){
            this.route = 'works';
            this.title = "កែប្រែការងារ";
        }

        if(this.formGroup.get('image_uri').value == this.data?.file){
            this.formGroup.get('image_uri').setValue(null);
        }
        // console.log(this.formGroup.value);


        this._service.updateTestingMethod(this.formGroup.value).subscribe((res:any)=>{
            this.isloading = false;
            this.saving = false;
            this.DateInfo.emit(res)
            this.formGroup.enable();
            this._snackBar.openSnackBar(this.title+'ដោយជោគជ័យ','');
            this.dialogRef.close();
        })

        if(this.data.title == "បន្ថែមជំពូកថវិការ"){
            this.route = "budgets";
            this.title = "បន្ថែមជំពូកថវិការ";

        } else if(this.data.title == "បន្ថែមប្រភេទលិខិត") {
            this.route = "letters";
            this.title = "បន្ថែមប្រភេទលិខិត";

        } else if(this.data.title == "បន្ថែមប្រភេទការងារ"){
            this.route = "worktypes";
            this.title = "បន្ថែមប្រភេទការងារ";
        }
        else{
            this.route = "testingtypes";
            this.title = "បន្ថែមប្រភេទតេស្ត";
        }

        this._service.creating(this.route,this.formGroup.value.name).subscribe((res:any)=>{
            this.DateInfo.emit({id:res.id,name:res.name});
            this._snackBar.openSnackBar(this.title+'ដោយជោគជ័យ','');
            this.dialogRef.close();
        });

    }


    removeParamData(index:any,id:any){
      console.log(id,index);
        this._service.deleteParam({id: id}).subscribe((res:any)=>{
            this.listing.splice(index,1);
            this.DateInfo.emit(res);
        })

    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }

    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.formGroup.get('image_uri').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.formGroup.get('image_uri').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.formGroup.get('image_uri').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.formGroup.get('image_uri').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }

    preview(){
        if(this.src == ""){
            document.getElementById('portrait-file').click();
        }
        else{
            this._dialog.open(PreviewImageComponent,{
                width: '850px',
                data: {
                  title: 'រូបភាពវិធីសាស្រ្តតេស្ត',
                  src: this.src
                }
              })
        }
    }
}
