import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { testingRoutes } from './testing-method.routing';
import { DialogComponent } from './create-dialog/dialog.component';
import { UpdateDialogComponent } from './update-dialog/dialog.component';
import { ListingComponent } from './listing/listing-method.component';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(testingRoutes)
    ],
    declarations: [
      DialogComponent,
      UpdateDialogComponent,
      ListingComponent,
    ]
})
export class TestingMethodModule{}