import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing-method.component";

export const testingRoutes: Route[] = [
    {
        path: 'testing-method',
        children: [
            { path  : '',  component: ListingComponent },
        ]
    }
];