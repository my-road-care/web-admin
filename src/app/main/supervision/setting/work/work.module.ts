import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { worksRoutes } from './work.routing';
import { DialogComponent } from './create-dialog/dialog.component';
import { DialongTestingMethodComponent } from './create-testingmethod/create.component';
import { UpdateDialogComponent } from './update-dialog/dialog.component';
@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(worksRoutes)
    ],
    declarations: [
      ListingComponent,
      DialogComponent,
      UpdateDialogComponent,
      DialongTestingMethodComponent
    ]
})
export class WorkModule{}