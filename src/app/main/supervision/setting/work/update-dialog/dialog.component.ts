import { Component, EventEmitter, Inject, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators, NgForm } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { SettingService } from "../../setting.service";

import { MatDialog } from '@angular/material/dialog';
import { DialongTestingMethodComponent } from '../create-testingmethod/create.component';
@Component({
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})
export class UpdateDialogComponent implements OnInit{
    @ViewChild('updateworkNgForm') updateworkNgForm: NgForm;
    constructor(
        public dialogRef: MatDialogRef<UpdateDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: SettingService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
        
    ){
        dialogRef.disableClose = true;
    }

    ngOnInit(): void {

        this.worksType = this.data?.worksType;
        this.src = this.data?.file;
        this.formGroup.get('image_uri').setValue( this.data?.file);
        this.formGroup.get('type_id').setValue( this.data?.typeId);
        this.formGroup.get('name').setValue( this.data?.name);
        this.formGroup.get('id').setValue( this.data?.id);
        this.worksGroup = this.data?.worksGroup;
        this.formGroup.get('group_id').setValue(this.data?.group_id);

        this.getTestingMethod();
        this.getAllTestingMethod();
    }

    public btnDisable: boolean = true;
    public route: string = "";
    public src: string = "";
    public title: string = this.data.title;
    public worksType:any = [];
    public show: boolean = false;
    public saving: boolean = false;
    public worksGroup = [];

    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';
    public isloading = false;
    public testing_method = [];
    public testing_methodID:any;


    public testingMethodData = [];
    public isSearching:boolean = false;
    

    formGroup = new FormGroup({
        id       : new FormControl(''),
        name     : new FormControl('',[Validators.required, Validators.maxLength(100)]),
        type_id  : new FormControl('',[Validators.required]),
        image_uri: new FormControl('',[Validators.required]),
        group_id : new FormControl('',[Validators.required])
    });


    form = new FormGroup({
        testing_method_id       : new FormControl('',[Validators.required]),
        work_id                 : new FormControl(this.data?.work_id)
    });
    
    DateInfo = new EventEmitter();
    

    getAllTestingMethod(){
        this._service.listingTestingMethod(this.data?.work_id).subscribe((res:any)=>{
            this.testingMethodData = res;
        })
    }

    addTestingMethod(){
        this.isSearching = true;
        this._service.workTestingMethod(this.form.value).subscribe((res:any)=>{
            this.getTestingMethod();
            this.getAllTestingMethod();
            this.DateInfo.emit(res);
        })


    }


    filterWorksType(){
        
    }

    getTestingMethod(){
        this._service.getTestingMethod(this.data?.id).subscribe((res:any)=>{
            this.testing_method = res;
            this.isSearching = false;
        })
    }
    
    create(){
        this.formGroup.disable();
        this.saving = true;
        this.isloading = true;
        // console.log(this.formGroup.value);
        if(this.data.title == "កែប្រែការងារ"){
            this.route = 'works';
            this.title = "កែប្រែការងារ";
        }
        if(this.formGroup.get('image_uri').value == this.data?.file){
            this.formGroup.get('image_uri').setValue(null);
        }

        this._service.update(this.route, this.formGroup.value).subscribe((res):any=>{
            this.isloading = false;
            this.saving = false;
            this.DateInfo.emit(res)
            this.formGroup.enable();
            this._snackBar.openSnackBar(this.title+'ដោយជោគជ័យ','');
            this.dialogRef.close();
        })

        // if(this.data.title == "បន្ថែមជំពូកថវិការ"){
        //     this.route = "budgets";
        //     this.title = "បន្ថែមជំពូកថវិការ";

        // } else if(this.data.title == "បន្ថែមប្រភេទលិខិត") {
        //     this.route = "letters";
        //     this.title = "បន្ថែមប្រភេទលិខិត";

        // } else if(this.data.title == "បន្ថែមប្រភេទការងារ"){
        //     this.route = "worktypes";
        //     this.title = "បន្ថែមប្រភេទការងារ";
        // }
        // else{
        //     this.route = "testingtypes";
        //     this.title = "បន្ថែមប្រភេទតេស្ត";
        // }

        // this._service.creating(this.route,this.formGroup.value.name).subscribe((res:any)=>{
        //     this.DateInfo.emit({id:res.id,name:res.name});
        //     this._snackBar.openSnackBar(this.title+'ដោយជោគជ័យ','');
        //     this.dialogRef.close();
        // });
        
    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }
    
    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.formGroup.get('image_uri').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.formGroup.get('image_uri').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.formGroup.get('image_uri').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.formGroup.get('image_uri').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }
    
    preview(){
        if(this.src == ""){
            document.getElementById('portrait-file').click();
        }
        else{
            this._dialog.open(PreviewImageComponent,{
                width: '850px',
                data: {
                  title: 'រូបភាពប្រភេទការងារ',
                  src: this.src
                }
              })
        }
    }

    deleteTestingMethod(row:any){
        // this._service.delete("testing-methods",row?.id).subscribe((res:any)=>{
        //     this.getTestingMethod();
        // })
        this.form.get('testing_method_id').setValue(row?.id);
        this.isSearching = true;
        this._service.workTestingMethod(this.form.value).subscribe((res:any)=>{
            this.getTestingMethod();
            this.getAllTestingMethod();
            this.DateInfo.emit(res);
        })
    }

    createTestingMethod(){
        const dia = this._dialog.open(DialongTestingMethodComponent,{
            width: "650px",
            data:{
                work_id: this.data?.work_id
            }
        })
        dia.componentInstance.DateInfo.subscribe((res:any)=>{
            this.getTestingMethod();
        })

    }
}