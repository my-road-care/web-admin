import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class WorkTypeService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}

    listing(testing_method_id = 0,params= {}):any{
        const httpOptions = {};
        httpOptions['params'] = params;
        if(testing_method_id == 0){
            return this.http.get(this.url+`/sup/setup/works`);
        }
        else{
            return this.http.get(this.url+`/sup/setup/works?testing_method_id=${testing_method_id}`);
        }
    }

    getWorkType():any{
        return this.http.get(this.url+`/sup/setup/workstype`);
    }

    getGroup():any{
        return this.http.get(this.url+`/sup/setup/worksgroup`);
    }

    filterWorkType(id = 0):any{
        if(id == 0){
            return this.http.get(this.url+`/sup/setup/works`)
        }
        else{
            return this.http.get(this.url+`/sup/setup/works?type=${id}`)
        }
    }

    listWork(id:any){
        return this.http.get(this.url+`/sup/setup/testing-methods/show?testing_method_id=${id}`);
    }

    

}
