import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PreviewImageComponent } from 'app/shared/preview-image/preview-image.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { DialogComponent } from '../create-dialog/dialog.component';
import { SettingService } from '../../setting.service';
import { WorkTypeService } from '../work.service';
import { environment as env} from 'environments/environment';
import { UpdateDialogComponent } from '../update-dialog/dialog.component';
import { CdkDragDrop, DragDropModule, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { LoadingService } from 'helpers/services/loading';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  displayedColumns: string[] = ['no','work_group','name','testing_method','creator','date','image','action'];

  public data:  any     = [];
  public worksType:any  = [];
  public worksGroup:any = []
  public total: number  = 10;
  public limit: number  = 10;
  public page:  number  = 1;
  public dataSource : any;
  public searchValue: String;
  public type_id: number = 1;
  public view:string = "list";
  public fileUrl = env.fileUrl;
  public isClosing :boolean =false;
  constructor(
    private _router  : Router,
    private _service : WorkTypeService,
    private _dialog  : MatDialog,
    private _settingService: SettingService,
    private _snackBar: SnackbarService, 
    private _loadingService: LoadingService,
  ) { }
  

  public isSearching: boolean = false;
  
  
  ngOnInit(): void {
    this.getWorksType();
    this.getGroup();
    this.filterWorksType();
  }

  filterWorksType(): void{
    this.isSearching = true;
    this._service.filterWorkType(this.type_id).subscribe((res):any=>{
      this.data = res;
      console.log(this.data);
      
      this.dataSource = res;
      this.isSearching = false;
    })
  }

  getWorksType(){
    this._service.getWorkType().subscribe((res):any =>{
      this.worksType = res;
      this.type_id = res[0]?.id;
    })
  }

  getGroup(){
    this._service.getGroup().subscribe((res:any)=>{
      this.worksGroup = res;
      
    })
  }

  onPageChanged(event:any){
    this.isSearching = true;
    if(event && event.pageSize){
      this.limit = event.pageSize;
      this.page  = event.pageIndex+1;
      this.listing();
    }
  }

  listing():any{
    this.isSearching = true;
    this._service.listing().subscribe((res:any)=>{
      this.data       = res;
      this.dataSource = new MatTableDataSource(this.data);
      this.total      = res.total;
      this.isSearching = false;
    })
  }

  openDialog(){
    const dia = this._dialog.open(DialogComponent,{
      width: "650px",
      data: {
        type_id  : this.type_id,
        title: "បន្ថែមសំណង់ផ្លូវថ្នល់",
        worksType: this.worksType,
        worksGroup: this.worksGroup
      }
    });
    dia.componentInstance.DateInfo.subscribe((res:any)=>{
      this.filterWorksType();
      
    });
    
  }

  delete(id:any){

    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.isClosing = true;
        this._loadingService.show();
        this._settingService.delete('works',id).subscribe((res:any)=>{
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message, '');
            this.filterWorksType();

          }, (err: any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.', 'error');
          }
        );
      }
    });
  }

  update(testing_method:any,group_id,id:any,name:any,typeId:any,file:any){
    const dia = this._dialog.open(UpdateDialogComponent,{
      width: "750px",
      data: {
        title: "កែប្រែការងារ",
        name : name,
        id   : id,
        typeId : typeId,
        file : file,
        worksType:  this.worksType,
        worksGroup: this.worksGroup,
        group_id: group_id,
        testing_method: testing_method,
        work_id: id
      }
    });
    dia.componentInstance.DateInfo.subscribe((res:any)=>{
      this.filterWorksType();
    })

    // dia.componentInstance.DateInfo.subscribe((res:any)=>{
    //   this.listing(this.limit,this.page);
      
    // });
  }

  searchLike(_limit: number = 10, _page: number = 1){
    if(this.searchValue == undefined){
      this.searchValue = ""; 
    }
    const params: any = {
      limit: _limit,
      page : _page,
      key  : this.searchValue
    };
    this.isSearching = true;
    this._service.listing(params).subscribe((res:any)=>{
      this.dataSource = res.data;
      this.data       = res.data;
      console.log(this.data);
      
      this.total      = res.total;
      this.isSearching = false;
    })
  }

  preview(name:any,image:any){
    this._dialog.open(PreviewImageComponent,{
      width: '850px',
      data: {
        title: name,
        src: image
      }
    })
  }

  viewAs(){
    if(this.view == "list"){
      this.view = "grid";
    }
    else{
      this.view = "list";
    }
  }
  

}
