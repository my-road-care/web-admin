import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const worksRoutes: Route[] = [{
    path: 'works',
    children: [
        { path  : '',  component: ListingComponent },
    ]
}];