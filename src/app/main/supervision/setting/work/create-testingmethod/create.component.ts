import { Component, EventEmitter, Inject, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PreviewImageComponent } from "app/shared/preview-image/preview-image.component";
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { SettingService } from "../../setting.service";
import { WorkTypeService } from "../work.service";


import { MatDialog } from '@angular/material/dialog';
@Component({
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class DialongTestingMethodComponent implements OnInit{
    constructor(
        public dialogRef: MatDialogRef<DialongTestingMethodComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: SettingService,
        private _dialog: MatDialog,
        private _snackBar: SnackbarService,
        private _workService: WorkTypeService
    ){}

    public works = [];
    ngOnInit(): void {
        this.getWorks();
        this. works.push(this.data?.work_id);
    }

    public btnDisable: boolean = true;
    public route: string = "";
    public src: string = "";
    public title: string = this.data.title;
    public TestsType:any = [];
    public show: boolean = false;
    public saving: boolean = false;

    public size: number = 0;
    public type: string = '';
    public error_size: string = '';
    public error_type: string = '';
    public isloading = false;

    formGroup = new FormGroup({
        name        : new FormControl('',[Validators.required, Validators.maxLength(100)]),
        unit        : new FormControl('',[Validators.required, Validators.maxLength(100)]),
        description : new FormControl('',[Validators.maxLength(100)]),
        type_id     : new FormControl('',[Validators.required, Validators.maxLength(100)]),
        image_uri   : new FormControl('',[Validators.required]),
        work_id     : new FormControl(this.works,[Validators.required]),
    })
    
    DateInfo = new EventEmitter();
    
    filterWorksType(){

    }
    
    create(){
        
        this.saving = true;
        this.isloading = true;
        // console.log(this.formGroup.value);
        this.route = 'testing-methods';
        this._service.creating(this.route, this.formGroup.value).subscribe((res):any=>{
            this.isloading = false;
            this.saving = false;
            this.DateInfo.emit(res)
            this.formGroup.enable();
            this._snackBar.openSnackBar('បង្កើតដោយជោគជ័យ','');
            this.dialogRef.close();
        })
        
    }

    selectFile(): any {
        document.getElementById('portrait-file').click();
    }
    
    fileChangeEvent(e: any): void {
        if (e?.target?.files) {
            this.error_size = '';
            this.error_type = '';
            this.show = false;
            this.saving = false;
            this.type = e?.target?.files[0]?.type;
            this.size = e?.target?.files[0]?.size;
            var reader = new FileReader();
            reader.readAsDataURL(e?.target?.files[0]);
            reader.onload = (event: any) => {
                this.src = event?.target?.result;
                this.formGroup.get('image_uri').setValue(this.src);
            }
            if (this.size > 3145728) { //3mb
                this.saving = true;
                this.formGroup.get('image_uri').setValue('');
                this.error_size = 'រូបភាពត្រូវមានទំហំតូចជាងឬស្មើ3Mb';
            }
            if (this.type.substring(0, 5) !== 'image') {
                this.saving = true;
                this.src = '';
                this.show = true;
                this.error_size = '';
                this.formGroup.get('image_uri').setValue(this.src);
                this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            }
        } else {
            this.saving = true;
            this.src = '';
            this.show = true;
            this.error_size = '';
            this.formGroup.get('image_uri').setValue(this.src);
            this.error_type = 'សូមធ្វើការបញ្ចូលជាប្រភេទរូបភាព';
            //it work when user cancel select file
        }
    }
    
    preview(){
        if(this.src == ""){
            document.getElementById('portrait-file').click();
        }
        else{
            this._dialog.open(PreviewImageComponent,{
                width: '850px',
                data: {
                  title: 'រូបភាពប្រភេទការងារ',
                  src: this.src
                }
              })
        }
    }

    getWorks(){
        this._service.listTestingMethodType().subscribe((res:any)=>{
            this.TestsType = res;
        })
    }
}