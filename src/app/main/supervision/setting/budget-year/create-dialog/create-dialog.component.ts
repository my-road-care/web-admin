import { Component, EventEmitter, Inject, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { BudgetYearService } from '../budget-year.service';
import * as _moment from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { GlobalConstants } from "app/shared/global-constants";
const moment = _moment;
const MY_DATE_FORMAT = {
  parse: {
    dateInput: 'YYYY', // this is how your date will be parsed from Input
  },
  display: {
    dateInput: 'YYYY', // this is how your date will get displayed on the Input
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  }
};

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.scss'],
  providers:[
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
  ]
})
export class CreateDialogComponent {
  @ViewChild('picker', { static: false })
  private picker!: MatDatepicker<Date>;  

  chosenYearHandler(ev, input){
    let { _d } = ev;
    this.selectYear = _d;
    this.newForm.get('year').setValue(_d);
    this.picker.close()
  }
  @ViewChild('CreateBudgetNgForm') CreateBudgetNgForm: NgForm;
  CreateBudget    = new EventEmitter();
  public newForm :UntypedFormGroup;
  public saving:boolean = false;
  public isLoading : boolean = false;
  public isloading : boolean = false;
  maxDate = new Date();
  selectYear

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(MAT_DIALOG_DATA) public id: any,
    private dialogRef: MatDialogRef<CreateDialogComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _service: BudgetYearService,
    private _snackBar: SnackbarService,
  ) {}
  ngOnInit(): void {
    this.data = this.data;
    this.formBuilder()
  }

  formBuilder(): void {
    this.newForm = this._formBuilder.group({
      year : [this.data, Validators.required],
      
    });
  }
  submit(){
    this.newForm.get('year').setValue(moment(this.newForm.get('year').value).format('YYYY'));
    if(this.newForm.value){
        this.newForm.disable();
        this.saving = true;
        this._service.create(this.newForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.CreateBudget.emit(res);
            this._snackBar.openSnackBar('បង្កើតបានជោគជ័យ!', '');
        },err =>{
          this.isloading = false;
          this.saving = false;
          this.newForm.enable();
          let errors: any[] = [];
          errors = err.error.errors;
          let messages: any[] = [];
          let text: string = '';
          if (errors.length > 0) {
              errors.forEach((v: any) => {
                  messages.push(v.year
                    )
              });
              if (messages.length > 1) {
                  text = messages.join('-');
              } else {
                  text = messages[0];
              }
          } else {
              text = err.error.errors.year;
          }
          this._snackBar.openSnackBar(text, GlobalConstants.error);
        });
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
  }
}
