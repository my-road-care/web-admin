import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class BudgetYearService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}

    listing(params= {}):any{
        const httpOptions = {};
        httpOptions['params'] = params;
        return this.http.get(this.url+`/sup/setup/budget-year`);
    }
    create(data: any = {}): any {
        return this.http.post(this.url + '/sup/setup/budget-year', data, this.httpOptions);
    }
    delete(budget_id: number = 0): Observable<any> {
        return this.http.delete(this.url+ '/sup/setup/budget-year/' + budget_id , this.httpOptions);
    }
    update (id:number = 0, data:any = {}): any {
        return this.http.post(this.url + '/sup/setup/budget-year/'+ id , data, this.httpOptions);
    }

    getprojectname(): any {
        const httpOptions = {};
        return this.http.get(this.url + '/sup/setup/budget-year/', httpOptions);
    }
    
}
