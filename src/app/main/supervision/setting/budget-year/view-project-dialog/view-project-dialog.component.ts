import { Component,  Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-view-project-dialog',
  templateUrl: './view-project-dialog.component.html',
  styleUrls: ['./view-project-dialog.component.scss']
})
export class ViewProjectDialogComponent {

  displayedColumns: string[] = ['id','code','project','budget_year','status','action'];
  public dataSource: any;
  public isSearching:boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ViewProjectDialogComponent>,
  ) 
  { 
    dialogRef.disableClose = false;
  }


  ngOnInit(): void {
    this.getproject();
  }
  getproject(){
    this.data = this.data; 
    this.dataSource = new MatTableDataSource (this.data.data.project);
  }
  download(){
    alert ("រង់ចាំធ្វើAPIថ្ងៃស្អែក");
  }
}
