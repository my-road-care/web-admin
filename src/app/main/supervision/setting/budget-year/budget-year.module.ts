import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { budgetRoutes } from './budget-year.routing';
import { CreateDialogComponent } from './create-dialog/create-dialog.component';
import { UpdateDialogComponent } from './update-dialog/update-dialog.component';
import { ViewProjectDialogComponent } from './view-project-dialog/view-project-dialog.component';
@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild(budgetRoutes)
    ],
    declarations: [
      ListingComponent,
      CreateDialogComponent,
      UpdateDialogComponent,
      ViewProjectDialogComponent,

    ]
})
export class BudgetModule{}