import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { BudgetYearService } from '../budget-year.service';
import { LoadingService } from 'helpers/services/loading';
import { CreateDialogComponent } from '../create-dialog/create-dialog.component';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { UpdateDialogComponent } from '../update-dialog/update-dialog.component';
import { ViewProjectDialogComponent } from '../view-project-dialog/view-project-dialog.component';
import { ApexOptions } from 'ng-apexcharts';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  displayedColumns: string[] = ['id','year','created_at','project_count','action'];
  
  public isSearching: boolean = false;
  public isClosing  : boolean = false;
  public selectedAcademic: number;
  public data : any[]   = [];
  public halsServicce: any;
  public dataSource: MatTableDataSource<unknown>;
  public key    : string = '';
  maxDate = new Date();
  public from: any;
  public to: any;
  public listView:boolean = false;
  public gridView:boolean = false;
  public dataChart:any[]=[]; 
  public chartOptions:ApexOptions;

  constructor(
    private _service: BudgetYearService,
    private _dialog: MatDialog,
    private _snackBar: SnackbarService, 
    private _loadingService: LoadingService,
    private _router: Router,

  ){
    this.chartOptions = {
      series: [
        {
          name: "Website Blog",
          type: "column",
          data: [440, 505, 414, 671, 227, 413, 201, 352, 752, 320, 257, 160]
        },
        {
          name: "Social Media",
          type: "line",
          data: [23, 42, 35, 27, 43, 22, 17, 31, 22, 22, 12, 16]
        }
      ],
      chart: {
        height: 350,
        type: "line"
      },
      stroke: {
        width: [0, 4]
      },
      title: {
        text: "Traffic Sources"
      },
      dataLabels: {
        enabled: true,
        enabledOnSeries: [1]
      },
      labels: [
        "01 Jan 2001",
        "02 Jan 2001",
        "03 Jan 2001",
        "04 Jan 2001",
        "05 Jan 2001",
        "06 Jan 2001",
        "07 Jan 2001",
        "08 Jan 2001",
        "09 Jan 2001",
        "10 Jan 2001",
        "11 Jan 2001",
        "12 Jan 2001"
      ],
      xaxis: {
        type: "datetime"
      },
      yaxis: [
        {
          title: {
            text: "Website Blog"
          }
        },
        {
          opposite: true,
          title: {
            text: "Social Media"
          }
        }
      ]
    };
  }

  ngOnInit(): void {
    this.isSearching = true;
    this.gridView=false;
    this.listView=true;
    this.listing();
  }

  // =======================================================>> Function Listing
  listing():any {
    
    this.isSearching = true;
    this._service.listing().subscribe(
      (res:any) =>{
      this.isSearching = false;
      this.data = res;
      this.dataSource = new MatTableDataSource (this.data);
      },
      (err :any) => {
        // console.log(err);
        this.isSearching = false;
        this._snackBar.openSnackBar('Something went wrong.','error');
      }
    )
  }
  CreateBudget() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = this.data;
    dialogConfig.autoFocus = false;
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(CreateDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.CreateBudget.subscribe((response: any) => {
      this.listing();
    });
  }
  Ondelete(data:any = null):void{
    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
    });
    dialogRef.afterClosed().subscribe((res:any)=>{
      if(res){
        this.isClosing = true;
        this._loadingService.show();
        this._service.delete(data.id).subscribe(
          (res : any) => {
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar(res.message,'');
            let copy: any[] = [];
            this.data.forEach((obj: any) => {
                if (obj.id !== data.id) {
                    copy.push(obj);
                }
            });
            this.data = copy;
            this.dataSource = new MatTableDataSource(this.data);
          },(err: any)=>{
            this.isClosing = false;
            this._loadingService.hide();
            this._snackBar.openSnackBar('Something went wrong.','error');
            // console.log(err);
          }
        );
      }
    });
  }
  UpdateBudget(row:any) : void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      data    : row,
    }
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(UpdateDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
    dialogRef.componentInstance.UpdateBudget.subscribe((response: any) => {
      this.listing();
      
    })
  }
  ViewProject(row: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      data: row,
    }
    dialogConfig.width = "850px";
    const dialogRef = this._dialog.open(ViewProjectDialogComponent, dialogConfig);
    this._router.events.subscribe(() => {
      dialogRef.close();
    })
  }
  getTotal(key:string) {
    return this.data.map(data =>data[key]).reduce((total, value) => {
      return total + value
    }, 0);
  }
  changeView(value){
    if(value =="grid"){
     this.listView=true;
     this.gridView=false;
    }else{
     this.listView=false;
     this.gridView=true;
    //  this.setDataChart(this.dataChart);
    }
  }
  Downloadgraph(){
    alert 
    ('hello');
  }
  
}
