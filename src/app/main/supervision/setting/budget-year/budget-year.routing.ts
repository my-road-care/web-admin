import { Route } from "@angular/router";
import { ListingComponent } from "./listing/listing.component";

export const budgetRoutes: Route[] = [{
    path: 'budget-year',
    children: [
        { path  : '',  component: ListingComponent },
    ]
}];