import { Route } from "@angular/router";
import {ListingComponent} from "../testing-standard/listing/listing.component";
import { OverviewComponent } from "./listing/overview/overview.component";
export const testingStandardRoute: Route[] = [
    {
        path: 'testing-standard',
        children: [
            { path  : '',  component: ListingComponent },
            { path  :':id/:name', component:OverviewComponent}
        ]
    }
];