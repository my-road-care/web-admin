import { Component, OnInit } from '@angular/core';
import { SettingService } from '../../setting.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  public data:any[]=[];
  public dataSource:any;
  public displayedColumns: string[] = ['no', 'name', 'num_nr', 'action'];
  isSearching:boolean=false;
  constructor(
    private _service:SettingService
  ){

  }
  ngOnInit(): void {
   this.listing();
   
  }
  listing(){
    this.isSearching=true;
    this._service.getgroupTest().subscribe((res:any)=>{
      this.isSearching=false;

      this.data=res;
      this.dataSource=new MatTableDataSource(this.data);
    })
    
    
  }
}

