import { Component, OnInit } from '@angular/core';
import { SettingService } from '../../../setting.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UpdateStandardComponent } from './update-standard/update-standard.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit{
  roadName:string='';
  isSearching:boolean=false;
  roadNum_id:number | string;
  data:any[]=[];
  colData:any[]=[];
  dataWithLongmethodId:number;
  methods:any[]=[];
  checkMethod:boolean=false;
  layers:any[] = [];
  // test 
  
  constructor(
    private _service:SettingService,
    private _route:ActivatedRoute,
    private _dialog:MatDialog,
    private _snackBar: SnackbarService,
  ){
    this._route.paramMap.subscribe((params: any) => {
      this.roadName = params.get('name');
      this.roadNum_id=params.get('id');
  });
  }
  ngOnInit(): void {
    this.listing();
  }
  listing(){
    this.isSearching=true;
    this._service.getTestingStandard(this.roadNum_id).subscribe((res:any)=>{
      this.data=res.data;
      this.methods=res.method;
      this.isSearching=false;
      this.layers = res.layers; 
      this.methods = res.methods; 
    })
   
  }
  dialogUpdate(data:any,id:any){
    let copyData=[];
  let copyStandards=[];
    const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
            data: data,
            road:this.roadName,
            group_id:this.roadNum_id,
            action:"update"
        };
        dialogConfig.width = "700px";
        dialogConfig.autoFocus = false;
        const dialogRef = this._dialog.open(UpdateStandardComponent, dialogConfig);
        dialogRef.componentInstance.updateStandard.subscribe((res: any) => {
          this.methods.forEach((m)=>{
            copyStandards=[];
            if(m.id==id){
              m.standards.forEach(s=>{ 
                if(s.layer_id == res.res.data.layer_id){
                  if(res.action == 'delete'){
                    s.layer_id=res.res.data.layer_id;
                    s.method_id=null;
                    s.value='';
                    s.des='';
                  }else{
                    s.layer_id=res.res.data.layer_id;
                    s.method_id=res.res.data.method_id;
                    s.value=res.res.data.value;
                    s.des=res.res.data.des;
                  }
                  copyStandards.push(s); 
                }else{
                  copyStandards.push(s);
                }
              })
              m.standards=copyStandards;
              copyData.push(m);
            }else{
              copyData.push(m);
            }
          })
          console.log(this.methods);
        }); 
        
  }
  create(d:any,id:number | string){
 
  let copyData=[];
  let copyStandards=[];
  const dialogConfig = new MatDialogConfig();
  dialogConfig.data = {
      data: d,
      road:this.roadName,
      group_id:this.roadNum_id,
      method_id:id,
      value:0,
      action:"create"
  };
  dialogConfig.width = "700px";
  dialogConfig.autoFocus = false;
  const dialogRef = this._dialog.open(UpdateStandardComponent, dialogConfig);
  dialogRef.componentInstance.updateStandard.subscribe((res: any) => {
   
    this.methods.forEach((m)=>{
      copyStandards=[];
      if(m.id==id){
        m.standards.forEach(s=>{
          if(s.layer_id == res.res.data.layer_id){
            s.layer_id=res.res.data.layer_id;
            s.method_id=res.res.data.method_id;
            s.value=res.res.data.value;
            s.des=res.res.data.des;
            copyStandards.push(s); 
          }else{
            copyStandards.push(s);
          }
        })
        m.standards=copyStandards;
        copyData.push(m);
      }else{
        copyData.push(m);
      }
    })
    // this.methods=copyData;
  }); 
   
  }

}
