import { Component, EventEmitter, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { SettingService } from 'app/main/supervision/setting/setting.service';
import { ConfirmDialogComponent } from 'app/shared/confirm-dialog/confirm-dialog.component';
import { SnackbarService } from 'app/shared/services/snackbar.service';

@Component({
  selector: 'app-update-standard',
  templateUrl: './update-standard.component.html',
  styleUrls: ['./update-standard.component.scss']
})
export class UpdateStandardComponent implements OnInit{
  @ViewChild('CreatePlanNgForm') CreateStandardNgForm: NgForm;
  public isLoading : boolean = false;
  public saving:boolean=false;
  public standardForm: UntypedFormGroup;
  constructor(
    public dialogRef: MatDialogRef<UpdateStandardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _service: SettingService,
    private _dialog: MatDialog,
    private _formBuilder: UntypedFormBuilder,
    private _snackBar: SnackbarService,
  ){}
  updateStandard=new EventEmitter();
  ngOnInit(): void {
     this.formBuilder();
  }

  formBuilder(): void {
    if(this.data.action == 'create'){
        this.standardForm = this._formBuilder.group({
            layer_id:[this.data.data?.layer_id? this.data.data?.layer_id:'',[Validators.required]],
            group_id:[this.data.group_id ? this.data.group_id:'',[Validators.required]],
            method_id:[this.data.method_id? this.data.method_id:'',[Validators.required]],
            value:[this.data.data?.value? this.data.data?.value:0, [Validators.required]],
        });
    }else{
        this.standardForm = this._formBuilder.group({
            layer_id:[this.data.data?.layer_id? this.data.data?.layer_id:'',[Validators.required]],
            group_id:[this.data.group_id ? this.data.group_id:'',[Validators.required]],
            method_id:[this.data.data?.method_id? this.data.data?.method_id:'',[Validators.required]],
            value:[this.data.data?.value? this.data.data?.value:0, [Validators.required]],
        });
    }
}

submit(){
    // console.log(this.standardForm.value);
    if(this.standardForm.value){
        this.standardForm.disable();
        this.saving = true;
      if(this.data.action == 'create'){
        this._service.CreateStandard(this.standardForm.value).subscribe((res:any) =>{
            this.isLoading = false; 
            this.updateStandard.emit({"res":res,"action":'create'});
            setTimeout(()=>{
              this.dialogRef.close();
            },700)
            this._snackBar.openSnackBar(res.message, '');

        },err =>{
            this.standardForm.enable();
            this.saving = false;
           
            for(let key in err.error.errors){
            let control = this.standardForm.get(key);
            control.setErrors({'servererror':true});
            control.errors.servererror = err.error.errors[key][0];
            }
        });
      }else{
        this._service.UpdateStandard(this.standardForm.value).subscribe((res:any) =>{
            this.isLoading = false;
            this.dialogRef.close();
            this.updateStandard.emit({"res":res,"action":'update'});
            setTimeout(()=>{
              this.dialogRef.close();
            },700)
            this._snackBar.openSnackBar(res.message, '');
        },err =>{
            this.standardForm.enable();
            this.saving = false;
            for(let key in err.error.errors){
            let control = this.standardForm.get(key);
            control.setErrors({'servererror':true});
            control.errors.servererror = err.error.errors[key][0];
            }
        });
      }
    }
    else{
        this._snackBar.openSnackBar('Please check your input.', 'error');
    }
}
delete(){
  const dialogRef = this._dialog.open(ConfirmDialogComponent, {
    data: { message: 'តើអ្នកពិតជាចង់ធ្វើប្រតិបត្តការណ៏នេះមែនឫទេ?' }
});
dialogRef.afterClosed().subscribe((result) => {
    if (result) {
      this._service.DeleteStandard(this.standardForm.value).subscribe((res:any) =>{
        this.isLoading = false;
        this.updateStandard.emit({"res":res,"action":'delete'});
        setTimeout(()=>{
          this.dialogRef.close();
        },700)
        this._snackBar.openSnackBar(res.message, '');
    },err =>{
        this.standardForm.enable();
        this.saving = false;  
        for(let key in err.error.errors){
        let control = this.standardForm.get(key);
        control.setErrors({'servererror':true});
        control.errors.servererror = err.error.errors[key][0];
        }
    });
    }
});
  
}

}
