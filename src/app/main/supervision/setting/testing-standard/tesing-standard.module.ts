import { NgModule } from '@angular/core';
import { ListingComponent } from './listing/listing.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {testingStandardRoute} from '../testing-standard/testing-standard.routing';
import { OverviewComponent } from './listing/overview/overview.component';
import { UpdateStandardComponent } from './listing/overview/update-standard/update-standard.component';
@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(testingStandardRoute)
    ],
    declarations: [
      ListingComponent,
      OverviewComponent,
      UpdateStandardComponent
    ],
})
export class TesingStandardModule{}