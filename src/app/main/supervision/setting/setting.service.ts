import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';
import { data } from "jquery";
@Injectable({
    providedIn: 'root',
})
export class SettingService {
    
    constructor(private _service: HttpClient){}
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    testingMethod(data:any){
        return this._service.post(this.url+`/sup/setup/testing-method`,data,this.httpOptions);
    }
    
    workTestingMethod(data:any){
        return this._service.post(this.url+`/sup/setup/works/testing-method`,data,this.httpOptions);
    }
    
    addParam(data:any){
        return this._service.post(this.url+`/sup/setup/testing-methods/add-param`,data,this.httpOptions);
    }

    deleteParam(id:any){
        return this._service.post(this.url+`/sup/setup/testing-methods/delete-param`,id,this.httpOptions);
    }

    listTestingMethodType():any{
        return this._service.get(this.url+`/sup/setup/testing-methods-types`,this.httpOptions);
    }

    filterListTestingMethodType(id:any):any{
        return this._service.get(this.url+`/sup/setup/testing-methods?type=${id}`,this.httpOptions);
    }

    listingTestingMethod(work_id = 0 ):any{
        if(work_id == 0){
            return this._service.get(this.url+`/sup/setup/testing-methods`,this.httpOptions);
        }
        else{
            return this._service.get(this.url+`/sup/setup/testing-methods?work_id=${work_id}`,this.httpOptions);

        }
    }

    updateTestingMethod(data:any):any{
        return this._service.post(this.url+`/sup/setup/testing-methods/update`,data,this.httpOptions);
    }

    deleteTestingMethod(id:any):any{
        return this._service.delete(this.url+`/sup/setup/testing-methods?id=${id}`,this.httpOptions);
    }

    creating(route:any,data:any){
        return this._service.post(this.url+`/sup/setup/${route}`,data,this.httpOptions);
    }

    delete(data:any,id:any){
        return this._service.delete(this.url+`/sup/setup/${data}?id=${id}`,this.httpOptions);
    }
    
    update(route:any,data:any){
        return this._service.post(this.url+`/sup/setup/${route}/update`,data,this.httpOptions);
    }
    
    getTestingMethod(id:any){
        return this._service.get(this.url+`/sup/setup/works/show?work_id=${id}`);
    }
    getgroupTest(){
        return this._service.get(this.url+'/sup/setup/testing-methods/group',this.httpOptions);
    }
    getTestingStandard(id:any){
        return this._service.get(this.url+"/sup/setup/testing-methods/group/"+id,this.httpOptions);
    }
    UpdateStandard(body:any){
        return this._service.post(this.url+"/sup/setup/testing-methods/group/update",body,this.httpOptions);
    }
    CreateStandard(body:any){
        return this._service.post(this.url+"/sup/setup/testing-methods/group/create",body,this.httpOptions);
    }
    DeleteStandard(body:any){
        return this._service.post(this.url+"/sup/setup/testing-methods/group/delete",body,this.httpOptions);
    }
}