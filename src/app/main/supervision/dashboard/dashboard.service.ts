import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'environments/environment';
import staticData from './dashboard-static-data.json'

@Injectable({
    providedIn: 'root',
})
export class DashboardService {
    url = env.apiUrl;
    httpOptions = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };

    constructor(private http: HttpClient) {}

    get staticData() { return staticData}
    getData() {
      return this.http.get(`${this.url}/sup/dashboard`)
  }
}
