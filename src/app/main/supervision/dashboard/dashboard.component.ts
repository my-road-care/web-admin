import { Component, OnInit } from '@angular/core';

@Component({
  selector    : 'app-sup-dashboard',
  templateUrl : './dashboard.component.html',
  styleUrls   : ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  
  mapLoaded: boolean

  constructor() { }

  ngOnInit(): void {
  }

  get role() {
    return localStorage.getItem('access')
  }

  show(componentName: string, btnValue: string) {
    if(!this.mapLoaded && btnValue === 'map') {
      this.mapLoaded = true
    }
    return componentName === btnValue ? '' : 'd-none'
  }

}