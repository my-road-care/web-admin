import { MouseEvent } from '@agm/core';
import { GoogleMap, Polyline, PolylineOptions } from '@agm/core/services/google-maps-types';
import { Component, Input, OnInit } from '@angular/core';
import { DashboardService } from '../../dashboard.service';
import styles from './map-styles/grey.json'
import svg from './marker.json'
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingDialogComponent } from 'app/shared/loading-dialog/loading-dialog.component';
import { RoadAllService } from 'app/main/cp/nationalRoad/all/all.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  // Custom polylines
  public polylines: any[]= [];
  public projects:any=[];
  map: GoogleMap
  directions: Direction[] = []
  routeMarkers: marker[] = []
  allPks:any[]=[]
  markers: marker[] = []
  styles = styles
  pks: any[] = []
  loadingDialog: MatDialogRef<LoadingDialogComponent>

  selectedType: string = 'all';
  types = []

  constructor(private service: DashboardService,private roadservice:RoadAllService, private matDialog: MatDialog) { }

  ngOnInit(): void {
    this.loadingDialog = this.showLoading()
    this.service.getData().subscribe((res:any)=>{
      this.projects=res.project.projects;
      this.getAllRead();
    })
    this.types = this.service.staticData.map_legends
  }
  getAllRead(){
    let projectleng=0;

    this.projects.forEach((p:any)=>{

      this.roadservice.viewPk(p?.road[0].id ,{ limit: 173, with_potholes: 1 }).subscribe((res:any)=>{
        projectleng+=1;
        this.setPoliline(res,p.status.color );
        // this.allPks.push(res.data);
        // console.log(this.allPks)
        // console.log()
        if(this.projects.length == projectleng){
          this.loadingDialog.close();
        }
      })

      // if){
      //   console.log('hi');
      // }
    })
  }







  ngOnDestroy() {
    console.log('destroy map')
  }

  showLoading() {
    const dialog = this.matDialog.open(LoadingDialogComponent, {
      data: {
        message: 'សូមមេត្តារង់ចាំ...'
      }
    })
    return dialog
  }

  clickedMarker(label: string, index: number) {
    // console.log(`clicked the marker: ${label || index}`)
    // console.log(this.routeMarkers[index])
  }

  filterParentheses(s: string) {
    return s.includes('(') ? s.substring(0, s.indexOf('(')) : s
  }

  onResponse(direction, event) {
    // this.loadingDialog.close()
    // console.log('res', direction)
    // Default style
    const polylineOptions: PolylineOptions = {
      strokeWeight: 6,
      strokeOpacity: .5,
      icons: [
      ]
    };

    // Polylines strokeColor
    const colors = ['#FF0000', '#00FF00', '#0000FF'];

    // Clear exist polylines
    const { legs } = event.routes[0];

    this.directions.splice(this.directions.indexOf(direction), 1)
    legs.forEach((leg) => {
      const stepPolyline: Polyline = new google.maps.Polyline(polylineOptions as any) as any;
    });
  }
  setPoliline(res,color){
        // console.log(res)
        // const polylineOptions: PolylineOptions = {
        //   strokeWeight: 6,
        //   strokeOpacity: .5,
        //   icons: [
        //     // {
        //     //   icon: {
        //     //     path: 0,
        //     //     fillColor: '#000'
        //     //   },
        //     //   offset: '0%',
        //     //   repeat: '10%'
        //     // }
        //   ]
        // };
        this.pks = res.data
        const pkPaths = this.pks
        const origin = pkPaths[0].points[0].latlng
        const destination = pkPaths[pkPaths.length - 1].points[0].latlng
        const colorMainRoad=color ? color :'orange'
        this.directions = [{
          origin, destination, renderOptions: { draggable: false }
        }]

        const  stepPolyline = pkPaths.map(({ code, points }) => {
          const line = new google.maps.Polyline({ strokeColor: colorMainRoad, strokeOpacity: .4, strokeWeight: 6   })

          points.forEach(({ latlng, meter, n_of_potholes, potholes }) => {
            const info = potholes.length ? { ...potholes[0], code, meter, n_of_potholes } : null
            const color = info ? 'red' : 'black'
            const icon: google.maps.Symbol = {
              path: 0,
              scale: 3,
              fillColor: color,
              strokeColor: color,
              labelOrigin: new google.maps.Point(11, 1)
            }

            // this.routeMarkers.push({
            //   ...latlng, label: `${code}+${meter}`, icon, info
            // })
          })
          line.setPath(points.map(({ latlng }) => latlng))
          line.setMap(this.map as any)
          return line
        })
        this.polylines.push(stepPolyline)
  }
  mapReady(map: GoogleMap) {
    this.map = map

    const latLngBounds = {
      south: 10.069775255302284,
      west: 101.801119586494,
      north: 14.746719947756558,
      east: 108.0468471255565
    }

    map.setOptions({
      // center: { lat: 12.71642962743061, lng: 104.92398335602526 },
      // zoom: 7.8,
      restriction: {
        latLngBounds,
        strictBounds: true,
      }
    })

    this.service.staticData.directions.forEach(direction => {
      this.directions.push({
        ...direction, renderOptions: { draggable: false }
      })
    })

    window['map'] = map
  }

  mapClicked($event: MouseEvent) {
    // console.log('clicked', $event.coords)
    // if (this.markers.length) this.markers.pop()
    // else
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      icon: { url: 'data:image/svg+xml;charset=UTF-8,' + svg.data, scaledSize: new google.maps.Size(80, 80) },
      draggable: true
    });
  }

  calculator(markers, numStyles) {
    return {
      text: '',
      index: 2
      // title: count
    };
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    // console.log('dragEnd', m, $event);
  }

  legendChange() {
    this.polylines.forEach(polyline => polyline.setMap(null));

    let directions = this.service.staticData.directions;
    if (this.selectedType !== 'all')
      directions = this.service.staticData.directions.filter(d => d.type_id == (this.selectedType as unknown as number))

    this.directions = directions.map(d => {
      return { ...d, renderOptions: { draggable: false } }
    })
  }

}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  info?: any;
  icon?: any;
  draggable: boolean;
}

interface Direction {
  origin: string | { lat: number, lng: number }
  destination: string | { lat: number, lng: number }
  renderOptions: {
    draggable: Boolean
  }
}
