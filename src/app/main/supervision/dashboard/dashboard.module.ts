import {NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { dashboardRoutes } from './dashboard.routing';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AgmCoreModule } from '@agm/core'
import { AgmDirectionModule } from 'agm-direction';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { MapComponent } from './components/map/map.component';

@NgModule({
    declarations: [
        DashboardComponent,
        MapComponent
    ],
    imports: [
        PerfectScrollbarModule,
        RouterModule.forChild(dashboardRoutes),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAeOPNbz8UGspqu4TqT2vAA7XFYBClQpnU'
        }),
        AgmDirectionModule,
        AgmJsMarkerClustererModule,
        SharedModule
    ]
})
export class DashboardModule {}
