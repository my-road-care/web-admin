import { NavigationItem } from "helpers/components/navigation";

export const MinisterNavigation: NavigationItem[]  = [
    // =======================================>> Dashboard
    {
        id      : 'dashboard',
        title   : 'ផ្ទាំងគ្រប់គ្រង',
        icon    : 'mat_solid:dashboard',
        type    : 'basic',
        link    : '/dashboard',
    },


    // =======================================>> Supervision
    { 
        id      : 'supervision',
        title   : 'ត្រួតពិនិត្យគុណភាពផ្លូវ',
        icon    : 'mat_outline:assignment',
        type    : 'collapsable',
        children: [
            // =======================================>> Dashboard
            {
                id      : 'sub.dashboard',
                title   : 'ព័ត៌មានយុទ្ធសាស្ត្រ',
                icon    : 'mat_solid:area_chart',
                type    : 'basic',
                link    : '/sup/dashboard'
            },
            // =======================================>> Project
            {
                id      : 'sub.projects',
                title   : 'គម្រោង',
                icon    : 'mat_solid:format_list_bulleted',
                type    : 'basic',
                link    : '/sup/projects'
            },
        ]
    },

    // =======================================>> Pothole
    {
        id      : 'pot',
        title   : 'រាយការណ៍ផ្លូវខូច',
        icon    : 'mat_outline:smartphone',
        type    : 'collapsable',
        children: [
            // =======================================>> Reports
            {
                id      : 'pot.pothole',
                title   : 'ផ្លូវខូច',
                icon    : 'mat_solid:report_problem',
                type    : 'basic',
                link : '/pot/pothole'
            },
            // =======================================>> Report
            {
                id      : 'pot.report',
                title   : 'របាយការណ៍',
                icon    : 'heroicons_solid:trending-up',
                type    : 'collapsable',
                children: [
                    // =======================================>> Provinces
                    {
                        id   : 'pot.report.provinces',
                        title: 'ខេត្ត',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/provinces'
                    },
                    // =======================================>> months
                    {
                        id   : 'pot.report.months',
                        title: 'តាមខែ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/monthly'
                    },
                    // =======================================>> total_reports
                    {
                        id   : 'pot.report.total_reports',
                        title: 'តារាងរបាយការណ៍សរុប',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/trc'
                    },
                    // =======================================>> provincails
                    {
                        id   : 'pot.report.provincails',
                        title: 'របាយការណ៍តាមខេត្ត',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/provincail'
                    },
                    // =======================================>> monthly_reports
                    {
                        id   : 'pot.report.monthly_reports',
                        title: 'របាយការណ៍តាមខែ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/mrc'
                    },
                    // =======================================>> monthly_users
                    {
                        id   : 'pot.report.monthly_users',
                        title: 'របាយការអ្នកប្រើប្រាស់',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/muc'
                    }
                ]
            },
        ]
    },


    // =======================================>> National Road
    {
        id      : 'road',
        title   : 'ផ្លូវជាតិ',
        icon    : 'mat_solid:map',
        type    : 'collapsable',
        children: [
            {
                id   : 'all',
                title: 'ផ្លូវទាំងអស់',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/road/alls'
            },
            {
                id   : 'map',
                title: 'ផែនទី',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/road/maps'
            }
        ]
    },

    // =======================================>> Location
    {
        id      : 'location',
        title   : 'ទីតាំង',
        icon    : 'mat_solid:location_on',
        type    : 'collapsable',
        children: [
            {
                id   : 'province',
                title: 'ខេត្ត',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/provinces'
            },
            {
                id   : 'district',
                title: 'ស្រុក',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/districts'
            },
            {
                id   : 'commune',
                title: 'ឃុំ',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/communes'
            },
            {
                id   : 'village',
                title: 'ភូមិ',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/villages'
            }
        ]
    },

    // ========================================>> My profile
    {
        id   : 'my-profile',
        title: 'គណនី',
        type : 'basic',
        icon : 'mat_solid:account_circle',
        link : '/my-profile'
       
    }
];
