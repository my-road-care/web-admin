import { NavigationItem } from "helpers/components/navigation";

export const SUNavigation: NavigationItem[]  = [
    // =======================================>> Dashboard
    {
        id      : 'dashboard',
        title   : 'ផ្ទាំងគ្រប់គ្រង',
        icon    : 'mat_solid:dashboard',
        type    : 'basic',
        link    : '/dashboard',
    },

    // =======================================>> Project
    {
        id      : 'sub.projects',
        title   : 'គម្រោង',
        icon    : 'mat_solid:format_list_bulleted',
        type    : 'basic',
        link    : '/sup/projects'
    },


    // ========================================>> My profile
    {
        id   : 'my-profile',
        title: 'គណនី',
        type : 'basic',
        icon : 'mat_solid:account_circle',
        link : '/my-profile' 
    }
];
