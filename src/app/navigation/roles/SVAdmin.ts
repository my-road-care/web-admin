import { NavigationItem } from "helpers/components/navigation";

export const SVAdminNavigation: NavigationItem[]  = [
    // =======================================>> Dashboard
    {
        id      : 'sub.dashboard',
        title   : 'ព័ត៌មានយុទ្ធសាស្ត្រ',
        icon    : 'mat_solid:area_chart',
        type    : 'basic',
        link    : '/sup/dashboard'
    },
    // =======================================>> Project
    {
        id      : 'sub.projects',
        title   : 'គម្រោង',
        icon    : 'mat_solid:format_list_bulleted',
        type    : 'basic',
        link    : '/sup/projects'
    },

    // =======================================>> Authorities
    {
        id      : 'sup.authorities',
        title   : 'អ្នកប្រើប្រាស់',
        icon    : 'heroicons_solid:users',
        type    : 'collapsable',
        children: [
            // =======================================>> accounts
            {
                id   : 'sup.authorities.accounts',
                title: 'គណនី',
                icon : 'heroicons_solid:chevron-right',
                type : 'basic',
                link : '/sup/authorities/accounts'
            },
            // =======================================>> system role
            {
                id   : 'sup.authorities.types',
                title: 'តួនាទីក្នុងប្រព័ន្ទ',
                icon : 'heroicons_solid:chevron-right',
                type : 'basic',
                link : '/sup/authorities/types'
            },
            // =======================================>> permissions
            {
                id   : 'sup.authority.permissions',
                title: 'កំណត់សិទ្ធិ',
                icon : 'heroicons_solid:chevron-right',
                type : 'basic',
                link : '/sup/authorities/permissions'
            }
        ]
    },

    // =======================================>> Organization
    {
        id      : 'sub.organization',
        title   : 'អង្គភាព',
        icon    : 'mat_solid:account_balance',
        type    : 'basic',
        link    : '/sup/organizations'
    },

    // =======================================>> setting
    {
        id      : 'sup.setting',
        title   : 'កំណត់',
        icon    : 'heroicons_outline:cog',
        type    : 'collapsable',
        children: [
            // =======================================>> Works
            {
                id   : 'sup.setting.works',
                title: 'កាងារ',
                icon : 'heroicons_solid:chevron-right',
                type : 'basic',
                link : '/sup/setting/works'
            },
            // =======================================>> Test Method
            {
                id   : 'sup.setting.test-method',
                title: 'វិធីសាស្ត្រតេស្ត',
                icon : 'heroicons_solid:chevron-right',
                type : 'basic',
                link : '/sup/setting/testing-method'
            },
            // =======================================>> Testing Standard
            {
                id   : 'sup.setting.testing-standard',
                title: 'ស្តង់ដាតេស្ត',
                icon : 'heroicons_solid:chevron-right',
                type : 'basic',
                link : '/sup/setting/testing-standard'
            },
            // =======================================>> Budget Year
            {
                id   : 'sup.setting.budget-year',
                title: 'ថវិកាប្រចាំឆ្នាំ',
                icon : 'heroicons_solid:chevron-right',
                type : 'basic',
                link : '/sup/setting/budget-year'
            }   
        ]
    },

    // ========================================>> My profile
    {
        id   : 'my-profile',
        title: 'គណនី',
        type : 'basic',
        icon : 'mat_solid:account_circle',
        link : '/my-profile'
    }
];
