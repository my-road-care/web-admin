import { NavigationItem } from "helpers/components/navigation";

export const SuperAdminNavigation: NavigationItem[]  = [

    // =======================================>> Dashboard
    {
        id      : 'dashboard',
        title   : 'ផ្ទាំងគ្រប់គ្រង',
        icon    : 'mat_solid:dashboard',
        type    : 'basic',
        link    : '/dashboard',
    },

    // =======================================>> Supervision
    {
        id      : 'supervision',
        title   : 'ត្រួតពិនិត្យគុណភាពផ្លូវ',
        icon    : 'mat_outline:assignment',
        type    : 'collapsable',
    
        children: [
            // =======================================>> Dashboard
            {
                id      : 'sub.dashboard',
                title   : 'ព័ត៌មានយុទ្ធសាស្ត្រ',
                icon    : 'mat_solid:area_chart',
                type    : 'basic',
                link    : '/sup/dashboard'
            },
            // =======================================>> Project
            {
                id      : 'sub.projects',
                title   : 'គម្រោង',
                icon    : 'mat_solid:format_list_bulleted',
                type    : 'basic',
                link    : '/sup/projects'
            },
            // =======================================>> Authorities
            {
                id      : 'sup.authorities',
                title   : 'អ្នកប្រើប្រាស់',
                icon    : 'heroicons_solid:users',
                type    : 'collapsable',
                children: [
                    // =======================================>> accounts
                    {
                        id   : 'sup.authorities.accounts',
                        title: 'គណនី',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/sup/authorities/accounts'
                    },
                    // =======================================>> system role
                    {
                        id   : 'sup.authorities.types',
                        title: 'តួនាទីក្នុងប្រព័ន្ទ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/sup/authorities/types'
                    },
                    // =======================================>> permissions
                    {
                        id   : 'sup.authority.permissions',
                        title: 'កំណត់សិទ្ធិ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/sup/authorities/permissions'
                    }
                ]
            },
            // =======================================>> Organization
            {
                id      : 'sub.organization',
                title   : 'អង្គភាព',
                icon    : 'mat_solid:account_balance',
                type    : 'basic',
                link    : '/sup/organizations'
            },
            // =======================================>> setting
            {
                id      : 'sup.setting',
                title   : 'កំណត់',
                icon    : 'heroicons_outline:cog',
                type    : 'collapsable',
                children: [
                    // =======================================>> Works
                    {
                        id   : 'sup.setting.works',
                        title: 'កាងារ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/sup/setting/works'
                    },
                    // =======================================>> Test Method
                    {
                        id   : 'sup.setting.test-method',
                        title: 'វិធីសាស្ត្រតេស្ត',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/sup/setting/testing-method'
                    }, 
                    // =======================================>> Testing Standard
                    {
                        id   : 'sup.setting.testing-standard',
                        title: 'ស្តង់ដាតេស្ត',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/sup/setting/testing-standard'
                    },  
                    // =======================================>> Budget Year
                    {
                        id   : 'sup.setting.budget-year',
                        title: 'ថវិកាប្រចាំឆ្នាំ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/sup/setting/budget-year'
                    }     
                ]
            },
        ]
    },

    // =======================================>> Pothole
    {
        id      : 'pot',
        title   : 'រាយការណ៍ផ្លូវខូច',
        icon    : 'mat_outline:smartphone',
        type    : 'collapsable',
        children: [
            // =======================================>> Dashboard
            {
                id      : 'dashboard',
                title   : 'ផ្ទាំងគ្រប់គ្រង',
                icon    : 'mat_solid:dashboard',
                type    : 'basic',
                link    : '/pot/dashboard',
            },
            // =======================================>> Reports
            {
                id      : 'pot.pothole',
                title   : 'ផ្លូវខូច',
                icon    : 'mat_solid:report_problem',
                type    : 'basic',
                link    : '/pot/pothole'
            },
            // =======================================>> Authority
            {
                id      : 'pot.authority',
                title   : 'មន្ត្រី/អាជ្ញាធរ',
                icon    : 'mat_outline:badge',
                type    : 'collapsable',
                children: [
                    // =======================================>> Ministrys
                    {
                        id   : 'pot.authority.ministrys',
                        title: 'ក្រសួង',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/authorities/ministries'
                    },
                    // =======================================>> Officers
                    {
                        id   : 'pot.authority.mos',
                        title: 'ការិយាល័យ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/authorities/mos'
                    },
                    // =======================================>> Fixers
                    {
                        id   : 'pot.authority.fixers',
                        title: 'ក្រុមជួសជុល',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/authorities/mts'
                    }
                ]
            },
            // ======================================>> UserRoad 
            {
                id      : 'pot.roaduser',
                title   : 'អ្នកប្រើប្រាស់ផ្លូវ',
                icon    : 'mat_solid:person',
                type    : 'basic',
                link    : '/pot/users'
            },
            // =======================================>> Report
            {
                id      : 'pot.report',
                title   : 'របាយការណ៍',
                icon    : 'heroicons_solid:trending-up',
                type    : 'collapsable',
                children: [
                    // =======================================>> Provinces
                    {
                        id   : 'pot.report.provinces',
                        title: 'ខេត្ត',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/provinces'
                    },
                    // =======================================>> months
                    {
                        id   : 'pot.report.months',
                        title: 'តាមខែ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/monthly'
                    },
                    // =======================================>> total_reports
                    {
                        id   : 'pot.report.total_reports',
                        title: 'តារាងរបាយការណ៍សរុប',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/trc'
                    },
                    // =======================================>> provincails
                    {
                        id   : 'pot.report.provincails',
                        title: 'របាយការណ៍តាមខេត្ត',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/provincail'
                    },
                    // =======================================>> monthly_reports
                    {
                        id   : 'pot.report.monthly_reports',
                        title: 'របាយការណ៍តាមខែ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/mrc'
                    },
                    // =======================================>> monthly_users
                    {
                        id   : 'pot.report.monthly_users',
                        title: 'របាយការអ្នកប្រើប្រាស់',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/report/muc'
                    }
                ]
            },
            // =======================================>> Maintenance
            {
                id      : 'pot.maintenance',
                title   : 'គ្រប់គ្រងលេខកូដថែទាំ',
                icon    : 'heroicons_outline:cog',
                type    : 'collapsable',
                children: [
                    // =======================================>> Maintenance Code
                    {
                        id   : 'pot.maintenance.code',
                        title: 'កូដថែទាំ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/maintenance/codes'
                    },
                    // =======================================>> Maintenance Group
                    {
                        id   : 'pot.maintenance.group',
                        title: 'ក្រុមថែទាំ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/maintenance/groups'
                    },
                    // =======================================>> Maintenance Type
                    {
                        id   : 'pot.maintenance.type',
                        title: 'ប្រភេទថែទាំ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/maintenance/types'
                    },
                    // =======================================>> Maintenance Sub Type
                    {
                        id   : 'pot.maintenance.subs',
                        title: 'អនុប្រភេទថែទាំ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/maintenance/subs'
                    },
                    // =======================================>> Maintenance Unit
                    {
                        id   : 'pot.setting.monthly_settings',
                        title: 'ខ្នាតថែទាំ',
                        icon : 'heroicons_solid:chevron-right',
                        type : 'basic',
                        link : '/pot/maintenance/units'
                    }
                ]
            }
        ]
    },


    // =======================================>> National Road
    {
        id      : 'road',
        title   : 'ផ្លូវជាតិ',
        icon    : 'mat_solid:map',
        type    : 'collapsable',
        children: [
            {
                id   : 'all',
                title: 'ផ្លូវទាំងអស់',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/road/alls'
            },
            {
                id   : 'map',
                title: 'ផែនទី',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/road/maps'
            }
        ]
    },

    // =======================================>> Location
    {
        id      : 'location',
        title   : 'ទីតាំង',
        icon    : 'mat_solid:location_on',
        type    : 'collapsable',
        children: [
            {
                id   : 'province',
                title: 'ខេត្ត',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/provinces'
            },
            {
                id   : 'district',
                title: 'ស្រុក',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/districts'
            },
            {
                id   : 'commune',
                title: 'ឃុំ',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/communes'
            },
            {
                id   : 'village',
                title: 'ភូមិ',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/location/villages'
            }
        ]
    },

    // =======================================>> New Feature
    {
        id   : 'new-feature',
        title: 'មុខងារថ្មី',
        type : 'collapsable',
        icon : 'heroicons_solid:briefcase',
        children    :[
            {
                id   : 'refer_friend',
                title: 'ណែនាំមិត្ត',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/new-feature/refer_friends'
            },
            {
                id   : 'new',
                title: 'មានអ្វីថ្មី',
                type : 'basic',
                icon : 'heroicons_solid:chevron-right',
                link : '/cp/new-feature/news'
            }
        ]
    },

    // =======================================>> Admin
    {
        id   : 'admin',
        title: 'គ្រប់គ្រងអ្នកប្រើប្រាស់',
        type : 'collapsable',
        icon : 'mat_solid:person',
        children :[
            {
                id      : 'all',
                title   : 'គណនី',
                type    : 'basic',
                icon    : 'heroicons_solid:chevron-right',
                link    : '/cp/admin/accounts'
            },
            {
                id      : 'newcreate',
                title   : 'អង្គភាព',
                type    : 'basic',
                icon    : 'heroicons_solid:chevron-right',
                link    : '/cp/admin/organizations'
            },
            {
                id      : 'newcreate',
                title   : 'តួនាទីក្នុងស្ថាប័ន',
                type    : 'basic',
                icon    : 'heroicons_solid:chevron-right',
                link    : '/cp/admin/roles'
            }
        ]
    },

    // ========================================>> My profile
    {
        id   : 'my-profile',
        title: 'គណនី',
        type : 'basic',
        icon : 'mat_solid:account_circle',
        link : '/my-profile'
    }
];
