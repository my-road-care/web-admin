import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash-es';
import { NavigationItem } from 'helpers/components/navigation';
import { NavigationApiService } from 'helpers/navigation-api';
import { defaultNavigation } from 'app/navigation/navigation';

@Injectable({
    providedIn: 'root',
})
export class Navigation {
    private readonly _defaultNavigation: NavigationItem[] = defaultNavigation;
    constructor(private _navigationApiService: NavigationApiService) {
        this.registerHandlers();
    }
    registerHandlers(): void {
        this._navigationApiService.onGet('api/common/navigation').reply(() =>
            [
                200,
                {
                    default: cloneDeep(this._defaultNavigation),
                },
            ]
        );
    }
}
