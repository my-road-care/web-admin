import { NavigationItem } from 'helpers/components/navigation';
import { SuperAdminNavigation } from './roles/SuperAdmin';
import { SVAdminNavigation } from './roles/SVAdmin';
import { RPAdminNavigation } from './roles/RPAdmin';
import { MinisterNavigation } from './roles/Minister';
import { SUNavigation } from './roles/SU';

enum Type {
    SuperAdmin = 1,
    SVAdmin = 6,
    SupervisionUser = 7,
    RPAdmin = 8,
    HeadOfCommittee = 9,
    Minister = 10,
    SVInfoEntry = 11,
    SVSpecialInspector = 12
}

let type_id: number = 0;
let list: NavigationItem[] = [];
let user: any = localStorage.getItem('user');
if (user) {
    user = JSON.parse(user);
    type_id = user.type_id
}

if (Type.SuperAdmin === type_id) {
    list = SuperAdminNavigation;
} else if (Type.RPAdmin === type_id) {
    list = RPAdminNavigation;
} else if (Type.SVAdmin === type_id) {
    list = SVAdminNavigation;
} else if (Type.Minister === type_id || Type.HeadOfCommittee === type_id) {
    list = MinisterNavigation;
} else if (Type.SupervisionUser === type_id || Type.SVInfoEntry === type_id || Type.SVSpecialInspector === type_id) {
    list = SUNavigation;
}
export const defaultNavigation: NavigationItem[] = list;
