import { Component } from '@angular/core';
import { CacheService } from './shared/services/cache.service';

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})

export class AppComponent
{
    /**
     * Constructor
     */
    constructor(private cacheService: CacheService)
    {
    }
}
