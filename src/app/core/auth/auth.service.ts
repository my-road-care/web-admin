import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, switchMap, throwError } from 'rxjs';
import { environment as env } from 'environments/environment';
import { CacheService } from 'app/shared/services/cache.service';

@Injectable()
export class AuthService
{
    
    private _authenticated: boolean = false;
    private _url: string = env.apiUrl;

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient
        
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for access token
     */
    private setToken(token: string): any
    {
        localStorage.setItem('accessToken', token);
    }

    public getToken(): string
    {
        return localStorage.getItem('accessToken');
    }
    /**
     * Setter & getter for access token
     */
    private setRole(token: string): any
    {
        localStorage.setItem('roles', token);
    }

    public getRole(): string
    {
        return localStorage.getItem('roles');
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    /**
     * Login
     *
     * @param credentials
     */
    login(credentials: { phone: string; password: string }): Observable<any>
    {
        // Throw error, if the user is already logged in
        if ( this._authenticated )
        {
            return throwError('User is already logged in.');
        }
        return this._httpClient.post(this._url+'/auth/login', credentials).pipe(
            switchMap((response: any) => {

                // Store the access token in the local storage
                this.setToken(response.token);

                // Store the role on the local storage
                this.setRole(response.roles);
                // Return a new observable with the response
                return of(response);
            })
        );
    }

    /**
     * ForgotPassowrd
     */
    forgotPassword(credentials: { phone: string }):Observable<any>{
        return this._httpClient.post(this._url+'/auth/forgetPassword', credentials).pipe(
            switchMap((response: any) => {

                // Return a new observable with the response
                return of(response);
            })
        );
    }
    /**
     * Check the authentication status
     */
    check(): Observable<boolean>
    {
        // Check if the user is logged in
        if ( this._authenticated )
        {
            return of(true);
        }

        // Check the access token availability
        if ( !this.getToken )
        {
            return of(false);
        }
    }
}
