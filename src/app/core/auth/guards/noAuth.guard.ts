import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from '../auth.service';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { GlobalConstants } from 'app/shared/global-constants';

@Injectable({
    providedIn: 'root',
})
export class NoAuthGuard implements CanActivate {
    private token: any;
    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _router: Router,
        private _snackBarService: SnackbarService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Can activate
     */
    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        this.token = this._authService.getToken();
        let role: string = localStorage.getItem('access');
        if (this.token) {

            if (role === 'MO' || role === 'MT' || role === 'RU') {
                this._snackBarService.openSnackBar(GlobalConstants.unauthorized, GlobalConstants.error);
                //access
                return of(true);
            }
            //not access this login page
            this._router.navigateByUrl('/dashboard');
            return of(false);
        }
        //access
        return of(true);
    }
}
