import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from '../auth.service';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate , CanLoad{
    public token: any;
    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _router: Router
    ) {
    }
    canLoad(): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        this.token = this._authService.getToken();
        if(this.token){
            //access
            return of(true);
        }
        //not access
        this._router.navigateByUrl('/auth/login');
        return of(false);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Can activate
     */
    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        this.token = this._authService.getToken();
        if(this.token){
            //access
            return of(true);
        }
        //not access
        this._router.navigateByUrl('/auth/login');
        return of(false);
    }

}
