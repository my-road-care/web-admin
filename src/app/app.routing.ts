import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';
import { NoAuthGuard } from './core/auth/guards/noAuth.guard';

export const appRoutes: Route[] = [

    // Redirect empty path to 'dashboards'
    {
        path: '', pathMatch: 'full', redirectTo: 'dashboard'
    },

    // =============================>> Auth
    {
        path: 'auth',
        canActivate: [NoAuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        loadChildren: () => import('app/main/auth/auth.module').then(m => m.AuthModule)
    },

    // =============================>> Dashboard
    {
        path: 'dashboard',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        resolve: {
            initialData: InitialDataResolver,
        },
        loadChildren: () => import('app/main/dashboard/dashboard.module').then(m => m.DashboardModule),
        canLoad: [AuthGuard]
    },

    // =============================>> Supervision
    {
        path: 'sup',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('app/main/supervision/dashboard/dashboard.module').then(m => m.DashboardModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'projects',
                loadChildren: () => import('app/main/supervision/project/project.module').then(m => m.ProjectModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'authorities',
                loadChildren: () => import('app/main/supervision/authorities/authorities.module').then(m => m.AuthoritiesModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'organizations',
                loadChildren: () => import('app/main/supervision/organization/organization.module').then(m => m.OrganizationModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'setting',
                loadChildren: () => import('app/main/supervision/setting/setting.module').then(m => m.SettingModule),
                canLoad: [AuthGuard]
            }
        ]
    },

    // =============================>> Pothole
    {
        path: 'pot',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('app/main/pot/dashboard/dashboard.module').then(m => m.DashboardModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'pothole',
                loadChildren: () => import('app/main/pot/pothole/pothole.module').then(m => m.PotholeModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'authorities',
                loadChildren: () => import('app/main/pot/authorities/authorities.module').then(m => m.AuthoritiesModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'users',
                loadChildren: () => import('app/main/pot/roaduser/roaduser.module').then(m => m.RoadUserModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'report',
                loadChildren: () => import('app/main/pot/report/report.module').then(m => m.ReportModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'maintenance',
                loadChildren: () => import('app/main/pot/maintenance/maintenance.module').then(m => m.MaintenanceModule),
                canLoad: [AuthGuard]
            }
        ]
    },

    // =============================>> CP
    {
        path: 'cp',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {
                path: 'road',
                loadChildren: () => import('app/main/cp/nationalRoad/nationalroad.module').then(m => m.RoadModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'location',
                loadChildren: () => import('app/main/cp/location/location.module').then(m => m.LocationModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'new-feature',
                loadChildren: () => import('app/main/cp/new-feature/new-feature.module').then(m => m.NewFeatureModule),
                canLoad: [AuthGuard]
            },
            {
                path: 'admin',
                loadChildren: () => import('app/main/cp/admin/admin.module').then(m => m.AdminModule),
                canLoad: [AuthGuard]
            }
        ]
    },

    // =============================>> My Profile
    {
        path: 'my-profile',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        resolve: {
            initialData: InitialDataResolver,
        },
        loadChildren: () => import('app/main/my-profile/my-profile.module').then(m => m.MyProfileModule),
        canLoad: [AuthGuard]
    },

    //  =============================>> 404 & Catch all
    {
        path: '',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {
                path: '**', redirectTo: '404-not-found'
            },
            {
                path: '404-not-found', pathMatch: 'full',
                loadChildren: () => import('app/main/error/error-404.module').then(m => m.Error404Module),
                canLoad: [AuthGuard]
            },
        ]
    }
];
