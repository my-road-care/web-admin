import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { MediaWatcherService } from 'helpers/services/media-watcher';
import { HelpersNavigationService, NavigationComponent } from 'helpers/components/navigation';
import { Navigation } from 'app/core/navigation/navigation.types';
import { NavigationService } from 'app/core/navigation/navigation.service';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { DashboardService } from 'app/main/pot/dashboard/dashboard.service';


@Component({
    selector: 'dense-layout',
    templateUrl: './dense.component.html',
    styleUrls: ['dense.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DenseLayoutComponent implements OnInit, OnDestroy {
    year: number = 0;
    filters: { id: number, year: number }[] = [
        {
            id: 1,
            year: 2018,
        },
        {
            id: 2,
            year: 2019,
        },
        {
            id: 3,
            year: 2020,
        },
        {
            id: 4,
            year: 2021,
        },
        {
            id: 5,
            year: 2022,
        },
        {
            id: 6,
            year: 2023,
        },
    ]
    search: boolean;
    isScreenSmall: boolean;
    navigation: Navigation;
    navigationAppearance: 'default' | 'dense' = 'default';
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    public access = localStorage.getItem('access');
    /**
     * Constructor
     */
    constructor(
        private _navigationService: NavigationService,
        private _mediaWatcherService: MediaWatcherService,
        private _helpersNvigationService: HelpersNavigationService,
        private _route: ActivatedRoute,
        private _dashboardService: DashboardService,
        private _changeDetectorRef: ChangeDetectorRef,
    ) {
        if (this._route.routeConfig.path === 'pot' && this._route.children[0].routeConfig.path === 'dashboard') {
            this.search = true;
        } else {
            this.search = false;
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for current year
     */
    get currentYear(): number {
        return new Date().getFullYear();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this._dashboardService.filters = { year: this.year, listing: true };
        // Subscribe to navigation data
        this._dashboardService.show$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res: {search: boolean}) => {
                this.search = res.search;
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to navigation data
        this._navigationService.navigation$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((navigation: Navigation) => {
                this.navigation = navigation;
            });

        // Subscribe to media changes
        this._mediaWatcherService.onMediaChange$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(({ matchingAliases }) => {

                // Check if the screen is small
                this.isScreenSmall = !matchingAliases.includes('md');

                // Change the navigation appearance
                if (this.isScreenSmall) {
                    this.navigationAppearance = this.isScreenSmall ? 'default' : 'dense';
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    listing(): void {
        this._dashboardService.filters = { year: this.year, listing: true };
    }

    /**
     * Toggle navigation
     *
     * @param name
     */
    toggleNavigation(name: string): void {
        // Get the navigation
        const navigation = this._helpersNvigationService.getComponent<NavigationComponent>(name);

        if (navigation) {
            // Toggle the opened status
            navigation.toggle();
        }
    }

    /**
     * Toggle the navigation appearance
     */
    toggleNavigationAppearance(): void {
        this.navigationAppearance = (this.navigationAppearance === 'default' ? 'dense' : 'default');
    }
}
