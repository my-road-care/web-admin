import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from 'app/main/my-profile/profile.service';
import { CacheService } from 'app/shared/services/cache.service';
import { Subject, takeUntil } from 'rxjs';
import { environment as env } from 'environments/environment';

@Component({
    selector: 'user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    exportAs: 'user'
})
export class UserComponent implements OnInit, OnDestroy {
    user: any;
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    /**
     * Constructor
     */
    constructor(
        private cacheService: CacheService,
        private _router: Router,
        private _proService: ProfileService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _http: HttpClient
    ) {

    }


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._proService.userProfile$.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
            this.user = res;
            // // console.log(this.user);
            this._changeDetectorRef.markForCheck();
        })
        this.user = localStorage.getItem('user');
        if (this.user) {
            this.user = JSON.parse(this.user);
            this._proService.userProfile = this.user;
        }
        // console.log(this.user);
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Profile
     */
    myProfile(): void {
        this._router.navigate(['my-profile']);
    };

    /**
     * Sign out
     */
    Logout(): void {
        // Remove the access token from the local storage
        this._http.get<{ message: string }>(env.apiUrl + '/auth/logout').subscribe((response: {message: string})=>{
            console.log(response.message);
        });
        localStorage.clear();
        // Clear cache
        this.cacheService.clear();
        this._router.navigateByUrl('/auth/login');
    }

}
