import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment as env } from 'environments/environment';
@Component({
  selector: 'app-preview-image-multi',
  templateUrl: './preview-image-multi.component.html',
  styleUrls: ['./preview-image-multi.component.scss']
})
export class PreviewImageMultiComponent implements OnInit {
  public images:any =[];
  public url=env.fileUrl;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<PreviewImageMultiComponent>,
  ) {
    // dialogRef.disableClose = true;
  }

  ngOnInit(): void {
   this.images=this.data?.src;
   // console.log(this.data);

  }

}
