import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { environment as env } from 'environments/environment';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalConstants } from '../global-constants';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'app-report-working-table',
    templateUrl: './report-working-table.component.html',
    styleUrls: ['./report-working-table.component.scss']
})
export class ReportWorkingTableComponent implements OnInit {
    public isLoading: boolean;
    public saving: boolean
    public pkForm: UntypedFormGroup
    public pks: any[] = [];
    public pksStart: any[] = [];
    public pksEnd: any[] = [];
    public code: string = '';
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<ReportWorkingTableComponent>,
        private _httpClient: HttpClient,
        private _formBuilder: UntypedFormBuilder,
        private _snackbarService: SnackbarService
    ) { }

    ngOnInit(): void {
        this.isLoading = true;
        this._httpClient.get(`${env.apiUrl}/sup/projects/${this.data.project_id}/report/system-report/setup`).subscribe((res: any) => {
            this.isLoading = false
            this.pks = res?.pks
            this.code = res?.project?.code
            this.pksStart = this.pks
            this.pksEnd = this.pks
            this.pkFormBuilder();
        }, (err: any) => {
            this.isLoading = false
            console.log(err)
            this._snackbarService.openSnackBar('invalid pks_stations', GlobalConstants.error)
        })

    }

    changeStart(e: any) {
        this.pksEnd = this.pks.filter(pk => pk.id >= e.value)
    }
    changeEnd(e: any) {
        this.pksStart = this.pks.filter(pk => pk.id <= e.value)
    }

    pkFormBuilder(): void {
        console.log(this.pksStart[0]?.id);

        this.pkForm = this._formBuilder.group({
            pk_start_id: [this.pksStart[0]?.id, Validators.required],
            pk_end_id: [this.pksEnd[this.pksEnd.length - 1]?.id, Validators.required],
        });
    }

    submit(): void {
        this.saving = true;
        const obj: any = {
            ...this.pkForm.value,
            report: 2
        }
        this._httpClient.post(`${env.apiUrl}/sup/projects/${this.data.project_id}/report/system-report`, obj, this.httpOptions).subscribe((res: any) => {
            this.saving = false;
            const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            const day = ("0" + currentDate.getDate()).slice(-2);
            const hours = ("0" + currentDate.getHours()).slice(-2);
            const minutes = ("0" + currentDate.getMinutes()).slice(-2);
            const seconds = ("0" + currentDate.getSeconds()).slice(-2);

            const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
            let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
            FileSaver.saveAs(blob, this.code + '-របាយការណ៏តារាងការងារ-' + formattedDate + '.pdf');
            this.dialogRef.close();
        }, (err: any) => {
            this.saving = false;
            console.log(err);
            this.dialogRef.close();
        });
    }
    // =================================>> Convert base64 to blob 
    private b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
}
