import { Injectable } from '@angular/core'
import heic2any from 'heic2any'
import { piexif } from 'piexifjs'
import exif from 'exifreader'
import { MatDialog, MatDialogRef } from '@angular/material/dialog'
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component'
import { LoadingDialogComponent } from '../loading-dialog/loading-dialog.component'

@Injectable({
  providedIn: 'root'
})
export class ImageGpsService {

  constructor(private matDialog: MatDialog) { }

  processAnyImage(file: File): Promise<ImageGPS> {
    return new Promise((res, rej) => {
      if (!file) return rej('File is required')

      const reader = new FileReader
      reader.readAsDataURL(file)

      reader.onload = async () => {
        const base64 = reader.result as string
        const metaData = await exif.load(base64)
        const { GPSLatitude, GPSLongitude } = metaData
        let lat, lng

        if (GPSLatitude && GPSLongitude) {
          lat = Number(GPSLatitude.description)
          lng = Number(GPSLongitude.description)
        } else {

          const dialog = this.matDialog.open(ConfirmDialogComponent, {
            data: { message: 'រូបភាពគ្មានទីតាំង! បញ្ចូលទីតាំងបច្ចុប្បន្ន?' }
          })

          await this.waitingForComfirmation(dialog)

          const { coords: { latitude, longitude } } = await this.getCurrentPosition()
          lat = latitude
          lng = longitude
        }

        if (file.name.toLocaleLowerCase().endsWith('heic'))
          return res(this.processHEIC(file, lat, lng))
        else return res({
          img: base64,
          lat,
          lng
        })
      }
    })
  }
  waitingForComfirmation(dialog: MatDialogRef<ConfirmDialogComponent, any>) {
    return new Promise((res, rej) => {
      dialog.afterClosed().subscribe((agree => agree ? res(agree) : rej('Could get GPS info!')))
    })
  }

  blob2Base64(blob: Blob): Promise<string> {
    return new Promise((res, rej) => {
      const reader = new FileReader
      reader.onload = () => {
        res(reader.result as string)
      }
      reader.onerror = e => rej(e)
      reader.readAsDataURL(blob)
    })
  }

  async processHEIC(file: File, lat: number, lng: number): Promise<ImageGPS> {
    const dialog = this.matDialog.open(LoadingDialogComponent, {
      data: {
        message: 'Converting HEIC to JPEG'
      }
    })

    const newBlob = await heic2any({ blob: file, toType: 'image/jpeg' })
    const newBase64 = await this.blob2Base64(newBlob as Blob)

    const newMetaData = piexif.load(newBase64)

    newMetaData.GPS[piexif.GPSIFD.GPSLatitude] = piexif.GPSHelper.degToDmsRational(lat);
    newMetaData.GPS[piexif.GPSIFD.GPSLatitudeRef] = 'N';

    newMetaData.GPS[piexif.GPSIFD.GPSLongitude] = piexif.GPSHelper.degToDmsRational(lng);
    newMetaData.GPS[piexif.GPSIFD.GPSLongitudeRef] = 'E';

    const newImg = piexif.insert(piexif.dump(newMetaData), newBase64)
    dialog.close()
    return {
      img: newImg,
      lat, lng
    }
  }

  async createImageFile(img: string, name: string): Promise<File> {
    const res = await fetch(img)
    const blob = await res.blob()
    return new File([blob], name, { type: "image/jpeg" })
  }

  getCurrentPosition(): Promise<GeolocationPosition> {
    const dialog = this.matDialog.open(LoadingDialogComponent, {
      data: {
        message: 'Detecting location'
      }
    })

    return new Promise(async (res, rej) => {
      const permission = await navigator.permissions.query({ name: 'geolocation' })
      permission.onchange = () => {
        if (permission.state === 'denied') {
          dialog.close()
          return rej('Please allow GPS permission.')
        }
      }
      
      if (permission.state === 'denied') {
        dialog.close()
        return rej('Please allow GPS permission.')
      }
      else {
        navigator.geolocation.getCurrentPosition(s => {
          dialog.close()
          return res(s)
        })
      }
    })
  }

}

export interface ImageGPS {
  img: string
  lat: number
  lng: number
}
