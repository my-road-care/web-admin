import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Buffer } from 'buffer'
import { ICacheResponse, CacheEventHandler, CacheSubscription, ICacheItem, CacheResult } from '../interfaces/cache-service.interfaces';
abstract class CacheHelper {
  protected _memory = {}
  protected events = {}
  protected observables = {}
  private prefix: string = 'cache'

  constructor() {
    this.loadMemory()
    addEventListener('beforeunload', () => {
      Object.keys(this._memory).forEach(key => {
        try {
          localStorage.setItem(this.encodeKey(key), JSON.stringify(this._memory[key]))
        } catch (error) { }
      })
    })
  }

  public clear() {
    Object.keys(this._memory).forEach(key => {
      delete this._memory[key]
    })
  }

  protected subscribe(key: string, handler: CacheEventHandler): CacheSubscription {
    if (handler.call) {
      const handlers = this.events[key] || []
      handlers.push(handler)
      this.events[key] = handlers
      handler({ fromCache: true, data: this.fromCache(key) })
    }

    return { unsubscribe: () => this.unsubscribe(key, handler) }
  }

  protected unsubscribe(key: string, handler: CacheEventHandler) {
    const handlers = this.events[key] || []
    handlers.splice(handlers.indexOf(handler, 1))
  }

  private encode(s: string) {
    return Buffer.from(s).toString('base64')
  }

  private decode(s: string) {
    return Buffer.from(s, 'base64').toString()
  }

  private encodeKey(k: string) {
    return `${this.encode(this.prefix)}-${this.encode(k)}`
  }

  private loadMemory() {
    Object.keys(localStorage).forEach(key => {
      const prefix = this.encode(this.prefix)
      if (key.startsWith(prefix)) {
        const suffix = this.decode(key.replace(`${prefix}-`, ''))
        try {
          this._memory[suffix] = JSON.parse(localStorage.getItem(key))
        } catch (error) { }
        localStorage.removeItem(key)
      }
    })
  }

  protected set memory({ key, value }: ICacheItem) {
    const old = this._memory[key]
    this._memory[key] = value
    if (old != value) this.dispatch(key)
  }

  protected fromCache(key: string) {
    return this._memory[key]
  }

  protected dispatch(key: string) {
    const handlers = this.events[key] || []
    handlers.forEach((handler: CacheEventHandler) => handler({ fromCache: false, data: this.fromCache(key) }))
  }

  protected get(key: string, observable: Observable<any> = null): CacheResult {
    return {
      toPromise: () => {
        return new Promise((done, fail) => {
          if (this.has(key)) {
            observable?.subscribe(value => {
              this.memory = { key, value }
            }, err => null)
            return done({ fromCache: true, data: this.fromCache(key) })
          }
          return observable?.subscribe(value => {
            this._memory[key] = value
            return done({ fromCache: false, data: value })
          }, err => fail(err))
        })
      },
      subscribe: (handler: CacheEventHandler) => {
        setTimeout(() => {
          observable?.subscribe(value => {
            this.memory = { key, value }
          }, err => { throw err })
        }, 10)
        return this.subscribe(key, handler)
      }
    }
  }

  abstract has(key: string): boolean
}

@Injectable({
  providedIn: 'root'
})
export class CacheService extends CacheHelper {
  /**
   * Retrieve data from cache memory. If not exists, retrieve from API.
   * @param key name of the cache item
   * @param observable api service request method
   * @returns Promise<ICacheResponse>
   */
  get(key: string, observable: Observable<any> = null): CacheResult {
    return super.get(key, observable)
  }

  /**
   * Put item to cache
   * @param key name of the cache item
   * @param value cache item
   */
  put(key: string, value: any) {
    this.memory = { key, value }
  }

  /**
   * Check if cache has item with the given name
   * @param key 
   * @returns true if has, otherwise false
   */
  has(key: string) {
    return this.fromCache(key) ? true : false
  }

  /**
   * Delete item from cache memory
   * @param key name of the cache item
   */
  delete(key: string) {
    delete this._memory[key]
    this.dispatch(key)
  }

  /**
   * Subscribe for cache item changed
   * @param key cache name
   * @param handler function handler when cache changed
   * @returns closure with unsubscribe function
   */
  public subscribe(key: string, handler: CacheEventHandler) {
    return super.subscribe(key, handler)
  }

  /**
   * Unsubscribe to cache changed event
   * @param key cache name
   * @param handler the function handler given during subscription
   */
  public unsubscribe(key: string, handler: CacheEventHandler) {
    return super.unsubscribe(key, handler)
  }
}