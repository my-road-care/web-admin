import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SnackbarService } from 'app/shared/services/snackbar.service';

class InputNumberListeners {
    public isPressed: boolean = false
    public selectionStart: number;
    public selectionEnd: number;
    public selectionRange: number;
    public oldValue: string;
    public onlyNumber: string = '';
    public down: number = 0;
    public up: number = 0;

    constructor(
        private input: HTMLInputElement,
        private formGroup: FormGroup,
        private _snackBar: SnackbarService
    ) {
        this.input.addEventListener('keydown', e => this.onKeydown(e))
        this.input.addEventListener('keyup', e => this.onKeyup(e))
        this.input.addEventListener('input', e => this.onInput(e))
        this.input.addEventListener('paste', e => this.onPaste(e))
    }

    onKeydown(e: any): void {
        this.down++;
        if (!isNaN(e.key) && (this.down > this.up + 1)) {
            this._snackBar.openSnackBar('Your number must not press down', 'error');
            return e.preventDefault();
        }

        const controlKeys = 'Shift | Control | Meta'.split(' | ');
        if (controlKeys.indexOf(e.key) !== -1) this.isPressed = true;

        if (!this.isPressed) {
            const keys = 'Backspace | ArrowRight | ArrowLeft'.split(' | ');
            if (isNaN(e.key) && keys.indexOf(e.key) === -1) {
                this._snackBar.openSnackBar('Allow Number Only', 'error');
                return e.preventDefault();
            };
            if (e.key == 0 && !this.input.value) {
                this._snackBar.openSnackBar('Allow Number Only', 'error');
                return e.preventDefault();
            }
        }

        this.selectionStart = this.input.selectionStart;
        this.selectionEnd = this.input.selectionEnd;
        this.selectionRange = Math.abs(this.selectionEnd - this.selectionStart);
        this.oldValue = this.input.value;
        //===================
        if (e.key == 'Backspace' && this.selectionRange <= 1 && this.oldValue?.charAt(this.selectionEnd - 1) == ',') {
            e.preventDefault()
            this._snackBar.openSnackBar('You cannot remove the comma.', 'error');
            // const left = this.oldValue.slice(0, this.selectionEnd - 2)
            // const right = this.oldValue.slice(this.selectionEnd - 1)
            // e.target.value = this.formatNumber(this.parseNumber(left + right));
            // e.target.setSelectionRange(this.selectionStart - 2, this.selectionEnd - 2)
        }
    };

    onInput(e: any): void {
        this.input.value = this.formatNumber(this.parseNumber(this.input.value)) || null;
        if (this.oldValue) {
            const value = this.input.value;
            this.selectionStart += this.selectionRange;
            this.selectionStart += value.length - this.oldValue.length;
            if (this.selectionStart < 0) this.selectionStart = 0;
            this.input.setSelectionRange(this.selectionStart, this.selectionStart);
        }
    };

    onKeyup(e: any): void {
        this.isPressed = false;

        var myString = e.target.value;
        //===================
        let num: number = Number(this.parseNumber(e.target.value));
        let string: string = String(num)
        e.target.value = this.formatNumber(string);
        var newString = myString.split(",").join("");
        if (newString.charAt(0) == '0' && newString.length > 2 && this.down < 2) {
            this.input.setSelectionRange(0, 0);
        }
        this.down = 0;
        this.up = 0;
        this.formGroup?.get(this.input.getAttribute('ng-reflect-name'))?.setValue(e.target.value);
    };

    async onPaste(e: any): Promise<void> {
        e.preventDefault();
        this._snackBar.openSnackBar('You cannot paste value!. Please input the value.', 'error');
        // const data = this.parseNumber(await navigator.clipboard.readText());

        // if (isNaN(data)) {
        //     this._snackBar.openSnackBar('Allow Number Only', 'error');
        //     return;
        // }

        // let oldValue = this.input.value;

        // if (oldValue) {
        //     this.selectionStart = this.input.selectionStart;
        //     this.selectionEnd = this.input.selectionEnd;
        //     this.selectionRange = Math.abs(this.selectionEnd - this.selectionStart);

        //     if (this.selectionStart === this.selectionEnd) {
        //         // not selecting
        //         if (oldValue.length === this.input.selectionStart)
        //             // at the end
        //             this.input.value = this.formatNumber(`${this.parseNumber(oldValue)}${data}`);
        //         else {
        //             // somewhere in the middle
        //             const left = oldValue.substring(0, this.selectionStart);
        //             const right = oldValue.substring(this.selectionStart, oldValue.length);
        //             this.input.value = this.formatNumber(this.parseNumber(`${left}${data}${right}`));

        //             this.selectionStart += this.input.value.length - oldValue.length;
        //             if (this.selectionStart < 0) this.selectionStart = 0;
        //             this.input.setSelectionRange(this.selectionStart, this.selectionStart);
        //         }
        //     } else {
        //         const rep_str = oldValue.substring(this.selectionStart, this.selectionEnd);
        //         oldValue = this.input.value.replace(rep_str, data);
        //         this.selectionStart += data.length;

        //         this.input.value = this.formatNumber(this.parseNumber(oldValue));

        //         this.selectionStart += this.input.value.length - oldValue.length;
        //         if (this.selectionStart < 0) this.selectionStart = 0;
        //         this.input.setSelectionRange(this.selectionStart, this.selectionStart);
        //     }
        // } else this.input.value = this.formatNumber(data);

        // const formControlName = this.input.getAttribute('ng-reflect-name');
        // if (formControlName && this.formGroup) {
        //     this.formGroup.get(formControlName)?.setValue(this.input.value);
        // }
    };

    parseNumber(value: any) {
        if (value === undefined || value === null) return;
        if (isNaN((value = value.replace(/ |,/g, '')))) return;
        return value;
    }

    formatNumber(value: any) {
        if (isNaN(value)) return;
        return value.replace(/(.)(?=(\d{3})+$)/g, '$1,');
    }

}


@Injectable({
    providedIn: 'root'
})
export class InputNumberFormatService {
    constructor(
        private _snackBar: SnackbarService
    ) { }

    init(form: HTMLFormElement, formGroup?: FormGroup) {
        const inputs = [...form.querySelectorAll('input') as any].filter((input: HTMLInputElement) => {
            return input.hasAttribute('formatInput')
        })

        inputs.forEach((input: HTMLInputElement) => this.addListener(input, formGroup))
    }

    private addListener(input: HTMLInputElement, formGroup: FormGroup) {
        new InputNumberListeners(input, formGroup, this._snackBar)
    }
}