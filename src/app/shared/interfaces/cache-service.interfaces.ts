export interface ICacheResponse { fromCache: boolean, data: any }

export interface ICacheItem { key: string, value: any }

export type CacheEventHandler = (res: ICacheResponse) => void

export type CacheSubscription = { unsubscribe: () => void }

export type CacheResult = {
    toPromise: () => Promise<ICacheResponse>
    subscribe: (handler: CacheEventHandler) => CacheSubscription
  }