import { Component, Inject, OnInit } from '@angular/core';
import { SnackbarService } from 'app/shared/services/snackbar.service';
import { environment as env } from 'environments/environment';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalConstants } from '../global-constants';
import * as FileSaver from 'file-saver';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import * as _moment from 'moment';
import { MatSelectChange } from '@angular/material/select';

const moment = _moment;
const MY_DATE_FORMAT = {
    parse: {
        dateInput: 'YYYY-MM-DD', // this is how your date will be parsed from Input
    },
    display: {
        dateInput: 'YYYY-MM-DD', // this is how your date will get displayed on the Input
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY'
    }
};

interface DataParent {
    report: 8 | 9,
    title: string,
    project_id: number,
    code: string
}

interface Station {
    id: number,
    number: string
}

interface Work {
    id: number,
    name: string
}

interface Side {
    id: SideType,
    name: string
}

enum SideType {
    ALL = 0,
    LEFT = 1,
    RIGHT = 2
}

@Component({
    selector: 'app-project-reports',
    templateUrl: './project-reports.component.html',
    styleUrls: ['./project-reports.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMAT }
    ]
})
export class ProjectReportsComponent {
    public isLoading: boolean;
    public saving: boolean
    public pkForm: UntypedFormGroup
    public pks: any[] = [];
    public stations: Station[] = [];
    public stationsStart: Station[] = [];
    public stationsEnd: Station[] = [];
    public works: Work[] = [];
    public sides: Side[] = [
        {
            id: SideType.ALL,
            name: "ទាំងអស់"
        },
        {
            id: SideType.LEFT,
            name: "ឆ្វេង (L)"
        },
        {
            id: SideType.RIGHT,
            name: "ស្តាំ (R)"
        }
    ];
    public side_id: number = null;
    public code: string = '';
    private httpOptions: object = {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
    };
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: DataParent,
        private dialogRef: MatDialogRef<ProjectReportsComponent>,
        private _httpClient: HttpClient,
        private _formBuilder: UntypedFormBuilder,
        private _snackbarService: SnackbarService
    ) { }

    ngOnInit(): void {
        this.isLoading = true;
        this._httpClient.get(`${env.apiUrl}/sup/projects/${this.data.project_id}/report/system-report/setup`).subscribe((res: any) => {
            this.isLoading = false
            this.pks = res?.pks
            this.code = res?.code
            this.works = res?.works as Work[]
            console.log(res)
            this.pkFormBuilder();
        }, (err: any) => {
            this.isLoading = false
            console.log(err)
            this._snackbarService.openSnackBar('invalid pks_stations', GlobalConstants.error)
        })
    }

    handleStation(e: MatSelectChange): void {
        const pk =  this.pks.find((pk) => pk.id === e.value);
        if(!pk) return this._snackbarService.openSnackBar('Invalid id of pk.', GlobalConstants.error)
        this.stations = pk?.stations as Station[];
        this.stationsStart = this.stations;
        this.stationsEnd = this.stations;
    }
    changeStart(e: MatSelectChange): void {
        this.stationsEnd = this.stations.filter(station => station.id >= e.value)
    }
    changeEnd(e: MatSelectChange): void {
        this.stationsStart = this.stations.filter(station => station.id <= e.value)
    }

    pkFormBuilder(): void {
        this.pkForm = this._formBuilder.group({
            pk_id: [null, Validators.required],
            station_start_id: [null, Validators.required],
            station_end_id: [null, Validators.required],
            side_id: [SideType.ALL],
            work_id: [null, Validators.required],
            from: [],
            to: []
        });
    }

    submit(): void {
        if (this.pkForm.get('from').value != null)
            this.pkForm.get('from').setValue(moment(this.pkForm.get('from').value).format('YYYY-MM-DD'));
        else
            this.pkForm.get('from').setValue(null);

        if (this.pkForm.get('to').value != null)
            this.pkForm.get('to').setValue(moment(this.pkForm.get('to').value).format('YYYY-MM-DD'));
        else
            this.pkForm.get('to').setValue(null)

        // alert('pk_id: '+this.pkForm.get('pk_id').value+'\n'+ 
        // 'station_start_id: '+this.pkForm.get('station_start_id').value+'\n'+
        // 'station_end_id: '+this.pkForm.get('station_end_id').value+'\n'+
        // 'work_id: '+this.pkForm.get('work_id').value+'\n'+ 'date: ' +
        // this.pkForm.get('from').value + " to " + this.pkForm.get('to').value);
        // return;
        this.saving = true;
        this.dialogRef.disableClose = true;
        const obj: any = {
            ...this.pkForm.value,
            report: this.data.report
        }
        this._httpClient.post(`${env.apiUrl}/sup/projects/${this.data.project_id}/report/system-report`, obj, this.httpOptions).subscribe((res: any) => {
            this.saving = false;
            const currentDate = new Date();
            const year = currentDate.getFullYear();
            const month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
            const day = ("0" + currentDate.getDate()).slice(-2);
            const hours = ("0" + currentDate.getHours()).slice(-2);
            const minutes = ("0" + currentDate.getMinutes()).slice(-2);
            const seconds = ("0" + currentDate.getSeconds()).slice(-2);
            const formattedDate = `${year}-${month}-${day}-${hours}-${minutes}-${seconds}`;
            let blob = this.b64toBlob(res.file_base64, 'application/pdf', '');
            FileSaver.saveAs(blob, this.data?.code + '-របាយការណ៏តារាងការងារ-' + formattedDate + '.pdf');
            this.dialogRef.close();
        }, (err: any) => {
            this.dialogRef.disableClose = false;
            this.saving = false;
            console.log(err);
            this.dialogRef.close();
        });
    }
    // =================================>> Convert base64 to blob 
    private b64toBlob(b64Data: any, contentType: any, sliceSize: any) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
}
