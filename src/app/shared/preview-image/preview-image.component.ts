import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-preview-image',
  templateUrl: './preview-image.component.html',
  styleUrls: ['./preview-image.component.scss']
})
export class PreviewImageComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<PreviewImageComponent>,
  ) { 
    // dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

}
