import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material-module';
import { ImageCropperModule } from 'ngx-image-cropper';
import { PortraitComponent, PortraitDialogComponent } from './portrait/portrait.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { PreviewImageComponent } from './preview-image/preview-image.component';
import { PreviewImageMultiComponent } from './preview-image-multi/preview-image-multi.component';
import { ScrollbarModule } from 'helpers/directives/scrollbar';
import { SVAdminDashboardComponent } from 'app/main/dashboard/layout/sv-admin/dashboard.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SuperAdminDashboardComponent } from 'app/main/dashboard/layout/super-admin/dashboard.component';
import { ListingDashboardComponent } from 'app/main/dashboard/listing-dashboard/listing-dashboard.component';
import { MinisterDashboardComponent } from 'app/main/dashboard/layout/minister/dashboard.component';
import { HeadCommitteeDashboardComponent } from 'app/main/dashboard/layout/head-committee/dashboard.component';
import { RPAdminDashboardComponent } from 'app/main/dashboard/layout/rp-admin/dashboard.component';
import { SpecialInspectorDashboardComponent } from 'app/main/dashboard/layout/special-inspector/dashboard.component';
import { SuperUserDashboardComponent } from 'app/main/dashboard/layout/super-user/dashboard.component';
import { ReportWorkingTableComponent } from './report-working-table/report-working-table.component';
import { ProjectReportsComponent } from './project-reports/project-reports.component';

@NgModule({
    declarations: [
        PortraitComponent,
        PortraitDialogComponent,
        ConfirmDialogComponent,
        PreviewImageComponent,
        PreviewImageMultiComponent,

        SVAdminDashboardComponent,
        SuperAdminDashboardComponent,
        ListingDashboardComponent,
        MinisterDashboardComponent,
        HeadCommitteeDashboardComponent,
        RPAdminDashboardComponent,
        SpecialInspectorDashboardComponent,
        SuperUserDashboardComponent,
        ReportWorkingTableComponent,
        ProjectReportsComponent
    ],
    imports: [
        ScrollbarModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        ImageCropperModule,
        PdfViewerModule,
        NgApexchartsModule,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        ScrollbarModule,
        PortraitComponent,
        PortraitDialogComponent,
        PreviewImageMultiComponent,
        PdfViewerModule,
        SVAdminDashboardComponent,
        SuperAdminDashboardComponent,
        ListingDashboardComponent,
        MinisterDashboardComponent,
        HeadCommitteeDashboardComponent,
        RPAdminDashboardComponent,
        SpecialInspectorDashboardComponent,
        SuperUserDashboardComponent
    ],
})
export class SharedModule
{


}
